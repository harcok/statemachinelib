package statemachine.test;


import java.util.List;
import java.util.Random;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.BaseTransition;
import statemachine.model.fsm.ia.IaDetModel;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.ia.conversion.LtsZip;
import statemachine.model.fsm.ia.walker.IaDetWalker;
import statemachine.model.fsm.ia.walker.IaNonDetRandomWalker;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.format.Dot;
import statemachine.model.fsm.lts.format.GML;
import statemachine.util.IO;

public class TestIa {

	public static void main(String[] args) {
		InputAction a =  new InputAction("a");
		InputAction b =  new InputAction("b");
		OutputAction x =  new OutputAction("x");
		OutputAction y =  new OutputAction("y");

		HiddenAction tau =  new HiddenAction("tau");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		System.out.println("\nIANonDetModel\n");



		IaNonDetModel.ImmutableBuilder builder =new IaNonDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addHiddenTransition(s1, s2, tau);
		builder.addInputTransition(s1,s2,a);
		builder.addInputTransition(s1,s2,b);
		builder.addOutputTransition(s2,s1,x);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		IaNonDetModel iaNonDet= builder.build();

		ImmutableSet<InputAction> inputAlphabet = iaNonDet.getInputAlphabet();
		ImmutableSet<OutputAction> outputAlphabet = iaNonDet.getOutputAlphabet();
		ImmutableSet<BaseTransition<LocationState>> transitions = iaNonDet.getModelTransitions();
		ImmutableSet<LocationState> locations = iaNonDet.getLocations();

		System.out.println("\ninput alphabet");
		for ( InputAction action : inputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\noutput alphabet");
		for ( OutputAction action : outputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\nlocations");
		for ( LocationState loc :locations ) {
			System.out.println(loc);
		}
		System.out.println("\ntransitions");
		for ( BaseTransition transition :transitions ) {
			System.out.println(transition);
		}
		System.out.println("\nmodel");
		System.out.println(iaNonDet);

		LtsNonDetModel ltsNonDet = LtsZip.zipIa2Lts(iaNonDet);

		System.out.println("\nmodel ltsNonDet");
		System.out.println(ltsNonDet);


		List<String> lines_gml=GML.export(ltsNonDet);
		IO.writeSmallTextFile( lines_gml, "ltsNonDet.gml");
		List<String> lines_dot=Dot.export(ltsNonDet);
		IO.writeSmallTextFile( lines_dot, "ltsNonDet.dot");


		statemachine.model.fsm.ia.file.Export.dot(iaNonDet, "iaNonDet2.dot");
		IaNonDetModel importmodel= statemachine.model.fsm.ia.file.Import.dot2IaModel("iaNonDet2.dot");

		System.out.println( "importmodel");
		System.out.print( importmodel);
		System.out.println( "");



		// WALKING
		//--------
		IaNonDetRandomWalker walker=new IaNonDetRandomWalker(iaNonDet,new Random(12313));



		System.out.println(walker.getLocation());

		boolean possible;

		possible=walker.checkInputPossible(b);
		System.out.println("in start state input b is possible: " + possible );


		walker.stepInput(a);
		System.out.println(walker.getLocation());

		possible=walker.checkInputPossible(b);
		System.out.println("input b is possible: " + possible );

		possible=walker.checkInputPossible(a);
		System.out.println("input a is possible: " + possible );
		possible=walker.checkOutputPossible(y);
		System.out.println("output y is possible: " + possible );

		possible=walker.checkOutputPossible(x);
		System.out.println("output x is possible: " + possible );

		walker.stepInput(a);
		System.out.println(walker.getLocation());
		walker.stepInput(b);
		System.out.println(walker.getLocation());
		walker.stepOutput(y);
		System.out.println(walker.getLocation());
		walker.stepOutput(x);
		System.out.println(walker.getLocation());


	// Deterministic (both input as output determinitic)

		IaDetModel.ImmutableBuilder builderDet =new IaDetModel.ImmutableBuilder();
		builderDet.setStartLocation(s1);
		//builderDet.addInternalTransition(s1, s2, tau); // gives UnsupportedOperationException
		builderDet.addInputTransition(s1,s2,a);
		builderDet.addInputTransition(s1,s2,b);
		builderDet.addOutputTransition(s2,s1,x);
		//builderDet.addInputTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		builderDet.addOutputTransition(s2,s1,y);
		//builderDet.addOutputTransition(s2,s2,y);
		IaDetModel iaDet= builderDet.build();

		// WALKING
		//--------
		IaDetWalker walkerDet=new IaDetWalker(iaDet);

		System.out.println("deterministic walker");

		System.out.println(walkerDet.getLocation());

		//boolean possible;

		possible=walkerDet.checkInputPossible(b);
		System.out.println("in start state input b is possible: " + possible );


		walkerDet.stepInput(a);
		System.out.println(walkerDet.getLocation());

		possible=walkerDet.checkInputPossible(b);
		System.out.println("input b is possible: " + possible );

		possible=walkerDet.checkInputPossible(a);
		System.out.println("input a is possible: " + possible );
		possible=walkerDet.checkOutputPossible(y);
		System.out.println("output y is possible: " + possible );

		possible=walkerDet.checkOutputPossible(x);
		System.out.println("output x is possible: " + possible );

		walkerDet.stepInput(a);
		System.out.println(walkerDet.getLocation());
		walkerDet.stepInput(b);
		System.out.println(walkerDet.getLocation());
		walkerDet.stepOutput(y);
		System.out.println(walkerDet.getLocation());
		walkerDet.stepOutput(x);
		System.out.println(walkerDet.getLocation());




	}

}
