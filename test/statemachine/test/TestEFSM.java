package statemachine.test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashMap;

import org.antlr.symtab.PrimitiveType;
import org.antlr.symtab.VariableSymbol;

import statemachine.model.efsm.ia.IaNonDetModel;
import statemachine.model.efsm.ia.conversion.Conversion;
import statemachine.model.efsm.mealy.MealyNonDetModel;
import statemachine.model.efsm.mealy.file.Export;
import statemachine.model.efsm.mealy.file.Import;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.Location;

public class TestEFSM {
    public static void mainTest(String[] args) {

        PrimitiveType integer = new PrimitiveType("INT");

        PrimitiveType bool = new PrimitiveType("BOOL");

        Location s1 = new Location("1");
        Location s2 = new Location("2");

        DataInputActionType A =  new DataInputActionType("a",integer,integer);
        DataInputActionType B =  new DataInputActionType("b",integer);
        DataOutputActionType OUT =  new DataOutputActionType("OUT",integer,integer);
        DataOutputActionType OK =  new DataOutputActionType("OK",integer);

        HashMap<VariableSymbol,Value> constants = new HashMap<>();
        constants.put(new VariableSymbol("c1"), new Value(integer,3));
        constants.put(new VariableSymbol("c2"), new Value(integer,5));


        VariableSymbol var_x1= new VariableSymbol("x1");
        var_x1.setType(integer);
        HashMap<VariableSymbol,Value> statevars = new HashMap<>();
        statevars.put(var_x1, new Value(integer,2));
        statevars.put(new VariableSymbol("x2"), new Value(integer,1));




        SymbolicInputAction a1 = new SymbolicInputAction(A,  Arrays.asList("p0", "p1") );
        SymbolicInputAction b1 = new SymbolicInputAction(B,  Arrays.asList("p0") );
        SymbolicInputAction b2 = new SymbolicInputAction(B,  Arrays.asList("u") );

        SymbolicOutputAction Ox1= new SymbolicOutputAction(OUT,  Arrays.asList("p0", "p1") );
        SymbolicOutputAction Oy1 = new SymbolicOutputAction(OK,  Arrays.asList("p0") );
        SymbolicOutputAction Oy2 = new SymbolicOutputAction(OK,  Arrays.asList("v") );



        System.out.println("\nMealyNonDetModel\n");

        //

        MealyNonDetModel.ImmutableBuilder builder =new MealyNonDetModel.ImmutableBuilder();
        builder.setStartState(s1,statevars);
        builder.setConstants(constants);

        builder.setInputAlphabet(A,B);
        builder.setOutputAlphabet(OUT,OK);

        builder.addTransition(s1,s2,a1,Ox1,"x1==p1", "x1=p0");

        //builder.addTransition(s1,s2,"a(p0,p1)",Ox1,"x1==p1", "x1=p0"); // builder dsl
        builder.addTransition(s1,s2,"a(p0,p1)","OUT(x1)","x1==p1", "x1=p0"); // builder dsl

        builder.addTransition(s1,s2,b1,Ox1, "x1==b");
        builder.addTransition(s2,s1,b2,Oy2,"","x1=u");  // empty guard means no guard or true
        builder.addTransition(s1,s1,b1,Oy1); // for a in s1 : either go to s1 or s2
        MealyNonDetModel mealyNonDet= builder.build();


        System.out.println(mealyNonDet.toString());

        Export.dot(mealyNonDet, "efsm.dot");
        MealyNonDetModel mealyNonDet2=Import.dot2MealyModel("efsm.dot");

        System.out.println(mealyNonDet2.toString());


        IaNonDetModel.ImmutableBuilder ibuilder =new IaNonDetModel.ImmutableBuilder();
        ibuilder.setStartState(s1,statevars);
        ibuilder.setConstants(constants);

        ibuilder.setInputAlphabet(A,B);
        ibuilder.setOutputAlphabet(OUT,OK);

        ibuilder.addOutputTransition(s1,s2,"OUT(x1)","x1==p1"); // builder dsl

        ibuilder.addInputTransition(s1,s2,a1,"x1==p1", "x1=p0");

        ibuilder.addInputTransition(s1,s2,"a(p0,p1)","x1==p1", "x1=p0"); // builder dsl
        ibuilder.addOutputTransition(s1,s2,"OUT(x1)","x1==p1", "x1=p0"); // builder dsl




        ibuilder.addInputTransition(s1,s2,b1, "x1==b");
        ibuilder.addOutputTransition(s2,s1,Oy2,"","x1=u");  // empty guard means no guard or true
        ibuilder.addOutputTransition(s1,s1,Oy1); // for a in s1 : either go to s1 or s2

        IaNonDetModel iaModel= ibuilder.build();

        System.out.println(iaModel.toString());

        statemachine.model.efsm.ia.file.Export.dot(iaModel, "efsm_ia.dot");
        IaNonDetModel iaModel2=statemachine.model.efsm.ia.file.Import.dot2IaModel("efsm_ia.dot");
        System.out.println(iaModel2.toString());

        IaNonDetModel iaModel3=
          //statemachine.model.efsm.ia.file.Import.register2IaModel("examples/model_ABP_output.register.xml");
        statemachine.model.efsm.ia.file.Import.register2IaModel("examples/model_Channel_Frame.register.xml");
        System.out.println(iaModel3.toString());

        statemachine.model.efsm.ia.file.Export.dot(iaModel3, "efsm_ia_channel.dot");


        MealyNonDetModel mealyNonDetConverted=Conversion.IaModel2MealyModel( iaModel3);
        System.out.println(mealyNonDetConverted.toString());
        Export.dot(mealyNonDetConverted, "efsm_mealy_channel.dot");

        IaNonDetModel iaModel4=statemachine.model.efsm.ia.file.Import.register2IaModel("examples/model_ABP_output.register.xml");
        MealyNonDetModel mealyModel=Conversion.IaModel2MealyModel( iaModel4);
        Export.dot(mealyNonDetConverted, "efsm_mealy_ABP_output.dot");
        System.out.println(mealyModel.toString());
        MealyNonDetModel.printStatistics(mealyModel,"ABP_output");

    }

    public static void test_mealy_import_export(String filepath,String outfilepath) {
        statemachine.model.fsm.mealy.MealyNonDetModel mealyModel=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(filepath);
        statemachine.model.fsm.mealy.file.Export.dot(mealyModel, outfilepath);
    }

    public static void main(String[] args) {

        String filename = "Mealy/BenchmarkSSH/DropBear.dot";
        test_mealy_import_export(filename,"test_export.dot");
        //  String filename ="Mealy/BenchmarkTLS/JSSE_1.8.0_31_server_regular.dot";

        System.out.println("\n!! Register \n");

        final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.xml");
        try {
            Files.walkFileTree(Paths.get("Register/"), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (matcher.matches(file)) {
                        //String model=file.toString().substring(9).replaceFirst("/model.register.xml", "").replaceFirst(".register.xml", "").replaceFirst("model_", "");
                        String model=file.toString().substring(9);
                        MealyNonDetModel.printStatistics(file.toString(),model);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("\n!! Mealy \n");

        final PathMatcher matcher2 = FileSystems.getDefault().getPathMatcher("glob:**/*.dot");
        try {
            Files.walkFileTree(Paths.get("Mealy/"), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (matcher2.matches(file)) {
                     //   System.out.println(file);
                       // String model=file.toString().substring(6).replaceFirst("/model.dot", "").replaceFirst(".register.xml", "").replaceFirst("model_", "");
                        String model=file.toString().substring(6);

                        statemachine.model.fsm.mealy.MealyNonDetModel.printStatistics(file.toString(),model);

                    }
                  //  System.gc();
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
