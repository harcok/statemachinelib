package statemachine.test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.ImmutableSet;


import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.elements.transition.BaseTransition;
import statemachine.model.fsm.mealy.MealyBaseModel.ImmutableBuilder;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.mealy.file.Export;
import statemachine.model.fsm.mealy.file.Import;
import statemachine.model.fsm.mealy.walker.MealyDetEnabledWalker;
import statemachine.model.fsm.moore.MooreNonDetModel;
import statemachine.model.fsm.mealy.conversion.Conversion;

public class TestMealy {

    public static void query(List<InputAction> actions,MealyDetEnabledWalker walker) {
        LinkedList<LocationState> path = new LinkedList<>();
        path.add(walker.getLocation());
        LinkedList<OutputAction> outputs = new LinkedList<>();

        for ( InputAction action : actions ) {
            //System.out.println(walker.getLocation());
            OutputAction output=walker.stepInput(action);
            path.add(walker.getLocation());
            outputs.add(output);
        }
        System.out.println(actions + " : " + path + " " + outputs );
    }

	public static void main(String[] args) {


        MealyNonDetModel xnondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");
        MealyDetEnabledModel xdetlts  = MealyDetEnabledModel.fromMealyModel(xnondetlts);
        statemachine.model.fsm.mealy.file.Export.nusmv(xdetlts, "coffeemachine.nusmv");

		InputAction a =  new InputAction("a");
		InputAction b =  new InputAction("b");
		OutputAction x =  new OutputAction("x");
		OutputAction y =  new OutputAction("y");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		System.out.println("\nMealyNonDetModel\n");



		MealyNonDetModel.ImmutableBuilder builder =new MealyNonDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a,x);
		//builder.addTransition(s1,s2,b,x);
		//builder.addTransition(s2,s1,a,x);
		builder.addTransition(s1,s2,b,x)
		       .addTransition(s2,s1,a,x);
//		builder.addTransition(s1,s1,a,y); // for a in s1 : either go to s1 or s2
//		MealyNonDetModel mealyNonDet= builder.build();

		MealyNonDetModel mealyNonDet=builder.addTransition(s1,s1,a,y).build();


		ImmutableSet<InputAction> inputAlphabet = mealyNonDet.getInputAlphabet();
		ImmutableSet<OutputAction> outputAlphabet = mealyNonDet.getOutputAlphabet();
		ImmutableSet<MealyTransition> transitions = mealyNonDet.getModelTransitions();
		ImmutableSet<LocationState> locations = mealyNonDet.getLocations();

		System.out.println("\ninput alphabet");
		for ( InputAction action : inputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\noutput alphabet");
		for ( OutputAction action : outputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\nlocations");
		for ( LocationState loc :locations ) {
			System.out.println(loc);
		}
		System.out.println("\ntransitions");
		for ( BaseTransition transition :transitions ) {
			System.out.println(transition);
		}
		System.out.println("\nmodel");
		System.out.println(mealyNonDet);

		Export.dot(mealyNonDet, "testMealyExport.dot");


		System.out.println("\nMealyDetModel\n");


		MealyDetModel.ImmutableBuilder dbuilder =new MealyDetModel.ImmutableBuilder();
		dbuilder.setStartLocation(s1);
		dbuilder.addTransition(s1,s2,a,x);
		dbuilder.addTransition(s1,s2,b,x);
		dbuilder.addTransition(s2,s1,a,x);
	//	dbuilder.addTransition(s1,s1,a,y); // for a in s1 : either go to s1 or s2
		MealyDetModel mealyDet= dbuilder.build();

		//mealyDet.getTransitionsFor(s1, a);

		MealyDetEnabledModel mealyDetEnabled;

//		// throws exception because model not input enabled
//		mealyDetEnabled = MealyDetEnabledModel.fromMealyModel(mealyDet);
//      System.out.println("\n mealyDet -> MealyDetEnabledModel\n");
//      System.out.println( mealyDetEnabled.toString() );


		Export.dot(mealyDet, "testDetMealyExport.dot");

		mealyDet= MealyDetModel.fromMealyModel(Import.dot2MealyModel( "testDetMealyExport.dot"));
		System.out.println( mealyDet.toString() );

		mealyDet= MealyDetModel.fromMealyModel(Import.dot2MealyModel( "RiverCrossingPuzzle/RiverCrossingPuzzle.dot"));
		System.out.println( mealyDet.toString() );

		Export.dot(mealyDet, "RiverCrossingPuzzle.dot");
		Export.aldebaran(mealyDet, "RiverCrossingPuzzle.aut");
		Export.gml(mealyDet, "RiverCrossingPuzzle.gml");

		mealyDet= MealyDetModel.fromMealyModel(Import.aldebaran2MealyModel( "RiverCrossingPuzzle.aut"));
		System.out.println("\nimported aldebaran MealyDetModel\n");
		System.out.println( mealyDet.toString() );


		mealyDetEnabled = MealyDetEnabledModel.fromMealyModel(Import.aldebaran2MealyModel( "RiverCrossingPuzzle.aut"));
        System.out.println("\nimported aldebaran MealyDetEnabledModel\n");
        System.out.println( mealyDetEnabled.toString() );

        System.out.println("\nalphabet:"+ mealyDetEnabled.getInputAlphabet().toString());


        // check deterministic walking
        System.out.println("\nwalking");
        MealyDetEnabledWalker walker=new MealyDetEnabledWalker(mealyDetEnabled);

        InputAction g =  new InputAction("GOAT");
        InputAction w =  new InputAction("WOLF");
        InputAction c =  new InputAction("CABBAGE");
        InputAction f =  new InputAction("FARMER");
        List<InputAction> actions = Arrays.asList(g,f,w,c,g,f,w);
        query( actions ,walker);



        MooreNonDetModel moore_model= Conversion.Mealy2MooreNonDetModel(mealyNonDet);

        //MooreNonDetModel moore_model= Conversion.Mealy2MooreNonDetModel(mealyDetEnabled);
        System.out.println("\nmoore_model");
        System.out.println(moore_model);


        MealyDetModel.ImmutableBuilder pbuilder =new MealyDetModel.ImmutableBuilder();



        LocationState q0 = new LocationState("q0");
        LocationState q1 = new LocationState("q1");

        pbuilder.setStartLocation(q0);
        pbuilder.addTransition(q0,q0,a,x);
        pbuilder.addTransition(q0,q1,b,x);
        pbuilder.addTransition(q1,q1,b,y);
        pbuilder.addTransition(q1,q0,a,y);
        MealyDetModel paperMealyDet= pbuilder.build();

        Export.dot(paperMealyDet, "paperMoore_model");

        MooreNonDetModel paperMoore_model= Conversion.Mealy2MooreNonDetModel(paperMealyDet);

        System.out.println("\npaperMoore_model");
        System.out.println(paperMoore_model);

        //statemachine.model.fsm.moore.file.Export.dot(paperMoore_model, "paperMoore_model");




	}

}
