package statemachine.test;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.moore.conversion.Conversion;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.moore.file.Export;
import statemachine.model.fsm.moore.file.Import;

import statemachine.model.fsm.moore.MooreNonDetModel;

public class TestMoore {

	public static void main(String[] args) {
		InputAction a =  new InputAction("a");
		InputAction b =  new InputAction("b");
		OutputAction x = new OutputAction("x");
		OutputAction y = new OutputAction("y");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		System.out.println("\nMooreNonDetModel\n");



		MooreNonDetModel.ImmutableBuilder builder =new MooreNonDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addOutput(s1, x);
		builder.addOutput(s2, y);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2

		MooreNonDetModel mooreNonDet= builder.build();

		ImmutableSet<InputAction> inputAlphabet = mooreNonDet.getInputAlphabet();
		ImmutableSet<OutputAction> outputAlphabet = mooreNonDet.getOutputAlphabet();
		ImmutableSet<InputTransition> transitions = mooreNonDet.getModelTransitions();
		ImmutableSet<LocationState> locations = mooreNonDet.getLocations();

		System.out.println("\ninput alphabet");
		for ( InputAction action : inputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\noutput alphabet");
		for ( OutputAction action : outputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\nlocations");
		for ( LocationState loc :locations ) {
			System.out.println(loc);
		}
		System.out.println("\ntransitions");
		for ( InputTransition transition :transitions ) {
			System.out.println(transition);
		}
		System.out.println("\nmodel");
		System.out.println(mooreNonDet);

		Export.dot(mooreNonDet, "testMooreExport.dot");
		MooreNonDetModel mooreNonDet2 = Import.dot2MooreModel("testMooreExport.dot");
		System.out.println("\nmodel mooreNonDet2");
		System.out.println(mooreNonDet2);

		MealyNonDetModel mealy_model= Conversion.Moore2MealyNonDetModel(mooreNonDet);

	    System.out.println("\nmealy_model");
	    System.out.println(mealy_model);

	}


}
