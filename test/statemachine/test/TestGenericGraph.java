package statemachine.test;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;
import statemachine.model.graphs.graph.GenericMultiGraphModel;
import statemachine.model.graphs.graph.MultiGraphModel;
import statemachine.model.graphs.graph.MultiGraphModel2;

public class TestGenericGraph {
    public static void main(String[] args) {
        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        GenericMultiGraphModel.ImmutableBuilder<LocationState, GraphTransition> builder =
                new GenericMultiGraphModel.ImmutableBuilder<LocationState, GraphTransition>(GraphTransition::new);


//          builder.addTransition(s1, s2)
//                .addTransition(s1,s2)
//                .addTransition(s2,s1)
//                .addTransition(s1,s1);
//          GenericMultiGraphModel<LocationState, GraphTransition> model = builder.build();

        GenericMultiGraphModel<LocationState, GraphTransition> model = builder.addTransition(s1, s2)
           .addTransition(s1,s2)
           .addTransition(s2,s1)
           .addTransition(s1,s1).build();


        MultiGraphModel2.ImmutableBuilder b = new MultiGraphModel2.ImmutableBuilder();
        b.addTransition(s1, s2).addTransition(s1,s2).build();

        MultiGraphModel.ImmutableBuilder b2 = new MultiGraphModel.ImmutableBuilder();
        b2.addTransition(s1, s2).addTransition(s1,s2);
        b2.build();
    }
}
