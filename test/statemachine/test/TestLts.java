package statemachine.test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.format.Aldebaran;
import statemachine.model.fsm.lts.format.Dot;
import statemachine.model.fsm.lts.format.GML;
import statemachine.model.fsm.lts.format.GraphML;
import statemachine.model.fsm.lts.walker.LtsDetWalker;
import statemachine.model.fsm.lts.walker.LtsEnabledDetWalker;
import statemachine.model.fsm.lts.walker.LtsNonDetRandomWalker;
import statemachine.util.IO;
import statemachine.util.KeyLookupCache;
import statemachine.util.UniqueInstanceCache;

public class TestLts {
	public static void query(List<Action> actions,LtsDetWalker walker) {
		LinkedList<LocationState> path = new LinkedList<>();
		path.add(walker.getLocation());
		for ( Action action : actions ) {
			//System.out.println(walker.getLocation());
			walker.stepAction(action);
			path.add(walker.getLocation());
		}
		System.out.println(actions + " : " + path );
	}

	public static void main(String[] args) {


		Action a =  new Action("a");  // created on the fly in action pool if not yet existed
		Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");



		System.out.println("\nLTSDetModel\n");

		LtsDetModel.ImmutableBuilder builder =new LtsDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsDetModel lts= builder.build();

		ImmutableSet<Action> alphabet = lts.getAlphabet();
		ImmutableSet<ActionTransition> transitions = lts.getModelTransitions();
		ImmutableSet<LocationState> locations = lts.getLocations();


		// hashmap can also be used instead of KeyLookupSet, the key is then delivered by hashcode

		KeyLookupCache<String,LocationState> lookup= new KeyLookupCache<String,LocationState>( locations, LocationState::toString, false );
		LocationState s1copy = lookup.get("1");
		if ( s1copy == s1 ) {
		    System.out.println("yes works");
		} else {
		    System.out.println("failing");
		}
		// => TO FETCH the instance in the set  => reusing instead of recreating saves memory!!
        ImmutableSet<LocationState> values3=lookup.getValues();
        System.out.println(values3);

        KeyLookupCache<String,LocationState> lookup2= new KeyLookupCache<String,LocationState>(  LocationState::toString );
        LocationState s1copy2 = lookup2.uniqueInstance(s1);  // s1 added as uniqueInstance, and returned as uniqueinstance
        LocationState s1copy3 = lookup2.get("1");
        if ( s1copy2 == s1 && s1copy3 == s1) {
            System.out.println("yes works");
        } else {
            System.out.println("failing");
        }

        ImmutableSet<LocationState> values2=lookup2.getValues();
        System.out.println(values2);

		/*
        // use value itself as key; equal objects have same hashkey , so we can use hashkey of copy of object to fetch the one in the set!
		// => we use here to knowledge that internally in KeyLookupSet a HashMap is used => we shouldn't!!
        Function<LocationState,LocationState> identity = v -> v;  // identity
        KeyLookupSet<LocationState,LocationState> uniquelookup=
                new KeyLookupSet<LocationState,LocationState>(locations , identity );
        LocationState s2copy =   uniquelookup.get( new LocationState("2"));
        if ( s2copy == s2 ) {
            System.out.println("yes works");
        } else {
            System.out.println("failing");
        }
        */

		/*
        // below wrong: because not sure hashCode can give hash colissions!!
        Function<LocationState,Integer> identity = v -> v.hashCode();  // identity
        KeyLookupSet<Integer,LocationState> uniquelookup=
                new KeyLookupSet<Integer,LocationState>(locations , identity );
        LocationState s2copy =   uniquelookup.get( (new LocationState("2")).hashCode() );
        if ( s2copy == s2 ) {
            System.out.println("yes works");
        } else {
            System.out.println("failing");
        }*/



        UniqueInstanceCache<LocationState> uniquelookup= new UniqueInstanceCache<LocationState>(locations,false);
        LocationState anothers2copy =   uniquelookup.uniqueInstance( new LocationState("2"));
        if ( anothers2copy == s2 ) {
            System.out.println("yes works");
        } else {
            System.out.println("failing");
        }



        LocationState newlocation = new LocationState("223324");
        LocationState notincache =   uniquelookup.uniqueInstance( newlocation);
        if ( notincache == null ) {
            System.out.println("yes works");
        }else {
            System.out.println("failing");
        }

        UniqueInstanceCache<LocationState> uniquelookup2= new UniqueInstanceCache<LocationState>(locations,true);

        LocationState incache =   uniquelookup2.uniqueInstance( newlocation);
        if ( incache == newlocation  ) {
            System.out.println("yes works");
        }else {
            System.out.println("failing");
        }

        ImmutableSet<LocationState> values=uniquelookup2.getValues();
        System.out.println(values);
//        System.out.println(values.toArray()[2]);
//        if ( values.toArray()[2] == newlocation  ) {
//            System.out.println("yes works");
//        }else {
//            System.out.println("failing");
//        }



		System.out.println("\nalphabet");
		for ( Action action :alphabet ) {
			System.out.println(action);
		}
		System.out.println("\nlocations");
		for ( LocationState loc :locations ) {
			System.out.println(loc);
		}
		System.out.println("\ntransitions");
		for ( ActionTransition transition :transitions ) {
			System.out.println(transition);
		}

		// check failure to convert not enabled model to an enabled one
		try {
			System.err.println( "");
			System.err.println( "unable to convert LTSDetModel to LTSEnabledDetModel:");
			@SuppressWarnings("unused")
			LtsEnabledDetModel convertedModel =  LtsEnabledDetModel.fromLtsModel(lts);
		} catch (Exception e) {
			e.printStackTrace();
		}


		// check deterministic walking
		System.out.println("\nwalking");
		LtsDetWalker walker=new LtsDetWalker(lts);

		List<Action> actions = Arrays.asList(a,a,a,a,a);
		query( actions ,walker);


		// check import and export

		System.out.println("\nCoffeemachine\n");
		List<String> readlines=null;
		readlines=IO.readSmallTextFile(  "coffeemachine.dot");
		//LTSNoneDetModel coffeemachine= Dot.importLTSNoneDetModel(readlines); //OK
		//LTSEnabledDetModel coffeemachine= Dot.importLTSEnabledDetModel(readlines); // NOT OK
		LtsDetModel coffeemachine= Dot.importLtsDetModel(readlines); // OK

		statemachine.model.fsm.lts.file.Export.dot(lts, "lts.dot");
		statemachine.model.fsm.lts.file.Export.aldebaran(coffeemachine, "coffeemachine.aut");
		LtsNonDetModel importmodel= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel("coffeemachine.aut");

		System.out.println( "importmodel");
		System.out.print( importmodel);
		System.out.println( "");



		System.out.println( "coffeemachine");
		System.out.print( coffeemachine);
		System.out.println( "");


		//List<String> lines2=Dot.export(coffeemachine);
		//List<String> lines2=Aldebaran.export(coffeemachine);
		List<String> lines2=GraphML.export(coffeemachine);
		System.out.print(  String.join("\n", lines2 ) ) ;
		System.out.println( "");
		System.out.println( "");


		IO.writeSmallTextFile( lines2, "coffeemachine.graphml");
		IO.writeSmallTextFile( lines2, "coffeemachine.aut");


		// run write to location for which we have no permissions  and catch its exception
		try {
			IO.writeSmallTextFile( lines2, "/coffeemachine.graphml");
		} catch (Exception e) {
			e.printStackTrace();
		}



		List<String> lines_gml=GML.export(coffeemachine);
		IO.writeSmallTextFile( lines_gml, "coffeemachine.gml");



		System.out.println( "");
		System.out.print( "\ncoffeemachine from aut:\n\n");
		List<String> read_lines=IO.readSmallTextFile(  "coffeemachine.aut");

		LtsDetModel coffeemachine2= Aldebaran.importLtsDetModel(read_lines); // OK
		System.out.println( "");
		System.out.print( coffeemachine2);
		System.out.println( "");


		System.out.println("\nLTSNoneDetModel\n");

		LtsNonDetModel.ImmutableBuilder builder2 =new LtsNonDetModel.ImmutableBuilder();
		builder2.setStartLocation(s1);
		builder2.addTransition(s1,s2,a);
		builder2.addTransition(s2,s1,a);
		builder2.addTransition(s2,s1,b);
		builder2.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsNonDetModel ltsNonDet= builder2.build();


		// run conversion which is not possible and catch its exception
		try {
			System.err.println( "");
			System.err.println( "unable to convert LTSNoneDetModel to LTSDetModel:");
			@SuppressWarnings("unused")
			LtsDetModel convertedModel =  LtsDetModel.fromLtsModel(ltsNonDet);
		} catch (Exception e) {
			e.printStackTrace();
		}




		System.out.println( "");
		System.out.print( ltsNonDet);
		System.out.println( "");

		List<String> lines=Dot.export(ltsNonDet);
		System.out.print(  String.join("\n", lines ) ) ;
		System.out.println( "");
		System.out.println( "");

		IO.writeSmallTextFile( lines, "out.dot");



		// walking nondeterministic

		LtsNonDetRandomWalker walker2=new LtsNonDetRandomWalker(ltsNonDet,new Random(12313));



		System.out.println(walker2.getLocation());

		boolean possible=walker2.checkActionPossible(b);
		System.out.println("in start state action b is possible: " + possible );


		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);

		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);

		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);

		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);
		System.out.println(walker2.getLocation());
		walker2.stepAction(a);



		System.out.println("\nLTSEnabledDetModel\n");


		LtsEnabledDetModel.ImmutableBuilder builder3 =new LtsEnabledDetModel.ImmutableBuilder();
		builder3.setStartLocation(s1);
		builder3.addTransition(s1,s2,a);
		//builder3.addTransition(s1,s2,b);
		builder3.addTransition(s2,s1,a);
		//builder3.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsEnabledDetModel enabledDeterministicLts= builder3.build();

		LtsEnabledDetWalker walker3=new LtsEnabledDetWalker(enabledDeterministicLts);
		System.out.println(walker3.getLocation());
		walker3.stepAction(a);
		System.out.println(walker3.getLocation());
		walker3.stepAction(a);
		System.out.println(walker3.getLocation());
		walker3.stepAction(a);

		System.out.println(walker3.getLocation());
		walker3.stepAction(a);
		System.out.println(walker3.getLocation());
		walker3.stepAction(a);
		System.out.println(walker3.getLocation());
		walker3.stepAction(a);
	}

}
