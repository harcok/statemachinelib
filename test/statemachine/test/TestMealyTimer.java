package statemachine.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jdt.annotation.NonNull;
import org.javatuples.Pair;

import statemachine.interfaces.system.execution.OutputListener;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.location.Timer;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.walker.MealyDetEnabledWalker;
import statemachine.model.time.Util;
import statemachine.model.time.mealy.MealyTimerDetEnabledModel;
import statemachine.model.time.mealy.conversion.MealyTimerNonDetModel;
import statemachine.model.time.mealy.execution.MealyTimerDetEnableAsyncTraceRunner;
import statemachine.model.time.mealy.execution.MealyTimerDetEnableRunner;
import statemachine.model.time.mealy.execution.MealyTimerDetEnableTraceRunner;
import statemachine.model.time.mealy.execution.MealyTimerDetEnabledAsyncRunner;
import statemachine.model.time.mealy.walker.MealyTimerDetEnabledWalker;

import statemachine.model.time.mealy.file.Export;
import statemachine.model.time.mealy.file.Import;

public class TestMealyTimer {
    public static void query(List<InputAction> actions,MealyDetEnabledWalker walker) {
        LinkedList<LocationState> path = new LinkedList<>();
        path.add(walker.getLocation());
        LinkedList<OutputAction> outputs = new LinkedList<>();

        for ( InputAction action : actions ) {
            //System.out.println(walker.getLocation());
            OutputAction output=walker.stepInput(action);
            path.add(walker.getLocation());
            outputs.add(output);
        }
        System.out.println(actions + " : " + path + " " + outputs );
    }

    public static void printHeader(String string) {
        String line = StringUtils.repeat("-", string.length());
        System.out.println("\n\n"+string+"\n"+line+"\n");
    }

    public static void mainx(String... args) {

    }

    public static void main(String[] args) {


        //================================
        // model construction/import/export
        //================================

        // show creating a model
        //--------------------------------
        printHeader("show creating a model");


        InputAction a =  new InputAction("a");
        InputAction b =  new InputAction("b");
        OutputAction o =  new OutputAction("o");
        OutputAction u =  new OutputAction("u");
        OutputAction timeout_x =  new OutputAction("timeout_x");
        OutputAction timeout_y =  new OutputAction("timeout_y");
        TimerVariable timer_x = new TimerVariable("timer_x");
        TimerVariable timer_y = new TimerVariable("timer_y");

        Location s1 = new Location("1");
        Location s2 = new Location("2");

        MealyTimerDetEnabledModel.ImmutableBuilder dbuilder =new MealyTimerDetEnabledModel.ImmutableBuilder();
        dbuilder.setStartLocation(s1);
        dbuilder.addTransition(s1,s1,a,o,"timer_x=1");
        dbuilder.addTransition(s1,s2,b,o);
        dbuilder.addTransition(s2,s2,a,o,"timer_y=2");
        dbuilder.addTransition(s2,s1,b,o); // uncomment this line , and it is not input enabled anymore => gives error when building
        dbuilder.addTransition(s1,s1,timer_x,timeout_x,"timer_y=2");  // add timeout transition for timer variable x

        //dbuilder.addTransition(s1,s2,x,u); // uncomment this line , and it is not timeout determinisitc anymore
        dbuilder.addTransition(s1,s1,timer_y,timeout_y);  // you may uncomment this line, because ghost timers are allowed
        MealyTimerDetEnabledModel model= dbuilder.build();

        System.out.println("\nmodel");
        System.out.println(model);


        // show dot export/import
        //------------------------
        printHeader("show dot export/import");

        String filename="mealytimer.dot";
        Export.dot(model, filename);

        MealyTimerNonDetModel importedModel = Import.dot2MealyTimerModel(filename);
        System.out.println("imported model:\n" + importedModel.toString());


        //================================
        // Walking
        //================================

        // show walking step by step
        //--------------------------------

        printHeader("show walking step by step");
        MealyTimerDetEnabledWalker walker = new MealyTimerDetEnabledWalker(model);
        OutputAction output;
        List<Pair<OutputAction, Long>> timedoutputs;

        System.out.println("state: " + walker.getState() );

        output=walker.stepInput(a);
        System.out.println("i/o: " + a + "/" + output );
        System.out.println("state: " + walker.getState() );

        output=walker.stepInput(b);
        System.out.println("i/o: " + b + "/" + output );
        System.out.println("state: " + walker.getState() );

        output=walker.stepInput(a);
        System.out.println("i/o: " + a + "/" + output );
        System.out.println("state: " + walker.getState() );

//        timedoutputs = walker.waitDelay(3);
//        System.out.println("wait : " + 3 + " timedoutputs : " + timedoutputs );
//        System.out.println("state: " + walker.getState() );

        output=walker.stepInput(b);
        System.out.println("i/o: " + b + "/" + output );
        System.out.println("state: " + walker.getState() );

        timedoutputs = walker.waitDelay(3);
        System.out.println("wait : " + 3 + " timedoutputs : " + timedoutputs );
        System.out.println("state: " + walker.getState() );




        // show walking a timed input word
        //--------------------------------


        printHeader("show walking a timed input word");

        List<Pair<InputAction,Long>> timedInputWord = Arrays.asList(
                  new Pair<>(a,2L),
                  new Pair<>(b,3L),
                  new Pair<>(a,5L),
                  new Pair<>(a,8L),
                  new Pair<>(b,9L),
                  new Pair<>(a,20L),
                  new Pair<>(a,25L)
                );


        List<Pair<OutputAction, Long>> timedOutputWord = MealyTimerDetEnabledWalker.walkTimedInputWord(model,timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        System.out.println("");


        System.out.println("\n\n");
        timedInputWord = Arrays.asList(
                  new Pair<>(b,0L),
                  new Pair<>(a,0L),
                  new Pair<>(b,0L),
                  new Pair<>(a,1L),
                  //new Pair<>(b,2L)  // overrules  timeout x
                  new Pair<>(b,20L) // allow timetout x and timeout y triggered by timeout x  => note both x and y are 0 => walker chooses alphabettically so x done!

                );


        timedOutputWord = MealyTimerDetEnabledWalker.walkTimedInputWord(model,timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        System.out.println("");







        // show sorting timers and timervars
        //----------------------------------

        printHeader("show sorting timers and timervars");
        TimerVariable timer_a=new TimerVariable("timer_a");
        TimerVariable timer_z=new TimerVariable("timer_z");
        TreeSet<Timer> timers= new TreeSet<>();
        timers.add(new Timer(timer_y,2L));
        timers.add(new Timer(timer_x,2L));
        timers.add(new Timer(timer_a,1L));
        timers.add(new Timer(timer_z,1L));

        System.out.println("timers: " + timers);

        TreeSet<TimerVariable> timervars= new TreeSet<>();
        timervars.add(timer_y);
        timervars.add(timer_x);
        timervars.add(timer_a);
        timervars.add(timer_z);
        System.out.println("timervars: " + timervars);


        // make new model
        //--------------------------------

        printHeader("make new model");
        dbuilder =new MealyTimerDetEnabledModel.ImmutableBuilder();
        dbuilder.setStartLocation(s1);
        dbuilder.addTransition(s1,s1,a,o,"timer_x=100");
        dbuilder.addTransition(s1,s2,b,o);
        dbuilder.addTransition(s2,s2,a,o,"timer_y=200");
        dbuilder.addTransition(s2,s1,b,o); // uncomment this line , and it is not input enabled anymore => gives error when building
        dbuilder.addTransition(s1,s1,timer_x,timeout_x,"timer_y=200");  // add timeout transition for timer variable x
        //dbuilder.addTransition(s1,s2,x,u); // uncomment this line , and it is not timeout determinisitc anymore
        dbuilder.addTransition(s1,s1,timer_y,timeout_y);  // you may uncomment this line, because ghost timers are allowed
        model= dbuilder.build();

        System.out.println("\nmodel");
        System.out.println(model);
        System.out.println("\n\n");


        //================================
        // Async Running
        //================================



        // show running a timed input word
        //--------------------------------

        // create input and output words
        timedInputWord = Arrays.asList(
                new Pair<>(a,200L),
                new Pair<>(b,300L),
                new Pair<>(a,400L),
                new Pair<>(b,500L),
                new Pair<>(b,700L)
              );

        printHeader("ASYNC: show running a timed input word");

        System.out.println("walkTimedInputWord");
        timedOutputWord = MealyTimerDetEnabledWalker.walkTimedInputWord(model,timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        System.out.println("");

        System.out.println("runTimedInputWord");
        long epsilon=50L;
        timedOutputWord = MealyTimerDetEnableAsyncTraceRunner.runTimedInputWord(model, timedInputWord, epsilon);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );
        System.out.println("");





        // basic usage runner
        //-------------------

        printHeader("ASYNC: basic usage runner");

        // create Sul from model and epsilon (maximal processing delay time => extra sleep time after last input to catch last output)
        // note: asyncRunner implement SendInputListenOutput => mealy output is not directly return when sending input, instead output is handled by listener just as for timeout outputs
        MealyTimerDetEnabledAsyncRunner asyncrunner=new MealyTimerDetEnabledAsyncRunner(model);

        // start runner thread
        asyncrunner.start();


        // subscribe output listener
        asyncrunner.setOnOutputListener( new OutputListener<OutputAction>() {
            public void onOutput(OutputAction output) {
                System.out.println("out: " +output);
            }
        });

        asyncrunner.sendInput(a);
        try {Thread.sleep(200);} catch (InterruptedException e) {}
        asyncrunner.sendInput(a);
        asyncrunner.sendInput(b);
        try {Thread.sleep(200);} catch (InterruptedException e) {}
        asyncrunner.reset();
        asyncrunner.sendInput(b);
        try {Thread.sleep(200);} catch (InterruptedException e) {}


        // stop runner thread
        asyncrunner.interrupt();  // needed otherwise thread will keep  go on running on background!

        // show running multiple timed input words using single SUL  (more efficient then static method, because can reuse sul thread)
        //---------------------------------------------------------
        printHeader("ASYNC: show running multiple timed input words using single SUL");


        // create trace runner which runs full traces using runner
        MealyTimerDetEnableAsyncTraceRunner asyncTraceRunner = new MealyTimerDetEnableAsyncTraceRunner(model,epsilon);
        // start traceRunner thread
        asyncTraceRunner.start();

        // run first query
        timedOutputWord=asyncTraceRunner.sendTimedInputWord(timedInputWord); // special method which blocks until whole trace done!


        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );

        System.out.println("");

        // run second query
        timedOutputWord=asyncTraceRunner.sendTimedInputWord(timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );


        // stop traceRunner thread
        asyncTraceRunner.interrupt();  // needed otherwise thread will keep  go on running on background!



        //================================
        // Sync Running
        //================================



        printHeader("SYNC: show running a timed input word");


        System.out.println("walkTimedInputWord");
        timedOutputWord = MealyTimerDetEnabledWalker.walkTimedInputWord(model,timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        System.out.println("");

        System.out.println("runTimedInputWord");
        epsilon=50L;
        timedOutputWord = MealyTimerDetEnableTraceRunner.runTimedInputWord(model, timedInputWord, epsilon);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );
        System.out.println("");


        // basic usage runner
        //-------------------

        printHeader("SYNC: basic usage runner");

        // create Sul from model and epsilon (maximal processing delay time => extra sleep time after last input to catch last output)
        MealyTimerDetEnableRunner runner=new MealyTimerDetEnableRunner(model);

        // start runner thread
        runner.start();


        // subscribe output listener
        runner.setOnOutputListener( new OutputListener<OutputAction>() {
            public void onOutput(OutputAction output) {
                System.out.println("timeout: " +output);
            }
        });

        output=runner.sendInput(a);
        System.out.println("mealy out: " +output);
        try {Thread.sleep(200);} catch (InterruptedException e) {}
        output=runner.sendInput(a);
        System.out.println("mealy out: " +output);
        output=runner.sendInput(b);
        System.out.println("mealy out: " +output);
        try {Thread.sleep(200);} catch (InterruptedException e) {}
        runner.reset();
        output=runner.sendInput(b);
        System.out.println("mealy out: " +output);
        try {Thread.sleep(200);} catch (InterruptedException e) {}


        // stop runner thread
        runner.interrupt();  // needed otherwise thread will keep  go on running on background!

        // show running multiple timed input words using single SUL  (more efficient then static method, because can reuse sul thread)
        //---------------------------------------------------------
        printHeader("SYNC:  show running multiple timed input words using single SUL");


        // create trace runner which runs full traces using runner
        MealyTimerDetEnableTraceRunner traceRunner = new MealyTimerDetEnableTraceRunner(model,epsilon);
        // start traceRunner thread
        traceRunner.start();

        // run first query
        timedOutputWord=traceRunner.sendTimedInputWord(timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );

        System.out.println("");

        // run second query
        timedOutputWord=traceRunner.sendTimedInputWord(timedInputWord);
        System.out.println("inputWord: " + timedInputWord );
        System.out.println("outputWord: " + timedOutputWord );
        timedOutputWord = Util.roundDown(timedOutputWord, epsilon);
        System.out.println("roundDown outputWord: " + timedOutputWord );


        // stop traceRunner thread
        traceRunner.interrupt();  // needed otherwise thread will keep  go on running on background!


        printHeader("the end");
    }
}
