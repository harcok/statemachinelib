package statemachine.test;

import java.util.List;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.fa.conversion.Conversion;
import statemachine.model.fsm.fa.format.Dot;
import statemachine.model.fsm.moore.MooreDetModel;
import statemachine.model.fsm.moore.MooreNonDetModel;
import statemachine.util.IO;

public class TestDfa {

	public static void main(String[] args) {
		InputAction a = new InputAction("a");
		InputAction b = new InputAction("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		System.out.println("\nDFANonDetModel\n");



		NfaModel.ImmutableBuilder builder =new NfaModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		//builder.addOutput(s1, x);
		//builder.addOutput(s2, y);
		builder.setAccepting(s2);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2

		NfaModel dfaNonDet= builder.build();

		ImmutableSet<InputAction> inputAlphabet = dfaNonDet.getInputAlphabet();
		ImmutableSet<OutputAction> outputAlphabet = dfaNonDet.getOutputAlphabet();
		ImmutableSet<InputTransition> transitions = dfaNonDet.getModelTransitions();
		ImmutableSet<LocationState> locations = dfaNonDet.getLocations();

		System.out.print("\ninput alphabet: ");
		System.out.println(inputAlphabet);
		for ( InputAction action : inputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\noutput alphabet");
		for ( OutputAction action : outputAlphabet ) {
			System.out.println(action);
		}
		System.out.println("\nlocations");
		for ( LocationState loc :locations ) {
			System.out.println(loc);
		}
		System.out.println("\ntransitions");
		for ( InputTransition transition :transitions ) {
			System.out.println(transition);
		}
		System.out.println("\nmodel");
		System.out.println(dfaNonDet);


		List<String> lines=Dot.export(dfaNonDet);
		IO.writeSmallTextFile( lines, "simpleDfa.dot");


		System.out.println( "");
		System.out.print( "\nsimpleDfa import from simpleDfa.dot:\n\n");
		List<String> read_lines=IO.readSmallTextFile(  "simpleDfa.dot");

		NfaModel simpleDfa= Dot.importNfaModel(read_lines); // OK
		System.out.println( "");
		System.out.print( simpleDfa);
		System.out.println( "");




		statemachine.model.fsm.fa.file.Export.dot(dfaNonDet, "simpleDfa2.dot");
		statemachine.model.fsm.fa.file.Export.gml(dfaNonDet, "simpleDfa2.gml");

		//TODO: test following after gml import implemented :  DfaNonDetModel importmodel= statemachine.model.fsm.dfa.file.Import.gml2DfaModel("simpleDfa2.gml");
		NfaModel importmodel= statemachine.model.fsm.fa.file.Import.dot2NfaModel("simpleDfa2.dot");

		System.out.println( "importmodel");
		System.out.print( importmodel);
		System.out.println( "");



		MooreNonDetModel model= Conversion.Fa2MooreNonDetModel(importmodel);

        System.out.println( "moore model");
        System.out.print( model);
        System.out.println( "");


        MooreDetModel detmodel=MooreDetModel.fromMooreModel(model);

        System.out.println( "moore  det model");
        System.out.print( detmodel);
        System.out.println( "");
        
        
	}



}
