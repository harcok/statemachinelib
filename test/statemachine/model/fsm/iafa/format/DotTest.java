package statemachine.model.fsm.iafa.format;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.iafa.IaFaDetModel;
import statemachine.model.fsm.iafa.IaFaNonDetModel;
import statemachine.model.fsm.iafa.format.Dot;

public class DotTest {


    static public IaFaDetModel getCtrlInterfaceModel() {
        IaFaDetModel.ImmutableBuilder builderDet =new IaFaDetModel.ImmutableBuilder();

        // INPUTS
        InputAction calibrate = new InputAction("calibrate");
        InputAction stop =  new InputAction("stop");
        // in void move(position_t target);  => ignore param in learner
        InputAction move =  new InputAction("move");

        // OUTPUTS
        OutputAction homed =  new OutputAction("homed");
        OutputAction arrived =  new OutputAction("arrived");
        // extra output
        OutputAction quiescence =  new OutputAction("quiescence");

        // STATES
        LocationState stateLost = new LocationState("Lost");
        LocationState stateCalibrating = new LocationState("Calibrating");
        LocationState stateIdle = new LocationState("Idle");
        LocationState stateMoving = new LocationState("Moving");

        // extra state because after calibrate request we can either
        //  - homed  ( request disapproved : stateIdle)
        //  - quiescence ( request approved  : stateCalibrating )
        LocationState stateRequest = new LocationState("Request");




        // START STATE
        builderDet.setStartLocation(stateLost);

        //TRANSITIONS per state
        builderDet.addInputTransition(stateLost,stateLost, stop);
        builderDet.addInputTransition(stateLost,stateRequest, calibrate);

        builderDet.addOutputTransition(stateRequest,stateCalibrating, quiescence);
        builderDet.addOutputTransition(stateRequest,stateIdle, homed);
        // note: in stateRequest any input is illegal;
        //       within run-to-completion run of calibrate input in stateLost
        //       the system first goes to this intermediate state stateRequest
        //       after which the system should immediately give
        //       output homed or no output(quiescence)
        //       so that it immediately leaves this state ()

        builderDet.addInputTransition(stateCalibrating,stateLost, stop);
        builderDet.addOutputTransition(stateCalibrating,stateIdle,homed);

        builderDet.addInputTransition(stateIdle,stateLost, stop);
        builderDet.addInputTransition(stateIdle,stateMoving, move);

        builderDet.addInputTransition(stateMoving,stateLost, stop);
        builderDet.addOutputTransition(stateMoving,stateIdle,arrived);

        builderDet.setAccepting(stateLost);
        builderDet.setAccepting(stateCalibrating);
        builderDet.setAccepting(stateIdle);
        builderDet.setAccepting(stateMoving);

        // BUILD,SAVE,RETURN
        IaFaDetModel iaDet= builderDet.build();
        statemachine.model.fsm.iafa.file.Export.dot(iaDet, "ictrl.dot");
        return iaDet;
 }


    @Test
    public void testDotExport() {
        String expected="digraph g {\n" +
                "\n" +
                "    __start0 [label=\"\" shape=\"none\"]\n" +
                "\n" +
                "    Lost [shape=\"doublecircle\" label=\"Lost\"];\n" +
                "    Request [shape=\"circle\" label=\"Request\"];\n" +
                "    Calibrating [shape=\"doublecircle\" label=\"Calibrating\"];\n" +
                "    Idle [shape=\"doublecircle\" label=\"Idle\"];\n" +
                "    Moving [shape=\"doublecircle\" label=\"Moving\"];\n" +
                "    Lost -> Lost [label=\"?stop\"];\n" +
                "    Lost -> Request [label=\"?calibrate\"];\n" +
                "    Calibrating -> Lost [label=\"?stop\"];\n" +
                "    Idle -> Lost [label=\"?stop\"];\n" +
                "    Idle -> Moving [label=\"?move\"];\n" +
                "    Moving -> Lost [label=\"?stop\"];\n" +
                "    Request -> Calibrating [label=\"!quiescence\"];\n" +
                "    Request -> Idle [label=\"!homed\"];\n" +
                "    Calibrating -> Idle [label=\"!homed\"];\n" +
                "    Moving -> Idle [label=\"!arrived\"];\n" +
                "\n" +
                "    __start0 -> Lost;\n" +
                "}";
        IaFaDetModel model=getCtrlInterfaceModel();

        String result=String.join("\n", Dot.export(model));
        assertEquals(expected,result);
    }



    @Test
    public void testImport() {
        String expected="digraph g {\n" +
                "\n" +
                "    __start0 [label=\"\" shape=\"none\"]\n" +
                "\n" +
                "    Lost [shape=\"doublecircle\" label=\"Lost\"];\n" +
                "    Request [shape=\"circle\" label=\"Request\"];\n" +
                "    Calibrating [shape=\"doublecircle\" label=\"Calibrating\"];\n" +
                "    Idle [shape=\"doublecircle\" label=\"Idle\"];\n" +
                "    Moving [shape=\"doublecircle\" label=\"Moving\"];\n" +
                "    Lost -> Lost [label=\"?stop\"];\n" +
                "    Lost -> Request [label=\"?calibrate\"];\n" +
                "    Calibrating -> Lost [label=\"?stop\"];\n" +
                "    Idle -> Lost [label=\"?stop\"];\n" +
                "    Idle -> Moving [label=\"?move\"];\n" +
                "    Moving -> Lost [label=\"?stop\"];\n" +
                "    Request -> Calibrating [label=\"!quiescence\"];\n" +
                "    Request -> Idle [label=\"!homed\"];\n" +
                "    Calibrating -> Idle [label=\"!homed\"];\n" +
                "    Moving -> Idle [label=\"!arrived\"];\n" +
                "\n" +
                "    __start0 -> Lost;\n" +
                "}";

        List<String> importlines=Arrays.asList(expected.split("\n"));

      //  IaFaDetModel model=getCtrlInterfaceModel();
        IaFaNonDetModel model=Dot.importIaFaModel(importlines);
        String result = String.join("\n", Dot.export(model));
        assertEquals(expected,result);
    }

}
