package statemachine.model.fsm.iafa.walker;

import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.iafa.IaFaDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class IaFaDetWalkerTest {


    IaFaDetModel model;

    // INPUTS
    InputAction calibrate = new InputAction("calibrate");
    InputAction stop =  new InputAction("stop");
    // in void move(position_t target);  => ignore param in learner
    InputAction move =  new InputAction("move");

    // OUTPUTS
    OutputAction homed =  new OutputAction("homed");
    OutputAction arrived =  new OutputAction("arrived");
    // extra output
    OutputAction quiescence =  new OutputAction("quiescence");

    // STATES
    LocationState stateLost = new LocationState("Lost");
    LocationState stateCalibrating = new LocationState("Calibrating");
    LocationState stateIdle = new LocationState("Idle");
    LocationState stateMoving = new LocationState("Moving");

    // extra state because after calibrate request we can either
    //  - homed  ( request disapproved : stateIdle)
    //  - quiescence ( request approved  : stateCalibrating )
    LocationState stateRequest = new LocationState("Request");



    @Before
    public void setupCtrlInterfaceModel() {
        IaFaDetModel.ImmutableBuilder builderDet =new IaFaDetModel.ImmutableBuilder();


        // START STATE
        builderDet.setStartLocation(stateLost);

        //TRANSITIONS per state
        builderDet.addInputTransition(stateLost,stateLost, stop);
        builderDet.addInputTransition(stateLost,stateRequest, calibrate);

        builderDet.addOutputTransition(stateRequest,stateCalibrating, quiescence);
        builderDet.addOutputTransition(stateRequest,stateIdle, homed);
        // note: in stateRequest any input is illegal;
        //       within run-to-completion run of calibrate input in stateLost
        //       the system first goes to this intermediate state stateRequest
        //       after which the system should immediately give
        //       output homed or no output(quiescence)
        //       so that it immediately leaves this state ()

        builderDet.addInputTransition(stateCalibrating,stateLost, stop);
        builderDet.addOutputTransition(stateCalibrating,stateIdle,homed);

        builderDet.addInputTransition(stateIdle,stateLost, stop);
        builderDet.addInputTransition(stateIdle,stateMoving, move);

        builderDet.addInputTransition(stateMoving,stateLost, stop);
        builderDet.addOutputTransition(stateMoving,stateIdle,arrived);

        builderDet.setAccepting(stateLost);
        builderDet.setAccepting(stateCalibrating);
        builderDet.setAccepting(stateIdle);
        builderDet.setAccepting(stateMoving);

        model = builderDet.build();

    }

    @Test
    public void testCheckInputOutputPossible() {
        IaFaDetWalker walker= new IaFaDetWalker(model);

        assertFalse(walker.checkInputPossible(move));
        assertTrue(walker.checkInputPossible(stop));
        assertFalse(walker.checkOutputPossible(arrived));
    }

    @Test
    public void testStepInput(){
        IaFaDetWalker walker= new IaFaDetWalker(model);
        assertEquals( walker.getLocation(),stateLost );
        walker.stepInput(stop);
        assertEquals( walker.getLocation(),stateLost );
        walker.stepInput(move);
        assertEquals( walker.getLocation(),stateLost );
        walker.stepInput(calibrate);
        assertEquals( walker.getLocation(),stateRequest );
    }

    @Test
    public void testStepOutput(){
        IaFaDetWalker walker= new IaFaDetWalker(model);
        walker.stepInput(calibrate);
        walker.stepOutput(arrived);
        assertEquals( walker.getLocation(),stateRequest );
        walker.stepOutput(homed);
        assertEquals( walker.getLocation(),stateIdle );
    }

    @Test
    public void testReset(){
        IaFaDetWalker walker= new IaFaDetWalker(model);
        walker.stepInput(calibrate);
        walker.stepOutput(homed);
        walker.resetState();
        assertEquals( walker.getLocation(),stateLost );
    }


    @Test
    public void testCheckInputOutputPossible2() {
        IaFaDetWalker walker= new IaFaDetWalker(model);

        walker.stepInput(calibrate);

        assertFalse(walker.checkInputPossible(move));
        assertFalse(walker.checkInputPossible(stop));
        assertFalse(walker.checkOutputPossible(arrived));
        assertTrue(walker.checkOutputPossible(quiescence));

        walker.stepOutput(homed);

        assertTrue(walker.checkInputPossible(move));
        assertTrue(walker.checkInputPossible(stop));
        assertFalse(walker.checkOutputPossible(arrived));
        assertFalse(walker.checkOutputPossible(quiescence));
    }

    @Test
    public void testAccepting(){
        IaFaDetWalker walker= new IaFaDetWalker(model);
        assertTrue(walker.isAccepting());
        walker.stepInput(calibrate);
        assertFalse(walker.isAccepting());
        walker.stepOutput(homed);
        assertTrue(walker.isAccepting());
        walker.resetState();
        assertTrue(walker.isAccepting());
    }

}
