package statemachine.model.fsm.iafa;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

public class IaFaDetModelTest {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public  void testNoneDeterministicBuild() {
        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");


        IaFaDetModel.ImmutableBuilder builder = new IaFaDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s2);
        builder.addInputTransition(s1,s2,a);
        builder.addInputTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
        builder.addInputTransition(s1, s2, b);
        builder.addOutputTransition(s2, s1, x);
        builder.addOutputTransition(s1, s1, y);



        thrown.expect(RuntimeException.class);
        thrown.expectMessage("none deterministic choice");
        builder.build();
    }

    @Test
    public  void testFromIaFaModel() {
        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");


        IaFaNonDetModel.ImmutableBuilder builder = new IaFaNonDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s2);
        builder.addInputTransition(s1,s2,a);
        builder.addInputTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
        builder.addInputTransition(s1, s2, b);
        builder.addOutputTransition(s2, s1, x);
        builder.addOutputTransition(s1, s1, y);

        IaFaNonDetModel nondetmodel = builder.build();

        thrown.expect(RuntimeException.class);
        thrown.expectMessage("none deterministic choice");
        IaFaDetModel.fromIaFaModel(nondetmodel);
    }


    @Test
    public void testGetTransitionFor() {

        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        IaFaDetModel.ImmutableBuilder builder = new IaFaDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s2);
        builder.addInputTransition(s1,s2,a);
        builder.addInputTransition(s1, s2, b);
        builder.addOutputTransition(s2, s1, x);
        builder.addOutputTransition(s1, s1, y);


        IaFaDetModel detmodel= builder.build();

        // note: transition is no data object, and therefore doesn't implement equals
        //       transition is construction object for model
        assertReflectionEquals(detmodel.getTriggeredTransition(s1, a), Optional.of(new InputTransition(s1,s2,a))  );
        assertReflectionEquals(detmodel.getTriggeredTransition(s1, b), Optional.of(new InputTransition(s1,s2,b))  );
        assertReflectionEquals(detmodel.getSystemTransition(s2, x), Optional.of(new OutputTransition(s2,s1,x))  );
        assertReflectionEquals(detmodel.getSystemTransition(s2, y), Optional.empty()  );

    }
}
