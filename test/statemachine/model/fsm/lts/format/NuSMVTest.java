package statemachine.model.fsm.lts.format;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeUtils;
import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.util.Util;

public class NuSMVTest {
    static public InputAction WATER = new InputAction("WATER");
    static public InputAction POD = new InputAction("POD");
    static public InputAction BUTTON = new InputAction("BUTTON");
    static public InputAction CLEAN = new InputAction("CLEAN");

    static LtsDetModel getLtsDetModel() {
        Action MSG = new Action("MSG");
        Action BEGIN = new Action("BEGIN");

        LocationState q0 = new LocationState("q0");
        LocationState q1 = new LocationState("q1");

        LtsDetModel.ImmutableBuilder builder = new LtsDetModel.ImmutableBuilder();
        builder.setStartLocation(q0);
        builder.addTransition(q0, q0, MSG);
        builder.addTransition(q0, q1, BEGIN);
        builder.addTransition(q1, q1, MSG);
        builder.addTransition(q1, q1, BEGIN);
        LtsDetModel detlts = builder.build();
        return detlts;
    }

    @Test
    public void testExport() {
        LtsDetModel detlts=getLtsDetModel() ;
        List<String> result_lines = statemachine.model.fsm.lts.format.NuSMV.export(detlts);

        //@formatter:off
        List<String> expected_lines =  Arrays.asList(
                "MODULE main",
                "VAR",
                "    state : { q0, q1 }",
                "    newact : { MSG, BEGIN }",
                "    oldact : { MSG, BEGIN }",
                "ASSIGN",
                "    init(state) := q0;",
                "    next(state) := case",
                "                    state = q0 & newact = MSG : q0;",
                "                    state = q0 & newact = BEGIN : q1;",
                "                    state = q1 & newact = MSG : q1;",
                "                    state = q1 & newact = BEGIN : q1;",
                "                   esac",
                "    oldact := newact;"
         );


  // transition order can be changed in export so we sort lines
 //       expected_lines.sort(Comparator.naturalOrder());
 //       result_lines.sort(Comparator.naturalOrder());


        assertEquals(String.join("\n",expected_lines),String.join("\n",result_lines));

    }
}
