package statemachine.model.fsm.lts;


import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsEnabledDetModelTest {

	@Test
	public void testFromLtsModel() {
		Action a = new Action("a");
		Action b = new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsNonDetModel nondetlts= builder.build();

		try {
			LtsEnabledDetModel.fromLtsModel(nondetlts);
			fail("none enabled model cannot be converted to enabled");
		} catch (RuntimeException e) {
		}
	}


	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testNoneEnabledBuild() {
		Action a =  new Action("a");
		Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsEnabledDetModel.ImmutableBuilder builder =new LtsEnabledDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2


		thrown.expect(RuntimeException.class);
        thrown.expectMessage("error in model; enabled model doesn't has a transition in state");
        builder.build();

	}

	@Test
	public void testNoneDetermisticBuild() {
        Action a =  new Action("a");
        Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsEnabledDetModel.ImmutableBuilder builder =new LtsEnabledDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		builder.addTransition(s2,s2,b);
		builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2


		thrown.expect(RuntimeException.class);
        thrown.expectMessage("none deterministic choice");
        builder.build();

	}

	@Test
	public void testGetTransitionsForLocationStateAction() {
        Action a =  new Action("a");
        Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsEnabledDetModel.ImmutableBuilder builder =new LtsEnabledDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		builder.addTransition(s2,s2,b);
		LtsEnabledDetModel enabledDetLts= builder.build();

		// verify real result against expected result
		assertLenientEquals(enabledDetLts.getTriggeredTransition(s1, a),new ActionTransition(s1,s2,a));
		assertReflectionEquals(enabledDetLts.getTriggeredTransition(s1, b),new ActionTransition(s1,s2,b));
	    assertReflectionEquals(enabledDetLts.getTriggeredTransition(s2, a),new ActionTransition(s2,s1,a));
	    assertReflectionEquals(enabledDetLts.getTriggeredTransition(s2, b),new ActionTransition(s2,s2,b));

	}

}
