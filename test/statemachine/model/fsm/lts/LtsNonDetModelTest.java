package statemachine.model.fsm.lts;

// http://www.unitils.org/tutorial-reflectionassert.html

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;
import static org.unitils.reflectionassert.ReflectionComparatorMode.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsNonDetModelTest {

    static private LtsNonDetModel nondetlts;

    @BeforeClass
    public static void setup() {
        Action a = new Action("a");
        Action b = new Action("b");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.addTransition(s1, s2, a);
        builder.addTransition(s1, s2, b);
        builder.addTransition(s2, s1, a);
        builder.addTransition(s1, s1, a); // for a in s1 : either go to s1 or s2
        nondetlts = builder.build();
    }

    /*
    public <T> void assertObjectEquals(final T expected, final T actual) {
         assertTrue(new ReflectionEquals(actual,
                                        (String[])null).matches(expected));
    }
    */

    @Test
    public void testFromLtsModel() {

        // next doesn't work because Model doesn't implement equals
        //assertEquals(nondetlts,LtsNonDetModel.fromLtsModel(nondetlts));

        // however you can compare the models with reflection
        assertReflectionEquals(nondetlts, LtsNonDetModel.fromLtsModel(nondetlts));

        // HACK:
        //assertEquals(nondetlts.toString(),LtsNonDetModel.fromLtsModel(nondetlts).toString());

        // FAIL using  org.mockito.internal.matchers.apachecommons.ReflectionEquals.assertTrue
        // assertTrue(new ReflectionEquals(nondetlts,(String[])null).matches(LtsNonDetModel.fromLtsModel(nondetlts)));
    }

    @Test
    public void testGetStartLocation() {
        assertEquals(nondetlts.getStartLocation(), new LocationState("1"));
    }

    @Test
    public void testGetLocations() {

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");
        assertEquals(nondetlts.getLocations(), new HashSet<>(Arrays.asList(s2, s1)));
    }

    @Test
    public void testGetTransitions() {

        Action a = new Action("a");
        Action b = new Action("b");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        List<ActionTransition> l = Arrays.asList(new ActionTransition(s2, s1, a), // different order also when constructing it
                new ActionTransition(s1, s2, a), new ActionTransition(s1, s2, b), new ActionTransition(s1, s1, a));

        //
        //		ImmutableSet<ActionTransition> li = ImmutableSet.of(
        //				   new ActionTransition(s1,s2,a),
        //				   new ActionTransition(s1,s2,b),
        //				   new ActionTransition(s2,s1,a),  // different order also when constructing it
        //				   new ActionTransition(s1,s1,a)
        //				);
        //		assertReflectionEquals(nondetlts.getTransitions(),li);
        //
        ImmutableSet<ActionTransition> li = ImmutableSet.of(new ActionTransition(s2, s1, a), // different order also when constructing it
                new ActionTransition(s1, s2, a), new ActionTransition(s1, s2, b), new ActionTransition(s1, s1, a));
        assertLenientEquals(nondetlts.getModelTransitions(), li);

        //	assertReflectionEquals(nondetlts.getTransitions(),new HashSet<>(l));
        //  assertThat(nondetlts.getTransitions(),equalTo(new HashSet<>(l)));

        assertLenientEquals(nondetlts.getModelTransitions(), new HashSet<>(l));

        //assertEquals(nondetlts.getTransitions(),new HashSet<>(l));
    }

    @Test
    public void testGetTransitionsForLocationState() {
        Action a = new Action("a");
        Action b = new Action("b");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        List<ActionTransition> l1 = Arrays.asList(new ActionTransition(s1, s2, a), new ActionTransition(s1, s2, b),
                new ActionTransition(s1, s1, a));
        //assertReflectionEquals(nondetlts.getTransitionsFor(s1),new HashSet<>(l1) );
        assertLenientEquals(nondetlts.getModelTransitions(s1), new HashSet<>(l1));

        List<ActionTransition> l2 = Arrays.asList(new ActionTransition(s2, s1, a));
        assertLenientEquals(nondetlts.getModelTransitions(s2), new HashSet<>(l2));
        //assertEquals(nondetlts.getTransitionsFor(s2),new HashSet<>(l2) );
    }

    @Test
    public void testGetAlphabet() {
        Action a = new Action("a");
        Action b = new Action("b");
        assertEquals(nondetlts.getAlphabet(), new HashSet<Action>(Arrays.asList(a, b)));
    }

    @Test
    public void testGetTransitionsForLocationStateAction() {
        Action a = new Action("a");
        Action b = new Action("b");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        // expected transitions for  location and action combo :
        List<ActionTransition> l1a = Arrays.asList(new ActionTransition(s1, s1, a), new ActionTransition(s1, s2, a));
        List<ActionTransition> l1b = Arrays.asList(new ActionTransition(s1, s2, b));
        List<ActionTransition> l2a = Arrays.asList(new ActionTransition(s2, s1, a));
        HashSet<ActionTransition> l2b = new HashSet<>();

        // verify real result against expected result
        assertReflectionEquals(nondetlts.getModelTransitions(s1, a), l1a, LENIENT_ORDER);
        assertReflectionEquals(nondetlts.getModelTransitions(s1, b), l1b, LENIENT_ORDER);
        assertReflectionEquals(nondetlts.getModelTransitions(s2, a), l2a, LENIENT_ORDER);
        assertReflectionEquals(nondetlts.getModelTransitions(s2, b), l2b, LENIENT_ORDER);

    }

}
