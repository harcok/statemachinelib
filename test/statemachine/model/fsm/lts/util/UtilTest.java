package statemachine.model.fsm.lts.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeUtils;
import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.format.Dot;
import statemachine.model.fsm.lts.util.Util;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.util.IO;

public class UtilTest {
    static public InputAction WATER = new InputAction("WATER");
    static public InputAction POD = new InputAction("POD");
    static public InputAction BUTTON = new InputAction("BUTTON");
    static public InputAction CLEAN = new InputAction("CLEAN");

    static LtsDetModel getLtsDetModel() {
        Action a = new Action("a");
        Action b = new Action("b");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        LtsDetModel.ImmutableBuilder builder = new LtsDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.addTransition(s1, s2, a);
        builder.addTransition(s1, s2, b);
        builder.addTransition(s2, s1, a);
        LtsDetModel detlts = builder.build();
        return detlts;
    }

    @Test
    public void testSpanningTree1() {
        String result;
        LtsDetModel detlts = getLtsDetModel();
        Tree<EdgeNode<Action, LocationState>> tree = Util.getSpanningTreeLts(detlts);

        result = String.join("\n", nl.ru.cs.tree.format.Dot.edgeNodeTree2dot(tree, e -> e.toString(), n -> n.toString()));

        //@formatter:off
        String expected = "digraph g {\n" +
                  "    __start [label=\"\" style=\"invis\" width=\"0\" height=\"0\" ];\n" +
                  "    __start ->  n0;\n" +
                  "\n" +
                  "    n0 [label=\"1\"];\n" +
                  "    n1 [label=\"2\"];\n" +
                  "    n0 -> n1 [label=\"a\"];\n" +
                  "}";
        //@formatter:on

        assertEquals(expected, result);
    }

    @Test
    public void testSpanningTree2() {

        LtsNonDetModel nondetlts = statemachine.model.fsm.lts.file.Import.dot2LtsModel("coffeemachine.dot");
        LtsDetModel detlts  = LtsDetModel.fromLtsModel(nondetlts);

        Tree<EdgeNode<Action, LocationState>> result_tree = Util.getSpanningTreeLts(detlts);



        //nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("spanLtsResult.dot",result_tree);

        // expected spanning tree
        //@formatter:off
        List<String> expected_lines =  Arrays.asList("digraph g {" ,
              "    s0 [shape=\"circle\" label=\"s0\"];" ,
              "    s1 [shape=\"circle\" label=\"s1\"];" ,
              "    s2 [shape=\"circle\" label=\"s2\"];" ,
              "    s3 [shape=\"circle\" label=\"s3\"];" ,
              "    s4 [shape=\"circle\" label=\"s4\"];" ,
              "    s5 [shape=\"circle\" label=\"s5\"];" ,
              "    s0 -> s1 [label=\"BUTTON / error\"];" ,
              "    s0 -> s2 [label=\"POD / ok\"];" ,
              "    s0 -> s4 [label=\"WATER / ok\"];" ,
              "    s3 -> s5 [label=\"BUTTON / coffee!\"];" ,
              "    s4 -> s3 [label=\"POD / ok\"];" ,
              "}");


        Tree<EdgeNode<Action, LocationState>> expected_tree = nl.ru.cs.tree.format.Dot.dot2edgeNodeTree(expected_lines, e -> new Action(e), n -> new LocationState(n));

        //nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("spanLtsExpected.dot",expected_tree);

        // use compareTreeDeterministic because order of childs may vary, but if you sort them on label then
        // trees should be exactly same.
        assertTrue(TreeUtils.compareTreeDeterministic(expected_tree, result_tree));


    }

}
