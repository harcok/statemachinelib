package statemachine.model.fsm.lts;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.unitils.reflectionassert.ReflectionAssert.*;


import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsDetModelTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();


	@Test
	public void testFromLtsModel() {
		Action a =  new Action("a");
		Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsNonDetModel nondetlts= builder.build();

	    thrown.expect(RuntimeException.class);
	    thrown.expectMessage("none deterministic choice");
		LtsDetModel.fromLtsModel(nondetlts);
	}



	@Test
	public void testNoneDetermisticBuild() {
		Action a =  new Action("a");
		Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsDetModel.ImmutableBuilder builder =new LtsDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		builder.addTransition(s2,s2,b);
		builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2


		thrown.expect(RuntimeException.class);
        thrown.expectMessage("none deterministic choice");
        builder.build();

	}

	@Test
	public void testGetTransitionFor() {
		Action a =  new Action("a");
		Action b =  new Action("b");

		LocationState s1 = new LocationState("1");
		LocationState s2 = new LocationState("2");

		LtsDetModel.ImmutableBuilder builder =new LtsDetModel.ImmutableBuilder();
		builder.setStartLocation(s1);
		builder.addTransition(s1,s2,a);
		builder.addTransition(s1,s2,b);
		builder.addTransition(s2,s1,a);
		//builder.addTransition(s1,s1,a); // for a in s1 : either go to s1 or s2
		LtsDetModel detlts= builder.build();

		// note: transition is no data object, and therefore doesn't implement equals
		//       transition is construction object for model
		assertReflectionEquals(detlts.getTriggeredTransition(s1, a), Optional.of(new ActionTransition(s1,s2,a))  );
		assertReflectionEquals(detlts.getTriggeredTransition(s1, b), Optional.of(new ActionTransition(s1,s2,b))  );
		assertReflectionEquals(detlts.getTriggeredTransition(s2, a), Optional.of(new ActionTransition(s2,s1,a))  );
		assertReflectionEquals(detlts.getTriggeredTransition(s2, b), Optional.empty()  );

	}

}
