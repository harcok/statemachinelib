package statemachine.model.fsm.graphs.digraph;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import statemachine.model.elements.location.LocationState;
import statemachine.model.graphs.digraph.DiGraphModel;

public class DiGraphModelTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testNoMultiTransitions() {
        DiGraphModel.ImmutableBuilder builder = new DiGraphModel.ImmutableBuilder();
//        LocationState a = new LocationState("a");
//        LocationState b = new LocationState("b");
//        LocationState c = new LocationState("c");
//        LocationState d = new LocationState("d");
//        LocationState e = new LocationState("e");
//        LocationState f = new LocationState("f");
        LocationState g = new LocationState("g");
        LocationState h = new LocationState("h");
//        builder.addTransition(a, b);
//        builder.addTransition(b, c);
//        builder.addTransition(b, e);
//        builder.addTransition(b, f);
//        builder.addTransition(c, d);
//        builder.addTransition(c, g);
//        builder.addTransition(d, c);
//        builder.addTransition(d, h);
//        builder.addTransition(e, a);
//        builder.addTransition(e, f);
//        builder.addTransition(f, g);
//        builder.addTransition(g, f);
        builder.addTransition(g, h);

        thrown.expect(RuntimeException.class);
        thrown.expectMessage("no two transitions with same source and destination allowed");
        builder.addTransition(g, h);  // second transition with  src=g and dst=h

//        DiGraphModel model = builder.build();
    }



}
