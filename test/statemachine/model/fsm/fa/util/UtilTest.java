package statemachine.model.fsm.fa.util;

import static org.junit.Assert.*;

//import static org.hamcrest.CoreMatchers.*;
//import static org.unitils.reflectionassert.ReflectionAssert.*;
//import static org.unitils.reflectionassert.ReflectionComparatorMode.*;


import org.junit.Test;

import statemachine.model.fsm.fa.util.Util;
import statemachine.model.fsm.fa.util.TraceDiff;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.fa.DfaModel;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.fa.file.Export;
import statemachine.model.fsm.fa.format.Dot;

public class UtilTest {



    // test model which already differs at  first transition
    @Test
    public void testCompare0() {
        LocationState s1 = new LocationState("a");
        LocationState s2 = new LocationState("b");
        LocationState s3 = new LocationState("c");

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.addTransition(s1, s2, num0);
        DfaModel machine1 = builder.build();

        builder =new DfaModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.addTransition(s1, s2, num1);
        DfaModel machine2 = builder.build();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true ?0/MISSING";
        assertEquals(expected, diff.toString());
    }

    // test model which already differs at  first state  accepting value
    @Test
    public void testCompare0b() {
        LocationState s1 = new LocationState("a");
        LocationState s2 = new LocationState("b");
        LocationState s3 = new LocationState("c");

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.addTransition(s1, s2, num0);
        DfaModel machine1 = builder.build();

        builder =new DfaModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        //builder.setAccepting(s1);
        builder.addTransition(s1, s2, num1);
        DfaModel machine2 = builder.build();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true/false";
        assertEquals(expected, diff.toString());
    }


    @Test
    public void testCompare1() {

        DfaModel machine1 = getDfaModel1();
        DfaModel machine2 = getDfaModel1transitionchanged();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true ?0  true ?0  true ?0 true MISSING/?1";

        assertEquals(expected, diff.toString());
    }

    @Test
    public void testCompare1b() {

        DfaModel machine1 = getDfaModel1transitionchanged();
        DfaModel machine2 = getDfaModel1();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true ?0  true ?0  true ?0 true ?1/MISSING";

        assertEquals(expected, diff.toString());
    }

    @Test
    public void testCompare2() {

        DfaModel machine1 = getDfaModel1();
        DfaModel machine2 = getDfaModel1();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        assertNull(diff);

    }

    @Test
    public void testCompare3() {

        DfaModel machine1 = getDfaModel1();
        DfaModel machine2 = getDfaModel1acceptingchanged();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true ?0  true ?0  true ?0 true/false";
        assertEquals(expected, diff.toString());
    }

    @Test
    public void testCompare4() {

        DfaModel machine1 = getDfaModel1acceptingchanged();
        DfaModel machine2 = getDfaModel1transitionchanged();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT: true ?0  true ?0  true ?0 false/true";
        assertEquals(expected, diff.toString());
    }

    // // see: https://web.archive.org/web/20171116102440/https://en.wikipedia.org/wiki/Powerset_construction
    public NfaModel getNfaModel1() {
        NfaModel.ImmutableBuilder builder =new NfaModel.ImmutableBuilder();

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");
        LocationState s3 = new LocationState("3");
        LocationState s4 = new LocationState("4");

        builder.setStartLocation(s1);

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");
        InputAction epsilon = new InputAction("e");

        builder.addTransition(s1, s2, num0);
        builder.addTransition(s1, s3, epsilon);

        builder.addTransition(s2, s2, num1);
        builder.addTransition(s2, s4, num1);

        builder.addTransition(s3, s2, epsilon);
        builder.addTransition(s3, s4, num0);

        builder.addTransition(s4, s3, num0);

        builder.setStartLocation(s1);
        builder.setAccepting(s3);
        builder.setAccepting(s4);

        return builder.build();
    }

    // see: https://web.archive.org/web/20171116102440/https://en.wikipedia.org/wiki/Powerset_construction
    public DfaModel getDfaModel1() {
        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        LocationState s1 = new LocationState("{1,2,3}")  ;
        LocationState s2 = new LocationState("{2,4}");
        LocationState s3 = new LocationState("{2,3}") ;
        LocationState s4 = new LocationState("{4}");

        builder.setStartLocation(s1);

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        builder.addTransition(s1, s2, num0);
        builder.addTransition(s1, s2, num1);

        builder.addTransition(s2, s2, num1);
        builder.addTransition(s2, s3, num0);

        builder.addTransition(s3, s2, num1);
        builder.addTransition(s3, s4, num0);

        builder.addTransition(s4, s3, num0);

        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.setAccepting(s2);
        builder.setAccepting(s3);
        builder.setAccepting(s4);

        return builder.build();
    }

    public DfaModel getDfaModel1transitionchanged() {
        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        // changed labels on states => shouldn't matter for observational compare
        LocationState s1 = new LocationState("1")  ;
        LocationState s2 = new LocationState("2");
        LocationState s3 = new LocationState("3}") ;
        LocationState s4 = new LocationState("4");

        builder.setStartLocation(s1);

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        builder.addTransition(s1, s2, num0);
        builder.addTransition(s1, s2, num1);

        builder.addTransition(s2, s2, num1);
        builder.addTransition(s2, s3, num0);

        builder.addTransition(s3, s2, num1);
        builder.addTransition(s3, s4, num0);

        builder.addTransition(s4, s3, num0);

        // added transition
        builder.addTransition(s4, s1, num1);

        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.setAccepting(s2);
        builder.setAccepting(s3);
        builder.setAccepting(s4);

        return builder.build();
    }

    public DfaModel getDfaModel1acceptingchanged() {
        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        // changed labels on states => shouldn't matter for observational compare
        LocationState s1 = new LocationState("1")  ;
        LocationState s2 = new LocationState("2");
        LocationState s3 = new LocationState("3}") ;
        LocationState s4 = new LocationState("4");

        builder.setStartLocation(s1);

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        builder.addTransition(s1, s2, num0);
        builder.addTransition(s1, s2, num1);

        builder.addTransition(s2, s2, num1);
        builder.addTransition(s2, s3, num0);

        builder.addTransition(s3, s2, num1);
        builder.addTransition(s3, s4, num0);

        builder.addTransition(s4, s3, num0);


        builder.setStartLocation(s1);
        builder.setAccepting(s1);
        builder.setAccepting(s2);
        builder.setAccepting(s3);
       // builder.setAccepting(s4);  => changed accepting in state4 to rejecting

        return builder.build();
    }


 // see: https://web.archive.org/web/20171116102440/https://en.wikipedia.org/wiki/Powerset_construction
    public NfaModel getNfaModel2() {
        NfaModel.ImmutableBuilder builder =new NfaModel.ImmutableBuilder();

        LocationState sX = new LocationState("X");
        LocationState s0 = new LocationState("0");
        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");
        LocationState s3 = new LocationState("3");

        builder.setStartLocation(sX);

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");


        builder.addTransition(sX, sX, num0);
        builder.addTransition(sX, sX, num1);
        builder.addTransition(sX, s0, num1);

        builder.addTransition(s0, s1, num0);
        builder.addTransition(s0, s1, num1);

        builder.addTransition(s1, s2, num0);
        builder.addTransition(s1, s2, num1);

        builder.addTransition(s2, s3, num0);
        builder.addTransition(s2, s3, num1);


        builder.setAccepting(s3);

        return builder.build();
    }


    public DfaModel getDfaModel2() {
        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        LocationState sX = new LocationState("{X}");

        LocationState sX0 = new LocationState("{0,X}") ;

        LocationState sX1 = new LocationState("{1,X}");
        LocationState sX01 = new LocationState("{0,1,X}");


        LocationState sX2 = new LocationState("{2,X}");
        LocationState sX02 = new LocationState("{0,2,X}");
        LocationState sX12 = new LocationState("{1,2,X}");
        LocationState sX012 = new LocationState("{0,1,2,X}");


        LocationState sX3 = new LocationState("{3,X}");
        LocationState sX03 = new LocationState("{0,3,X}");
        LocationState sX13 = new LocationState("{1,3,X}");
        LocationState sX013 = new LocationState("{0,1,3,X}");

        LocationState sX23 = new LocationState("{2,3,X}");
        LocationState sX023 = new LocationState("{0,2,3,X}");
        LocationState sX123 = new LocationState("{1,2,3,X}");
        LocationState sX0123 = new LocationState("{0,1,2,3,X}");

        builder.setStartLocation(sX);

        builder.addTransition(sX, sX, num0);
        builder.addTransition(sX, sX0, num1);

        builder.addTransition(sX0, sX1, num0);
        builder.addTransition(sX0, sX01, num1);

        builder.addTransition(sX1, sX2, num0);
        builder.addTransition(sX1, sX02, num1);

        builder.addTransition(sX01, sX12, num0);
        builder.addTransition(sX01, sX012, num1);


        builder.addTransition(sX2, sX3, num0);
        builder.addTransition(sX2, sX03, num1);

        builder.addTransition(sX02, sX13, num0);
        builder.addTransition(sX02, sX013, num1);

        builder.addTransition(sX12, sX23, num0);
        builder.addTransition(sX12, sX023, num1);

        builder.addTransition(sX012, sX123, num0);
        builder.addTransition(sX012, sX0123, num1);



        builder.addTransition(sX3, sX, num0);
        builder.addTransition(sX3, sX0, num1);

        builder.addTransition(sX03, sX1, num0);
        builder.addTransition(sX03, sX01, num1);

        builder.addTransition(sX13, sX2, num0);
        builder.addTransition(sX13, sX02, num1);

        builder.addTransition(sX013, sX12, num0);
        builder.addTransition(sX013, sX012, num1);

        builder.addTransition(sX23, sX3, num0);
        builder.addTransition(sX23, sX03, num1);

        builder.addTransition(sX023, sX13, num0);
        builder.addTransition(sX023, sX013, num1);

        builder.addTransition(sX123, sX23, num0);
        builder.addTransition(sX123, sX023, num1);

        builder.addTransition(sX0123, sX123, num0);
        builder.addTransition(sX0123, sX0123, num1);


        builder.setAccepting(sX3);
        builder.setAccepting(sX03);
        builder.setAccepting(sX13);
        builder.setAccepting(sX013);

        builder.setAccepting(sX23);
        builder.setAccepting(sX023);
        builder.setAccepting(sX123);
        builder.setAccepting(sX0123);

        return builder.build();
    }


    // same as getDfaModel2 but with different location labels
    // see: https://web.archive.org/web/20171116102440/https://en.wikipedia.org/wiki/Powerset_construction
    public DfaModel getDfaModel2diffloclabels() {
        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        InputAction num0 = new InputAction("0");
        InputAction num1 = new InputAction("1");

        LocationState sX = new LocationState("X");

        LocationState sX0 = new LocationState("X0");

        LocationState sX1 = new LocationState("X1");
        LocationState sX01 = new LocationState("X01");


        LocationState sX2 = new LocationState("X2");
        LocationState sX02 = new LocationState("X02");
        LocationState sX12 = new LocationState("X12");
        LocationState sX012 = new LocationState("X012");


        LocationState sX3 = new LocationState("X3");
        LocationState sX03 = new LocationState("X03");
        LocationState sX13 = new LocationState("X13");
        LocationState sX013 = new LocationState("X013");

        LocationState sX23 = new LocationState("X23");
        LocationState sX023 = new LocationState("X023");
        LocationState sX123 = new LocationState("X123");
        LocationState sX0123 = new LocationState("X0123");

        builder.setStartLocation(sX);

        builder.addTransition(sX, sX, num0);
        builder.addTransition(sX, sX0, num1);

        builder.addTransition(sX0, sX1, num0);
        builder.addTransition(sX0, sX01, num1);

        builder.addTransition(sX1, sX2, num0);
        builder.addTransition(sX1, sX02, num1);

        builder.addTransition(sX01, sX12, num0);
        builder.addTransition(sX01, sX012, num1);


        builder.addTransition(sX2, sX3, num0);
        builder.addTransition(sX2, sX03, num1);

        builder.addTransition(sX02, sX13, num0);
        builder.addTransition(sX02, sX013, num1);

        builder.addTransition(sX12, sX23, num0);
        builder.addTransition(sX12, sX023, num1);

        builder.addTransition(sX012, sX123, num0);
        builder.addTransition(sX012, sX0123, num1);



        builder.addTransition(sX3, sX, num0);
        builder.addTransition(sX3, sX0, num1);

        builder.addTransition(sX03, sX1, num0);
        builder.addTransition(sX03, sX01, num1);

        builder.addTransition(sX13, sX2, num0);
        builder.addTransition(sX13, sX02, num1);

        builder.addTransition(sX013, sX12, num0);
        builder.addTransition(sX013, sX012, num1);

        builder.addTransition(sX23, sX3, num0);
        builder.addTransition(sX23, sX03, num1);

        builder.addTransition(sX023, sX13, num0);
        builder.addTransition(sX023, sX013, num1);

        builder.addTransition(sX123, sX23, num0);
        builder.addTransition(sX123, sX023, num1);

        builder.addTransition(sX0123, sX123, num0);
        builder.addTransition(sX0123, sX0123, num1);


        builder.setAccepting(sX3);
        builder.setAccepting(sX03);
        builder.setAccepting(sX13);
        builder.setAccepting(sX013);

        builder.setAccepting(sX23);
        builder.setAccepting(sX023);
        builder.setAccepting(sX123);
        builder.setAccepting(sX0123);

        return builder.build();
    }

    @Test
    public void testDeterminize1() {
        DfaModel expected=getDfaModel1();
        //Export.dot(expected, "dfaModel1expected.dot");
        DfaModel result=Util.determinize(getNfaModel1());
        //Export.dot(result, "dfaModel1result.dot");

        // they not only need observable equal, but also syntactically equal,
        // if you use dot export (which sorts states and transitions in a specific  but consistent manner)
        assertEquals(Dot.export(result),Dot.export(expected));
    }
    @Test
    public void testDeterminize2() {
        DfaModel expected=getDfaModel2();
        //Export.dot(expected, "dfaModel2expected.dot");
        DfaModel result=Util.determinize(getNfaModel2());
        //Export.dot(result, "dfaModel2result.dot");

        // they not only need observable equal, but also syntactically equal,
        // if you use dot export (which sorts states and transitions in a specific  but consistent manner)
        assertEquals(Dot.export(result),Dot.export(expected));
    }

    @Test
    public void testDeterminize3() {
        DfaModel expected=getDfaModel2diffloclabels();
        //Export.dot(expected, "dfaModel2difflabels_expected.dot");
        DfaModel result=Util.determinize(getNfaModel2());
        //Export.dot(result, "dfaModel2result.dot");

        // they are only need observable equal, but not syntactically equal,
        // so next will fail
        //assertEquals(Dot.export(result),Dot.export(expected));

        assertNull(Util.compare(result,expected));
    }

}
