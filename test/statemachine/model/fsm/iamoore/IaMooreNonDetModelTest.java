package statemachine.model.fsm.iamoore;

import static org.junit.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertLenientEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;
import static org.unitils.reflectionassert.ReflectionComparatorMode.LENIENT_ORDER;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

public class IaMooreNonDetModelTest {

    static private IaMooreNonDetModel nonedetmodel;

    @BeforeClass
    public static void setup() {
        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");


        IaMooreNonDetModel.ImmutableBuilder builder = new IaMooreNonDetModel.ImmutableBuilder();
        builder.setStartLocation(s1);
        builder.addOutput(s1, x);
        builder.addOutput(s2, y);
        builder.addInputTransition(s1, s2, a);
        builder.addInputTransition(s1, s2, b);
        builder.addOutputTransition(s2, s1, x);
        builder.addOutputTransition(s1, s1, y);

        nonedetmodel = builder.build();
    }


    @Test
    public void testGetStateOutput() {
        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");
        assertEquals(nonedetmodel.getOutputAt(s1), new OutputAction("x"));
        assertEquals(nonedetmodel.getOutputAt(s2), new OutputAction("y"));
    }
    @Test
    public void testGetStartLocation() {
        assertEquals(nonedetmodel.getStartLocation(), new LocationState("1"));
    }

    @Test
    public void testGetLocations() {

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");
        assertEquals(nonedetmodel.getLocations(), new HashSet<>(Arrays.asList(s1, s2)));
    }

    @Test
    public void testGetTransitions() {

        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        ImmutableSet<InputTransition> li = ImmutableSet.of(new InputTransition(s1, s2, a), new InputTransition(s1, s2, b));

        ImmutableSet<OutputTransition> lo = ImmutableSet.of(new OutputTransition(s2, s1, x), new OutputTransition(s1, s1, y));

        assertLenientEquals(nonedetmodel.getModelInputTransitions(), li);

        assertLenientEquals(nonedetmodel.getModelOutputTransitions(), lo);
    }

    @Test
    public void testGetTransitionsForLocationState() {
        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        ImmutableSet<InputTransition> li = ImmutableSet.of(new InputTransition(s1, s2, a), new InputTransition(s1, s2, b));

        ImmutableSet<OutputTransition> lo = ImmutableSet.of(new OutputTransition(s2, s1, x));

        assertLenientEquals(nonedetmodel.getModelInputTransitions(s1), li);

        assertLenientEquals(nonedetmodel.getModelOutputTransitions(s2), lo);

    }


    @Test
    public void testGetTransitionsForLocationStateAction() {

        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");

        LocationState s1 = new LocationState("1");
        LocationState s2 = new LocationState("2");

        ImmutableSet<InputTransition> l1i = ImmutableSet.of(new InputTransition(s1, s2, b));

        ImmutableSet<OutputTransition> l2o = ImmutableSet.of(new OutputTransition(s2, s1, x));



        assertReflectionEquals(nonedetmodel.getModelInputTransitions(s1, b), l1i, LENIENT_ORDER);
        assertReflectionEquals(nonedetmodel.getModelOutputTransitions(s2, x), l2o, LENIENT_ORDER);


    }

    @Test
    public void testGetAlphabet() {
        InputAction a = new InputAction("a");
        InputAction b = new InputAction("b");
        OutputAction x = new OutputAction("x");
        OutputAction y = new OutputAction("y");
        assertEquals(nonedetmodel.getInputAlphabet(), new HashSet<InputAction>(Arrays.asList(a, b)));
        assertEquals(nonedetmodel.getOutputAlphabet(), new HashSet<OutputAction>(Arrays.asList(x, y)));
    }
}
