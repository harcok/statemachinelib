package statemachine.model.fsm.mealy.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javatuples.Pair;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeNode;
import nl.ru.cs.tree.TreeUtils;
import nl.ru.cs.tree.format.Dot;
import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.algorithm.DetermineTraverseTree;
import statemachine.model.algorithm.DetermineWCC;
import statemachine.model.algorithm.DetermineDirectedTraverseTree;
import statemachine.model.algorithm.DetermineDistanceMap;
import statemachine.model.algorithm.DetermineTraversalAnnotatedInLts;
import statemachine.model.algorithm.DetermineSCC;
import statemachine.model.algorithm.DetermineSCCrecursive;
import statemachine.model.algorithm.traversal.TraverseStyle;
import statemachine.model.algorithm.DetermineCycle;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.DiGraphTransition;
import statemachine.model.elements.transition.GraphTransition;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.graphs.digraph.DiGraphModel;
import statemachine.model.graphs.digraph.SimpleDiGraphModel;
import statemachine.model.graphs.digraph.conversion.Conversion;
import statemachine.model.graphs.graph.MultiGraphModel;

public class UtilTest {
    static public InputAction WATER = new InputAction("WATER");
    static public InputAction POD = new InputAction("POD");
    static public InputAction BUTTON = new InputAction("BUTTON");
    static public InputAction CLEAN = new InputAction("CLEAN");




    static public MealyDetModel buildCoffeeMachine1() {

        OutputAction out_ok = new OutputAction("ok");
        OutputAction out_error = new OutputAction("error");
        OutputAction out_coffee = new OutputAction("coffee");

        // states are labeled with letters
        LocationState a = new LocationState("a");
        LocationState b = new LocationState("b");
        LocationState c = new LocationState("c");
        LocationState d = new LocationState("d");
        LocationState e = new LocationState("e");
        LocationState f = new LocationState("f");

        MealyDetModel.ImmutableBuilder builder = new MealyDetModel.ImmutableBuilder();
        builder.setStartLocation(a);

        builder.addTransition(a, c, WATER, out_ok);
        builder.addTransition(a, b, POD, out_ok);
        builder.addTransition(a, f, BUTTON, out_error);
        builder.addTransition(a, a, CLEAN, out_ok);

        builder.addTransition(b, d, WATER, out_ok);
        builder.addTransition(b, b, POD, out_ok);
        builder.addTransition(b, f, BUTTON, out_error);
        builder.addTransition(b, a, CLEAN, out_ok);

        builder.addTransition(c, c, WATER, out_ok);
        builder.addTransition(c, d, POD, out_ok);
        builder.addTransition(c, f, BUTTON, out_error);
        builder.addTransition(c, a, CLEAN, out_ok);

        builder.addTransition(d, d, WATER, out_ok);
        builder.addTransition(d, d, POD, out_ok);
        builder.addTransition(d, e, BUTTON, out_coffee);
        builder.addTransition(d, a, CLEAN, out_ok);

        builder.addTransition(e, f, WATER, out_error);
        builder.addTransition(e, f, POD, out_error);
        builder.addTransition(e, f, BUTTON, out_error);
        builder.addTransition(e, a, CLEAN, out_ok);

        builder.addTransition(f, f, WATER, out_error);
        builder.addTransition(f, f, POD, out_error);
        builder.addTransition(f, f, BUTTON, out_error);
        builder.addTransition(f, f, CLEAN, out_error);

        MealyDetModel mealyDet = builder.build();

        return mealyDet;
    }

    static public MealyDetModel buildCoffeeMachine2() {

        OutputAction out_ok = new OutputAction("ok");
        OutputAction out_error = new OutputAction("error");
        OutputAction out_coffee = new OutputAction("coffee");

        // states  are labeled with numbers
        LocationState a = new LocationState("0");
        LocationState b = new LocationState("1");
        LocationState c = new LocationState("2");
        LocationState d = new LocationState("3");
        LocationState e = new LocationState("4");
        LocationState f = new LocationState("5");

        MealyDetModel.ImmutableBuilder builder = new MealyDetModel.ImmutableBuilder();
        builder.setStartLocation(a);

        builder.addTransition(a, c, WATER, out_ok);
        builder.addTransition(a, b, POD, out_ok);
        builder.addTransition(a, f, BUTTON, out_error);
        builder.addTransition(a, a, CLEAN, out_ok);

        builder.addTransition(b, d, WATER, out_ok);
        builder.addTransition(b, b, POD, out_ok);
        builder.addTransition(b, f, BUTTON, out_error);
        builder.addTransition(b, a, CLEAN, out_ok);

        builder.addTransition(c, c, WATER, out_ok);
        builder.addTransition(c, d, POD, out_ok);
        builder.addTransition(c, f, BUTTON, out_error);
        builder.addTransition(c, a, CLEAN, out_ok);

        builder.addTransition(d, d, WATER, out_ok);
        builder.addTransition(d, d, POD, out_ok);
        builder.addTransition(d, e, BUTTON, out_coffee);
        builder.addTransition(d, a, CLEAN, out_ok);

        builder.addTransition(e, f, WATER, out_error);
        builder.addTransition(e, f, POD, out_error);
        builder.addTransition(e, f, BUTTON, out_error);
        builder.addTransition(e, a, CLEAN, out_ok);

        builder.addTransition(f, f, WATER, out_error);
        builder.addTransition(f, f, POD, out_error);
        builder.addTransition(f, f, BUTTON, out_error);
        builder.addTransition(f, f, CLEAN, out_error);

        MealyDetModel mealyDet = builder.build();

        return mealyDet;
    }

    static public MealyDetModel buildCoffeeMachine3() {

        OutputAction out_ok = new OutputAction("ok");
        OutputAction out_error = new OutputAction("error");
        OutputAction out_coffee = new OutputAction("thea"); // different output!!

        LocationState a = new LocationState("0");
        LocationState b = new LocationState("1");
        LocationState c = new LocationState("2");
        LocationState d = new LocationState("3");
        LocationState e = new LocationState("4");
        LocationState f = new LocationState("5");

        MealyDetModel.ImmutableBuilder builder = new MealyDetModel.ImmutableBuilder();
        builder.setStartLocation(a);

        builder.addTransition(a, c, WATER, out_ok);
        builder.addTransition(a, b, POD, out_ok);
        builder.addTransition(a, f, BUTTON, out_error);
        builder.addTransition(a, a, CLEAN, out_ok);

        builder.addTransition(b, d, WATER, out_ok);
        builder.addTransition(b, b, POD, out_ok);
        builder.addTransition(b, f, BUTTON, out_error);
        builder.addTransition(b, a, CLEAN, out_ok);

        builder.addTransition(c, c, WATER, out_ok);
        builder.addTransition(c, d, POD, out_ok);
        builder.addTransition(c, f, BUTTON, out_error);
        builder.addTransition(c, a, CLEAN, out_ok);

        builder.addTransition(d, d, WATER, out_ok);
        builder.addTransition(d, d, POD, out_ok);
        builder.addTransition(d, e, BUTTON, out_coffee);
        builder.addTransition(d, a, CLEAN, out_ok);

        builder.addTransition(e, f, WATER, out_error);
        builder.addTransition(e, f, POD, out_error);
        builder.addTransition(e, f, BUTTON, out_error);
        builder.addTransition(e, a, CLEAN, out_ok);

        builder.addTransition(f, f, WATER, out_error);
        builder.addTransition(f, f, POD, out_error);
        builder.addTransition(f, f, BUTTON, out_error);
        builder.addTransition(f, f, CLEAN, out_error);

        MealyDetModel mealyDet = builder.build();

        return mealyDet;
    }

    static public MealyDetModel buildCoffeeMachine4() {

        OutputAction out_ok = new OutputAction("ok");
        OutputAction out_error = new OutputAction("error");
        OutputAction out_coffee = new OutputAction("coffee");

        // states are labeled with letters
        LocationState a = new LocationState("a");
        LocationState b = new LocationState("b");
        LocationState c = new LocationState("c");
        LocationState d = new LocationState("d");
        LocationState e = new LocationState("e");
        LocationState f = new LocationState("f");

        MealyDetModel.ImmutableBuilder builder = new MealyDetModel.ImmutableBuilder();
        builder.setStartLocation(a);

        builder.addTransition(a, c, WATER, out_ok);
        builder.addTransition(a, b, POD, out_ok);
        builder.addTransition(a, f, BUTTON, out_error);
        builder.addTransition(a, a, CLEAN, out_ok);

        builder.addTransition(b, d, WATER, out_ok);
        builder.addTransition(b, b, POD, out_ok);
        builder.addTransition(b, f, BUTTON, out_error);
        builder.addTransition(b, a, CLEAN, out_ok);

        builder.addTransition(c, c, WATER, out_ok);
        builder.addTransition(c, d, POD, out_ok);
        builder.addTransition(c, f, BUTTON, out_error);
        builder.addTransition(c, a, CLEAN, out_ok);

        builder.addTransition(d, d, WATER, out_ok);
        builder.addTransition(d, d, POD, out_ok);
        builder.addTransition(d, e, BUTTON, out_coffee);
        builder.addTransition(d, a, CLEAN, out_ok);

        builder.addTransition(e, f, WATER, out_error);
        builder.addTransition(e, f, POD, out_error);
        //       builder.addTransition(e, f, BUTTON, out_error);
        builder.addTransition(e, a, CLEAN, out_ok);

        builder.addTransition(f, f, WATER, out_error);
        builder.addTransition(f, f, POD, out_error);
        builder.addTransition(f, f, BUTTON, out_error);
        builder.addTransition(f, f, CLEAN, out_error);

        MealyDetModel mealyDet = builder.build();

        return mealyDet;
    }

    @Test
    public void testSplittingTreeCoffeeMachine() {

        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Util.exportSplittingTree("splitResultCoffeeMachine.dot",splittingTree);

    }

    @Test
    public void testSplittingTreeRick() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rickmachine.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Util.exportSplittingTree("splitResultRickMachine.dot",splittingTree);

    }

    @Test
    public void testSepFamTreeRick() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rickmachine.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

        System.out.println(separatingFamily);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("stateDistinguishingTree.dot", combined);


        Tree<ADSdata> adsTree = Util.getAdaptiveDistinguishingSequence(detlts,splittingTree);
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("ads.dot", adsTree);

    }


    @Test
    public void testSplittingTreeRick2() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rickmachine2.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Util.exportSplittingTree("splitResultRickMachine.dot",splittingTree);

    }

    @Test
    public void testSepFamTreeRick2() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rickmachine2.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

        System.out.println(separatingFamily);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree2.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("stateDistinguishingTree2.dot", combined);


        Tree<ADSdata> adsTree = Util.getAdaptiveDistinguishingSequence(detlts,splittingTree);
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("ads2.dot", adsTree);

    }

    @Test
    public void testSplittingTree_ly_nonedist() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("lee_yannakakis_non_distinguishable_fig5.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Util.exportSplittingTree("splitResult_ly_nondist.dot",splittingTree);

    }

    @Test
    public void testSepFamTree_ly_nondist() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("lee_yannakakis_non_distinguishable_fig5.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

        System.out.println(separatingFamily);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree_ly_nondist.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("stateDistinguishingTree_ly_nondist.dot", combined);


        Tree<ADSdata> adsTree = Util.getAdaptiveDistinguishingSequence(detlts,splittingTree);
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("ads_ly_nondist.dot", adsTree);

    }



    @Test
    public void testSplittingTree_ly_dist() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("lee_yannakakis_distinguishable_fig6.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Util.exportSplittingTree("splitResult_ly_dist.dot",splittingTree);

    }


    @Test
    public void testSepFamTree_ly_dist() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("lee_yannakakis_distinguishable_fig6.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

        System.out.println(separatingFamily);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree_ly_dist.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("stateDistinguishingTree_ly_dist.dot", combined);


        Tree<ADSdata> adsTree = Util.getAdaptiveDistinguishingSequence(detlts,splittingTree);
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("ads_ly_dist.dot", adsTree);

    }

    /*

AG( (<a><x>true) => <a><x>(<a><y>true) )
AG( (<a><x>true) => <b><x>(<a><y>true) )
AG( (<a><y>true) => <a><y>(<a><x>true) )
AG( (<a><y>true) => <b><x>(<a><y>true) )

     */
    @Test
    public void testParseAltTtrace() {
        String input = "<a><x>true";
        String expected = "EX(input=a & EX(output=x))";

        String result = Util.parseAltTrace(input,true);

        assertEquals(expected,result);

    }
    @Test
    public void testParseAltTtrace2() {
        String input = "<a><x><b><y>true";
        String expected = "EX(input=a & EX(output=x & EX(input=b & EX(output=y))))";

        String result = Util.parseAltTrace(input,true);

        assertEquals(expected,result);


    }

    @Test
    public void testParseFormula() {
        String input = "AG( (<a><x>true) => <a><x>(<a><y>true) )";
        String expected = "AG( (EX(input=a & EX(output=x))) -> EX(input=a & EX(output=x & (EX(input=a & EX(output=y))))) )";

        String result = Util.parseFormula(input);

        assertEquals(expected,result);

    }


    @Test
    public void testImportSeparatingFamily() {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rersModel.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(detlts,1);
    }

    @Test
    public void testRers() {


        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rersModel.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

 //       statemachine.model.fsm.mealy.file.Export.nusmv(detlts, "rersModel.nusmv");


//
////        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
////        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);
//
//        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(detlts,1);
//
//        System.out.println("separatingFamily");
//        System.out.println(separatingFamily);
//
//        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(detlts,separatingFamily);
//
//        Map<LocationState, String> state2distformulaPlukked = Util.getState2DistinguishingFormulaPlukked(detlts,separatingFamily);
//
//        System.out.println("distformula s11");
//        System.out.println( state2distformula.get(new LocationState("s8")).toString()) ;
//        System.out.println("\n\n");
//        System.out.println("distformula s11 plukked");
//        System.out.println( state2distformulaPlukked.get(new LocationState("s8")).toString()) ;
//        System.out.println("\n\n");
//
//
//        System.out.println("state2distformula.toString()");
//        System.out.println( state2distformula.toString() ) ;
//
//        System.out.println("state2distformulaPlukked.toString()");
//        System.out.println( state2distformulaPlukked.toString() ) ;

        statemachine.model.fsm.mealy.file.Export.nusmvWithSpecs(detlts, "rersModel.specs.nusmv");

//        ImmutableSet<MealyTransition> transitions = detlts.getModelTransitions();
//        for ( MealyTransition transition : transitions ) {
//            LocationState src = transition.getSource();
//            LocationState dst = transition.getDestination();
//            InputAction input = transition.getInput();
//            OutputAction output = transition.getOutput();
//            if (output.getName().equals("error")) continue;
//            // SPEC  prefix for nusmv
//            String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";
//          //  System.out.println(formula);
//
//            String nusmvFormula = Util.parseFormula(formula);
//            System.out.println("SPEC " + nusmvFormula);
//        }

    }

    @Test
    public void testTrainingSet() {

        List<String> models = Arrays.asList("m217", "m34", "m85");
        //                                     14    115    2221  states



        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("training_set/"+model+".aut");
            MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);
            statemachine.model.fsm.mealy.file.Export.nusmvWithSpecsTrueAndFalse(detlts, "training_set/"+model);
        }
    }

    @Test
    public void testChallengeSet() {

       List<String> models = Arrays.asList("m106","m131","m132","m135","m158","m159","m164","m167","m172","m173","m181","m182","m183","m185","m189","m190","m196","m199","m201","m22","m24","m27","m41","m45","m49","m54","m55","m65","m76","m95");

        // subset
      //  List<String> models = Arrays.asList("m106","m135","m158","m159","m164","m167");

     //   List<String> models = Arrays.asList("m65");
      // List<String> models = Arrays.asList("m76","m95");

        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);
            statemachine.model.fsm.mealy.file.Export.nusmvWithSpecsTrueAndFalse(detlts, "challenge_set/"+model);
        }
    }


    @Test
    public void testChallengeSetFiX() {

       List<String> models = Arrays.asList("m27","m201");

        // subset
      //  List<String> models = Arrays.asList("m106","m135","m158","m159","m164","m167");

     //   List<String> models = Arrays.asList("m65");
      // List<String> models = Arrays.asList("m76","m95");

        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set_fix/"+model+".aut");
            MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);
            statemachine.model.fsm.mealy.file.Export.nusmvWithSpecsTrueAndFalse(detlts, "challenge_set_fix/"+model);
        }
    }



    @Test
    public void testChallengeSetFiXTarjan() {

        List<String> models = Arrays.asList("m27","m201");

        for ( String model:models) {
            System.out.println("");
            System.out.println(model+"\n======");
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");

            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
            for( HashSet<LocationState> scc : sccs) {
                System.out.println("connectedComponent:" +scc);
            }
        }

    }
    @Test
    public void testChallengeSetFiXTarjanSpecificModels() {

       //List<String> models = Arrays.asList("m27","m201");
     //  List<String> models = Arrays.asList("m106","m131","m132","m135","m158","m159","m164","m167","m172","m173","m181","m182","m183","m185","m189","m190","m196","m199","m201","m22","m24","m27","m41","m45","m49","m54","m55","m65","m76","m95");
     //  List<String> models = Arrays.asList("m199"); //27
        List<String> models = Arrays.asList("m41");

       for ( String model : models) {
           System.out.println("");
           System.out.println(model+"\n======");
           MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
//           HashSet<HashSet<LocationState>> scc = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
//           System.out.println(scc);

           HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
           for( HashSet<LocationState> scc : sccs) {
               System.out.println("connectedComponent:" +scc);
            }

           System.out.println("");

           HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
           for( HashSet<LocationState> scc : sccs2) {
              System.out.println("connectedComponent:" +scc);
           }

           System.out.println("equal: " + sccs.equals(sccs2) + " size: " + nondetlts.getLocations().size());

           MealyNonDetModel model2 = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("challenge_set/"+model+".strippederror.dot");

//           LtsNonDetModel result= DetermineDfsTraversalAnnotatedInLts.execute(model2,model2.getStartLocation());
//           statemachine.model.fsm.lts.file.Export.dot(result, "m41.traversed.dot");

           Tree<MealyTransition> tree = DetermineDirectedTraverseTree.execute(model2,model2.getStartLocation(),TraverseStyle.DFS);
           DetermineDirectedTraverseTree.TraverseTreeToDot(tree,"m41.treetraversed.dot");
       }
    }

    @Test
    public void testChallengeSetTarjanEquals() {

        List<String> models = Arrays.asList("m106","m131","m132","m135","m158","m159","m164","m167","m172","m173","m181","m182","m183","m185","m189","m190","m196","m199","m201","m22","m24","m27","m41","m45","m49","m54","m55","m65","m76","m95");

        for ( String model:models) {
            System.out.println("");
            System.out.println(model+"\n======");
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");

            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts, nondetlts.getStartLocation());
//            for( HashSet<LocationState> scc : sccs) {
//                System.out.println("connectedComponent:" +scc);
//            }

//            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts);
            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
//            for( HashSet<LocationState> scc : sccs2) {
//               System.out.println("connectedComponent:" +scc);
//            }

            System.out.println("equal: " + sccs.equals(sccs2) + " size: " + nondetlts.getLocations().size());

        }

    }
// DetermineSCCrecursive2 beter als DetermineSCCrecursive (geen extra nodeonstack)
    @Test
    public void testChallengeSetTarjanSpeed() {

        List<String> models = Arrays.asList("m106","m131","m132","m135","m158","m159","m164","m167","m172","m173","m181","m182","m183","m185","m189","m190","m196","m199","m201","m22","m24","m27","m41","m45","m49","m54","m55","m65","m76","m95");


        long now;

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed recursive: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
        }
        System.out.println("speed iterative: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed recursive: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
           HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
           // HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed iterative: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed recursive: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
            //HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed iterative: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
          //  HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
        }
        System.out.println("speed recursive: " + (System.currentTimeMillis()-now));

        now = System.currentTimeMillis();
        for ( String model:models) {
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
           // HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(nondetlts);
        }
        System.out.println("speed iterative: " + (System.currentTimeMillis()-now));

    }
//System.currentTimeMillis()

    @Test
    public void testChallengeSetTarjan2() {

        List<String> models = Arrays.asList("m106","m131","m132","m135","m158","m159","m164","m167","m172","m173","m181","m182","m183","m185","m189","m190","m196","m199","m201","m22","m24","m27","m41","m45","m49","m54","m55","m65","m76","m95");
        for ( String model : models) {
            System.out.println("");
            System.out.println(model+"\n======");
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/"+model+".aut");
            HashSet<HashSet<LocationState>> scc = DetermineSCC.execute(nondetlts, nondetlts.getStartLocation());
            System.out.println(scc);
        }

    }




    @Test
    public void testTarjan() {
            statemachine.model.graphs.digraph.SimpleDiGraphModel.ImmutableBuilder builder = new SimpleDiGraphModel.ImmutableBuilder();
            LocationState a = new LocationState("a");
            LocationState b = new LocationState("b");
            LocationState c = new LocationState("c");
            LocationState d = new LocationState("d");
            LocationState e = new LocationState("e");
            LocationState f = new LocationState("f");
            LocationState g = new LocationState("g");
            LocationState h = new LocationState("h");
            builder.addTransition(a, b);
            builder.addTransition(b, c);
            builder.addTransition(b, e);
            builder.addTransition(b, f);
            builder.addTransition(c, d);
            builder.addTransition(c, g);
            builder.addTransition(d, c);
            builder.addTransition(d, h);
            builder.addTransition(e, a);
            builder.addTransition(e, f);
            builder.addTransition(f, g);
            builder.addTransition(g, f);
            builder.addTransition(g, h);
      //      builder.addTransition(g, h);
       //     builder.addTransition(h, h);
            SimpleDiGraphModel model = builder.build();


            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(model);
            for( HashSet<LocationState> scc : sccs) {
                System.out.println("connectedComponent:" +scc);
            }

            System.out.println("");

            // MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("training_set/m217.aut");
            MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/m27.aut");
        //    MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("challenge_set/m201.aut");

            HashSet<HashSet<LocationState>> sccs2 = DetermineSCCrecursive.execute(nondetlts);
            for( HashSet<LocationState> scc : sccs2) {
                System.out.println("connectedComponent:" +scc);
            }


    }


    @Test
    public void testTarjanCLRS() {
            // figure 22.9 a)  in CLRS book Introduction to Algorithms
            statemachine.model.graphs.digraph.SimpleDiGraphModel.ImmutableBuilder builder = new SimpleDiGraphModel.ImmutableBuilder();
            LocationState a = new LocationState("a");
            LocationState b = new LocationState("b");
            LocationState c = new LocationState("c");
            LocationState d = new LocationState("d");
            LocationState e = new LocationState("e");
            LocationState f = new LocationState("f");
            LocationState g = new LocationState("g");
            LocationState h = new LocationState("h");

            builder.addTransition(c, d);


            builder.addTransition(a, b);
            builder.addTransition(b, c);
            builder.addTransition(b, e);
            builder.addTransition(b, f);


            builder.addTransition(c, g);
            builder.addTransition(d, c);
            builder.addTransition(d, h);
            builder.addTransition(e, a);
            builder.addTransition(e, f);
            builder.addTransition(f, g);
            builder.addTransition(g, f);
            builder.addTransition(g, h);
      //      builder.addTransition(g, h); // if added : multigraph  : two transitions from g to h!!
      //      builder.addTransition(h, h); // if added : not SimpleDiGraph because has  loop


            SimpleDiGraphModel model = builder.build();

            HashSet<HashSet<LocationState>> sccs = DetermineSCCrecursive.execute(model);
            for( HashSet<LocationState> scc : sccs) {
                System.out.println("connectedComponent:" +scc);
             }
            System.out.println("");

            HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(model);
          //  HashSet<HashSet<LocationState>> sccs2 = DetermineSCC.execute(model, model.getStartLocation());
            for( HashSet<LocationState> scc : sccs2) {
               System.out.println("connectedComponent:" +scc);
            }

            System.out.println("equal: " + sccs.equals(sccs2));



            System.out.println("");
            System.out.println("");


            HashSet<HashSet<LocationState>> sccs3 = DetermineSCCrecursive.execute(model, new LocationState("t"));
            for( HashSet<LocationState> scc : sccs3) {
                System.out.println("connectedComponent:" +scc);
             }
            System.out.println("");

            HashSet<HashSet<LocationState>> sccs4 = DetermineSCC.execute(model,new LocationState("t"));
            for( HashSet<LocationState> scc : sccs4) {
               System.out.println("connectedComponent:" +scc);
            }

            System.out.println("equal: " + sccs3.equals(sccs4));
    }


    @Test
    public void testCreateTraverseTree() {
            SimpleDiGraphModel model = getSimpleDigraph(); // has no loops, but has cycles
            statemachine.model.graphs.digraph.file.Export.dot(model, "simpledigraph.dot");

            LtsNonDetModel result1= DetermineTraversalAnnotatedInLts.execute(model,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(result1, "simpledigraph_ltsgraph.dot");

            String filepath="treetraversed.dot";
            Tree<EdgeNode<DiGraphTransition,LocationState>> tree= DetermineTraverseTree.execute(model,new LocationState("t"),TraverseStyle.DFS);
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(filepath, tree,e->e.getLabel(),n->n.toString());

            List<Tree<EdgeNode<DiGraphTransition,LocationState>>> trees= DetermineTraverseTree.executeAll(model,new LocationState("t"),TraverseStyle.DFS);
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("treetraversed0.dot", trees.get(0),e->e.getLabel(),n->n.toString());
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("treetraversed1.dot", trees.get(1),e->e.getLabel(),n->n.toString());


            MultiGraphModel graph = Conversion.digraphAsGraph(model);
            statemachine.model.graphs.graph.file.Export.dot(graph, "simplegraph.dot");
            LtsNonDetModel result2= DetermineTraversalAnnotatedInLts.execute(graph,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(result2, "simplegraph_ltsgraph.dot");

            List<Tree<EdgeNode<GraphTransition,LocationState>>> graphtrees= DetermineTraverseTree.executeAll(graph,new LocationState("t"),TraverseStyle.DFS);

            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("graphtreetraversed0.dot", graphtrees.get(0),e->e.getLabel(),n->n.toString());
 //           nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("graphtreetraversed1.dot", graphtrees.get(1),e->e.getLabel(),n->n.toString());

    }

    @Test
    public void testCreateTraverseTree2() {

            MealyNonDetModel model = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");

            Tree<MealyTransition> tree = DetermineDirectedTraverseTree.execute(model,model.getStartLocation(),TraverseStyle.DFS);
            DetermineDirectedTraverseTree.TraverseTreeToDot(tree,"treetraversedCoffeeMachine1.dot");

    }

    @Test
    public void testCreateTraverseTree3() {

            MealyNonDetModel model = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");

            Tree<EdgeNode<MealyTransition, LocationState>> tree = DetermineTraverseTree.execute(model,model.getStartLocation(),TraverseStyle.DFS);
           // DetermineDfsLocationTransitionTree.dfsTransitionTreeToDot(tree,"treetraversed2.dot");
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("treetraversedCoffeeMachine2.dot", tree,e->e.getLabel(),n->n.toString());
    }

    @Test
    public void testCycle1() {

            MealyNonDetModel model = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");

            List<Pair<LocationState, MealyTransition>> cycle = DetermineCycle.execute(model,model.getStartLocation());
            System.out.println(cycle);
    }

    @Test
    public void testCycle2() {

            SimpleDiGraphModel model = getSimpleDigraph(); // has no loops, but has cycles
            boolean hasCycle = DetermineCycle.check(model,new LocationState("t"));
            System.out.println(hasCycle);
            List<Pair<LocationState, DiGraphTransition>> cycle = DetermineCycle.execute(model,new LocationState("t"));
            System.out.println(cycle);
    }

    @Test
    public void testCycle3() {

            //SimpleDiGraphModel digraph = getSimpleDigraph(); // has no loops, but has cycles
            DiGraphModel digraph = getDigraph(); // has loops and cycles (no multiedges though)

            LtsNonDetModel diresult_v= DetermineTraversalAnnotatedInLts.execute(digraph,new LocationState("v"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_v, "simpledigraph_ltsgraph_v.dot");

            LtsNonDetModel diresult_z= DetermineTraversalAnnotatedInLts.execute(digraph,new LocationState("z"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_z, "simpledigraph_ltsgraph_z.dot");

            LtsNonDetModel diresult_t= DetermineTraversalAnnotatedInLts.execute(digraph,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_t, "simpledigraph_ltsgraph_t.dot");


            LtsNonDetModel diresult_v_bfs= DetermineTraversalAnnotatedInLts.executeBFS(digraph,new LocationState("v"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_v_bfs, "simpledigraph_ltsgraph_v_bfs.dot");

            LtsNonDetModel diresult_z_bfs= DetermineTraversalAnnotatedInLts.executeBFS(digraph,new LocationState("z"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_z_bfs, "simpledigraph_ltsgraph_z_bfs.dot");

            LtsNonDetModel diresult_t_bfs= DetermineTraversalAnnotatedInLts.executeBFS(digraph,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(diresult_t_bfs, "simpledigraph_ltsgraph_t_bfs.dot");

            HashMap<LocationState, Integer> distanceMap = DetermineDistanceMap.execute(digraph, new LocationState("t"));
            System.out.println("distanceMap:"+distanceMap);

            HashSet<HashSet<LocationState>> wcc = DetermineWCC.execute(digraph);
            System.out.println("wcc automata:"+wcc);


            Tree<EdgeNode<DiGraphTransition,LocationState>> tree_bfs= DetermineTraverseTree.execute(digraph,new LocationState("t"),TraverseStyle.BFS);
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("treetraversed_t_bfs.dot", tree_bfs,e->e.getLabel(),n->n.toString());

            Tree<EdgeNode<DiGraphTransition,LocationState>> tree_dfs= DetermineTraverseTree.execute(digraph,new LocationState("t"),TraverseStyle.DFS);
            nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("treetraversed_t_dfs.dot", tree_dfs,e->e.getLabel(),n->n.toString());




            MultiGraphModel graph = Conversion.digraphAsGraph(digraph);
            statemachine.model.graphs.graph.file.Export.dot(graph, "simplegraph.dot");

            HashSet<HashSet<LocationState>> wcc2 = DetermineWCC.execute(graph);
            System.out.println("wcc graph:"+wcc2);


            boolean hasCycle = DetermineCycle.check(graph,new LocationState("t"));
            System.out.println(hasCycle);
            List<Pair<LocationState, GraphTransition>> cycle_v = DetermineCycle.execute(graph,new LocationState("v"));
            System.out.println(cycle_v);

            List<Pair<LocationState, GraphTransition>> cycle_t = DetermineCycle.execute(graph,new LocationState("t"));
            System.out.println(cycle_t);

            List<Pair<LocationState, GraphTransition>> cycle_z = DetermineCycle.execute(graph,new LocationState("z"));
            System.out.println(cycle_z);

            LtsNonDetModel result_v= DetermineTraversalAnnotatedInLts.execute(graph,new LocationState("v"),true);
            statemachine.model.fsm.lts.file.Export.dot(result_v, "simplegraph_ltsgraph_v.dot");

            LtsNonDetModel result_z= DetermineTraversalAnnotatedInLts.execute(graph,new LocationState("z"),true);
            statemachine.model.fsm.lts.file.Export.dot(result_z, "simplegraph_ltsgraph_z.dot");

            LtsNonDetModel result_t= DetermineTraversalAnnotatedInLts.execute(graph,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(result_t, "simplegraph_ltsgraph_t.dot");




    }

    public SimpleDiGraphModel getSimpleDigraph() {
            statemachine.model.graphs.digraph.SimpleDiGraphModel.ImmutableBuilder builder = new SimpleDiGraphModel.ImmutableBuilder();
            LocationState y = new LocationState("y");
            LocationState z = new LocationState("z");
            LocationState s = new LocationState("s");
            LocationState t = new LocationState("t");
            LocationState x = new LocationState("x");
            LocationState w = new LocationState("w");
            LocationState v = new LocationState("v");
            LocationState u = new LocationState("u");
            builder.addTransition(y, x);
            builder.addTransition(z, y);
            builder.addTransition(z, w);
            builder.addTransition(s, z);
            builder.addTransition(s, w);
            builder.addTransition(t, v);
            builder.addTransition(t, u);
            builder.addTransition(x, z);
            builder.addTransition(w, x);
            builder.addTransition(v, w);
            builder.addTransition(v, s);
            builder.addTransition(u, v);
            builder.addTransition(u, t);

            SimpleDiGraphModel model = builder.build();
            return model;
    }

    public DiGraphModel getDigraph() {
        statemachine.model.graphs.digraph.DiGraphModel.ImmutableBuilder builder = new DiGraphModel.ImmutableBuilder();
        LocationState y = new LocationState("y");
        LocationState z = new LocationState("z");
        LocationState s = new LocationState("s");
        LocationState t = new LocationState("t");
        LocationState x = new LocationState("x");
        LocationState w = new LocationState("w");
        LocationState v = new LocationState("v");
        LocationState u = new LocationState("u");
        builder.addTransition(y, x);
        builder.addTransition(z, y);
        builder.addTransition(z, w);
        builder.addTransition(s, z);
        builder.addTransition(s, w);
        builder.addTransition(t, v);
        builder.addTransition(t, u);
        builder.addTransition(x, z);
        builder.addTransition(w, x);
        builder.addTransition(v, w);
        builder.addTransition(v, s);
        builder.addTransition(u, v);
        builder.addTransition(u, t);

        // add self loop in v
        builder.addTransition(z, z);
    //    builder.addTransition(t, t);

        DiGraphModel model = builder.build();
        return model;
}

    @Test
    public void testCreateTraverseLTS() {
            SimpleDiGraphModel model = getSimpleDigraph(); // has no loops, but has cycles
            statemachine.model.graphs.digraph.file.Export.dot(model, "simpledigraph.dot");

            LtsNonDetModel result= DetermineTraversalAnnotatedInLts.execute(model,new LocationState("t"),true);
            statemachine.model.fsm.lts.file.Export.dot(result, "ltsgraph.dot");

    }


    @Test
    public void testRersMUTATED() {

        System.out.println("import model");

      //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("rersModel.dot");   //m217.dot

     MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("training_set/m217.aut");  //14 states
   //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("training_set/m34.aut");  //115 states


   //   MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel("training_set/m85.aut");  //2221 states




      //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("m217.dot");  //14 states
      //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("m34.dot");  //115 states
      //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("m85.dot");  //2221 states


        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        System.out.println("generate nusmv model");
        statemachine.model.fsm.mealy.file.Export.nusmvWithSpecsTrueAndFalse(detlts, "rersModel");
    }

    @Test
    public void testSepFamTree() {
        /*
           $ for f in m217 m34 m85 ; do printf "$f: "; stm info mealy $f.dot; done
           m217: states:14          inputs:159        outputs:13          transitions:2226
           m34: states:115         inputs:72         outputs:17          transitions:8280
           m85: states:2221        inputs:121        outputs:38          transitions:268741


         */
        Util.exportSepFamilyTree("m34.dot");
        Util.exportSepFamilyTree("m85.dot");
        Util.exportSepFamilyTree("m217.dot");
    }


    @Test
    public void testSepFamTree_simplemealy() {

      //  MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("simplemealy.dot");
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(detlts);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

        System.out.println(separatingFamily);

         Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(detlts,separatingFamily);
        System.out.println( state2distformula.toString() ) ;

        ImmutableSet<MealyTransition> transitions = detlts.getModelTransitions();
        for ( MealyTransition transition : transitions ) {
            LocationState src = transition.getSource();
            LocationState dst = transition.getDestination();
            InputAction input = transition.getInput();
            OutputAction output = transition.getOutput();
            // SPEC  prefix for nusmv
            String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";
            System.out.println(formula);

            String nusmvFormula = Util.parseFormula(formula);
            System.out.println("SPEC " + nusmvFormula);
        }

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree_coffeemachine.dot", sdt);

          // sdt  => formula
          String formula = Util.sdt2ctlFormula(sdt);
          System.out.println("formula: " +formula);

          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("stateDistinguishingTree_coffeemachine.dot", combined);


        Tree<ADSdata> adsTree = Util.getAdaptiveDistinguishingSequence(detlts,splittingTree);
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("ads_coffeemachine.dot", adsTree);

    }


    //lee_yannakakis_non_distinguishable.dot

    @Test
    public void testAccessSequences() {

        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");
        MealyDetModel detlts  = MealyDetModel.fromMealyModel(nondetlts);

        Map<LocationState, List<InputAction>> result = Util.getAccessSequences(detlts);
        System.out.println(result);

        Map<LocationState, List<InputAction>> expected = new HashMap<>();
        expected.put(new LocationState("s0"), Arrays.asList() );
        expected.put(new LocationState("s1"), Arrays.asList(BUTTON) );
        expected.put(new LocationState("s2"), Arrays.asList(POD) );
        expected.put(new LocationState("s3"), Arrays.asList(WATER,POD) );
        expected.put(new LocationState("s4"), Arrays.asList(WATER) );
        expected.put(new LocationState("s5"), Arrays.asList(WATER,POD,BUTTON) );

        assertEquals(expected, result);
    }

    @Test
    public void testSpanningTree() {


        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel("coffeemachine.dot");
        MealyDetModel detlts  = MealyDetModel.fromMealyModel(nondetlts);

        Tree<EdgeNode<InputAction,LocationState>> result_tree = Util.getSpanningTreeMealy(detlts);

        System.out.println(Util.getAccessSequences(detlts).toString());


        // expected spanning tree
        List<String> expected_lines =  Arrays.asList( "digraph g {",
                "    __start [label=\"\" style=\"invis\" width=\"0\" height=\"0\" ];" ,
                "    __start ->  s0;" ,
                "",
                "    s0 [label=\"s0\"];" ,
                "    s1 [label=\"s1\"];" ,
                "    s2 [label=\"s2\"];" ,
                "    s3 [label=\"s3\"];" ,
                "    s4 [label=\"s4\"];" ,
                "    s5 [label=\"s5\"];" ,
                "    s0 -> s1 [label=\"BUTTON\"];" ,
                "    s0 -> s2 [label=\"POD\"];" ,
                "    s0 -> s4 [label=\"WATER\"];" ,
                "    s3 -> s5 [label=\"BUTTON\"];" ,
                "    s4 -> s3 [label=\"POD\"];" ,
                "}");

        Tree<EdgeNode<InputAction,LocationState>> expected_tree = Dot.dot2edgeNodeTree(expected_lines, v->new InputAction(v), v->new LocationState(v));

        //nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("spanExpected.dot", result_tree);

        // use compareTreeDeterministic because order of childs may vary, but if you sort them on label then
        // trees should be exactly same.
        assertTrue(TreeUtils.compareTreeDeterministic(expected_tree, result_tree));
    }

    @Test
    public void testCompare1() {

        MealyDetModel machine1 = buildCoffeeMachine1();
        MealyDetModel machine2 = buildCoffeeMachine3();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT:  ?WATER/!ok ?POD/!ok ?BUTTON/!coffee,!thea";
        assertEquals(expected, diff.toString());
    }

    @Test
    public void testCompare2() {

        MealyDetModel machine1 = buildCoffeeMachine1();
        MealyDetModel machine2 = buildCoffeeMachine2();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        assertNull(diff);

    }

    @Test
    public void testCompare3() {

        MealyDetModel machine1 = buildCoffeeMachine1();
        MealyDetModel machine2 = buildCoffeeMachine4();

        TraceDiff diff = Util.compare(machine1, machine2, machine1.getInputAlphabet());
        String expected = "DIFFERENT:  ?WATER/!ok ?POD/!ok ?BUTTON/!coffee ?BUTTON/!error,!QUIESCENCE";
        assertEquals(expected, diff.toString());

    }

}
