package statemachine.interfaces.system.actionmarkers;


/**
 *  Concept: hidden internal event within machine
 *  
 * @author harcok
 *
 */
public interface SystemHiddenAction {

}
