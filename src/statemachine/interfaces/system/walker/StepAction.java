package statemachine.interfaces.system.walker;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface StepAction<S extends SystemState,A extends SystemAction> extends CurrentState<S> {
	
	public boolean checkActionPossible(A action);
	public void stepAction(A action);
}
