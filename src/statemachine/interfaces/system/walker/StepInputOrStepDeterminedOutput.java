package statemachine.interfaces.system.walker;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;


@NonNullByDefault
public interface StepInputOrStepDeterminedOutput<S extends SystemState,I extends SystemInput, O extends SystemOutput> extends CurrentState<S>{

	 public boolean checkInputPossible(I input);
	 public boolean checkOutputPossible();
	
	 public void stepInput(I input);  
	 public O stepOutput();
}
