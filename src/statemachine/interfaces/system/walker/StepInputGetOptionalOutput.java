package statemachine.interfaces.system.walker;



import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;


/*  check method: checks step possible (input possible/guard true)
 *   => to check input possible before really executing if model not input enabled
 *   
 *  step methods : always does step, and throws exception if step not possible!   
 *                 
 *    note: for step methods we always assume at max one output possible; multiple outputs over multiples steps ( with delta input !) 
 *          
 *    note: exception semantically important, eg. if a failed step would not throw an exception
 *          how would we for optional-output-mealy machines distinguish a none output from a failed step!!
 *                                                        `-> fix would be return both boolean and output , 
 *                                                            but that makes interface less clear
 *
 *    note: implementation optimalization: cache found transition in check,  it can be reused by step! ( eg. for efsm:  per step cache: input2trans  )      
 *                                                                           `-> don't have to find out which transition has enabled guard for input again!!
 *
 * note: walker knows model so we demand that it also can return current State : extends StateInfo
 */
@NonNullByDefault
public interface StepInputGetOptionalOutput<S extends SystemState,I extends SystemInput, O extends SystemOutput> extends CurrentState<S> {
	
	 public boolean checkInputPossible( I input);
	 
	 public Optional<O> stepInput( I input);
}
