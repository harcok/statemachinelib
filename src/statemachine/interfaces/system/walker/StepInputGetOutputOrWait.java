package statemachine.interfaces.system.walker;



import java.util.List;



import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface StepInputGetOutputOrWait<S extends SystemState,I extends SystemInput, O extends SystemOutput> extends CurrentState<S>  {

     public boolean checkInputPossible( I input);

     public  O stepInput( I input);

     public  List<Pair<O,Float>> wait(double time);

}