package statemachine.interfaces.system.walker;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface CurrentState<S extends SystemState> {
     S getState();
     void setState(S state);
}
