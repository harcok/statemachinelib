package statemachine.interfaces.system.walker;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface ResetState<S extends SystemState> {
    void resetState();
}
