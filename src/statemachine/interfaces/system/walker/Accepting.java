package statemachine.interfaces.system.walker;

public interface Accepting {

    boolean isAccepting();
}
