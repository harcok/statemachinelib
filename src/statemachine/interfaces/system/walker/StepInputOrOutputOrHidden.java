package statemachine.interfaces.system.walker;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemHiddenAction;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;


@NonNullByDefault
public interface StepInputOrOutputOrHidden<S extends SystemState,I extends SystemInput, O extends SystemOutput, IA extends SystemHiddenAction> extends CurrentState<S> {

	 public boolean checkInputPossible(I input);
	 public boolean checkOutputPossible(O output);
	 public boolean checkHiddenPossible(IA hiddenAction);
	
	 public void stepInput(I input);  
	 public void stepOutput(O output);
	 public void stepHidden(IA hiddenAction);
}


