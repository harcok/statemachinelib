package statemachine.interfaces.system.execution;


import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;


/**
 * 
 *    
 * @author harcok
 *
 */
@NonNullByDefault
public interface SendInputOrProbeOptionalOutput {

	 /**
	  *  
	  * @param input
	  * @return boolean valid, because some  system can respond with "illegal" input not allowed ( or by some wrapper eg. a learner purpose )
	  */
	 public boolean sendInput( SystemInput input);
	 
	 public Optional<SystemOutput> probeOutput();
	
}
