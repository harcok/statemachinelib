package statemachine.interfaces.system.execution;

public interface StartInterrupt {

    public void start();
    public void interrupt();
}
