package statemachine.interfaces.system.execution;



import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;


/**
 *  illegal input 
 *    a system can respond that an input is not allowed, however for this interface this "notAllowed" output is
 *    an output as any other output.
 *    
 * @author harcok
 *
 */
@NonNullByDefault
public interface SendInputGetOutput {
	public  SystemOutput sendInput( SystemInput input);
}
