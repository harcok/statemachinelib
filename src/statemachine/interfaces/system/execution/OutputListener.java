package statemachine.interfaces.system.execution;


import statemachine.interfaces.system.actionmarkers.SystemOutput;

public interface OutputListener<O extends SystemOutput>  {
    public void onOutput(O outputAction);
}

