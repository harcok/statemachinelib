package statemachine.interfaces.system.execution;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;

public interface SendInputGetOutputListenOutput<I extends SystemInput, O extends SystemOutput>  {

    public  O sendInput( I input);
    void setOnOutputListener(OutputListener<O> onOutputListener);
}
