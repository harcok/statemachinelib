package statemachine.interfaces.system.execution;

import java.util.List;

import org.javatuples.Pair;

public interface SendTimedInputWordGetTimedOutputWord<I,O,T> {

    List<Pair<O,T>> sendTimedInputWord(List<Pair<I,T>> timedInputWord);

}
