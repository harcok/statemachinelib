package statemachine.interfaces.system.execution;

public interface Reset {
      void reset();
}
