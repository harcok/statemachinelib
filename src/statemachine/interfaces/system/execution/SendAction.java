package statemachine.interfaces.system.execution;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;

@NonNullByDefault
public interface SendAction {
	 /**
	  *  
	  * @param action
	  * @return boolean valid, because some  system can respond with "illegal" action not allowed ( or by some wrapper eg. a learner purpose )
	  */
	 public boolean sendAction( SystemAction action);
}
