package statemachine.interfaces.system.execution;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;

public interface SendInputListenOutput<I extends SystemInput, O extends SystemOutput>  {

    void sendInput(I input);
    void setOnOutputListener(OutputListener<O> onOutputListener);
}
