package statemachine.interfaces.system.execution;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;

@NonNullByDefault
public interface SendInputOrProbeOutput {

	 /**
	  *  
	  * @param input
	  * @return boolean valid, because some  system can respond with "illegal" input not allowed ( or by some wrapper eg. a learner purpose )
	  */
	 public boolean sendInput( SystemInput input);
	 
	 public  SystemOutput probeOutput();
}
