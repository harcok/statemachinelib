package statemachine.interfaces.system.transitionmarkers;


/**
 *  Concept action transition : change a system makes from one state to another state triggered by an action
 *  We call this action a system action because in case of EFSM models it is an real world action with data values
 *  given as input to the system. Note the distinction with an abstract model action.
 *  <p>
 *  Used to mark transitions in model which are abstract representation of the system's state change
 *
 * @author harcok
 *
 */
public interface SystemActionTriggeredTransition {

}
