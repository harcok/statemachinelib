package statemachine.interfaces.system.transitionmarkers;


/**
 *  Concept: transition a system makes from one state to another state triggered by an input from environment.
 *  We call this input a system input because in case of EFSM models it is an real world input with data values
 *  given as input to the system. Note the distinction with an abstract model input.
 *  <p>
 *  In a model we only have model transitions, there is no real system transition entity. However Model transitions
 *  which are caused by outside-world input triggers on the system are marked as SystemInputTransition.
 *  <p>
 *  Note that in a mealy model the transition has both an input and an output, but the transition is triggered
 *  by an input so a mealy transitions is a SystemInputTransition.
 */
public interface SystemInputTriggeredTransition {

}
