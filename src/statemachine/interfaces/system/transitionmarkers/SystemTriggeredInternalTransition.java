package statemachine.interfaces.system.transitionmarkers;


/**
 *  Concept: transition a system makes from one state to another state triggered by an internal system event
 *  <p>
 *  Note that an internal mealy transition in the model is triggered by an internal action, but outputs an observable
 *  output action. Eg. an internal timer times out which triggers an internal transition with an output.
 */
public interface SystemTriggeredInternalTransition {

}
