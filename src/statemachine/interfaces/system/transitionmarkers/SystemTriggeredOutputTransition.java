package statemachine.interfaces.system.transitionmarkers;


/**
 *  Concept: transition a system makes from one state to another state where it does send output to environment.
 *           The transition is not triggered by any external event, but initiated by the system itself.
 * @author harcok
 *
 */
public interface SystemTriggeredOutputTransition {

}
