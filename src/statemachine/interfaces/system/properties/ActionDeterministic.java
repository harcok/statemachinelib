package statemachine.interfaces.system.properties;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition;

/**
 *   ActionDeterministic: system property which specifies that the system for each state has at most one outgoing transition for a specific observable action. 
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface ActionDeterministic <S extends SystemState, A extends SystemAction, T extends SystemActionTriggeredTransition> {
	@DeterministicFunction 
	Optional<T> getTriggeredTransition(S state,A action);
}
