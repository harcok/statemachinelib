package statemachine.interfaces.system.properties;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;


/**
 *   InputDeterministic: system property which specifies that the system for each state has at most one outgoing input transition for an input action.
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface InputDeterministic<S extends SystemState, I extends SystemInput, T extends SystemInputTriggeredTransition> {
	@DeterministicFunction
	Optional<T> getTriggeredTransition(S state, I input);
}
