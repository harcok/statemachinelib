package statemachine.interfaces.system.properties;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.statemarkers.SystemState;

public interface InitialState<L extends ModelLocation, S extends SystemState> {
    // model has a single or preferred initial state
    // - to get initial state (location + initial values state variables)
    S getStartState();
    // - to get only initial location
    L getStartLocation();
}
