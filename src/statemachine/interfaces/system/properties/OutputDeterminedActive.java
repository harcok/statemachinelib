package statemachine.interfaces.system.properties;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;


/**
 *   OutputDeterminedActive: system property which specifies that the system does not have a choice in the output it can do (OutputDetermined)
 *                           but the system has always the possibility to do an output (Active).
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface OutputDeterminedActive<S extends SystemState, T extends SystemTriggeredOutputTransition>  {
	 @DeterministicFunction
	 T getSystemTriggeredOutputTransition(S state);
}