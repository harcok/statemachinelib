package statemachine.interfaces.system.properties;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;

/**
 *   InputEnabled: system property which specifies that the system allows any input from the environment.
 * <p>  
 *   The @NonEmpty property is enforced when constructing the model with the builder. 
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface InputEnabled<S extends SystemState, I extends SystemInput, T extends SystemInputTriggeredTransition> {
	 @DeterministicFunction
	 @NonEmpty Set<T> getTriggeredTransitions(S state,I input);
}

