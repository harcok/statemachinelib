package statemachine.interfaces.system.properties;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;

/**
 *   InputEnabledDeterministic: system property which specifies that the system for each state has at exactly one
 *   outgoing input transition for each input action.
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 *   <p>
 *   This property can only be defined on models and not on black box systems because to define this property
 *   we must return a model transition which describes the transition the system takes for given system state and  input.
 *   A system transition is describe abstractly by a model transition which is concretized by given system state and  input.
 */
@NonNullByDefault
public interface InputEnabledDeterministic<S extends SystemState, I extends SystemInput, T extends SystemInputTriggeredTransition>  {
	@DeterministicFunction
	T getTriggeredTransition(S state, I input);
}
