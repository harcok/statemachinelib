
package statemachine.interfaces.system.properties;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition;


/**
 *   ActionEnabledDeterministic: system property which specifies that the system for each state has exact one outgoing transition for each observable action. 
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface ActionEnabledDeterministic <S extends SystemState, A extends SystemAction, T extends SystemActionTriggeredTransition> {
	 T getTriggeredTransition(S state, A action);
}
