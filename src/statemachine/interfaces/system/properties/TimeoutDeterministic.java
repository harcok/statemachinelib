package statemachine.interfaces.system.properties;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;

/**
 *   TimeoutDeterministic: system property which specifies that the system for each state has at most one outgoing transition for a specific timer timeout.
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface TimeoutDeterministic <S extends SystemState, TIV, T extends SystemInputTriggeredTransition> {
    @DeterministicFunction
    Optional<T> getSystemTimeoutTransition(S state,TIV timerVariable);
}
