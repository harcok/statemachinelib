package statemachine.interfaces.system.properties;


import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition;

/**
 *   StateDeterministic: system property which specifies that for every possible trace of observable actions, the state of the system after the trace 
 *   is the same for every run of the trace. 
 * <p>
 *   So StateDeterministic is a contract which specifies that for an observable action the system 
 *   will deterministically go to another state, so for each specific observable action 
 *   only one specific change of state is possible. 
 * <p>
 *   State deterministic also requires that no hidden internal transitions can happen, therefore 
 *   this interface extends the NoHiddenBehavior marker interface so that implementations
 *   are notified to disallow internal hidden transitions.
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
public interface StateDeterministicLts<S extends SystemState, A extends SystemAction> 
{
	 @DeterministicFunction
	 S getSystemsNextState(S state, A action);
}

