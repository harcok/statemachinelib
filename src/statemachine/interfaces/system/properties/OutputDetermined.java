package statemachine.interfaces.system.properties;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;

/*
 *  in interface automata a state can be output determined;
 *    meaning it has only maximal one outgoing transition which has an output!
 *    In such interface automata we can apply this on states, because
 *    from state we can only see one output.
 *
 *
 *   A moore/mealy machine transition's are caused only by input trigger, and each state/transition has one output
 *   so by definition a moore/mealy machine is a reactive system which when an input happens it is output determined.
 *   Because for a moore/mealy machine it is implicitly by definition output determined we don't need this interface for it.
 */

/**
 *   OutputDetermined: system property which specifies that the system does not have a choice in the output it can do.
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface OutputDetermined<S extends SystemState, T extends SystemTriggeredOutputTransition> {
	@DeterministicFunction
	Optional<T> getSystemTriggeredOutputTransition(S state);
}


