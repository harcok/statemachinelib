package statemachine.interfaces.system.properties;

/** Attribute to specify that a method in an object is implemented as a deterministic function without side effects.
 */
public @interface DeterministicFunction {

}
