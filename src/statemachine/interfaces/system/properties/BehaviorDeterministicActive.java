package statemachine.interfaces.system.properties;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;

/**  BehaviorDeterministic and OutputDeterminedActive:
 */
public interface BehaviorDeterministicActive <S extends SystemState, I extends SystemInput, O extends SystemOutput, OT extends SystemTriggeredOutputTransition>
extends StateDeterministic<S,I,O>,
        OutputDeterminedActive<S,OT>
{

}
