package statemachine.interfaces.system.properties;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition;

/**
 *   ActionEnabled: system property which specifies that the system allows any observable action a.
 * <p>  
 *   The @NonEmpty property is enforced when constructing the model with the builder. 
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface ActionEnabled <S extends SystemState, A extends SystemAction, T extends SystemActionTriggeredTransition> {
	@DeterministicFunction 
	@NonEmpty Set<T> getTriggeredTransitions(S state, A action);
}
