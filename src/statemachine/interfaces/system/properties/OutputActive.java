package statemachine.interfaces.system.properties;



import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;

/**
 *   OutputActive: system property which specifies that the system has always the possibility to do an output.
 * <p>  
 *   The @NonEmpty property is enforced when constructing the model with the builder. 
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface OutputActive<S extends SystemState, T extends SystemTriggeredOutputTransition> {
	 @DeterministicFunction
	 @NonEmpty Set<T> getSystemTriggeredOutputTransitions(S state);
}


