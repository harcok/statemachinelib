package statemachine.interfaces.system.properties;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;

/** BehaviorDeterministic: system property which specifies that for every stream of input values that is provided by the
 *  environment, the stream of output values that is computed by the system is the same for every run.
 *  Note that inputs and outputs do not have to alternate in the observed trace.
 *
 * <p>
 * This means that a system which is state deterministic and output determined is behavior deterministic. <br>
 * However a behavior deterministic system doesn't has to be state deterministic.
 *
 * Although there are behavior deterministic systems which aren't state deterministic we take in the statemachinelib 
 * a practical definition of behavior deterministic  where we only regard the subset of
 * behavior deterministic systems which are StateDeterministic and OutputDetermined.
 *
 */
public interface BehaviorDeterministic <S extends SystemState, I extends SystemInput, O extends SystemOutput, OT extends SystemTriggeredOutputTransition>
       extends StateDeterministic<S,I,O>,
               OutputDetermined<S,OT>
{

}
