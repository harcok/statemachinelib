package statemachine.interfaces.system.properties;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;

/**
 *   OutputDeterministic: system property which specifies that the system for each state has at most one outgoing output transition for an output action.
  * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 */
@NonNullByDefault
public interface OutputDeterministic<S extends SystemState, O extends SystemOutput, T extends SystemTriggeredOutputTransition> {
	@DeterministicFunction
	Optional<T> getSystemTransition(S state, O output);
}