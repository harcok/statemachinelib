package statemachine.interfaces.system.properties;

/** Attribute to specify that a collection is not empty.
 */
public @interface NonEmpty {

}
