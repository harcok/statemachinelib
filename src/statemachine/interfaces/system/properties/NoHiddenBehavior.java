package statemachine.interfaces.system.properties;


/**
 *   NoHiddenBehavior: system property which specifies that the system doesn't do internal transitions which are hidden from the observer.
 * <p>
 *   NoHiddenBehavior is a Marker interface which is used by model builders to enforce that no hidden internal action 
 *   and transitions are added to a model.
 */
public interface NoHiddenBehavior {

}
