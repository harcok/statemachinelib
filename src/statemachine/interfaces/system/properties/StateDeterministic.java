package statemachine.interfaces.system.properties;

import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;

/**
 *   StateDeterministic : system property which specifies that for every possible trace of observable input and output actions,
 *   the state of the system after the trace is the same for every run of the trace.
 * <p>
 *   StateDeterministic is a contract which specifies that for an input or output the system
 *   will deterministically go to another state, so for each specific input or output action
 *   only one specific change of state is possible.
 * <p>
 *   State deterministic also requires that no hidden internal transitions can happen, therefore
 *   this interface extends the NoHiddenBehavior marker interface so that implementations
 *   are notified to disallow internal hidden transitions.
 * <p>
 *   Notice that a state deterministic system doesn't has to be behavior deterministic.
 *   A behavior deterministic system expects the same outputs for every run of a set of inputs.
 *   However a state deterministic system  allows many different possible outputs in a state,
 *   so causing possible different outputs per run. Although every output state change is deterministic,
 *   the system can still be none-deterministic in the selection of output.
 * <p>
 *   Each method in this interface is a deterministic function without side effects
 *   which is indicated by the @DeterministicFunction annotation.
 *
 */
public interface StateDeterministic<S extends SystemState, I extends SystemInput, O extends SystemOutput>
       extends  NoHiddenBehavior
{
	 /**
	  *  For each state of the system given an input there is only one next state it can go to.
	  * @param state
	  * @param input
	  * @return
	  */
	 @DeterministicFunction
	 S getSystemsNextState(S state, I input);

	 /**
	  *  For each state of the system given an output there is only one next state it can go to.
	  * @param state
	  * @param input
	  * @return
	  */
	 @DeterministicFunction
	 S getSystemsNextState(S state, O output);
}

