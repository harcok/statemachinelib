package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelMealyTransition;

@NonNullByDefault
public interface ModelMealyFiniteAcceptor <MIT extends ModelInputType,
										MOT extends ModelOutputType,
										L extends ModelLocation,
			                            T extends ModelMealyTransition<?,?,L>> 
                extends ModelMealy<MIT,MOT,L,T> {

	boolean isAccepting(L location);
}