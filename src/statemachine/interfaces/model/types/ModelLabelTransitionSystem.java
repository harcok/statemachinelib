	package statemachine.interfaces.model.types;


import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelAction;
import statemachine.interfaces.model.actiontypemarkers.ModelActionType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelActionTransition;

@NonNullByDefault
public interface ModelLabelTransitionSystem<MAT extends ModelActionType,
                          L extends ModelLocation,
                          T extends ModelActionTransition<?,L>>  
                 extends ModelAutomaton<L,T> {
	
	// model actions
	public ImmutableSet<MAT> getAlphabet();
	//public ImmutableSet<MIA> getHiddenAlphabet();
	
	// model structure 	
	ImmutableSet<T> getModelTransitions(L location, MAT actionType); 

}
