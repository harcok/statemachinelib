package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;

@NonNullByDefault
public interface ModelFiniteAcceptor <MIT extends ModelInputType,
							L extends ModelLocation,
							T extends ModelInputTransition<?,L>>  
		extends ModelAutomaton<L,T>{

	// model actions
	public ImmutableSet<MIT> getInputAlphabet();
	
	// model structure 
	ImmutableSet<T> getModelTransitions(L location, MIT actionType);
	boolean isAccepting(L location);
}
