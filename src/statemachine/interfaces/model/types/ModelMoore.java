package statemachine.interfaces.model.types;


import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;


//  moore is type of model not a system property

@NonNullByDefault
public interface ModelMoore<	MIT extends ModelInputType,
							MOT extends ModelOutputType,
							MO extends ModelOutput,
							L extends ModelLocation,
							T extends ModelInputTransition<?,L>>  
				extends ModelAutomaton<L,T>{
	
	// model actions
	public ImmutableSet<MIT> getInputAlphabet();
	public ImmutableSet<MOT> getOutputAlphabet();
	
	// model structure 
	ImmutableSet<T> getModelTransitions(L location, MIT action);
	MO getOutputAt(L location);
			
}


