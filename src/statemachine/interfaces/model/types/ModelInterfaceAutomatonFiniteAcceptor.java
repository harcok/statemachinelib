package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelHiddenAction;
import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelHiddenActionType;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.model.transitions.ModelHiddenTransition;
import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.interfaces.model.transitions.ModelTransition;





@NonNullByDefault
public interface ModelInterfaceAutomatonFiniteAcceptor <MIT extends ModelInputType,
										MOT extends ModelOutputType,
										MHT extends ModelHiddenActionType,
										L extends ModelLocation,
										T extends ModelTransition<L>,
										IT extends ModelInputTransition<?, L>,
										OT extends ModelOutputTransition<?, L>,
										HT extends ModelHiddenTransition<?, L>>  
       extends ModelInterfaceAutomaton<MIT,MOT,MHT,L,T,IT,OT,HT> 
{
	boolean isAccepting(L location);
}
