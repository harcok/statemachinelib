package statemachine.interfaces.model.types;

import org.javatuples.Pair;
import org.eclipse.jdt.annotation.NonNullByDefault;


import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelMealyTransition;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface ModelExtendedMealy <SI extends SystemInput,
									SO extends SystemOutput,
									S extends SystemState,
									MI extends ModelInput,
									MO extends ModelOutput,
									MIT extends ModelInputType,
									MOT extends ModelOutputType,
									L extends ModelLocation,
									T extends ModelMealyTransition<MI,MO,L>>
                 extends ModelMealy<MIT, MOT, L, T> {

	// model's operational semantics
	// - guard evaluation  : condition for transition
	boolean isEnabledTransition( S state , SI input ,  T transition );
	// - update and output evaluation : effect of transition
	//   Note: allows you to evaluate a transition even when it is not enabled
	Pair<S,SO> evaluateTransition( S state , SI input ,  T transition );

}
