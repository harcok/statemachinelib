package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;



@NonNullByDefault
public interface ModelExtendedMoore <SI extends SystemInput,
									SO extends SystemOutput,
									S extends SystemState,
									MIT extends ModelInputType,
									MOT extends ModelOutputType,
									MO extends ModelOutput,
									L extends ModelLocation,
									T extends ModelInputTransition<?,L>>
                 extends ModelMoore<MIT,MOT,MO, L, T> {
	
	// model's operational semantics 
	// - guard evaluation  : condition for transition
    boolean isEnabledTransition( S state , SI input ,  T transition );
	// - update evaluation : effect of transition
    SO evaluateTransition( S state , SI input ,  T transition );
    // - output evaluation in state : output in state
    SO getOutputAt(S state);
}
