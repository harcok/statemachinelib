package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelMealyTransition;


@NonNullByDefault
public interface ModelMealy	<MIT extends ModelInputType,
                             MOT extends ModelOutputType,
                             L extends ModelLocation,
                             T extends ModelMealyTransition<?,?,L>>  
        extends ModelAutomaton<L,T>{

	// model actions
	public ImmutableSet<MIT> getInputAlphabet();
	public ImmutableSet<MOT> getOutputAlphabet();
	
	// model structure 
	ImmutableSet<T> getModelTransitions(L location, MIT actionType);

}


