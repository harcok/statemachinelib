package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;

@NonNullByDefault
public interface ModelAutomaton<L extends ModelLocation,T extends ModelTransition<L>> {
	//model locations 
	ImmutableSet<L> getLocations();
	
	//model base structure 
	ImmutableSet<T> getModelTransitions();
	ImmutableSet<T> getModelTransitions(L location);
}
