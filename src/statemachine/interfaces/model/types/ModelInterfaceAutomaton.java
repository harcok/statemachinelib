package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelHiddenAction;
import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelHiddenActionType;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.model.transitions.ModelHiddenTransition;
import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.interfaces.model.transitions.ModelTransition;

@NonNullByDefault
public interface ModelInterfaceAutomaton <MIT extends ModelInputType,
                                         MOT extends ModelOutputType,
                                         MHT extends ModelHiddenActionType,
                                         L extends ModelLocation,
                                         T extends ModelTransition<L>,
                                         IT extends ModelInputTransition<?, L>,
                                         OT extends ModelOutputTransition<?, L>,
                                         HT extends ModelHiddenTransition<?, L>>
                 extends ModelAutomaton<L,T>{

	// model actions
	public ImmutableSet<MIT> getInputAlphabet();
	public ImmutableSet<MOT> getOutputAlphabet();
	public ImmutableSet<MHT> getHiddenAlphabet();

	// model structure
	public ImmutableSet<IT> getModelInputTransitions();
	public ImmutableSet<OT> getModelOutputTransitions();
	public ImmutableSet<HT> getModelHiddenTransitions();
	public ImmutableSet<IT> getModelInputTransitions(L location);
	public ImmutableSet<OT> getModelOutputTransitions(L location);
	public ImmutableSet<HT> getModelHiddenTransitions(L location);
	public ImmutableSet<IT> getModelInputTransitions(L location,MIT inputType);
	public ImmutableSet<OT> getModelOutputTransitions(L location,MOT outputType);
	public ImmutableSet<HT> getModelHiddenTransitions(L location,MHT hiddenType);


}
