package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface ModelExtendedFiniteAcceptor <SI extends SystemInput,
									S extends SystemState,
									MIT extends ModelInputType,
									L extends ModelLocation,
									T extends ModelInputTransition<?,L>>
                 extends ModelFiniteAcceptor<MIT, L, T> {
	
	// model's operational semantics 
	// - guard evaluation  : condition for transition
	boolean isEnabledTransition( S state , SI input ,  T transition );
	// - update and output evaluation : effect of transition
    S evaluateTransition( S state , SI input ,  T transition );

    // boolean isAccepting(S State);  // just calls isAccepting(Location)  
    //   `=> don't allow; only one way principle!! 
    //       user must call isAccepting(state.getLocation()) which 
    //       clearly communicates the accepting property purely depends on location and independent of statevars!! 
}