package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.model.actiontypemarkers.ModelHiddenActionType;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelHiddenTransition;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.system.actionmarkers.SystemHiddenAction;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface ModelExtendedInterfaceAutomaton <SI extends SystemInput,
                                         SO extends SystemOutput,
                                         SH extends SystemHiddenAction,
                                         S extends SystemState,
                                         MIT extends ModelInputType,
                                         MOT extends ModelOutputType,
                                         MHT extends ModelHiddenActionType,
                                         L extends ModelLocation,
                                         T extends ModelTransition<L>,
                                         IT extends ModelInputTransition<?, L>,
                                         OT extends ModelOutputTransition<?, L>,
                                         HT extends ModelHiddenTransition<?, L>>

                 extends ModelInterfaceAutomaton<MIT,MOT,MHT,L,T,IT,OT,HT>{

	// model's operational semantics
	// - guard evaluation  : condition for transition
    boolean isEnabledInputTransition( S state , SI input ,  IT inputTransition );
    boolean isEnabledOutputTransition( S state ,  OT outputTransition );
    boolean isEnabledHiddenTransition( S state , SH hiddenAction, HT hiddenTransition );
	// - update and output evaluation : effect of transition
    S evaluateInputTransition( S state , SI input ,  IT inputTransition );
    Pair<S,SO> evaluateOutputTransition( S state ,  OT outputTransition );
    S evaluateHiddenTransition( S state , SH hiddenAction ,  HT hiddenTransition );

}
