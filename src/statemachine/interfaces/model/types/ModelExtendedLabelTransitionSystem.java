package statemachine.interfaces.model.types;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.actionmarkers.ModelAction;
import statemachine.interfaces.model.actiontypemarkers.ModelActionType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelActionTransition;
import statemachine.interfaces.system.actionmarkers.SystemAction;
import statemachine.interfaces.system.statemarkers.SystemState;

@NonNullByDefault
public interface  ModelExtendedLabelTransitionSystem <A extends SystemAction,
									S extends SystemState,
									MA extends ModelAction,
									MAT extends ModelActionType,
									L extends ModelLocation,
									T extends ModelActionTransition<MA,L>>  
				  extends ModelLabelTransitionSystem<MAT,L,T> {
	
	// model's operational semantics 
	// - guard evaluation  : condition for transition
	boolean isEnabledTransition( S state ,  A action ,  T transition );
	// - update and output evaluation : effect of transition
	S evaluateTransition( S state , A action ,  T transition );	
}


