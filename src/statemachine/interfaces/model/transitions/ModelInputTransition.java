package statemachine.interfaces.model.transitions;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;

@NonNullByDefault
public interface ModelInputTransition<I extends ModelInput,L extends ModelLocation>  extends ModelDirectedTransition<L>, SystemInputTriggeredTransition {
	I getInput();
}

