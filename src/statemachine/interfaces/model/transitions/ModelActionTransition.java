package statemachine.interfaces.model.transitions;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelAction;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition;

@NonNullByDefault
public interface ModelActionTransition<A extends ModelAction,L extends ModelLocation> extends ModelDirectedTransition<L>, SystemActionTriggeredTransition  {
	public A getAction() ;
}
