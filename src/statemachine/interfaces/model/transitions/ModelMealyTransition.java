package statemachine.interfaces.model.transitions;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition;


@NonNullByDefault
public interface ModelMealyTransition<I extends ModelInput ,O extends ModelOutput,L extends ModelLocation> extends ModelDirectedTransition<L>, SystemInputTriggeredTransition {
	I getInput();
    O getOutput();
}
