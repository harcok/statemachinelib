package statemachine.interfaces.model.transitions;



import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;

@NonNullByDefault
public interface ModelTransition <L extends ModelLocation>{

    Pair<L,L> getLocations();
    String getLabel();
}
