package statemachine.interfaces.model.transitions;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelHiddenAction;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredInternalTransition;

@NonNullByDefault
public interface ModelInternalMealyTransition<IA extends ModelHiddenAction, O extends ModelOutput, L extends ModelLocation>  extends ModelDirectedTransition<L>, SystemTriggeredInternalTransition {
    IA getHiddenAction();
    O getOutput();
}

