package statemachine.interfaces.model.transitions;



import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.locationmarkers.ModelLocation;

@NonNullByDefault
public interface ModelUndirectedTransition <L extends ModelLocation> extends ModelTransition<L> {


}