package statemachine.interfaces.model.transitions;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.locationmarkers.ModelLocation;

@NonNullByDefault
public interface ModelOutputTransition<O extends ModelOutput,L extends ModelLocation>  extends ModelDirectedTransition<L> {
    O getOutput();
}
