
package statemachine.interfaces.model.transitions;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelHiddenAction;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredInternalTransition;


@NonNullByDefault
public interface ModelHiddenTransition<IA extends ModelHiddenAction, L extends ModelLocation>  extends ModelDirectedTransition<L>,SystemTriggeredInternalTransition {
	IA getHiddenAction();
}