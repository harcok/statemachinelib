package statemachine.model.time.mealy;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.Timer;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;

@NonNullByDefault
public abstract class MealyTimerBaseModel implements MealyTimerModel {

    protected final ImmutableSet<InputAction> inputalphabet;
    protected final ImmutableSet<OutputAction> outputalphabet;
    protected final ImmutableSet<TimerVariable> timeralphabet;
    private final Location startLocation;
    private final TimersLocationState startState;
    private final ImmutableSet<Location> locations;
    private final ImmutableSet<MealyTimerTransition> transitions;
    protected final ImmutableSetMultimap<Location, MealyTimerTransition> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder {

        protected ImmutableSet<InputAction> inputalphabet;
        protected ImmutableSet<OutputAction> outputalphabet;
        protected ImmutableSet<TimerVariable> timeralphabet;
        protected Location startLocation;
        protected TimersLocationState startState;
        protected ImmutableSet<Location> locations;
        protected ImmutableSet<MealyTimerTransition> transitions;
        protected ImmutableSetMultimap<Location, MealyTimerTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<Location> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<MealyTimerTransition> transitionsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<InputAction> inputalphabetBuilder = ImmutableSet.builder();
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<OutputAction> outputalphabetBuilder = ImmutableSet.builder();
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<TimerVariable> timeralphabetBuilder = ImmutableSet.builder();

        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<Location, MealyTimerTransition> loc2transBuilder = ImmutableSetMultimap.builder();

        public ImmutableBuilder addTransition(Location src, Location dst, InputAction input, OutputAction output,String update) {
            this.locationsBuilder.add(src, dst);
            this.inputalphabetBuilder.add(input);
            this.outputalphabetBuilder.add(output);
            MealyTimerTransition transition = new MealyTimerTransition(src, dst, input, output,update);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addTransition(Location src, Location dst, InputAction input, OutputAction output) {
            this.addTransition(src,dst,input,output,null);
            return this;
        }


        /** adds timeout transition for a timer with a timer update
         *
         * @param src
         * @param dst
         * @param timer
         * @param output
         * @param update
         * @return
         */
        public ImmutableBuilder addTransition(Location src, Location dst,TimerVariable timer, OutputAction output,String update) {
            this.locationsBuilder.add(src, dst);
            this.timeralphabetBuilder.add(timer);
            this.outputalphabetBuilder.add(output);
            MealyTimerTransition transition = new MealyTimerTransition(src, dst, timer, output,update);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }

        /**  adds timeout transition for a timer
         *
         * @param src
         * @param dst
         * @param timer
         * @param output
         * @return
         */
        public ImmutableBuilder addTransition(Location src, Location dst, TimerVariable timer, OutputAction output) {
            this.addTransition(src,dst,timer,output,null);
            return this;
        }

        public ImmutableBuilder setStartLocation(Location startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        public ImmutableBuilder addFromMealyTimerModel(MealyTimerModel model) {
            for (MealyTimerTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination(), trans.getInput(), trans.getOutput());
            }
            this.setStartLocation(model.getStartLocation());
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            this.inputalphabet = inputalphabetBuilder.build();
            this.outputalphabet = outputalphabetBuilder.build();
            this.timeralphabet = timeralphabetBuilder.build();
            //this.startLocation = ...; // no builder for startlocation
            this.startState= new TimersLocationState(this.startLocation, new HashSet<Timer>()); // initially no timers!
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }
    }

    // Constructor

    protected MealyTimerBaseModel(ImmutableBuilder builder) {
        this.inputalphabet = builder.inputalphabet;
        this.outputalphabet = builder.outputalphabet;
        this.timeralphabet = builder.timeralphabet;
        this.startLocation = builder.startLocation;
        this.startState = builder.startState;
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }

    // interface InitialState

    @Override
    public @NonNull Location getStartLocation() {
        return startLocation;
    }

    @Override
    public @NonNull TimersLocationState getStartState() {
        return startState;
    }

    // interface ModelAutomaton

    @Override
    public ImmutableSet<Location> getLocations() {
        return locations;
    }

    @Override
    public ImmutableSet<MealyTimerTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public ImmutableSet<MealyTimerTransition> getModelTransitions(Location location) {
        return loc2trans.get(location);
    }

    // interface ModelMealy

    @Override
    public ImmutableSet<InputAction> getInputAlphabet() {
        return inputalphabet;
    }

    @Override
    public ImmutableSet<OutputAction> getOutputAlphabet() {
        return outputalphabet;
    }


    public ImmutableSet<TimerVariable> getTimerAlphabet() {
        return timeralphabet;
    }

    /** get mealy transitions for an input **/
    @Override
    public @NonNull ImmutableSet<@NonNull MealyTimerTransition> getModelTransitions(Location location, InputAction action) {
        ImmutableSet.Builder<MealyTimerTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull MealyTimerTransition> transitions = loc2trans.get(location);

        for (MealyTimerTransition transition : transitions) {
            if (transition.getInput().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }


    /** get model transitions for a timeout **/
    public ImmutableSet<MealyTimerTransition> getModelTimeoutTransitions(Location location) {
        ImmutableSet.Builder<MealyTimerTransition> transitionsBuilder = ImmutableSet.builder();

        ImmutableSet<MealyTimerTransition> transitions = loc2trans.get(location);

        for (MealyTimerTransition transition : transitions) {
            if (transition.isTimeout() ) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }

    /** get model transitions for a timeout of a timer variable **/
    public ImmutableSet<MealyTimerTransition> getModelTimeoutTransitions(Location location, TimerVariable timer) {
        ImmutableSet.Builder<MealyTimerTransition> transitionsBuilder = ImmutableSet.builder();

        ImmutableSet<MealyTimerTransition> timeout_transitions = getModelTimeoutTransitions(location);

        for (MealyTimerTransition transition : timeout_transitions) {
            if ( transition.getTimedOutTimer().get().equals(timer) ) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }


    // operational semantics
    //----------------------
    //  - transition within system
    //  - idle time passing (decrementing timers)

    /** evaluate a transition
          evaluate the timer update and return new timers and location State together with Output of transition evaluated
          => works for both  timeout transition as normal input transition
     **/
    @Override
    public Pair<TimersLocationState,OutputAction> evaluateTransition( TimersLocationState state, MealyTimerTransition  transition ){

        Location src=state.getLocation();
        if ( src != transition.getSource() ) {
            // throw exception
            new RuntimeException("error: given transition for other location then location given in current state");
        }

        HashSet<Timer> timers=new HashSet<>(state.getTimers());

        // in case of timer update  do update timers set
        if (transition.hasUpdate()) {

            String updateExpression= transition.getUpdate().get();
            //Timer update=new Timer(updateExpression);

            String[] parts=updateExpression.trim().split("=");
            TimerVariable timerVar=new TimerVariable(parts[0]);
            Long timeoutTime=Long.valueOf(parts[1]);

            Timer update=new Timer(timerVar, timeoutTime);

            // remove old timer setting if present
            Optional<Timer> oldtimer=state.getTimer(update.getName());
            if ( oldtimer.isPresent() ) {
              timers.remove(oldtimer.get());
            }

            // add new timer setting
            timers.add(update);

        }

        Location dest=transition.getDestination();
        TimersLocationState newState = new TimersLocationState(dest,timers);

        OutputAction output=transition.getOutput();
        return new Pair<>(newState,output) ;
     }

    /**  Model evaluates the maximal time the system can be idle. It returns maximal idle time and the modified state
     *   after being that time idle.
     *
     *   System can do requested idle time if a timeout transition does not become possible in that period of time.
     *   If a timeout transition becomes possible then the maximal idle time is the timeout time for that transition's  timer.
     *
     * @param state
     * @param requestedIdleTime
     * @return
     */
    @Override
    public Pair<TimersLocationState,Long> evaluateIdle( TimersLocationState state, Long  requestedIdleTime ){
        long doneIdleTimeDone,remainingIdleTime,timeoutTime;


        doneIdleTimeDone=0;
        remainingIdleTime=requestedIdleTime;
        while ( remainingIdleTime != 0 ) {
            timeoutTime = state.getTimeUntilTimeout();
            if ( timeoutTime == -1  || timeoutTime > remainingIdleTime)  {
                // update state with remaining idle time => only decrements counters in state , location stays the same   (discards timers which timeoutTime becomes <0)
                state=state.getNewStateAfterIdleTime(remainingIdleTime);

                // update idleTime done and remaining
                doneIdleTimeDone=requestedIdleTime;
                remainingIdleTime=0; // done idle
            } else {

                // update state with idle time  => only decrements counters in state , location stays the same   (discards timers which timeoutTime becomes <0)
                state=state.getNewStateAfterIdleTime(timeoutTime);

                // update idleTime done and remaining
                doneIdleTimeDone=doneIdleTimeDone+timeoutTime;
                remainingIdleTime=remainingIdleTime-timeoutTime; // >0

                // find possible timeout transition for timer timeout
                // get timers who timeout
                Set<TimerVariable> timerVars = state.getTimedOutTimers();
                // find possible timeout transition
                search: for (TimerVariable timerVar : timerVars) {
                    // get timeout transition for timerVar
                    ImmutableSet<MealyTimerTransition> timeoutTransitions = this.getModelTimeoutTransitions(state.getLocation(),timerVar);
                    if (!timeoutTransitions.isEmpty()) {
                        // we reached maximal idle time
                        remainingIdleTime=0; // done idle
                        break search;
                    }
                }
                if ( remainingIdleTime != 0 ) {
                    // no timeout transition found -> all timeouts missed
                    // remove timedoutTimers
                    state=state.removeTimedOutTimers();
                }
            }
        }
        return new Pair<>(state,doneIdleTimeDone);
    }





    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("start location: " + startLocation);
        joiner.add("start state: " + startState);
        joiner.add("inputAlphabet: " + inputalphabet);
        joiner.add("outputAlphabet: " + outputalphabet);
        joiner.add("timerAlphabet: " + timeralphabet);
        joiner.add("locations:");
        for (Location location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (MealyTimerTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }

}
