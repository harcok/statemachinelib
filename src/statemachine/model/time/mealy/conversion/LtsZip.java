package statemachine.model.time.mealy.conversion;



import java.util.Optional;

import org.javatuples.Quartet;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.location.Timer;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.MealyTimerTransition;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.time.mealy.MealyTimerModel;

public class LtsZip {

    // note: we use getName() to that it doesn't display ? or !
    //       because these are printed by the toString method of input and output)
    static public TransitionLabelFormatter defaultTransitionLabelFormatter =
            ( input,timeoutvar, output, update ) ->  {
                if ( !update.equals("") ) {
                    update=" " + update;
                }
                if ( ! timeoutvar.equals("") ) {   //  "timeout"
                    input="timeout[" + timeoutvar +"]";
                }
                return input + "/" + output +  update;
            };

    static public TransitionLabelParser defaultTransitionLabelParser = new TransitionLabelParser() {
        public Quartet<String,String,String,String> apply(String label) {
            String input="",timedOutTimerVariable="",output,update="";

            String[] parts = label.split("/");
            input =  parts[0].trim();
            output = parts[1].trim();
            if (input.contains("timeout[") ) {
                timedOutTimerVariable=input.substring(8).replaceAll("\\]", "");
                input="timeout";
            }
            if ( output.contains("=") ) {
                parts=output.split("\\s+");
                output = parts[0].trim();
                update = parts[1].trim();
            }
            return new Quartet<>(input,timedOutTimerVariable, output, update);
        }
    };

    static public LtsNonDetModel zipMealyTimer2Lts(MealyTimerModel model, TransitionLabelFormatter transitionFormatter) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

        for (MealyTimerTransition trans : model.getModelTransitions()) {
            String timedOutTimerVariable= trans.getTimedOutTimer().isPresent() ? trans.getTimedOutTimer().get().getName() : "";
            String update = trans.getUpdate().isPresent() ? trans.getUpdate().get().toString() : "";
            String label = transitionFormatter.apply(trans.getInput().getName(),timedOutTimerVariable,trans.getOutput().getName(),update);
            Action action = new Action(label);
            builder.addTransition( new LocationState(trans.getSource().getName()) , new LocationState(trans.getDestination().getName()), action);
        }
        builder.setStartLocation( new LocationState(model.getStartLocation().getName()) );
        return builder.build();
    }

    static public LtsNonDetModel zipMealyTimer2Lts(MealyTimerModel model) {
        return zipMealyTimer2Lts(model, defaultTransitionLabelFormatter);
    }

    static public MealyTimerNonDetModel unzipLts2MealyTimer(LtsModel model, TransitionLabelParser labelParser) {
        MealyTimerNonDetModel.ImmutableBuilder builder = new MealyTimerNonDetModel.ImmutableBuilder();

        for (ActionTransition trans : model.getModelTransitions()) {
            String label = trans.getAction().getName();
            Quartet<String,String,String,String>  parsed = labelParser.apply(label);
            String input = parsed.getValue0();
            String timedOutTimerVariable = parsed.getValue1();
            String output = parsed.getValue2();
            String update = parsed.getValue3();

            OutputAction outputAction=new OutputAction(output);
            if ( !timedOutTimerVariable.equals("") ) {
                TimerVariable timerVariable =new TimerVariable(timedOutTimerVariable);
                if (update.equals("")) {
                    builder.addTransition(new Location(trans.getSource().getName()) , new Location(trans.getDestination().getName()), timerVariable, outputAction);
                } else {
                    builder.addTransition(new Location(trans.getSource().getName()) , new Location(trans.getDestination().getName()), timerVariable, outputAction,update);
                }
            } else {
                InputAction inputAction=new InputAction(input);
                if (update.equals("")) {
                    builder.addTransition(new Location(trans.getSource().getName()) , new Location(trans.getDestination().getName()), inputAction, outputAction);
                } else {
                    builder.addTransition(new Location(trans.getSource().getName()) , new Location(trans.getDestination().getName()), inputAction, outputAction,update);
                }
            }


        }
        builder.setStartLocation(new Location(model.getStartLocation().getName()));
        return builder.build();
    }

    static public MealyTimerNonDetModel unzipLts2MealyTimer(LtsModel model) {
        return unzipLts2MealyTimer(model, defaultTransitionLabelParser);
    }

}

