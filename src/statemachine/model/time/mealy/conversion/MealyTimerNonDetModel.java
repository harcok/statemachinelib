package statemachine.model.time.mealy.conversion;

import statemachine.model.time.mealy.MealyTimerBaseModel;
import statemachine.model.time.mealy.MealyTimerModel;


public class MealyTimerNonDetModel extends MealyTimerBaseModel{


    //Builder class
    public static final class ImmutableBuilder extends MealyTimerBaseModel.ImmutableBuilder {

        public MealyTimerNonDetModel build(){
            super.baseBuild();
            return new MealyTimerNonDetModel(this);
        }
    }

    // Constructor
    private MealyTimerNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from MealyModel
    static public MealyTimerNonDetModel fromMealyTimerModel(MealyTimerModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromMealyTimerModel(model);
        return builder.build();
    }
}