package statemachine.model.time.mealy.conversion;




/** TransitionLabelFormatter
 *
 *   Defines how to format input and output of a mealy timer machine to a string label.
 *   This label can be used to zip an MealyTimer model to a LTS model.
 *   We use this to export a MealyTimer model to one of the the lts file formats. (eg. graphviz dot file)
 *
*/
@FunctionalInterface
public interface TransitionLabelFormatter {
    public String apply (String input,String timeoutVar, String output, String update);
}
