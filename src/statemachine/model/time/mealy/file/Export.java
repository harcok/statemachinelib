package statemachine.model.time.mealy.file;

import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.time.mealy.MealyTimerModel;
import statemachine.model.time.mealy.conversion.LtsZip;

public class Export {

    static public void dot(MealyTimerModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealyTimer2Lts(model);
        statemachine.model.fsm.lts.file.Export.dot(lts, filename);
    }

    static public void gml(MealyTimerModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealyTimer2Lts(model);
        statemachine.model.fsm.lts.file.Export.gml(lts, filename);
    }

    static public void aldebaran(MealyTimerModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealyTimer2Lts(model);
        statemachine.model.fsm.lts.file.Export.aldebaran(lts, filename);
    }

}
