package statemachine.model.time.mealy.file;



import statemachine.model.time.mealy.conversion.LtsZip;
import statemachine.model.time.mealy.conversion.MealyTimerNonDetModel;
import statemachine.model.fsm.lts.LtsNonDetModel;


public class Import {

    static public MealyTimerNonDetModel  dot2MealyTimerModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.dot2LtsModel( filename);
           return LtsZip.unzipLts2MealyTimer(lts);
    }

//  enduser must do this themselves => keep library light!  => document this instead!!
// ->  general approach:  <moreSpecificModel> model=<moreSpecificModel>.fromMealyModel(<basenondetmodel);
//
//    static public MealyDetModel importMealyDetModel(String filename) {
//        return MealyDetModel.fromMealyModel(dot2MealyModel(filename));
//    }

    static public MealyTimerNonDetModel  aldebaran2MealyTimerModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel( filename);
           return LtsZip.unzipLts2MealyTimer(lts);
    }

    static public MealyTimerNonDetModel  gml2MealyTimerModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel( filename);
           return LtsZip.unzipLts2MealyTimer(lts);
    }

}
