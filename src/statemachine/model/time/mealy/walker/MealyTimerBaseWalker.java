package statemachine.model.time.mealy.walker;


import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.CurrentState;
import statemachine.interfaces.system.walker.ResetState;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.time.mealy.MealyTimerModel;

@NonNullByDefault
public class MealyTimerBaseWalker<M extends MealyTimerModel> implements CurrentState<TimersLocationState>, ResetState<TimersLocationState> {

    protected TimersLocationState currentState;
    protected M model;

    // Constructor
    public MealyTimerBaseWalker(M model) {
        this.model = model;
        this.currentState = model.getStartState();
    }

    // getters

    public Location getLocation() {
        return currentState.getLocation();
    }

    public @NonNull M getModel() {
        return model;
    }

    // interface CurrentState

    @Override
    public TimersLocationState getState() {
        return currentState;
    }

    @Override
    public void setState(TimersLocationState state) {
        this.currentState = state;
    }

    // interface ResetState

    @Override
    public void resetState() {
        this.currentState = model.getStartState();
    }
}
