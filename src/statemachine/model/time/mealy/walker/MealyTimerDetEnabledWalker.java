package statemachine.model.time.mealy.walker;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.walker.StepInputGetOutput;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Timer;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;
import statemachine.model.time.mealy.MealyTimerDetEnabledModel;


@NonNullByDefault
public class MealyTimerDetEnabledWalker extends MealyTimerBaseWalker<MealyTimerDetEnabledModel> implements StepInputGetOutput<TimersLocationState,InputAction,OutputAction> {

    public static List<Pair<OutputAction,Long>> walkTimedInputWord(MealyTimerDetEnabledModel model, List<Pair<InputAction,Long>> timedInputWord ) {
        ArrayList<Pair<OutputAction,Long>> timedOutputWord= new ArrayList<>();

        MealyTimerDetEnabledWalker walker=new MealyTimerDetEnabledWalker(model);

        long timePassed=0;
        for ( Pair<InputAction,Long> inputTimePair : timedInputWord ) {
            long inputTime=inputTimePair.getValue1();
            InputAction inputAction=inputTimePair.getValue0();

            // wait till input => catch timeouts which can happen
            List<Pair<OutputAction,Long>> timedOutputs=walker.waitDelay(inputTime-timePassed);
            for (Pair<OutputAction,Long> outputTimePair: timedOutputs )  {
                OutputAction output=outputTimePair.getValue0();
                Long time=outputTimePair.getValue1();
                timedOutputWord.add(new Pair<>(output,timePassed+time));
            }
            timePassed=inputTime;

            OutputAction outputAction=walker.stepInput(inputAction);
            timedOutputWord.add(new Pair<>(outputAction,timePassed));
        }

        return timedOutputWord;
    }

    public MealyTimerDetEnabledWalker(MealyTimerDetEnabledModel model ) {
        super(model);
    }

    // interface StepInputGetOutput

    @Override
    public boolean checkInputPossible(InputAction action) {
        return true;  // because model is input enable ; implements InputEnabledDeterministic interface
    }




     /**
      *  step for always enabled input
      *
      * @param action
     * @return
      */
    @Override
    public OutputAction stepInput(InputAction action){
         // get model transition  for current trigger to system
         MealyTimerTransition transition=model.getTriggeredTransition(currentState, action);
         // evaluate effect of transition using operational semantics in model
         Pair<TimersLocationState,OutputAction> result=model.evaluateTransition(currentState,transition);
         currentState=result.getValue0();
         OutputAction output=result.getValue1();
         return output;// transition.getOutput();
    }





    /** just wait some delay and possible receive some outputs by timeouts
    *
    *  note: timers which have timeout at exactly  end of the delay are not executed;
    *        the reason is that if you immediately take an input step it overrules the timeout!!  (input wins race condition!!)
    *
    *  note: in case we have non-transparent timed input word, then we can have some non-determinism if two timers
    *        timeout at the same time!
    *        The  walker resolves this non-determinism by searching in alphabet order of timer name for a timeout transition.
    *
    *  returns list of (output,time) tuples  where time is relative to start time of waitDelay call
    */
    public List<Pair<OutputAction,Long>> waitDelay(long delay) {
        ArrayList<Pair<OutputAction,Long>> timedoutputs= new ArrayList<>();
        long remainingTime,dt;
        //TimersLocationState state;

        // evaluate idle time system may execute
        Pair<TimersLocationState, Long> idleResult = model.evaluateIdle(currentState, delay);
        currentState=idleResult.getValue0();
        dt=idleResult.getValue1();

        remainingTime=delay-dt;
        while ( remainingTime != 0  )  {
            // do timeout   (at least one should be possible at this state because evaluateIdle return smaller idle  time then requested )

            // find possible timeout transition for timer timeout
            MealyTimerTransition transition=null;
            // get timers who timeout
            TreeSet<TimerVariable> timerVars = currentState.getTimedOutTimers();
            // find possible timeout transition
            //System.out.println("timers: "+timerVars);

            // timers are sorted first by timeout value , then by alphabet order of timerVariable
            // so if both timers x and y  are timed out we  search first for x if there a transition exist!! (alphabetic!)
            search: for (TimerVariable timerVar : timerVars) {

                // get timeout transition for timerVar
                ImmutableSet<MealyTimerTransition> timeoutTransitions = model.getModelTimeoutTransitions(currentState.getLocation(),timerVar);
                if (!timeoutTransitions.isEmpty()) {
                    // we reached maximal idle time
                    transition = timeoutTransitions.asList().get(0); // done idle
                    break search;
                }
            }
            if (transition==null) {
                throw new RuntimeException("error: timeout transition should exist");
            }

            // do timeout transition => update location, but timers do not change
            Pair<TimersLocationState,OutputAction> result=model.evaluateTransition(currentState,transition);
            currentState=result.getValue0();
            OutputAction output=result.getValue1();

            // we handle a timeout, so remove timed out timers from state
            currentState=currentState.removeTimedOutTimers();

            // store output at specific delaytime it happened
            timedoutputs.add(new Pair<>(output,delay-remainingTime));

            // evaluate idle time system may execute
            idleResult = model.evaluateIdle(currentState, remainingTime);
            currentState=idleResult.getValue0();
            dt=idleResult.getValue1();

            remainingTime=remainingTime-dt;
        }

        return timedoutputs;
    }



}
