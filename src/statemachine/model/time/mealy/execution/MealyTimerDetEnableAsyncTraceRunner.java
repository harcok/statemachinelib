package statemachine.model.time.mealy.execution;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.javatuples.Pair;

import statemachine.interfaces.system.execution.OutputListener;
import statemachine.interfaces.system.execution.SendTimedInputWordGetTimedOutputWord;
import statemachine.interfaces.system.execution.StartInterrupt;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.time.mealy.MealyTimerDetEnabledModel;

/**  TraceRunner class which implements execution of timed input words on a MealyTimerModel where each send call blocks
 *   the needed time until its answer from the machine is ready.
 *
 *   Internally it uses a MealyTimerDetEnabledAsyncRunner where all outputs, both mealy as timeout outputs, are send asynchronous to a listener.
 */
public class MealyTimerDetEnableAsyncTraceRunner implements SendTimedInputWordGetTimedOutputWord<InputAction,OutputAction,Long>,StartInterrupt{

    private MealyTimerDetEnabledAsyncRunner runner;
    private long epsilon;


    private ArrayList<Pair<OutputAction,Long>> timedOutputWord;

    // for debugging
    private static boolean debug=false;
    private  Long startTime;

    public MealyTimerDetEnableAsyncTraceRunner(MealyTimerDetEnabledModel model, long epsilon) {
        super();
        this.runner = new MealyTimerDetEnabledAsyncRunner(model);
        this.epsilon=epsilon;

        // subscribe output listener
        runner.setOnOutputListener( new OutputListener<OutputAction>() {
            public void onOutput(OutputAction output) {
                Long time = System.currentTimeMillis()-startTime;
                if (debug) System.out.println(time +": out" +output);
                timedOutputWord.add(new Pair<OutputAction,Long>(output,time));
            }
        });
    }

    @Override
    public void start() {
        // start runner thread
        runner.start();

    }

    @Override
    public void interrupt() {
        // stop runner thread
        runner.interrupt();
    }

    public  List<Pair<OutputAction,Long>> sendTimedInputWord(List<Pair<InputAction,Long>> timedInputWord) {


        // first reset SUL
        runner.reset();
        //  create new empty output word
        timedOutputWord = new ArrayList<>();

        // set start time for query  => only used for debugging prints
        startTime = System.currentTimeMillis();


        // handle timed word
        long lastInputTime = 0;
        java.util.Timer timer = new java.util.Timer();
        for  ( Pair<InputAction,Long> pair: timedInputWord ) {
            InputAction input = pair.getValue0();
            Long time=pair.getValue1();
            lastInputTime=time;
            timer.schedule(new TimerTask() {
              @Override
              public void run() {
                runner.sendInput(input);
                if (debug) {
                    Long realtime = System.currentTimeMillis()-startTime;
                    System.out.println(realtime+": send " + input);
                }
              }
            }, time);
        }

        // sleep for lastInputTime + epsilon seconds
        try {
            Thread.sleep(lastInputTime + epsilon);
        } catch (InterruptedException e) {
            // ignore
        }

        // stop timer thread for input scheduling
        timer.cancel();

        return timedOutputWord;
    }






    /** static method to run single trace

        If you want to run multiple traces it is more efficient to create a TraceRunner instance object
        and reuse that to run multiple traces on a single instance of the running machine thread.
    **/
    public static List<Pair<OutputAction,Long>> runTimedInputWord(MealyTimerDetEnabledModel model, List<Pair<InputAction,Long>> timedInputWord, long epsilon ) {

        MealyTimerDetEnableAsyncTraceRunner traceRunner = new MealyTimerDetEnableAsyncTraceRunner(model,epsilon);
        // start runner thread
        traceRunner.start();

        // run query
        List<Pair<OutputAction,Long>> timedOutputWord;
        timedOutputWord=traceRunner.sendTimedInputWord(timedInputWord);

        // stop runner thread
        traceRunner.interrupt();

        return timedOutputWord;
    }


}
