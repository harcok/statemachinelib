package statemachine.model.time.mealy.execution;

import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import org.javatuples.Pair;

import statemachine.interfaces.system.execution.OutputListener;
import statemachine.interfaces.system.execution.Reset;
import statemachine.interfaces.system.execution.SendInputGetOutputListenOutput;
import statemachine.interfaces.system.execution.StartInterrupt;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;
import statemachine.model.time.mealy.MealyTimerDetEnabledModel;

public class MealyTimerDetEnableRunner extends Thread implements SendInputGetOutputListenOutput<InputAction,OutputAction>,Reset,StartInterrupt {

    private OutputListener<OutputAction> onOutputListener;

    private LinkedBlockingQueue<Pair<TimerVariable,InputAction>> queue;
    private HashMap<TimerVariable,java.util.Timer> timerVar2timer;

    private MealyTimerDetEnabledModel model;
    private Location location;


    private static boolean debug=false;

    public MealyTimerDetEnableRunner(MealyTimerDetEnabledModel  model) {
        super();
        this.model=model;
        this.location=model.getStartLocation();

        this.queue = new LinkedBlockingQueue<>();
        this.timerVar2timer=new HashMap<>();
    }

    @Override
    public OutputAction sendInput(InputAction input) {
        return executeMealyTransition(input);
    }

    @Override
    public void setOnOutputListener(OutputListener<OutputAction> onOutputListener) {
        this.onOutputListener = onOutputListener;
    }


    private void  executeTransitionWithUpdate(MealyTimerTransition transition) {
        // do transition with update => update timer
        if (debug) System.out.println("run: take trans: " + transition);

        // change location
        location=transition.getDestination();

        // do update
        if ( transition.hasUpdate() ) {
            String updateExpression=transition.getUpdate().get();

            String[] parts=updateExpression.trim().split("=");
            TimerVariable timerVarUpdated=new TimerVariable(parts[0]);
            Long timeoutTime=Long.valueOf(parts[1]);

            // stop old timer for timerVarUpdated
            if (timerVar2timer.containsKey(timerVarUpdated)) {
                timerVar2timer.get(timerVarUpdated).cancel();
            }
            // start new timer for timerVarUpdated with timeoutTime
            java.util.Timer internalTimer = new java.util.Timer();
            internalTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    Pair<TimerVariable,InputAction> newevent= new Pair<>(timerVarUpdated,null);
                    try {
                        queue.put(newevent);
                    } catch (InterruptedException e) {
                        // couldn't enqueue timeout event of timer => should not happen
                        throw new RuntimeException("Couldn't enqueue timeout event of timer");
                    }
                }
              }, timeoutTime);
            timerVar2timer.put(timerVarUpdated, internalTimer);
        } //END: if ( transition.hasUpdate() )
    }



    synchronized private void  executeTimeoutTransition( TimerVariable timerVar ) {
        // try to get transition
        Optional<MealyTimerTransition> optTransition= model.getModelTimeoutTransition(location,timerVar);
        if ( optTransition.isPresent()  ) {
            MealyTimerTransition transition=optTransition.get();
            // execute found transition
            executeTransitionWithUpdate(transition);
            // do output
            onOutputListener.onOutput(transition.getOutput());
        } else {
            if (debug) System.out.println("run: missed timeout " + timerVar + " because in current location no timeout transition defined");
        }
    }

    synchronized private  OutputAction executeMealyTransition( InputAction input ) {
        MealyTimerTransition transition=model.getTriggeredTransition(new TimersLocationState(location, null), input);
        executeTransitionWithUpdate(transition);
        return transition.getOutput();
    }

    @Override
    synchronized public void reset() {
        for ( Timer timer: timerVar2timer.values()) {
            timer.cancel();
        }
        timerVar2timer.clear();
        queue.clear();
        location=model.getStartLocation();
    }

    private void cleanup() {
        reset();
    }




    @Override
    public void run() {

        while ( true ) {
            Pair<TimerVariable, InputAction> event=null;

            if (Thread.currentThread().isInterrupted()) {
                // cleanup and stop execution by breaking loop
                cleanup();
                break;
              }

            try {
                event = queue.take();
                if (debug) {
                    System.out.println("event: " + event);
                }
            } catch (InterruptedException e1) {
                // catch interrupt of thread
                // cleanup and stop execution by breaking loop
                cleanup();
                break;
            }

            executeTimeoutTransition( event.getValue0());

        } //END: while ( true )
        if (debug) System.out.println("run: end TimedSul thread");

    } //END: run()


}
