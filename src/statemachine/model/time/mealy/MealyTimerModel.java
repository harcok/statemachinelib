package statemachine.model.time.mealy;



import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.model.types.ModelMealy;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;


@NonNullByDefault
public interface MealyTimerModel extends ModelMealy<InputAction, OutputAction, Location, MealyTimerTransition>,
        InitialState<Location, TimersLocationState> {


    Pair<TimersLocationState,OutputAction> evaluateTransition( TimersLocationState state, MealyTimerTransition  transition );

    Pair<TimersLocationState,Long> evaluateIdle( TimersLocationState state, Long  requestedIdleTime );


}
