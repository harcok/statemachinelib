package statemachine.model.time.mealy;


import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputEnabledDeterministic;
import statemachine.interfaces.system.properties.TimeoutDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.TimersLocationState;
import statemachine.model.elements.transition.MealyTimerTransition;



@NonNullByDefault
public class MealyTimerDetEnabledModel extends MealyTimerBaseModel
      implements MealyTimerModel, InputEnabledDeterministic<TimersLocationState,InputAction,MealyTimerTransition>,
      TimeoutDeterministic<TimersLocationState,TimerVariable,MealyTimerTransition>
{


    //Builder class
    public static final class ImmutableBuilder extends MealyTimerBaseModel.ImmutableBuilder {

        public MealyTimerDetEnabledModel build(){
            super.baseBuild();
            return new MealyTimerDetEnabledModel(this);
        }
    }

    // Constructor
    private MealyTimerDetEnabledModel(ImmutableBuilder builder) {
        super(builder);

        // very input-determinism and verify model is input enabled; if found it throws exception
        // NOTE: timeout is not in input alphabet but in timeout alphabet so it checks only for none-timeout inputs!
        for (Location location:  this.loc2trans.keySet()) {
            for ( InputAction action : this.inputalphabet ) {
                TimersLocationState state= new TimersLocationState(location, null); // ignore time!
                getTriggeredTransition(state,action);  // throws exception when none-determinism is detected or input not enabled!
            }
        }

        // verify timeout determinism :  in each state for a each timer variable it does at max one timeout transition
        for (Location location:  this.loc2trans.keySet()) {
            for ( TimerVariable timervar : this.timeralphabet ) {
                TimersLocationState state= new TimersLocationState(location,null); // ignore time!
                getSystemTimeoutTransition(state,timervar);  // throws exception when none-determinism is detected for timer timeout
            }
        }

        // note: we do not verify  timeout complete , so we can define ghost timers (which never timeout)

    }

    // static factory from MealyTimerModel
    static public MealyTimerDetEnabledModel fromMealyModel(MealyTimerModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromMealyTimerModel(model);
        return builder.build();
    }

    // interface InputEnabledDeterministic

    @Override
    public MealyTimerTransition getTriggeredTransition(TimersLocationState state,InputAction action) {
        // only location and input determine system transition you can take for an input and not the timers => so basicly ignore time!
        @NonNull ImmutableSet<@NonNull MealyTimerTransition> transitions = this.getModelTransitions(state.getLocation(), action);
        return Util.getTheTransition(transitions,state.getLocation().getName(),action.getName());
    }

    // interface TimeoutDeterministic

    // In model only timer variable and location determine the timeout transitions the system can take when timer variable is times out.
    // The current timers values in the state are ignored for this getter.
    // The semantic is more in the sense : if we wait can then for timer x  more then one timeout transition happen. => so basicly ignore time!

    @Override
    public Optional<MealyTimerTransition> getSystemTimeoutTransition(TimersLocationState state, TimerVariable timerVariable) {
        return getModelTimeoutTransition(state.getLocation(), timerVariable);
    }

    public Optional<MealyTimerTransition> getModelTimeoutTransition(Location location, TimerVariable timerVariable) {
         ImmutableSet<MealyTimerTransition> transitions = this.getModelTimeoutTransitions(location, timerVariable);
        return Util.getOptionalTransition(transitions);
    }

}
