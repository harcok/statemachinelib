package statemachine.model.time;

import java.util.ArrayList;
import java.util.List;

import org.javatuples.Pair;

public class Util {

    public static <A> List<Pair<A,Long>> roundDown(List<Pair<A,Long>> timedWord,long epsilon) {
        ArrayList<Pair<A,Long>> result= new ArrayList<>();
        for  ( Pair<A,Long> pair: timedWord ) {
            A input = pair.getValue0();
            Long time=pair.getValue1();
            result.add( new Pair<A,Long>(input, time - java.lang.Math.floorMod(time,epsilon) ));
        }
        return result;
    }

}
