package statemachine.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

import org.javatuples.Pair;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;

public class Util {

    /** checks that given set contains maximal one transition; if the check fails it throws an exception
     *
     * @param transitions
     */
    static private <T extends ModelTransition<?>> void checkForMaximalOneTransition(ImmutableSet<T> transitions) {
        if (transitions.size() > 1) {
            ArrayList<String> strings = new ArrayList<>();
            for (T transition : transitions) {
                strings.add(transition.toString());
            }

            String joined_transitions = String.join(" and ", strings);
            throw new RuntimeException(
                    "error in model; none deterministic choice in what should be deterministic; transitions :" + joined_transitions);
        }
    }

    /**  return Optional for given set of transitions; if set contains more then one transition then an exception is thrown
     *
     * @param transitions
     * @return
     */
    static public <T extends ModelTransition<?>> Optional<T> getOptionalTransition(ImmutableSet<T> transitions) {

        checkForMaximalOneTransition(transitions);

        if (transitions.isEmpty()) {
            return Optional.empty();
        }

        return Optional.of(transitions.iterator().next());
    }

    /**  return the single transition in the given set of transitions; if set contains more or less then one transition then an exception is thrown
    *
    * @param transitions
    * @return
    */
    public static  <T extends ModelTransition<?>> T getTheTransition(
            ImmutableSet<T> transitions, String location, String actionName) {

        checkForMaximalOneTransition(transitions);

        if (transitions.isEmpty()) {
            throw new RuntimeException("error in model; enabled model doesn't has a transition in state '" + location
                    + "' for action: '" + actionName + "'");
        }

        return transitions.iterator().next();
    }

    /** a model is symmetric in its directed transitions if for each directed transition in the model also a directed
     *  transition exist in which source and destination are reversed
     *
     * @param model
     * @return boolean answer whether model is symmetric or not
     */
    public static  <L extends ModelLocation,T extends ModelDirectedTransition<L>>  boolean isDirectedTransitionSymmetric(ModelAutomaton<L,T> model) {
        HashSet<Pair<L,L>> pairs= new HashSet<>();
        for (  T trans : model.getModelTransitions() ) {
              pairs.add( new Pair<> (trans.getSource(),trans.getDestination()));
        }
        for ( Pair<L,L> pair : pairs ) {
              Pair<L,L> reversedPair =new Pair<> ( pair.getValue0() , pair.getValue1() ) ;
              if ( ! pairs.contains(reversedPair)) {
                  return false;
              }
        }
        return true;
    }


    /** hasCycles checks whether a model has cycles
     * <p>
     *  A model represented by a graph G is acyclic if and only if a depth-first search of G yields no back edges
     *
     * @return true if model has cycles, else false
     */
     static public  <L extends ModelLocation,T extends ModelDirectedTransition<L>> boolean hasCycles(ModelAutomaton<L,T> model) {
         return false;
     }

}
