package statemachine.model.efsm.ia.conversion;

import org.javatuples.Quartet;


@FunctionalInterface
public interface TransitionLabelParser {
    public Quartet<String, String, String, String> apply (String label);
}
