package statemachine.model.efsm.ia.conversion;

import java.util.Optional;



@FunctionalInterface
public interface TransitionLabelFormatter {
    public String apply(String type, String action, Optional<String> guard, Optional<String> update);

}
