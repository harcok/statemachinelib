package statemachine.model.efsm.ia.conversion;

import org.antlr.symtab.PrimitiveType;
import org.javatuples.Quartet;

import statemachine.model.efsm.ia.IaModel;
import statemachine.model.efsm.ia.IaNonDetModel;
import statemachine.model.efsm.ia.conversion.TransitionLabelFormatter;
import statemachine.model.efsm.ia.conversion.TransitionLabelParser;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.SymbolicHiddenTransition;
import statemachine.model.elements.transition.SymbolicInputTransition;
import statemachine.model.elements.transition.SymbolicOutputTransition;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsZip {

    static public TransitionLabelFormatter defaultTransitionLabelFormatter =
            ( type, action, guard, update ) -> {
                String guardStr ="", updateStr="",sep="";
                if ( update.isPresent() ) {
                    updateStr = "{" + update.get() + "}";
                }
                if ( guard.isPresent() ) {
                    guardStr = "[" + guard.get() + "]";
                }
                if ( type.equals("input") ) {
                    if ( update.isPresent() ) sep="/";
                    return "?"+action +  guardStr + sep + updateStr;
                } else if ( type.equals("output") ) {
                    if ( guard.isPresent() ) sep="/";
                    return guardStr + sep + updateStr + "!"+action;
                } else if ( type.equals("hidden") ) {
                    if ( update.isPresent() ) sep="/";
                    return "_"+action +  guardStr + sep + updateStr;
                }
                return "error inTransitionLabelFormatter";
            };


    static public TransitionLabelParser defaultTransitionLabelParser = new TransitionLabelParser() {
        public Quartet<String,String, String, String> apply(String label) {

            String type="error",action,update="",guard="";
            int startIndex,endIndex;

            // "/" is only there if there is an update or action type='output'!

            String action_guard="", update_output="";
            if ( label.contains("/") ) {
                String[] parts = label.split("/");
                action_guard = parts[0];
                if (parts.length > 1 ) update_output = parts[1];
            } else {
                update_output=label;
            }

            if ( action_guard.contains("[") ) {
                startIndex=action_guard.indexOf("[");
                endIndex=action_guard.indexOf("]");
                guard=action_guard.substring(startIndex+1, endIndex).trim();
                action=action_guard.substring(0,startIndex).trim();
            } else {
                action=action_guard.trim();
            }
            if (action.startsWith("_")) {
               type="hidden";
            } else if (action.startsWith("?")) {
                type="input";
            } else {
                 // action is probably output
                 endIndex=update_output.indexOf("}");
                 action=update_output.substring(endIndex+1).trim();
                 if (action.startsWith("!")) {
                     type="output";
                 }
            }

            action=action.substring(1);

            if ( update_output.contains("{") ) {
                startIndex=update_output.indexOf("{");
                endIndex=update_output.indexOf("}");
                update=update_output.substring(startIndex+1, endIndex).trim();
            }
            return new Quartet<>(type, action,guard,update);
        }
    };


    public static LtsNonDetModel zipIa2Lts(IaModel model, TransitionLabelFormatter transitionFormatter) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

        for (SymbolicInputTransition trans : model.getModelInputTransitions() ) {
            String label = transitionFormatter.apply("input",trans.getInput().asString(),trans.getGuard(),trans.getUpdate());
            Action action = new Action(label);
            builder.addTransition(new LocationState(trans.getSource().getName()), new LocationState(trans.getDestination().getName()), action);
        }
        for (SymbolicOutputTransition trans : model.getModelOutputTransitions() ) {
            String label = transitionFormatter.apply("output",trans.getOutput().asString(),trans.getGuard(),trans.getUpdate());
            Action action = new Action(label);
            builder.addTransition(new LocationState(trans.getSource().getName()), new LocationState(trans.getDestination().getName()), action);
        }
        for (SymbolicHiddenTransition trans : model.getModelHiddenTransitions() ) {
            String label = transitionFormatter.apply("hidden",trans.getHiddenAction().asString(),trans.getGuard(),trans.getUpdate());
            Action action = new Action(label);
            builder.addTransition(new LocationState(trans.getSource().getName()), new LocationState(trans.getDestination().getName()), action);
        }
        builder.setStartLocation(new LocationState(model.getStartLocation().getName()));
        return builder.build();
    }

    static public LtsNonDetModel zipIa2Lts(IaModel model) {
        return zipIa2Lts(model, defaultTransitionLabelFormatter);
    }

    static public IaNonDetModel unzipLts2Ia(LtsModel model, TransitionLabelParser labelParser) {
        IaNonDetModel.ImmutableBuilder builder = new IaNonDetModel.ImmutableBuilder();

        // TODO: parse json in dot file 's comment to get  input and output alphabet
        //        => use flexible parsing of types to "our" builtin primitive types:
        //              int,integer  -> int            `-> every evaluator should support them!! => aggreement of expression machine communication => machine needs to store state => needs same basic types!!
        PrimitiveType integer = new PrimitiveType("INT");
        DataInputActionType A =  new DataInputActionType("a",integer,integer);
        DataInputActionType B =  new DataInputActionType("b",integer);
        DataOutputActionType OUT =  new DataOutputActionType("OUT",integer,integer);
        DataOutputActionType OK =  new DataOutputActionType("OK",integer);


        builder.setInputAlphabet(A,B);
        builder.setOutputAlphabet(OUT,OK);


        for (ActionTransition trans : model.getModelTransitions()) {
            String label = trans.getAction().getName();
            Quartet<String,String , String, String>  type_action_guard_update = labelParser.apply(label);

            String type= type_action_guard_update.getValue0();
            String  action = type_action_guard_update.getValue1();
            String guard = type_action_guard_update.getValue2();
            String update = type_action_guard_update.getValue3();
            if ( type.equals("input")) {
                builder.addInputTransition(new Location(trans.getSource().getName()), new Location(trans.getDestination().getName()), action, guard,update);
            } else if ( type.equals("output")) {
                builder.addOutputTransition(new Location(trans.getSource().getName()), new Location(trans.getDestination().getName()), action, guard,update);
            } else if ( type.equals("hidden")) {
                builder.addHiddenTransition(new Location(trans.getSource().getName()), new Location(trans.getDestination().getName()), action, guard,update);
            }  else {
                // TODO: thrown error of ignore ??
            }
        }

        // TODO: get initial statevars from dot file
        builder.setStartState(new Location(model.getStartLocation().getName()));
        // TODO: get constants from dot file
        return builder.build();
    }

    static public IaNonDetModel unzipLts2Ia(LtsModel model) {
        return unzipLts2Ia(model, defaultTransitionLabelParser);
    }


}
