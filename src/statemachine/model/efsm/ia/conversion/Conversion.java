package statemachine.model.efsm.ia.conversion;

import com.google.common.collect.ImmutableSet;

import statemachine.model.efsm.ia.IaNonDetModel;
import statemachine.model.efsm.mealy.MealyNonDetModel;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.transition.SymbolicInputTransition;
import statemachine.model.elements.transition.SymbolicOutputTransition;

public class Conversion {

    static public MealyNonDetModel  IaModel2MealyModel( IaNonDetModel model){

        MealyNonDetModel.ImmutableBuilder builder =new MealyNonDetModel.ImmutableBuilder();
        builder.setConstants(model.getConstants());
        builder.setStartState(model.getStartState());
        builder.setInputAlphabet(model.getInputAlphabet().asList());
        builder.setOutputAlphabet(model.getOutputAlphabet().asList());

        for ( Location location: model.getLocations() ) {
              ImmutableSet<SymbolicInputTransition> inputtrans = model.getModelInputTransitions(location);
              ImmutableSet<SymbolicOutputTransition> outputtrans = model.getModelOutputTransitions(location);
              if ( inputtrans.size() > 0) {
                  if ( outputtrans.size() > 0 ) {
                       throw new RuntimeException("cannot convert to Mealy, beceause no alternating input and output transitions");
                  }
                  for (SymbolicInputTransition itrans:inputtrans) {
                      Location middlelocation=itrans.getDestination();
                      for (SymbolicOutputTransition otrans:model.getModelOutputTransitions(middlelocation)) {
                          String guard ="";
                          String update = "";
                          if ( itrans.getUpdate().isPresent() ) update=update+ itrans.getUpdate().get();
                          if ( otrans.getUpdate().isPresent() ) update=update+ otrans.getUpdate().get();

                          if ( itrans.getGuard().isPresent() ) {
                              guard=itrans.getGuard().get();
                              if ( otrans.getGuard().isPresent() ) guard="(" + guard+") && (" + otrans.getGuard().get() + ")";
                          } else {
                              if ( otrans.getGuard().isPresent() ) guard= otrans.getGuard().get();
                          }

                          builder.addTransition(itrans.getSource(), otrans.getDestination(), itrans.getInput(), otrans.getOutput(), guard, update);
                      }
                  }
              }
        }


        return builder.build();
    }

}
