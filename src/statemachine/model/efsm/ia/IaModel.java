package statemachine.model.efsm.ia;


import org.antlr.symtab.VariableSymbol;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableMap;

import statemachine.interfaces.model.types.ModelExtendedInterfaceAutomaton;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.ConcreteHiddenAction;
import statemachine.model.elements.action.ConcreteInputAction;
import statemachine.model.elements.action.ConcreteOutputAction;
import statemachine.model.elements.action.DataHiddenActionType;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.DataLocationState;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.transition.SymbolicHiddenTransition;
import statemachine.model.elements.transition.SymbolicInputTransition;
import statemachine.model.elements.transition.SymbolicOutputTransition;
import statemachine.model.elements.transition.BaseTransition;

@NonNullByDefault
public interface IaModel extends ModelExtendedInterfaceAutomaton<ConcreteInputAction,ConcreteOutputAction,ConcreteHiddenAction,DataLocationState,
     DataInputActionType,DataOutputActionType,DataHiddenActionType,
     Location,BaseTransition<Location>,
     SymbolicInputTransition, SymbolicOutputTransition,SymbolicHiddenTransition>,


     InitialState<Location, DataLocationState> {

     ImmutableMap<VariableSymbol,Value> getConstants();

}