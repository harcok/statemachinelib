package statemachine.model.efsm.ia;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.symtab.VariableSymbol;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.efsm.mealy.MealyBaseModel.ImmutableBuilder;
import statemachine.model.elements.action.ConcreteHiddenAction;
import statemachine.model.elements.action.ConcreteInputAction;
import statemachine.model.elements.action.ConcreteOutputAction;
import statemachine.model.elements.action.DataHiddenActionType;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.SymbolicHiddenAction;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.DataLocationState;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.transition.SymbolicHiddenTransition;
import statemachine.model.elements.transition.SymbolicInputTransition;
import statemachine.model.elements.transition.SymbolicOutputTransition;
import statemachine.model.elements.transition.BaseTransition;

@NonNullByDefault
public  abstract class IaBaseModel implements IaModel {

    protected final ImmutableSet<DataInputActionType> inputalphabet;
    protected final ImmutableSet<DataOutputActionType> outputalphabet;
    protected final ImmutableSet<DataHiddenActionType> hiddenalphabet;
    private final DataLocationState startState;
    protected final ImmutableMap<VariableSymbol, Value> constants;
    private final ImmutableSet<Location> locations;

    private   final ImmutableSet<SymbolicInputTransition> inputTransitions;
    private   final ImmutableSet<SymbolicOutputTransition> outputTransitions;
    protected final ImmutableSet<SymbolicHiddenTransition> hiddenTransitions;


    protected final ImmutableSetMultimap<Location,  SymbolicInputTransition> loc2inputtrans;
    protected final ImmutableSetMultimap<Location,  SymbolicOutputTransition> loc2outputtrans;
    protected final ImmutableSetMultimap<Location,  SymbolicHiddenTransition> loc2hiddentrans;




    // Builder class
    public static abstract class ImmutableBuilder {

        protected ImmutableSet<DataInputActionType> inputalphabet;
        protected ImmutableSet<DataOutputActionType> outputalphabet;
        protected ImmutableSet<DataHiddenActionType> hiddenalphabet;

        //protected Location startLocation;
        protected DataLocationState startState;
        private ImmutableMap<VariableSymbol, Value> constants; // a constant is a variable which cannot be changed!
        protected ImmutableSet<Location> locations;

        protected ImmutableSet<SymbolicInputTransition> inputTransitions;
        protected ImmutableSet<SymbolicOutputTransition> outputTransitions;
        protected ImmutableSet<SymbolicHiddenTransition> hiddenTransitions;


        protected ImmutableSetMultimap<Location,  SymbolicInputTransition> loc2inputtrans;
        protected ImmutableSetMultimap<Location,  SymbolicOutputTransition> loc2outputtrans;
        protected ImmutableSetMultimap<Location,  SymbolicHiddenTransition> loc2hiddentrans;


        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<Location> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<SymbolicInputTransition> inputTransitionsBuilder = ImmutableSet.builder();
        protected ImmutableSet.Builder<SymbolicOutputTransition> outputTransitionsBuilder = ImmutableSet.builder();
        protected ImmutableSet.Builder<SymbolicHiddenTransition> hiddenTransitionsBuilder = ImmutableSet.builder();

//        @SuppressWarnings("null")
//        protected ImmutableSet.Builder<DataInputActionType> inputalphabetBuilder = ImmutableSet.builder();
//        @SuppressWarnings("null")
//        protected ImmutableSet.Builder<DataOutputActionType> outputalphabetBuilder = ImmutableSet.builder();
//        @SuppressWarnings("null")
//        protected ImmutableSet.Builder<DataHiddenActionType> hiddenalphabetBuilder = ImmutableSet.builder();
//


        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<Location, SymbolicInputTransition> loc2inputTransBuilder = ImmutableSetMultimap.builder();
        protected ImmutableSetMultimap.Builder<Location, SymbolicOutputTransition> loc2outputTransBuilder = ImmutableSetMultimap.builder();
        protected ImmutableSetMultimap.Builder<Location, SymbolicHiddenTransition> loc2hiddenTransBuilder = ImmutableSetMultimap.builder();


        private HashMap<String, DataInputActionType> name2input  = new HashMap<String,DataInputActionType>();
        private HashMap<String, DataOutputActionType> name2output = new HashMap<String,DataOutputActionType>();
        private HashMap<String, DataHiddenActionType> name2hidden = new HashMap<String,DataHiddenActionType>();







        public ImmutableBuilder addInputTransition(Location src, Location dst, SymbolicInputAction input, String guard,String update) {
            this.locationsBuilder.add(src, dst);
            //this.inputalphabetBuilder.add(input.getType());
            SymbolicInputTransition transition = new SymbolicInputTransition(src, dst, input,guard,update);
            this.inputTransitionsBuilder.add(transition);
            this.loc2inputTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addInputTransition(Location src, Location dst, SymbolicInputAction input, String guard) {
            this.addInputTransition(src,dst,input,guard,null);
            return this;
        }
        public ImmutableBuilder addInputTransition(Location src, Location dst, SymbolicInputAction input) {
            this.addInputTransition(src,dst,input,null,null);
            return this;
        }
        public ImmutableBuilder addInputTransition(Location src, Location dst, String inputStr, String guard,String update) {
            SymbolicInputAction input = parseInput(inputStr);
            this.locationsBuilder.add(src, dst);
            //this.inputalphabetBuilder.add(input.getType());
            SymbolicInputTransition transition = new SymbolicInputTransition(src, dst, input,guard,update);
            this.inputTransitionsBuilder.add(transition);
            this.loc2inputTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addInputTransition(Location src, Location dst, String input, String guard) {
            this.addInputTransition(src,dst,input,guard,"");
            return this;
        }
        public ImmutableBuilder addInputTransition(Location src, Location dst, String input) {
            this.addInputTransition(src,dst,input,"","");
            return this;
        }


        public ImmutableBuilder addOutputTransition(Location src, Location dst, SymbolicOutputAction output, String guard,String update) {
            this.locationsBuilder.add(src, dst);
            //this.outputalphabetBuilder.add(output.getType());
            SymbolicOutputTransition transition = new SymbolicOutputTransition(src, dst, output,guard,update);
            this.outputTransitionsBuilder.add(transition);
            this.loc2outputTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addOutputTransition(Location src, Location dst, SymbolicOutputAction output, String guard) {
            this.addOutputTransition(src,dst,output,guard,null);
            return this;
        }
        public ImmutableBuilder addOutputTransition(Location src, Location dst, SymbolicOutputAction output) {
            this.addOutputTransition(src,dst,output,null,null);
            return this;
        }
        public ImmutableBuilder addOutputTransition(Location src, Location dst, String outputStr, String guard,String update) {
            SymbolicOutputAction output = parseOutput(outputStr);
            this.locationsBuilder.add(src, dst);
            //this.outputalphabetBuilder.add(output.getType());
            SymbolicOutputTransition transition = new SymbolicOutputTransition(src, dst, output,guard,update);
            this.outputTransitionsBuilder.add(transition);
            this.loc2outputTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addOutputTransition(Location src, Location dst, String output, String guard) {
            this.addOutputTransition(src,dst,output,guard,"");
            return this;
        }
        public ImmutableBuilder addOutputTransition(Location src, Location dst, String output) {
            this.addOutputTransition(src,dst,output,"","");
            return this;
        }


        public ImmutableBuilder addHiddenTransition(Location src, Location dst, SymbolicHiddenAction hidden, String guard,String update) {
            this.locationsBuilder.add(src, dst);
            //this.hiddenalphabetBuilder.add(hidden.getType());
            SymbolicHiddenTransition transition = new SymbolicHiddenTransition(src, dst, hidden,guard,update);
            this.hiddenTransitionsBuilder.add(transition);
            this.loc2hiddenTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addHiddenTransition(Location src, Location dst, SymbolicHiddenAction hidden, String guard) {
            this.addHiddenTransition(src,dst,hidden,guard,null);
            return this;
        }
        public ImmutableBuilder addHiddenTransition(Location src, Location dst, SymbolicHiddenAction hidden) {
            this.addHiddenTransition(src,dst,hidden,null,null);
            return this;
        }
        public ImmutableBuilder addHiddenTransition(Location src, Location dst, String hiddenStr, String guard,String update) {
            SymbolicHiddenAction hidden = parseHidden(hiddenStr);
            this.locationsBuilder.add(src, dst);
            //this.hiddenalphabetBuilder.add(hidden.getType());
            SymbolicHiddenTransition transition = new SymbolicHiddenTransition(src, dst, hidden,guard,update);
            this.hiddenTransitionsBuilder.add(transition);
            this.loc2hiddenTransBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addHiddenTransition(Location src, Location dst, String hidden, String guard) {
            this.addHiddenTransition(src,dst,hidden,guard,"");
            return this;
        }
        public ImmutableBuilder addHiddenTransition(Location src, Location dst, String hidden) {
            this.addHiddenTransition(src,dst,hidden,"","");
            return this;
        }


        public ImmutableBuilder addTransition(Location src, Location dst, String action, String guard,String update) {

            this.locationsBuilder.add(src, dst);

            String name=parseActionName(action);
            if (name2input.containsKey(name)) {
                SymbolicInputAction input = parseInput(action);
                SymbolicInputTransition transition = new SymbolicInputTransition(src, dst, input,guard,update);
                this.inputTransitionsBuilder.add(transition);
                this.loc2inputTransBuilder.put(src, transition);
            }
            if (name2output.containsKey(name)) {
                SymbolicOutputAction output = parseOutput(action);
                SymbolicOutputTransition transition = new SymbolicOutputTransition(src, dst, output,guard,update);
                this.outputTransitionsBuilder.add(transition);
                this.loc2outputTransBuilder.put(src, transition);
            }
            if (name2hidden.containsKey(name)) {
                SymbolicHiddenAction hidden = parseHidden(action);
                SymbolicHiddenTransition transition = new SymbolicHiddenTransition(src, dst, hidden,guard,update);
                this.hiddenTransitionsBuilder.add(transition);
                this.loc2hiddenTransBuilder.put(src, transition);
            }

            return this;
        }

        public ImmutableBuilder addTransition(Location src, Location dst, String action, String guard) {
            this.addInputTransition(src,dst,action,guard,"");
            return this;
        }
        public ImmutableBuilder addTransition(Location src, Location dst, String action) {
            this.addInputTransition(src,dst,action,"","");
            return this;
        }


        public ImmutableBuilder setInputAlphabet(List<DataInputActionType> actionTypes ) {
            this.inputalphabet= ImmutableSet.copyOf(actionTypes);
            for (DataInputActionType inputType : actionTypes) {
                name2input.put(inputType.getName(), inputType);
            }
            return this;
          }

          public ImmutableBuilder setInputAlphabet(DataInputActionType... actionTypes ) {
              setInputAlphabet(Arrays.asList(actionTypes));
              return this;
          }

          public ImmutableBuilder setOutputAlphabet(List<DataOutputActionType> actionTypes ) {
              this.outputalphabet= ImmutableSet.copyOf(actionTypes);
              for (DataOutputActionType outputType : actionTypes) {
                  name2output.put(outputType.getName(), outputType);
              }
              return this;
           }

          public ImmutableBuilder setOutputAlphabet(DataOutputActionType... actionTypes ) {
              setOutputAlphabet(Arrays.asList(actionTypes));
              return this;
          }


          public ImmutableBuilder setHiddenAlphabet(List<DataHiddenActionType> actionTypes ) {
              this.hiddenalphabet= ImmutableSet.copyOf(actionTypes);
              for (DataHiddenActionType outputType : actionTypes) {
                  name2hidden.put(outputType.getName(), outputType);
              }
              return this;
           }

          public ImmutableBuilder setHiddenAlphabet(DataHiddenActionType... actionTypes ) {
              setHiddenAlphabet(Arrays.asList(actionTypes));
              return this;
          }



        private Pair<String,List<String>> parseAction(String actionString) {
            int startIndex,endIndex;
            startIndex=actionString.indexOf("(");
            endIndex=actionString.indexOf(")");
            String name =actionString.substring(0, startIndex).trim();
            String[] params=actionString.substring(startIndex+1, endIndex).trim().split("\\s*,\\s*");
            return new Pair<>(name,Arrays.asList(params));
        }

        private String parseActionName(String actionString) {
            int startIndex=actionString.indexOf("(");
            String name =actionString.substring(0, startIndex).trim();
            return name;
        }

        private SymbolicInputAction parseInput(String inputStr) {
            Pair<String,List<String>> name_params=parseAction(inputStr);
            DataInputActionType actionType=name2input.get(name_params.getValue0());
            SymbolicInputAction input = new SymbolicInputAction(actionType, name_params.getValue1() );
            return input;
        }

        private SymbolicOutputAction parseOutput(String inputStr) {
            Pair<String,List<String>> name_params=parseAction(inputStr);
            DataOutputActionType actionType=name2output.get(name_params.getValue0());
            SymbolicOutputAction input = new SymbolicOutputAction(actionType, name_params.getValue1() );
            return input;
        }

        private SymbolicHiddenAction parseHidden(String hiddenStr) {
            Pair<String,List<String>> name_params=parseAction(hiddenStr);
            DataHiddenActionType actionType=name2hidden.get(name_params.getValue0());
            SymbolicHiddenAction hidden = new SymbolicHiddenAction(actionType, name_params.getValue1() );
            return hidden;
        }


        // assumes initially no statevars set
        public ImmutableBuilder setStartState(Location startLocation) {
            this.startState = new DataLocationState(startLocation); // initially no statevars set
            return this;
        }

        public ImmutableBuilder setStartState(DataLocationState startState) {
            this.startState = startState;
            return this;
        }

        public ImmutableBuilder setStartState(Location startLocation, HashMap<VariableSymbol, Value> statevars) {
            this.startState = new DataLocationState(startLocation,statevars);
            return this;
        }


        public ImmutableBuilder setConstants(Map<VariableSymbol,Value>  constants) {
            this.constants =  ImmutableMap.<VariableSymbol,Value>copyOf(constants);
            return this;
        }


        public ImmutableBuilder addFromIaModel(IaModel model) {
            for (SymbolicInputTransition trans : model.getModelInputTransitions()) {
                this.addInputTransition(trans.getSource(), trans.getDestination(), trans.getInput());
            }
            for (SymbolicOutputTransition trans : model.getModelOutputTransitions()) {
                this.addOutputTransition(trans.getSource(), trans.getDestination(), trans.getOutput());
            }
            for (SymbolicHiddenTransition trans : model.getModelHiddenTransitions()) {
                this.addHiddenTransition(trans.getSource(), trans.getDestination(), trans.getHiddenAction());
            }
            this.setStartState(model.getStartState());
            this.setConstants(model.getConstants());
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            //this.inputalphabet = inputalphabetBuilder.build();
            //this.outputalphabet = outputalphabetBuilder.build();
            // no builder for this.startState & this.constants => immediately set immutable
            this.locations = locationsBuilder.build();
            this.inputTransitions = inputTransitionsBuilder.build();
            this.loc2inputtrans = loc2inputTransBuilder.build();
            this.outputTransitions = outputTransitionsBuilder.build();
            this.loc2outputtrans = loc2outputTransBuilder.build();
            this.hiddenTransitions = hiddenTransitionsBuilder.build();
            this.loc2hiddentrans = loc2hiddenTransBuilder.build();
        }
    }

    // Constructor

    protected IaBaseModel(ImmutableBuilder builder) {
        this.inputalphabet = builder.inputalphabet;
        this.outputalphabet = builder.outputalphabet;
        this.hiddenalphabet = builder.hiddenalphabet;
        this.startState = builder.startState;
        this.constants = builder.constants;
        this.locations = builder.locations;
        this.inputTransitions = builder.inputTransitions;
        this.loc2inputtrans = builder.loc2inputtrans;
        this.outputTransitions = builder.outputTransitions;
        this.loc2outputtrans = builder.loc2outputtrans;
        this.hiddenTransitions = builder.hiddenTransitions;
        this.loc2hiddentrans = builder.loc2hiddentrans;
    }





    public ImmutableMap<VariableSymbol,Value> getConstants() {
        return this.constants;
    }


    // interface InitialState

    @Override
    public @NonNull Location getStartLocation() {
        return startState.getLocation();
    }

    @Override
    public @NonNull DataLocationState getStartState() {
        return startState;
    }

    // interface ModelAutomaton

    @Override
    public ImmutableSet<Location> getLocations() {
        return locations;
    }


    @Override
    public ImmutableSet<BaseTransition<Location>> getModelTransitions() {
        HashSet<BaseTransition<Location>> set = new HashSet<>();
        set.addAll(inputTransitions);
        set.addAll(outputTransitions);
        set.addAll(hiddenTransitions);
        return ImmutableSet.copyOf(set);
    }

    @Override
    public ImmutableSet<BaseTransition<Location>> getModelTransitions(Location location) {
        ImmutableSet.Builder<BaseTransition<Location>> transitionsBuilder=ImmutableSet.builder();
        transitionsBuilder.addAll(loc2inputtrans.get(location));
        transitionsBuilder.addAll(loc2outputtrans.get(location));
        transitionsBuilder.addAll(loc2hiddentrans.get(location));
        return transitionsBuilder.build();
    }


    // interface ModelInterfaceAutomata



    @Override
    public ImmutableSet<DataInputActionType> getInputAlphabet() {
        return inputalphabet;
    }

    @Override
    public ImmutableSet<DataOutputActionType> getOutputAlphabet() {
        return outputalphabet;
    }

    @Override
    public ImmutableSet<DataHiddenActionType> getHiddenAlphabet() {
        return hiddenalphabet;
    }


    @Override
    public ImmutableSet<SymbolicInputTransition> getModelInputTransitions() {
        return inputTransitions;
    }

    @Override
    public ImmutableSet<SymbolicInputTransition> getModelInputTransitions(Location location) {
        return loc2inputtrans.get(location);
    }


    @Override
    public ImmutableSet<SymbolicOutputTransition> getModelOutputTransitions() {
        return outputTransitions;
    }

    @Override
    public ImmutableSet<SymbolicOutputTransition> getModelOutputTransitions(Location location) {
        return loc2outputtrans.get(location);
    }

    @Override
    public ImmutableSet<SymbolicHiddenTransition> getModelHiddenTransitions() {
        return hiddenTransitions;
    }

    @Override
    public ImmutableSet<SymbolicHiddenTransition> getModelHiddenTransitions(Location location) {
        return loc2hiddentrans.get(location);
    }




    /** get input transitions for an input type **/
    @Override
    public @NonNull ImmutableSet<@NonNull SymbolicInputTransition> getModelInputTransitions(Location location, DataInputActionType action) {
        ImmutableSet.Builder<SymbolicInputTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull SymbolicInputTransition> transitions = loc2inputtrans.get(location);

        for (SymbolicInputTransition transition : transitions) {
            if (transition.getInput().getType().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }

    /** get output transitions for an output type **/
    @Override
    public @NonNull ImmutableSet<@NonNull SymbolicOutputTransition> getModelOutputTransitions(Location location, DataOutputActionType action) {
        ImmutableSet.Builder<SymbolicOutputTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull SymbolicOutputTransition> transitions = loc2outputtrans.get(location);

        for (SymbolicOutputTransition transition : transitions) {
            if (transition.getOutput().getType().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }

    /** get hidden transitions for an hidden type **/
    @Override
    public @NonNull ImmutableSet<@NonNull SymbolicHiddenTransition> getModelHiddenTransitions(Location location, DataHiddenActionType action) {
        ImmutableSet.Builder<SymbolicHiddenTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull SymbolicHiddenTransition> transitions = loc2hiddentrans.get(location);

        for (SymbolicHiddenTransition transition : transitions) {
            if (transition.getHiddenAction().getType().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }






    // operational semantics
    //----------------------
    //  - transition within system
    //  - idle time passing (decrementing timers)

    /** evaluate a transition
          evaluate the timer update and return new timers and location State together with Output of transition evaluated
          => works for both  timeout transition as normal input transition
     **/




   // interface ModelExtendedInterfaceAutomaton
   //----------------------------------


    @Override
    public boolean isEnabledInputTransition(
            DataLocationState state, ConcreteInputAction input, SymbolicInputTransition inputTransition) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public boolean isEnabledOutputTransition(DataLocationState state, SymbolicOutputTransition outputTransition) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public boolean isEnabledHiddenTransition(
            DataLocationState state, ConcreteHiddenAction hiddenAction, SymbolicHiddenTransition hiddenTransition) {
        // TODO Auto-generated method stub
        return false;
    }





    @Override
    public DataLocationState evaluateInputTransition(
            DataLocationState state, ConcreteInputAction input, SymbolicInputTransition inputTransition) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<DataLocationState, ConcreteOutputAction> evaluateOutputTransition(
            DataLocationState state, SymbolicOutputTransition outputTransition) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DataLocationState evaluateHiddenTransition(
            DataLocationState state, ConcreteHiddenAction hiddenAction, SymbolicHiddenTransition hiddenTransition) {
        // TODO Auto-generated method stub
        return null;
    }







    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("start state: " + startState);
        joiner.add("constants: " + constants);
        joiner.add("inputAlphabet: " + inputalphabet);
        joiner.add("outputAlphabet: " + outputalphabet);
        joiner.add("hiddenAlphabet: " + hiddenalphabet);
        joiner.add("locations:");
        for (Location location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (SymbolicInputTransition transition : getModelInputTransitions()) {
            joiner.add(transition.toString());
        }
        for (SymbolicOutputTransition transition : getModelOutputTransitions()) {
            joiner.add(transition.toString());
        }
        for (SymbolicHiddenTransition transition : getModelHiddenTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }



}

