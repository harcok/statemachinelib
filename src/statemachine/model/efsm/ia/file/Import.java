package statemachine.model.efsm.ia.file;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXB;

import org.antlr.symtab.PrimitiveType;
import org.antlr.symtab.Type;
import org.antlr.symtab.VariableSymbol;

import exceptionslib.FileIOException;
import statemachine.model.efsm.ia.IaNonDetModel;
import statemachine.model.efsm.ia.conversion.LtsZip;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Alphabet.Inputs;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Alphabet.Outputs;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Constants.Constant;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Globals;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Globals.Variable;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Locations;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Transitions.Transition;
import statemachine.model.efsm.ia.file.RegisterAutomaton.Transitions.Transition.Assignments.Assign;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.DataLocationState;
import statemachine.model.elements.location.Location;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class Import {

    static public IaNonDetModel  dot2IaModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.dot2LtsModel( filename);
           return LtsZip.unzipLts2Ia(lts);
    }

    static public IaNonDetModel  aldebaran2IaModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel( filename);
           return LtsZip.unzipLts2Ia(lts);
    }

    static public IaNonDetModel  gml2IaModel( String filename){
           LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel( filename);
           return LtsZip.unzipLts2Ia(lts);
    }

    static public IaNonDetModel  register2IaModel( String filename){

        PrimitiveType integer = new PrimitiveType("INT");
        HashMap<String,PrimitiveType> name2type= new HashMap<>();
        name2type.put("int", integer);
        name2type.put("integer", integer);

        RegisterAutomaton register;
        try {
             //Charset ENCODING = StandardCharsets.UTF_8;
             Path path = Paths.get(filename);
             InputStream is=Files.newInputStream(path);
             register =  JAXB.unmarshal(is, RegisterAutomaton.class);
        } catch (Exception e) {
            throw new FileIOException(e).addDecoration("filename",filename );
        }

        IaNonDetModel.ImmutableBuilder builder =new IaNonDetModel.ImmutableBuilder();

        LinkedList<DataInputActionType> inputTypes= new LinkedList<>();
        for ( Inputs.Symbol symbol:register.getAlphabet().getInputs().getSymbol() ) {
            String actionname=symbol.getName();
            LinkedList<Type> actiontypes= new LinkedList<>();
            for ( Inputs.Symbol.Param param: symbol.getParam() ) {
                actiontypes.add(name2type.get(param.getType()));
            }
            inputTypes.add( new DataInputActionType(actionname,actiontypes ));
        }
        builder.setInputAlphabet(inputTypes);

        LinkedList<DataOutputActionType> outputTypes= new LinkedList<>();
        for ( Outputs.Symbol symbol:register.getAlphabet().getOutputs().getSymbol() ) {
            String actionname=symbol.getName();
            LinkedList<Type> actiontypes= new LinkedList<>();
            for ( Outputs.Symbol.Param param: symbol.getParam() ) {
                actiontypes.add(name2type.get(param.getType()));
            }
            outputTypes.add( new DataOutputActionType(actionname,actiontypes ));
        }
        builder.setOutputAlphabet(outputTypes);

        HashMap<VariableSymbol,Value> constants = new HashMap<>();
        for  ( Constant constant :register.getConstants().getConstant() ) {
            constants.put(new VariableSymbol(constant.getName()),
                    new Value(name2type.get(constant.getType()), constant.getValue()));
        }
        builder.setConstants(constants);

        HashMap<VariableSymbol,Value> statevars = new HashMap<>();
        for ( Variable variable: register.getGlobals().getVariable()  ) {
            statevars.put(new VariableSymbol(variable.getName()),
                          new Value(name2type.get(variable.getType()), variable.getValue()));
        }
        Location startLocation=null;
        for ( Locations.Location location : register.getLocations().getLocation() ) {
            String initial=location.getInitial();
            if ( initial != null && initial.equals("true") ) {
                startLocation = new Location(location.getName());
                break;
            }
        }
        if (startLocation ==null) {
            //TODO: improve missing start state
            System.err.println("start state missing");
        }
        DataLocationState startState= new DataLocationState(startLocation,statevars);
        builder.setStartState(startState);

        for (  Transition transition: register.getTransitions().getTransition() ) {
            String from=transition.getFrom();
            String to = transition.getTo();
            String guard = transition.getGuard();
            String comma_separated_params = transition.getParams();
            if (comma_separated_params == null) comma_separated_params="";
           // String[] params=comma_separated_params.split("\\s*,\\s*");
            List<String> updates = new LinkedList<>();
            if ( transition.getAssignments() != null) {
                for ( Assign assign: transition.getAssignments().getAssign() ) {
                    updates.add(assign.getTo() + "=" + assign.getValue() + ";");
                }
            }
            String update=String.join("", updates);
            String actionname=transition.getSymbol();

            builder.addTransition(new Location(from), new Location(to),
                    actionname+"("+comma_separated_params+")",guard,update);


        }
        return builder.build();
    }


}
