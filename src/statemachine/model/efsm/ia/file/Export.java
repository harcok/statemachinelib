package statemachine.model.efsm.ia.file;

import statemachine.model.efsm.ia.IaModel;
import statemachine.model.efsm.ia.conversion.LtsZip;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class Export {

    static public void  dot(IaModel model, String filename){
        LtsNonDetModel lts=LtsZip.zipIa2Lts(model);
        statemachine.model.fsm.lts.file.Export.dot(lts,filename);
    }

    static public void  gml(IaModel model, String filename){
        LtsNonDetModel lts=LtsZip.zipIa2Lts(model);
        statemachine.model.fsm.lts.file.Export.gml(lts,filename);
    }

    static public void  aldebaran(IaModel model, String filename){
        LtsNonDetModel lts=LtsZip.zipIa2Lts(model);
        statemachine.model.fsm.lts.file.Export.aldebaran(lts,filename);
    }


}
