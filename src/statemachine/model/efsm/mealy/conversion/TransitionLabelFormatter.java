package statemachine.model.efsm.mealy.conversion;

import java.util.Optional;

import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;


@FunctionalInterface
public interface TransitionLabelFormatter {
    public String apply(SymbolicInputAction input, SymbolicOutputAction output, Optional<String> guard, Optional<String> update);

}
