package statemachine.model.efsm.mealy.conversion;

import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.efsm.mealy.conversion.TransitionLabelParser;

import java.util.Arrays;
import java.util.List;

import org.antlr.symtab.PrimitiveType;
import org.antlr.symtab.Type;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Sextet;
import org.javatuples.Tuple;

import statemachine.model.efsm.mealy.MealyModel;
import statemachine.model.efsm.mealy.MealyNonDetModel;
//import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.efsm.mealy.conversion.TransitionLabelFormatter;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.SymbolicMealyTransition;

public class LtsZip {


    static public TransitionLabelFormatter defaultTransitionLabelFormatter =
            ( input, output, guard, update ) -> {
                String guardStr ="", updateStr="";
                if ( update.isPresent() ) {
                    updateStr = "{" + update.get() + "}";
                }
                if ( guard.isPresent() ) {
                    guardStr = "[" + guard.get() + "]";
                }
                return input.asString() +  guardStr + "/" + updateStr+ output.asString();
            };




    static public TransitionLabelParser defaultTransitionLabelParser = new TransitionLabelParser() {
        public Quartet<String,String, String, String> apply(String label) {

            String inputStr,outputStr,update="",guard="";
            int startIndex,endIndex;
            String[] parts = label.split("/");
            String input_guard = parts[0];
            String update_output = parts[1];


            if ( input_guard.contains("[") ) {
                startIndex=input_guard.indexOf("[");
                endIndex=input_guard.indexOf("]");
                guard=input_guard.substring(startIndex+1, endIndex).trim();
                inputStr=input_guard.substring(0,startIndex).trim();
            } else {
                inputStr=input_guard.trim();
            }

            if ( update_output.contains("{") ) {
                startIndex=update_output.indexOf("{");
                endIndex=update_output.indexOf("}");
                update=update_output.substring(startIndex+1, endIndex).trim();
                outputStr=update_output.substring(endIndex+1).trim();
            } else {
                outputStr=update_output.trim();
            }
            return new Quartet<>(inputStr, outputStr,guard,update);
        }
    };


    public static LtsNonDetModel zipMealy2Lts(MealyModel model, TransitionLabelFormatter transitionFormatter) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

        for (SymbolicMealyTransition trans : model.getModelTransitions()) {
            String label = transitionFormatter.apply(trans.getInput(),trans.getOutput(),trans.getGuard(),trans.getUpdate());
            Action action = new Action(label);
            builder.addTransition(new LocationState(trans.getSource().getName()), new LocationState(trans.getDestination().getName()), action);
        }
        builder.setStartLocation(new LocationState(model.getStartLocation().getName()));
        return builder.build();
    }

    static public LtsNonDetModel zipMealy2Lts(MealyModel model) {
        return zipMealy2Lts(model, defaultTransitionLabelFormatter);
    }

    static public MealyNonDetModel unzipLts2Mealy(LtsModel model, TransitionLabelParser labelParser) {
        MealyNonDetModel.ImmutableBuilder builder = new MealyNonDetModel.ImmutableBuilder();

        // TODO: parse json in dot file 's comment to get  input and output alphabet
        //        => use flexible parsing of types to "our" builtin primitive types:
        //              int,integer  -> int            `-> every evaluator should support them!! => aggreement of expression machine communication => machine needs to store state => needs same basic types!!
        PrimitiveType integer = new PrimitiveType("INT");
        DataInputActionType A =  new DataInputActionType("a",integer,integer);
        DataInputActionType B =  new DataInputActionType("b",integer);
        DataOutputActionType OUT =  new DataOutputActionType("OUT",integer,integer);
        DataOutputActionType OK =  new DataOutputActionType("OK",integer);


        builder.setInputAlphabet(A,B);
        builder.setOutputAlphabet(OUT,OK);


        for (ActionTransition trans : model.getModelTransitions()) {
            String label = trans.getAction().getName();
            Quartet<String,String , String, String>  input_output_guard_update = labelParser.apply(label);

            String input= input_output_guard_update.getValue0();
            String  output = input_output_guard_update.getValue1();
            String guard = input_output_guard_update.getValue2();
            String update = input_output_guard_update.getValue3();
            builder.addTransition(new Location(trans.getSource().getName()), new Location(trans.getDestination().getName()), input, output,guard,update);
        }

        // TODO: get initial statevars from dot file
        builder.setStartState(new Location(model.getStartLocation().getName()));
        // TODO: get constants from dot file
        return builder.build();
    }

    static public MealyNonDetModel unzipLts2Mealy(LtsModel model) {
        return unzipLts2Mealy(model, defaultTransitionLabelParser);
    }
}
