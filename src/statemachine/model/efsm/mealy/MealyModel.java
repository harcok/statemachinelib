package statemachine.model.efsm.mealy;



import org.antlr.symtab.VariableSymbol;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableMap;

import statemachine.interfaces.model.types.ModelExtendedMealy;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.ConcreteInputAction;
import statemachine.model.elements.action.ConcreteOutputAction;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.DataLocationState;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.transition.SymbolicMealyTransition;

@NonNullByDefault
public interface MealyModel extends ModelExtendedMealy<ConcreteInputAction,ConcreteOutputAction,DataLocationState,SymbolicInputAction, SymbolicOutputAction,DataInputActionType,DataOutputActionType, Location, SymbolicMealyTransition>,
        InitialState<Location, DataLocationState> {

    ImmutableMap<VariableSymbol,Value> getConstants();

}