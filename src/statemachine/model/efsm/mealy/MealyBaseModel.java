package statemachine.model.efsm.mealy;



import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.antlr.symtab.VariableSymbol;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.ConcreteInputAction;
import statemachine.model.elements.action.ConcreteOutputAction;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.DataLocationState;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.transition.SymbolicMealyTransition;

@NonNullByDefault
public  abstract class MealyBaseModel implements MealyModel {

    protected final ImmutableSet<DataInputActionType> inputalphabet;
    protected final ImmutableSet<DataOutputActionType> outputalphabet;
    private final DataLocationState startState;
    protected final ImmutableMap<VariableSymbol, Value> constants;
    private final ImmutableSet<Location> locations;
    private final ImmutableSet<SymbolicMealyTransition> transitions;
    protected final ImmutableSetMultimap<Location, SymbolicMealyTransition> loc2trans;


    // Builder class
    public static abstract class ImmutableBuilder {

        protected ImmutableSet<DataInputActionType> inputalphabet;
        protected ImmutableSet<DataOutputActionType> outputalphabet;
        //protected Location startLocation;
        protected DataLocationState startState;
        private ImmutableMap<VariableSymbol, Value> constants; // a constant is a variable which cannot be changed!
        protected ImmutableSet<Location> locations;
        protected ImmutableSet<SymbolicMealyTransition> transitions;
        protected ImmutableSetMultimap<Location, SymbolicMealyTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<Location> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<SymbolicMealyTransition> transitionsBuilder = ImmutableSet.builder();

//        @SuppressWarnings("null")
//        protected ImmutableSet.Builder<DataInputActionType> inputalphabetBuilder = ImmutableSet.builder();
//        @SuppressWarnings("null")
//        protected ImmutableSet.Builder<DataOutputActionType> outputalphabetBuilder = ImmutableSet.builder();


        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<Location, SymbolicMealyTransition> loc2transBuilder = ImmutableSetMultimap.builder();


        private HashMap<String, DataInputActionType> name2input;
        private HashMap<String, DataOutputActionType> name2output;


        public ImmutableBuilder addTransition(Location src, Location dst, SymbolicInputAction input, SymbolicOutputAction output, String guard,String update) {
            this.locationsBuilder.add(src, dst);
//            this.inputalphabetBuilder.add(input.getType());
//            this.outputalphabetBuilder.add(output.getType());
            SymbolicMealyTransition transition = new SymbolicMealyTransition(src, dst, input, output,guard,update);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }
        public ImmutableBuilder addTransition(Location src, Location dst, SymbolicInputAction input, SymbolicOutputAction output) {
            this.addTransition(src,dst,input,output,null,null);
            return this;
        }

        public ImmutableBuilder addTransition(Location src, Location dst, SymbolicInputAction input, SymbolicOutputAction output, String guard) {
            this.addTransition(src,dst,input,output,guard,null);
            return this;
        }



        public ImmutableBuilder addTransition(Location src, Location dst, String inputStr, SymbolicOutputAction output, String guard,String update) {

            SymbolicInputAction input = parseInput(inputStr);


            this.locationsBuilder.add(src, dst);
//            this.inputalphabetBuilder.add(input.getType());
//            this.outputalphabetBuilder.add(output.getType());
            SymbolicMealyTransition transition = new SymbolicMealyTransition(src, dst, input, output,guard,update);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }


        public ImmutableBuilder addTransition(Location src, Location dst, String inputStr, String outputStr, String guard,String update) {

            SymbolicInputAction input = parseInput(inputStr);

            SymbolicOutputAction output = parseOutput(outputStr);

            this.locationsBuilder.add(src, dst);
//            this.inputalphabetBuilder.add(input.getType());
//            this.outputalphabetBuilder.add(output.getType());
            SymbolicMealyTransition transition = new SymbolicMealyTransition(src, dst, input, output,guard,update);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }

        /*
        public ImmutableBuilder setInputAlphabet(List<DataInputActionType> actionTypes ) {

           // ImmutableSet.Builder<DataInputActionType> alphabetBuilder = ImmutableSet.builder();
           // alphabetBuilder


            this.inputalphabet = ImmutableSet.copyOf(actionTypes);
            this.name2input = new HashMap<String,DataInputActionType>();
            return this;
        }
        */

        public ImmutableBuilder setInputAlphabet(List<DataInputActionType> actionTypes ) {
          this.inputalphabet= ImmutableSet.copyOf(actionTypes);
          this.name2input = new HashMap<String,DataInputActionType>();
          for (DataInputActionType inputType : actionTypes) {
              name2input.put(inputType.getName(), inputType);
          }
          return this;
        }

        public ImmutableBuilder setInputAlphabet(DataInputActionType... actionTypes ) {
            setInputAlphabet(Arrays.asList(actionTypes));
            return this;
        }

        public ImmutableBuilder setOutputAlphabet(List<DataOutputActionType> actionTypes ) {
            this.outputalphabet= ImmutableSet.copyOf(actionTypes);
            this.name2output = new HashMap<String,DataOutputActionType>();
            for (DataOutputActionType outputType : actionTypes) {
                name2output.put(outputType.getName(), outputType);
            }
            return this;
         }

        public ImmutableBuilder setOutputAlphabet(DataOutputActionType... actionTypes ) {
            setOutputAlphabet(Arrays.asList(actionTypes));
            return this;
        }



        private Pair<String,List<String>> parseAction(String actionString) {
            int startIndex,endIndex;
            startIndex=actionString.indexOf("(");
            endIndex=actionString.indexOf(")");
            String name =actionString.substring(0, startIndex).trim();
            String[] params=actionString.substring(startIndex+1, endIndex).trim().split("\\s*,\\s*");
            return new Pair<>(name,Arrays.asList(params));
        }

        private SymbolicInputAction parseInput(String inputStr) {
            Pair<String,List<String>> name_params=parseAction(inputStr);
            DataInputActionType actionType=name2input.get(name_params.getValue0());
            SymbolicInputAction input = new SymbolicInputAction(actionType, name_params.getValue1() );
            return input;
        }

        private SymbolicOutputAction parseOutput(String inputStr) {
            Pair<String,List<String>> name_params=parseAction(inputStr);
            DataOutputActionType actionType=name2output.get(name_params.getValue0());
            SymbolicOutputAction input = new SymbolicOutputAction(actionType, name_params.getValue1() );
            return input;
        }


        // assumes initially no statevars set
        public ImmutableBuilder setStartState(Location startLocation) {
            this.startState = new DataLocationState(startLocation); // initially no statevars set
            return this;
        }

        public ImmutableBuilder setStartState(DataLocationState startState) {
            this.startState = startState;
            return this;
        }

        public ImmutableBuilder setStartState(Location startLocation, HashMap<VariableSymbol, Value> statevars) {
            this.startState = new DataLocationState(startLocation,statevars);
            return this;
        }


        public ImmutableBuilder setConstants(Map<VariableSymbol,Value>  constants) {
            this.constants =  ImmutableMap.<VariableSymbol,Value>copyOf(constants);
            return this;
        }


        public ImmutableBuilder addFromMealyModel(MealyModel model) {
            for (SymbolicMealyTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination(), trans.getInput(), trans.getOutput());
            }
            this.setStartState(model.getStartState());
            this.setConstants(model.getConstants());
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            //this.inputalphabet = inputalphabetBuilder.build();
            //this.outputalphabet = outputalphabetBuilder.build();
            // no builder for this.startState & this.constants => immediately set immutable
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }
    }

    // Constructor

    protected MealyBaseModel(ImmutableBuilder builder) {
        this.inputalphabet = builder.inputalphabet;
        this.outputalphabet = builder.outputalphabet;
        this.startState = builder.startState;
        this.constants = builder.constants;
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }





    public ImmutableMap<VariableSymbol,Value> getConstants() {
        return this.constants;
    }


    // interface InitialState

    @Override
    public @NonNull Location getStartLocation() {
        return startState.getLocation();
    }

    @Override
    public @NonNull DataLocationState getStartState() {
        return startState;
    }

    // interface ModelAutomaton

    @Override
    public ImmutableSet<Location> getLocations() {
        return locations;
    }

    @Override
    public ImmutableSet<SymbolicMealyTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public ImmutableSet<SymbolicMealyTransition> getModelTransitions(Location location) {
        return loc2trans.get(location);
    }

    // interface ModelMealy

    @Override
    public ImmutableSet<DataInputActionType> getInputAlphabet() {
        return inputalphabet;
    }

    @Override
    public ImmutableSet<DataOutputActionType> getOutputAlphabet() {
        return outputalphabet;
    }




    /** get mealy transitions for an input type **/
    @Override
    public @NonNull ImmutableSet<@NonNull SymbolicMealyTransition> getModelTransitions(Location location, DataInputActionType action) {
        ImmutableSet.Builder<SymbolicMealyTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull SymbolicMealyTransition> transitions = loc2trans.get(location);

        for (SymbolicMealyTransition transition : transitions) {
            if (transition.getInput().getType().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }




    // operational semantics
    //----------------------
    //  - transition within system
    //  - idle time passing (decrementing timers)

    /** evaluate a transition
          evaluate the timer update and return new timers and location State together with Output of transition evaluated
          => works for both  timeout transition as normal input transition
     **/




    @Override
    public boolean isEnabledTransition(DataLocationState state, ConcreteInputAction input, SymbolicMealyTransition transition) {
        // TODO Auto-generated method stub
        return false;
    }





    @Override
    public Pair<DataLocationState, ConcreteOutputAction> evaluateTransition(
            DataLocationState state, ConcreteInputAction input, SymbolicMealyTransition transition) {
        // TODO Auto-generated method stub
        return null;
    }


    /*
    @Override
    public Pair<DataLocationState,ConcreteOutputAction> evaluateTransition( DataLocationState state, SymbolicMealyTransition  transition ){

        Location src=state.getLocation();
        if ( src != transition.getSource() ) {
            // throw exception
            new RuntimeException("error: given transition for other location then location given in current state");
        }

        HashSet<Timer> timers=new HashSet<>(state.getTimers());

        // in case of timer update  do update timers set
        if (transition.hasUpdate()) {

            String updateExpression= transition.getUpdate().get();

            newValues

        }

        Location dest=transition.getDestination();
        DataLocationState newState = new DataLocationState(dest,newValues);

        SymbolicOutputAction output=transition.getOutput();




        return new Pair<>(newState,output) ;
     }


*/



    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("start state: " + startState);
        joiner.add("constants: " + constants);
        joiner.add("inputAlphabet: " + inputalphabet);
        joiner.add("outputAlphabet: " + outputalphabet);
        joiner.add("locations:");
        for (Location location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (SymbolicMealyTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }





}
