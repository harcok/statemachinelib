package statemachine.model.efsm.mealy;

import java.text.MessageFormat;
import java.util.HashMap;

import org.antlr.symtab.VariableSymbol;

import statemachine.model.efsm.ia.IaNonDetModel;
import statemachine.model.efsm.ia.conversion.Conversion;
import statemachine.model.efsm.mealy.file.Export;
import statemachine.model.elements.action.DataInputActionType;
import statemachine.model.elements.action.DataOutputActionType;
import statemachine.model.elements.action.Value;
import statemachine.model.elements.location.Location;

public class MealyNonDetModel extends MealyBaseModel{


    //Builder class
    public static final class ImmutableBuilder extends MealyBaseModel.ImmutableBuilder {

        public MealyNonDetModel build(){
            super.baseBuild();
            return new MealyNonDetModel(this);
        }

    }

    // Constructor
    private MealyNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from MealyModel
    static public MealyNonDetModel fromMealyModel(MealyModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromMealyModel(model);
        return builder.build();
    }




    static public void printStatistics(String filename, String name) {


        IaNonDetModel iaModel=statemachine.model.efsm.ia.file.Import.register2IaModel(filename);
        //System.out.println(iaModel.toString());

        MealyNonDetModel mealyModel=Conversion.IaModel2MealyModel( iaModel);
       //  System.out.println(mealyModel.toString());
       // Export.dot(mealyModel, "mealyModel.dot");

        printStatistics(mealyModel,  name);
    }

    static int counter=0;
    static public void printStatistics(MealyNonDetModel mealyModel, String name) {



        int num_locations=mealyModel.getLocations().size();
        int num_inputs=mealyModel.getInputAlphabet().size();
        int num_outputs=mealyModel.getOutputAlphabet().size();
        int num_trans=mealyModel.getModelTransitions().size();


        int num_constants=mealyModel.constants.size();
        int num_statevars=mealyModel.getStartState().getValues().size();
        int num_inparams=0,num_outparams=0;
        for (DataInputActionType type : mealyModel.getInputAlphabet() ) {
            num_inparams=num_inparams+ type.getParameterTypes().size();
        }
        for (DataOutputActionType type : mealyModel.getOutputAlphabet() ) {
            num_outparams=num_outparams+ type.getParameterTypes().size();
        }



//        String header=  "       states   inputs/outputs   transitions  constants  registers  inparams/outparams ";
//        String format = "          {1}          {2}/{3}             {4}            {5}         {6}          {7}/{8}";
//        String result= MessageFormat.format(format,"",num_locations,num_inputs,num_outputs,num_trans,num_constants,num_statevars,num_inparams,num_outparams);
        //   System.out.println(name);
        //   System.out.println(header);
        //   System.out.println(result);


        //text// String formatRow= "%1$-40.40s  %2$10.10s  %3$10.10s/%4$-10.10s  %5$10.10s  %6$10.10s  %7$10.10s  %8$10.10s/%9$-10.10s";
        //text// String formatHeader=formatRow;

        //pmwiki
        String formatRow= "||%1$-40.40s ||  %2$10.10s || %3$10.10s/%4$-10.10s ||  %5$10.10s ||  %6$10.10s ||  %7$10.10s || %8$10.10s/%9$-10.10s ||";
        String formatHeader= "||!%1$-40.40s ||!  %2$10.10s ||! %3$10.10s/%4$-10.10s ||!  %5$10.10s ||!  %6$10.10s ||!  %7$10.10s ||! %8$10.10s/%9$-10.10s ||";

        //latex// String formatRow= "%1$-40.40s & %2$10.10s & %3$10.10s & / & %4$-10.10s & %5$10.10s & %6$10.10s & %7$10.10s & %8$10.10s & / & %9$-10.10s \\\\";
        //latex// String formatHeader=formatRow;

        if ( (counter % 10000) == 0 )
            System.out.println(String.format(formatHeader,"model","states","inputs","outputs","transitions", "constants",  "registers",  "inparams", "outparams"));
        System.out.println(String.format(formatRow,name,num_locations,num_inputs,num_outputs,num_trans,num_constants,num_statevars,num_inparams,num_outparams));


        counter++;

    }

    public static void printSimpleStatistics(String filename) {
        // register xml file is in ia format
        IaNonDetModel iaModel=statemachine.model.efsm.ia.file.Import.register2IaModel(filename);
        MealyNonDetModel mealyModel=Conversion.IaModel2MealyModel( iaModel);

        int num_inputs=mealyModel.getInputAlphabet().size();
        int num_locations=mealyModel.getLocations().size();
        int num_outputs=mealyModel.getOutputAlphabet().size();
        int num_trans=mealyModel.getModelTransitions().size();

        int num_constants=mealyModel.constants.size();
        int num_statevars=mealyModel.getStartState().getValues().size();
        int num_inparams=0,num_outparams=0;
        for (DataInputActionType type : mealyModel.getInputAlphabet() ) {
            num_inparams=num_inparams+ type.getParameterTypes().size();
        }
        for (DataOutputActionType type : mealyModel.getOutputAlphabet() ) {
            num_outparams=num_outparams+ type.getParameterTypes().size();
        }


        String format= "locations:%1$-10.10s  inputs:%2$-10.10s outputs:%3$-10.10s  transitions:%4$-10.10s constants:%4$-10.10s statevars:%4$-10.10s inparams:%4$-10.10s outparams:%4$-10.10s ";
        System.out.println(String.format(format,num_locations,num_inputs,num_outputs,num_trans,num_constants,num_statevars,num_inparams,num_outparams));
    }
}