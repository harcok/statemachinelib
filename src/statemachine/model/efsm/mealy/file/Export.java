package statemachine.model.efsm.mealy.file;

import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.efsm.mealy.MealyModel;
import statemachine.model.efsm.mealy.conversion.LtsZip;

public class Export {

    static public void dot(MealyModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
        statemachine.model.fsm.lts.file.Export.dot(lts, filename);
    }

    static public void gml(MealyModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
        statemachine.model.fsm.lts.file.Export.gml(lts, filename);
    }

    static public void aldebaran(MealyModel model, String filename) {
        LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
        statemachine.model.fsm.lts.file.Export.aldebaran(lts, filename);
    }

}
