package statemachine.model.efsm.mealy.file;

import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.efsm.mealy.MealyNonDetModel;
import statemachine.model.efsm.mealy.conversion.LtsZip;

public class Import {

    static public MealyNonDetModel  dot2MealyModel( String filename){
        LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.dot2LtsModel( filename);
        return LtsZip.unzipLts2Mealy(lts);
    }


     static public MealyNonDetModel  aldebaran2MealyModel( String filename){
        LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel( filename);
        return LtsZip.unzipLts2Mealy(lts);
     }

     static public MealyNonDetModel  gml2MealyModel( String filename){
        LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel( filename);
        return LtsZip.unzipLts2Mealy(lts);
     }


}
