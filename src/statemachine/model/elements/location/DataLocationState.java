package statemachine.model.elements.location;

import java.util.Map;

import org.antlr.symtab.VariableSymbol;

import com.google.common.collect.ImmutableMap;

import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.model.elements.action.Value;

public final class DataLocationState implements SystemState {

	private final Location  location;
	private final ImmutableMap<VariableSymbol,Value> values;

	public DataLocationState( Location  location, Map<VariableSymbol,Value> values ) {
		this.location=location;
		this.values= ImmutableMap.<VariableSymbol,Value>copyOf(values);
	}

	// special case: no statevars initial set
    public DataLocationState( Location  location ) {
        this.location=location;
        this.values= ImmutableMap.<VariableSymbol,Value>of();
    }

	public Location getLocation() {
		return location;
	}

	public ImmutableMap<VariableSymbol,Value> getValues() {
        return values;
	}

    @Override
    public String toString() {
        return "[location=" + location + ", values=" + values + "]";
    }


}
