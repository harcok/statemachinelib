package statemachine.model.elements.location;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.system.statemarkers.SystemState;

/**
 * LocationState : implementation of location for machines without data where   location == state
 *                 so it is both a ModelLocation as SystemState
 *
 * note:
 * - implements ModelLocation marker interface which means it represents a location in the model
 * - implements SystemState marker interface which means each location represents state of the system
 *
 * note: we could have implemented LocationState by just wrapper Location, but that only adds an extra useless wrap
 *       which we better can omit.
 *
 * @author harcok
 *
 */
@NonNullByDefault
public final class LocationState extends BaseLocation implements  ModelLocation,SystemState {

    public LocationState(String name) {
        super( name);
    }


}

// ModelLocation,