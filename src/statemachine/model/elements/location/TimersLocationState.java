package statemachine.model.elements.location;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ImmutableSortedSet;

import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.model.elements.data.TimerVariable;

public class TimersLocationState implements SystemState {

    final private ImmutableSortedSet<Timer> timers;
    final private Location location;

    public TimersLocationState( Location  location, Set<Timer> timers ) {
        this.location=location;
        if (timers == null) {
            this.timers =  ImmutableSortedSet.<Timer>copyOf( new HashSet<>());
        } else {
            this.timers= ImmutableSortedSet.<Timer>copyOf(timers);
        }
    }

    public Location getLocation() {
        return location;
    }
    public ImmutableSortedSet<Timer> getTimers() {
        return timers;
    }


    public long getTimeUntilTimeout() {
        if ( timers.isEmpty() ) return -1;
        return timers.first().getTimeoutTime();
    }

    public Optional<Timer> getTimer(TimerVariable name) {
        for( Timer timer: timers) {
            if (timer.getName().equals(name)) {
                return Optional.of(timer);
            }
        }
        return Optional.empty();
    }

    /** get timer variables of timers which are timed out
     *
     * @return  Sorted Set of TimerVariable
     */
    public TreeSet<TimerVariable> getTimedOutTimers() {
        TreeSet<TimerVariable> timerVars = new TreeSet<>();
        for( Timer timer: timers) {
            if (timer.getTimeoutTime()==0) {
                timerVars.add(timer.getName());
            }
        }
        return timerVars;
    }

    // OLD: discards timers which have timoutTime <= 0
    public TimersLocationState getNewStateAfterDelay(long time) {
        // location doesn't change, but timers must be decremented
        HashSet<Timer> newtimers = new HashSet<>();
        for ( Timer timer : timers ) {
            long timeouttime=timer.getTimeoutTime()-time;
            if (timeouttime > 0) {
               newtimers.add(new Timer(timer.getName(),timeouttime));
            } // else remove timers
        }
        return new TimersLocationState(this.location,newtimers);
    }

    // NEW : discards timers which have timoutTime < 0
    public TimersLocationState getNewStateAfterIdleTime(long time) {
        // idle time -> location doesn't change, but timers must be decremented
        HashSet<Timer> newtimers = new HashSet<>();
        for ( Timer timer : timers ) {
            long timeouttime=timer.getTimeoutTime()-time;
            if (timeouttime >= 0) {
               newtimers.add(new Timer(timer.getName(),timeouttime));
            } // else remove timers
        }
        return new TimersLocationState(this.location,newtimers);
    }

    @Override
    public String toString() {
        return location.toString() + timers.toString();
    }

    public TimersLocationState removeTimedOutTimers() {
        // remove timers which are timeout (timedouttime==0 or <0)
        HashSet<Timer> newtimers = new HashSet<>();
        for ( Timer timer : timers ) {
            long timeouttime=timer.getTimeoutTime();
            if (timeouttime > 0) {
               newtimers.add(new Timer(timer.getName(),timeouttime));
            } // else remove timers
        }
        return new TimersLocationState(this.location,newtimers);
    }

}
