package statemachine.model.elements.location;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.locationmarkers.ModelLocation;

/**
 * Location : implementation of Model location for machines with data where  location != STATE
 *
 * note:
 * - implements ModelLocation marker interface which means it represents a location in the model
 *
 * @author harcok
 *
 */
@NonNullByDefault
public final class Location extends BaseLocation implements ModelLocation {

    public Location(String name) {
        super(name);
    }

}
