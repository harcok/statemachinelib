package statemachine.model.elements.location;

import statemachine.model.elements.data.TimerVariable;

public class Timer implements Comparable<Timer>{
     private TimerVariable name;
     private Long timeoutTime;



    public Timer(TimerVariable name, Long timeoutTime) {
        super();
        this.name = name;
        this.timeoutTime = timeoutTime;
    }

    public TimerVariable getName() {
        return name;
    }

    public Long getTimeoutTime() {
        return timeoutTime;
    }

    @Override
    public int compareTo(Timer o) {
        int compare=this.timeoutTime.compareTo(o.getTimeoutTime());
        if ( compare != 0) return compare;
        return name.compareTo(o.getName());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((timeoutTime == null) ? 0 : timeoutTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Timer other = (Timer) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (timeoutTime == null) {
            if (other.timeoutTime != null)
                return false;
        } else if (!timeoutTime.equals(other.timeoutTime))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name + "=" + timeoutTime;
    }

}
