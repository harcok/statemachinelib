package statemachine.model.elements.location;


import org.eclipse.jdt.annotation.NonNullByDefault;


/**
 *   Base type for two types of locations :
 *
 *    - LocationState  : special class for machines without data where location == STATE   (implements State marker interface)
 *    - Location       : for machines with data where location != STATE
 *
 * @author harcok
 *
 */
@NonNullByDefault
public abstract class BaseLocation  {

    final private String name;

    protected BaseLocation(String name) {
        this.name = name;
	}

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseLocation other = (BaseLocation) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return name;
    }

}
