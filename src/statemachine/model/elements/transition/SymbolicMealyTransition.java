package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelMealyTransition;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.location.Location;

@NonNullByDefault
public final class SymbolicMealyTransition extends BaseSymbolicTransition implements ModelMealyTransition<SymbolicInputAction,SymbolicOutputAction,Location>
{
    final private SymbolicInputAction input;
    final private SymbolicOutputAction output;

    public SymbolicMealyTransition(Location source, Location destination,SymbolicInputAction input,SymbolicOutputAction output, String guard, String update) {
        super(source, destination,guard,update);
        this.input = input;
        this.output = output;
    }

    @Override
    public SymbolicInputAction getInput() {
        return input;
    }

    @Override
    public SymbolicOutputAction getOutput() {
        return output;
    }

     // for debugging
     @Override
     public String getLabel() {
         String updateStr= "",guardStr="";
         if ( this.hasGuard() ) {
             guardStr= "[ " + guard +  " ]";
         }
         if ( this.hasUpdate() ) {
             updateStr= "{ " + update +  " }";
         }

         //  In(p0) [ p0 == x0 ] /  { x0 = 3; x1=p0; }  Ok(x0)
         //  In(p0) [p0 == x0]/{ x0 = 3; x1=p0;} Ok(x0)
         return  input.asString() + " " + guardStr  + '/'  +  updateStr + " " + output.asString() ;
     }
}
