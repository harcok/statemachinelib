package statemachine.model.elements.transition;

import java.util.Optional;

import statemachine.model.elements.location.Location;

public abstract class BaseSymbolicTransition extends  BaseTransition<Location> {

    public BaseSymbolicTransition(Location source, Location destination, String guard, String update) {
        super(source, destination);
        if (guard == null || guard.trim().equals("") ) {
            this.guard=null;
        } else {
            this.guard=guard.trim();
        }
        if (update == null ||update.trim().equals("") ) {
            this.update=null;
        } else {
            this.update=update.trim();
        }
    }

    protected final String guard;
    final protected String update;



    public boolean hasGuard() {
        return  guard!=null;
     }

     public Optional<String> getGuard() {

         if  (guard == null) {
             return Optional.empty();
         } else {
             return Optional.of(guard);
         }
     }


     public boolean hasUpdate() {
        return  update!=null;
     }

     public Optional<String> getUpdate() {

         if  (update == null) {
             return Optional.empty();
         } else {
             return Optional.of(update);
         }
     }


}
