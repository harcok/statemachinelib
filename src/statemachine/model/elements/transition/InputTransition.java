package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;

@NonNullByDefault
public final class InputTransition extends  BaseTransition<LocationState> implements ModelInputTransition<InputAction,LocationState>,statemachine.interfaces.system.transitionmarkers.SystemInputTriggeredTransition {


    final private InputAction action;

	public InputTransition(LocationState source, LocationState destination, InputAction action) {
		super(source, destination);
		this.action = action;
	}

	@Override
	public  InputAction getInput() {
		return action;
	}

    @Override
    public String getLabel() {
        return action.toString();
    }

}
