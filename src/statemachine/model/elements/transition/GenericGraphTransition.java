package statemachine.model.elements.transition;

import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;



public final class GenericGraphTransition<L extends ModelLocation>  implements ModelUndirectedTransition<L> {
    private L one;
    private L two;

    public GenericGraphTransition(L one, L two) {
        this.one=one;
        this.two=two;
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public Pair<L,L> getLocations() {
        return new Pair<>(one,two);
    }

    @Override
    public String toString() {
        String label=getLabel();
        String separator = " : ";
        if ( label.equals("") ) separator="";
        return one.toString() + " - " + two.toString() + separator + label;
    }

}