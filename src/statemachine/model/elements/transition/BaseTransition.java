package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;

@NonNullByDefault
public  class BaseTransition<L extends ModelLocation> implements ModelDirectedTransition<L> {

	final private L source,destination;

	public BaseTransition(L source, L destination) {
		super();
		this.source = source;
		this.destination = destination;
	}

	@Override
	public L getSource() {
		return source;
	}

	@Override
	public L getDestination() {
		return destination;
	}

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public Pair<L,L> getLocations() {
        return new Pair<>(source,destination);
    }

    @Override
    public String toString() {
        String label=getLabel();
        String separator = " : ";
        if ( label.equals("") ) separator="";
        return source.toString() + " -> " + destination.toString() + separator + label;
    }

}
