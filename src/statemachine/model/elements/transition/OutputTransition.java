
package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

@NonNullByDefault
public final class OutputTransition extends  BaseTransition<LocationState> implements ModelOutputTransition<OutputAction,LocationState>,statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition {


    final private OutputAction action;

	public OutputTransition(LocationState source, LocationState destination, OutputAction action) {
		super(source, destination);
		this.action = action;
	}

	@Override
	public  OutputAction getOutput() {
		return action;
	}

	@Override
	public String getLabel() {
		return action.toString();
	}

}

