package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelMealyTransition;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

@NonNullByDefault
public final class MealyTransition  extends  BaseTransition<LocationState> implements ModelMealyTransition<InputAction,OutputAction,LocationState> {


    final private InputAction input;

    final private OutputAction output;

	public MealyTransition(LocationState source, LocationState destination,InputAction input,OutputAction output) {
		super(source, destination);
		this.input = input;
		this.output = output;
	}

	@Override
	public InputAction getInput() {
		return input;
	}

	@Override
	public OutputAction getOutput() {
		return output;
	}


	@Override
	public String getLabel() {
		return  input.toString() + "/" + output.toString();
	}

}
