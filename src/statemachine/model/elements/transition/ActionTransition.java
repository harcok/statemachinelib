package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelActionTransition;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;

@NonNullByDefault
public final class ActionTransition extends  BaseTransition<LocationState> implements ModelActionTransition<Action,LocationState>,statemachine.interfaces.system.transitionmarkers.SystemActionTriggeredTransition {
    final private Action action;

	public ActionTransition(LocationState source, LocationState destination, Action action) {
		super(source, destination);
		this.action = action;
	}

	@Override
	public @NonNull Action getAction() {
		return action;
	}

    @Override
    public String getLabel() {
        return action.toString();
    }
}
