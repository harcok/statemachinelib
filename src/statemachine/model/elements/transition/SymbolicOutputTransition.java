package statemachine.model.elements.transition;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.model.elements.action.SymbolicOutputAction;
import statemachine.model.elements.location.Location;


@NonNullByDefault
public final class SymbolicOutputTransition   extends BaseSymbolicTransition implements ModelOutputTransition<SymbolicOutputAction,Location>
{
    final private SymbolicOutputAction output;

    public SymbolicOutputTransition(Location source, Location destination,SymbolicOutputAction output, String guard, String update) {
        super(source, destination,guard,update);
        this.output = output;
    }

    @Override
    public SymbolicOutputAction getOutput() {
        return output;
    }


     @Override
     public String getLabel() {
         String updateStr= "",guardStr="";
         if ( this.hasGuard() ) {
             guardStr= "[ " + guard +  " ] / ";
         }
         if ( this.hasUpdate() ) {
             updateStr= "{ " + update +  " }";
         }

         //   [ p0 == x0 ] /  { x0 = 3; x1=p0; }  Ok(x0)
         //   [p0 == x0]/{ x0 = 3; x1=p0;} Ok(x0)
         //   { x0 = 3; x1=p0;} Ok(x0)   => loose / when no guard
         return  guardStr   +  updateStr + " !" + output.asString() ;
     }

}
