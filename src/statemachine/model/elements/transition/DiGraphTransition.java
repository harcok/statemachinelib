package statemachine.model.elements.transition;

import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.model.elements.location.LocationState;

public final class DiGraphTransition  extends  BaseTransition<LocationState> implements ModelDirectedTransition<LocationState> {

    public DiGraphTransition(LocationState source, LocationState destination) {
        super(source, destination);
    }

    @Override
    public String getLabel() {
        return "";
    }

}
