
package statemachine.model.elements.transition;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelHiddenTransition;
import statemachine.model.elements.action.SymbolicHiddenAction;
import statemachine.model.elements.location.Location;


@NonNullByDefault
public final class SymbolicHiddenTransition  extends BaseSymbolicTransition implements ModelHiddenTransition<SymbolicHiddenAction,Location>
{
    final private SymbolicHiddenAction hiddenAction;

    public SymbolicHiddenTransition(Location source, Location destination,SymbolicHiddenAction hiddenAction, String guard, String update) {
        super(source, destination,guard,update);
        this.hiddenAction = hiddenAction;
    }

    @Override
    public SymbolicHiddenAction getHiddenAction() {
        return hiddenAction;
    }

     @Override
     public String getLabel() {
         String updateStr= "",guardStr="";
         if ( this.hasGuard() ) {
             guardStr= "[ " + guard +  " ]";
         }
         if ( this.hasUpdate() ) {
             updateStr= " / { " + update +  " }";
         }
         return  "_" + hiddenAction.asString() + " " + guardStr   +  updateStr ;
     }
}
