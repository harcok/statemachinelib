package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.model.elements.action.SymbolicInputAction;
import statemachine.model.elements.location.Location;




@NonNullByDefault
public final class SymbolicInputTransition  extends BaseSymbolicTransition implements ModelInputTransition<SymbolicInputAction,Location>
{
    final private SymbolicInputAction input;

    public SymbolicInputTransition(Location source, Location destination,SymbolicInputAction input, String guard, String update) {
        super(source, destination,guard,update);
        this.input = input;
    }

    @Override
    public SymbolicInputAction getInput() {
        return input;
    }


     @Override
     public String getLabel() {
         String updateStr= "",guardStr="";
         if ( this.hasGuard() ) {
             guardStr= "[ " + guard +  " ]";
         }
         if ( this.hasUpdate() ) {
             updateStr= " / { " + update +  " }";
         }

         //  ?In(p0) [ p0 == x0 ] /  { x0 = 3; x1=p0; }
         //  ?In(p0) [p0 == x0]/{ x0 = 3; x1=p0;}
         //  ?In(p0) [p0 == x0]         => loose / when no update
         return  "?" + input.asString() + " " + guardStr   +  updateStr ;
     }
}
