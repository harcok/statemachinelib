package statemachine.model.elements.transition;

import org.javatuples.Pair;

import statemachine.interfaces.model.transitions.ModelUndirectedTransition;
import statemachine.model.elements.location.LocationState;

public final class GraphTransition  implements ModelUndirectedTransition<LocationState> {
    private LocationState one;
    private LocationState two;

    public GraphTransition(LocationState one, LocationState two) {
        this.one=one;
        this.two=two;
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public Pair<LocationState,LocationState> getLocations() {
        return new Pair<>(one,two);
    }

    @Override
    public String toString() {
        String label=getLabel();
        String separator = " : ";
        if ( label.equals("") ) separator="";
        return one.toString() + " - " + two.toString() + separator + label;
    }

}