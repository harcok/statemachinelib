package statemachine.model.elements.transition;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.interfaces.model.transitions.ModelMealyTransition;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.data.TimerVariable;
import statemachine.model.elements.location.Location;
import statemachine.model.elements.location.Timer;


@NonNullByDefault
public final class MealyTimerTransition  extends  BaseTransition<Location> implements ModelMealyTransition<InputAction,OutputAction,Location> {

    private static final InputAction timeout=new InputAction("timeout");

    final private InputAction input;
    final private OutputAction output;
    final private TimerVariable timedOutTimer;
    final private String update;

    /* normal Mealy transition with timer update */
    public MealyTimerTransition(Location source, Location destination,InputAction input,OutputAction output,String update) {
        super(source, destination);

        this.timedOutTimer = null;
        this.input = input;
        this.output = output;
        this.update = update;
    }
    /* normal Mealy transition without timer update */
    public MealyTimerTransition(Location source, Location destination,InputAction input,OutputAction output) {
        this(source,destination,input,output,null);
    }


    /* internal timeout transition with update */
    public MealyTimerTransition(Location source, Location destination,TimerVariable timer, OutputAction output, String update) {
        super(source, destination);

        this.timedOutTimer = timer;
        this.input = timeout;
        this.output = output;
        this.update = update;
    }
    /* internal timeout transition without update */
    public MealyTimerTransition(Location source, Location destination,TimerVariable timer, OutputAction output) {
        this(source,destination,timer,output,null);
    }

    @Override
    public InputAction getInput() {
        return input;
    }

    @Override
    public OutputAction getOutput() {
        return output;
    }

    public boolean isTimeout() {
        return  input==timeout;
    }

    public Optional<TimerVariable> getTimedOutTimer() {

        if  (timedOutTimer == null) {
            return Optional.empty();
        } else {
            return Optional.of(timedOutTimer);
        }
    }

    public boolean hasUpdate() {
       return  update!=null;
    }

    public Optional<String> getUpdate() {

        if  (update == null) {
            return Optional.empty();
        } else {
            return Optional.of(update);
        }
    }

    @Override
    public String getLabel() {
        String updateStr= "",inputStr=input.toString();
        if ( this.hasUpdate() ) {
            updateStr= update;
        }
        if ( this.isTimeout() ) {
            inputStr=inputStr + "[" + this.timedOutTimer + "]";
        }
        return  inputStr  + '/' + output.toString() + updateStr;
    }

}
