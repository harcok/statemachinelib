package statemachine.model.elements.transition;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.transitions.ModelHiddenTransition;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.location.LocationState;

@NonNullByDefault
public final class HiddenTransition extends  BaseTransition<LocationState> implements ModelHiddenTransition<HiddenAction,LocationState> {


    final private HiddenAction action;

	public HiddenTransition(LocationState source, LocationState destination, HiddenAction action) {
		super(source, destination);
		this.action = action;
	}

	@Override
	public  HiddenAction getHiddenAction() {
		return action;
	}

    @Override
    public String getLabel() {
        return action.toString();
    }


}
