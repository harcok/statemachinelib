package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemAction;

@NonNullByDefault
public final class ConcreteAction extends BaseConcreteAction implements SystemAction {

    public ConcreteAction(DataActionType actionType, List<Value> values) {
        super(actionType, values);
        // TODO: check using actionType whether we have the right number and type of  parameters

    }

    @Override
    public DataActionType getType() {
        return (DataActionType) this.type; // assume type is immutable!
    }
}
