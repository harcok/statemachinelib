package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;

@NonNullByDefault
public final class SymbolicInputAction extends BaseSymbolicAction<DataInputActionType> implements ModelInput {

    public SymbolicInputAction(DataInputActionType actionType, List<String> symbolicParameters) {
        super(actionType, symbolicParameters);
    }

    // note: special formatting of input action with ? or something else happens
    // when exporting to fileformat

    // however for human debugging:
    @Override
    public String toString() {
        return "?" + this.asString();
    }



}
