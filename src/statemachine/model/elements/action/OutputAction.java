package statemachine.model.elements.action;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.system.actionmarkers.SystemOutput;

@NonNullByDefault
public final class OutputAction extends BaseActionType implements SystemOutput,ModelOutput,ModelOutputType  {

    public OutputAction(String name) {
        super(name);
    }

	// note: special formatting of output action with ! or something else happens when exporting to fileformat
	//       but not in getName
	// however for human  debugging:
	@Override
	public String toString() {
		return  "!" + this.getName();
	}

}