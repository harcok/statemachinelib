package statemachine.model.elements.action;

import java.util.Arrays;
import java.util.List;

import org.antlr.symtab.Type;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actiontypemarkers.ModelInputType;

@NonNullByDefault
public final class DataInputActionType extends BaseDataActionType implements ModelInputType {


    public DataInputActionType(String name, List<Type> parameterTypes) {
        super(name, parameterTypes);
    }

    public DataInputActionType( String name, Type... parameterTypes) {
        super(name,Arrays.asList(parameterTypes));
    }



    // note: special formatting of input action with ? or something else happens
    // when exporting to fileformat
    // but not in getName
    // however for human debugging:
    @Override
    public String toString() {
        return "?" + this.asString();
    }

}
