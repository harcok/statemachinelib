package statemachine.model.elements.action;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.system.actionmarkers.SystemInput;

@NonNullByDefault
public final class InputAction extends BaseActionType implements SystemInput,ModelInput,ModelInputType {

    public InputAction(String name) {
        super(name);
    }

	// note: special formatting of input action with ? or something else happens when exporting to fileformat
	//       but not in getName
	// however for human  debugging:
	@Override
	public String toString() {
		return  "?" + this.getName();
	}

}
