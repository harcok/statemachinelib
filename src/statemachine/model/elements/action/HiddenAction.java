package statemachine.model.elements.action;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelHiddenAction;
import statemachine.interfaces.model.actiontypemarkers.ModelHiddenActionType;
import statemachine.interfaces.system.actionmarkers.SystemHiddenAction;


@NonNullByDefault
public final class HiddenAction extends BaseActionType implements SystemHiddenAction,ModelHiddenAction,ModelHiddenActionType  {

    public HiddenAction(String name) {
        super(name);
    }

	// note: special formatting of internal action with _ or something else happens when exporting to fileformat
	//       but not in getName
	// however for human  debugging:
	@Override
	public String toString() {
		return  "_" + this.getName();
	}

}