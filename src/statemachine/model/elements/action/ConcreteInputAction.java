package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemInput;

@NonNullByDefault
public final class ConcreteInputAction extends BaseConcreteAction implements SystemInput {

    public ConcreteInputAction(DataInputActionType actionType, List<Value> parameterValues) {
        super(actionType, parameterValues);

        // TODO: check using actionType whether we have the right number and type of  parameters
    }

    // note: special formatting of input action with ? or something else happens
    // when exporting to fileformat
    // however for human debugging:
    @Override
    public String toString() {
        return "?" + super.toString();
    }

    @Override
    public DataInputActionType getType() {
        return (DataInputActionType) this.type; // assume type is immutable!
    }

}
