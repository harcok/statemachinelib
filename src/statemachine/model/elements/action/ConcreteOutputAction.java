package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemOutput;

@NonNullByDefault
public final class ConcreteOutputAction extends BaseConcreteAction implements SystemOutput {

    public ConcreteOutputAction(DataOutputActionType actionType, List<Value> parameterValues) {
        super(actionType, parameterValues);
        // TODO: check using actionType whether we have the right number and type of  parameters
    }

    // note: special formatting of output action with ! or something else happens
    // when exporting to fileformat however for human debugging:
    @Override
    public String toString() {
        return "!" + super.toString();
    }

    @Override
    public DataOutputActionType getType() {
        return (DataOutputActionType) this.type; // assume type is immutable!
    }
}
