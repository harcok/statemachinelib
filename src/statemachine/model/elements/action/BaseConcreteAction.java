package statemachine.model.elements.action;

import org.antlr.symtab.Type;
import org.antlr.symtab.TypedSymbol;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import com.google.common.collect.ImmutableList;

//IMMUTABLE
@NonNullByDefault
public abstract class BaseConcreteAction implements TypedSymbol {

    protected final Type type;

    protected final ImmutableList<Value> parameterValues;

    // note: ConcreteAction has no name, because its no symbol but an instance of a
    // DataAction type

    @SuppressWarnings("null") // to suppress warning for nonNull unchecked conversion with
                              // ImmutableList.copyOf(values) assignment
    public BaseConcreteAction(Type actionType, Iterable<Value> values) {
        if (!(actionType instanceof BaseDataActionType))
            throw new IllegalArgumentException("type must be a DataAction type");

        this.type = actionType;
        this.parameterValues = ImmutableList.copyOf(values);
    }


    /*
     * => disabled anyway because type made final in this class However still
     * inherit from TypedSymbol to classify like that in symtab library
     */
    public void setType(@Nullable Type type) {
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @return immutable List of values
     */
    public ImmutableList<Value> getArgumentValues() {
        return parameterValues;
    }

    @Override
    public String toString() {
        return this.type.getName() + parameterValues;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((parameterValues == null) ? 0 : parameterValues.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseConcreteAction other = (BaseConcreteAction) obj;
        if (!parameterValues.equals(other.parameterValues))
            return false;
        if (!type.equals(other.type))
            return false;
        return true;
    }

}
