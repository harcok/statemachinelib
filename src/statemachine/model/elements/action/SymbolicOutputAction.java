package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelOutput;

/**
 * for SymbolicOutputAction a symbolicParameter can be whole expression
 *
 * @author harcok
 *
 */
@NonNullByDefault
public final class SymbolicOutputAction extends BaseSymbolicAction<DataOutputActionType> implements ModelOutput {

    public SymbolicOutputAction(DataOutputActionType actionType, List<String> symbolicParameters) {
        super(actionType, symbolicParameters);
    }

    // note: special formatting of output action with ! or something else happens
    // when exporting to fileformat
    // however for human debugging:
    @Override
    public String toString() {
        return "!" + this.asString();
    }

}
