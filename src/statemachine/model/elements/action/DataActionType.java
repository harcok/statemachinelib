package statemachine.model.elements.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.antlr.symtab.Type;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actiontypemarkers.ModelActionType;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;

@NonNullByDefault
public final class DataActionType extends BaseDataActionType implements ModelActionType {



    public DataActionType(String name, List<Type> parameterTypes) {
        super(name, parameterTypes);
    }

    public DataActionType( String name, Type... parameterTypes) {
        super(name,Arrays.asList(parameterTypes));
    }




}
