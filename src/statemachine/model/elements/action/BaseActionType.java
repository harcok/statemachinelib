package statemachine.model.elements.action;

import org.antlr.symtab.Type;

/** BaseActionType is base type for all action types which extends BaseType
*
*   BaseActionType represents a type of action to communicate with an automaton
*
*   Its subtype BaseDataActionType implements a NamedTuple type:
*   eg. b(int,boolean)  with possible instances b(2,true),b(5,false),...
*
*   For
*     * fsm a type of action is purely specified by its name(label)
*        => only one concrete action instance per type
*        => can use ActionSymbol as concrete action
*       eg. Action,HiddenAction,InputAction,OutputAction
*         => note that we don't have Type in its name because its both type and instance!
*
*     * efsm a type of action is specified by its name and type of parameters
*        => multiple concrete/symbolic action instances possible per type
*        => use symbolic action in description of transition of abstract efsm description
*            note: for single action type we can have multiple symbolic actions because we  assign to
*                  param a symbolic param  (which is infinite set,  eg. a,b, x,y etc..)
*        => use concrete action when executing a transition in concrete efsm implementation (choose param from set of concrete values)
*        eg. DataActionType,DataHiddenActionType,DataInputActionType,DataOutputActionType
*
*  examples:
*    possible DataActionType is IOK(int)
*    corresponding possible SymbolicAction instance  IOK(v)      (defined on transition symbolic)
*    corresponding possible ConcreteAction instance  IOK(3)      (when used in concrete transition)
*      => when given IOK(3)  means  define v as variable with type int and set v=3 in global memoryspace for expression(s) on transition
*
*/
public class BaseActionType implements Type {

    final private String name;

    protected BaseActionType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int getTypeIndex() {
        // return -1 means no index implemented
        return -1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BaseActionType other = (BaseActionType) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }



}
