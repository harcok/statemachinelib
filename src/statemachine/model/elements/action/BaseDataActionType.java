package statemachine.model.elements.action;

import java.util.List;

import org.antlr.symtab.Type;
import org.eclipse.jdt.annotation.NonNullByDefault;


/** base type for action types with data in statemachinelib **/

@NonNullByDefault
public abstract class   BaseDataActionType extends BaseActionType {

	protected final List<Type> parameterTypes;

	protected BaseDataActionType( String name, List<Type> parameterTypes) {
		super(name);
		this.parameterTypes = parameterTypes;
	}

	public List<Type> getParameterTypes() {
		return parameterTypes;
	}

   public String asString() {
        return  this.getName() + parameterTypes;
    }

	@Override
	public String toString() {
		return  asString();
	}
}