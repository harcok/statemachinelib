package statemachine.model.elements.action;

import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.actionmarkers.SystemHiddenAction;

@NonNullByDefault
public final class ConcreteHiddenAction extends BaseConcreteAction implements SystemHiddenAction {

    public ConcreteHiddenAction(DataHiddenActionType actionType, List<Value> parameterValues) {
        super(actionType, parameterValues);
        // TODO: check using actionType whether we have the right number and type of  parameters
    }

    // note: special formatting of input action with ? or something else happens
    // when exporting to fileformat
    // however for human debugging:
    @Override
    public String toString() {
        return "_" + super.toString();
    }

    @Override
    public DataHiddenActionType getType() {
        return (DataHiddenActionType) this.type; // assume type is immutable!
    }

}
