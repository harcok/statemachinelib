package statemachine.model.elements.action;


import org.antlr.symtab.Type;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelAction;
import statemachine.interfaces.model.actiontypemarkers.ModelActionType;
import statemachine.interfaces.system.actionmarkers.SystemAction;

@NonNullByDefault
public final class Action extends BaseActionType implements Type, SystemAction, ModelAction, ModelActionType {

    public Action(String name) {
        super(name);
    }

}
