package statemachine.model.elements.action;

import java.util.LinkedList;

//import org.antlr.symtab.BaseSymbol;
import org.antlr.symtab.Type;
import org.antlr.symtab.TypedSymbol;
import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import com.google.common.collect.ImmutableList;



// IMMUTABLE
//public  class BaseSymbolicAction extends BaseSymbol implements TypedSymbol {
@NonNullByDefault
public  class  BaseSymbolicAction<T extends Type>  implements TypedSymbol {
	/*  IMPORTANT:  when parsing symbolic action we assume type of action is known ( so also types of parameters )
	                so we can immmediately create the SymbolicAction in one step!!
	                => no setter for type needed => all set in constructor!
	                => note: setter for type will fail because type made final!!


	        does NOT extend BaseSymbol  because doesn't has a name field which is unique,
	        because in model we can have IN(p0),IN(v)  symbolic actions with same name IN, but different symbols for params!

            A SymbolicAction is a TypedSymbol which symbol looks look a function call with symbols as parameters  eg.   IN(p0,p1).
	        It is an symbolic instance of an action type by instantiating the action type's parameter types as  parameter symbols with same types!
	        note: its action type is unique by name

	        note: we model a symbolicParameter within a Symbolic acttion just as a string, because parameter type already set in action.
	              Otherwise we would do double bookkeeping!!
	*/

	// only allow type and argumentnames to be set once!
	protected final T type;
	protected final ImmutableList<String> symbolicParameters;// just the symbol names, type of params set in type of action!!

	// no parameters
	public BaseSymbolicAction(T actionType ) {
		this(actionType, new LinkedList<String>());
	}

	@SuppressWarnings("null") // to suppress warning for nonNull unchecked conversion with ImmutableList.copyOf(values) assignment  => bug eclipse!!
	public BaseSymbolicAction(T actionType,Iterable<String> symbolicParameters) {
		// give it the same name as the type
		//super(actionType.getName());       => no name needed!!
		this.type=actionType;

		this.symbolicParameters = ImmutableList.copyOf(symbolicParameters);
	}
	/* alternative fix then @SuppressWarnings("null") is :
	 *
	 *  this.symbolicParameters = castToNonNull(ImmutableList.copyOf(symbolicParameters));
	 *
    static @NonNull <T> T castToNonNull(@Nullable T value) {
        if (value == null) throw new NullPointerException("failed NonNull cast");
        return value;
    }

      => but ImmutableList cannot be null, so bug of eclipse/guava
    */

	/**
	 *
	 * @return immutable List of symbolic parameter names
	 */
	public ImmutableList<String> getSymbolicParameters() {
		return symbolicParameters;
	}



	@Override
	public T getType() {
		return this.type;  // assume type is immutable!
	}

	/*  => disabled anyway because type made final in this class
	       However still inherit from TypedSymbol to classify like that in symtab library
	*/
	public void setType(@Nullable Type type) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbolicParameters == null) ? 0 : symbolicParameters.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseSymbolicAction<?> other = (BaseSymbolicAction<?>) obj;
        if (!symbolicParameters.equals(other.symbolicParameters))
			return false;
        if (!type.equals(other.type))
			return false;
		return true;
	}


    public String asString() {
        String joined = String.join(",", symbolicParameters);
        return this.type.getName() + "("  + joined + ")";
    }

    // for debugging
    @Override
    public String toString() {
        return  asString();
    }




}
