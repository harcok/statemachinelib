package statemachine.model.elements.action;

import java.util.List;

import org.antlr.symtab.Type;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelAction;

@NonNullByDefault
public final class SymbolicAction extends BaseSymbolicAction<DataActionType> implements ModelAction {

    public SymbolicAction(DataActionType actionType, List<String> symbolicParameters) {
        super(actionType, symbolicParameters);
    }

}
