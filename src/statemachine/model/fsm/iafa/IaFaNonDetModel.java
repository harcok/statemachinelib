package statemachine.model.fsm.iafa;

public class IaFaNonDetModel extends IaFaBaseModel {


    //Builder class
    public static final class ImmutableBuilder extends IaFaBaseModel.ImmutableBuilder {
        
        public IaFaNonDetModel build(){
            super.baseBuild();
            return new IaFaNonDetModel(this);
        }
    }
    
    // Constructor
    private IaFaNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }
    
    // static factory from Model
    static public IaFaNonDetModel fromIaFaModel(IaFaModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromIaFaModel(model);
        return builder.build();
    }

	
}
