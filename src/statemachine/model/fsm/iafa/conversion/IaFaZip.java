package statemachine.model.fsm.iafa.conversion;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.ia.conversion.LtsZip;
import statemachine.model.fsm.iafa.IaFaModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class IaFaZip {

    /**  Zip an IaFa model to a lts model
     *  <p>
     *   We serialize inputs and outputs to a label string such that we an always revert this operation.
     *   However when zipping to a lts model we loose the information whether a state is active or inactive.
     *
     * @param   IaFaModel
     * @return  LtsModel
     */
	static public LtsNonDetModel zipIaFa2Lts(IaFaModel model) {
	    return LtsZip.zipIa2Lts( zipIaFa2Ia(model) );
	  }


    /**  Zip an IaFa model to an Ia model
    *  <p>
    *   When zipping to an Ia model we loose the information whether a state is active or inactive.
    *
    * @param  IaFaModel
    * @return IaModel
    */
    static public IaNonDetModel zipIaFa2Ia(IaFaModel model) {
        IaNonDetModel.ImmutableBuilder builder = new IaNonDetModel.ImmutableBuilder();

        for (InputTransition trans: model.getModelInputTransitions() ) {
            builder.addInputTransition(trans.getSource(), trans.getDestination(), trans.getInput());
        }
        for (OutputTransition trans: model.getModelOutputTransitions() ) {
            builder.addOutputTransition(trans.getSource(), trans.getDestination(), trans.getOutput());
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
      }

    /**  Zip an IaFa model to an Fa model
    *  <p>
    *   When zipping to an Fa model we loose the information whether an action is input or output
    *   however we can encode this in the label of the Fa model using "?" for input, and "!" for output.
    *
    * @param  IaFaModel
    * @return FaModel
    */
    static public NfaModel zipIaFa2Fa(IaFaModel model) {
        NfaModel.ImmutableBuilder builder = new NfaModel.ImmutableBuilder();

        for (InputTransition trans: model.getModelInputTransitions() ) {
            builder.addTransition(trans.getSource(), trans.getDestination(), new InputAction("?" + trans.getInput().getName()) );
        }
        for (OutputTransition trans: model.getModelOutputTransitions() ) {
            builder.addTransition(trans.getSource(), trans.getDestination(), new InputAction("!" + trans.getOutput().getName()) );
        }
        for (LocationState  location: model.getLocations()) {
            if ( model.isAccepting(location) ) builder.setAccepting(location);
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
      }

}
