
package statemachine.model.fsm.iafa;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.interfaces.system.properties.OutputDeterministic;
import statemachine.interfaces.system.properties.StateDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

/**
 *  Interface Automaton Finite Acceptor which is state deterministic.
 *
 *  An Interface Automaton Finite Acceptor is an interface automaton which has accepting and rejecting states.
 *
 *  State deterministic is implemented by requiring the model to be both
 *  input and output deterministic. State deterministic also implies
 *  no internal hidden transitions.
 *
 * @author harcok
 *
 */
public class IaFaDetModel extends IaFaBaseModel implements IaFaDetInterface {

    //Builder class
    public static final class ImmutableBuilder extends IaFaBaseModel.ImmutableBuilder {

        public IaFaDetModel build() {
            super.baseBuild();
            return new IaFaDetModel(this);
        }
    }

    // Constructor
    private IaFaDetModel(ImmutableBuilder builder) {
        super(builder);

        // check for input none-determinism ; if found it throws exception
        for (LocationState location : this.loc2inputtrans.keySet()) {
            for (InputAction action : this.inputalphabet) {
                getTriggeredTransition(location, action); // throws exception when none-determinism is detected!
            }
        }

        // check for output none-determinism ; if found it throws exception
        for (LocationState location : this.loc2outputtrans.keySet()) {
            for (OutputAction action : this.outputalphabet) {
                getSystemTransition(location, action); // throws exception when none-determinism is detected!
            }
        }
    }

    // static factory from Model
    static public IaFaDetModel fromIaFaModel(IaFaModel model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromIaFaModel(model);
        return builder.build();
    }

    // interface InputDeterministic

    @Override
    public Optional<InputTransition> getTriggeredTransition(LocationState location, InputAction action) {
        ImmutableSet<@NonNull InputTransition> transitions = this.getModelInputTransitions(location, action);
        return Util.getOptionalTransition(transitions);
    }

    // interface OutputDeterministic

    @Override
    public Optional<OutputTransition> getSystemTransition(LocationState location, OutputAction action) {
        ImmutableSet<@NonNull OutputTransition> transitions = this.getModelOutputTransitions(location, action);
        return Util.getOptionalTransition(transitions);
    }

    // interface StateDeterministic
    @Override
    public LocationState getSystemsNextState(LocationState state, InputAction input) {
        Optional<InputTransition> optTrans = getTriggeredTransition(state, input);
        if (optTrans.isPresent()) {
            return optTrans.get().getDestination();
        }
        return state;
    }

    @Override
    public LocationState getSystemsNextState(LocationState state, OutputAction output) {
        Optional<OutputTransition> optTrans = getSystemTransition(state, output);
        if (optTrans.isPresent()) {
            return optTrans.get().getDestination();
        }
        return state;
    }

}