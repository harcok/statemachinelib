package statemachine.model.fsm.iafa.file;

import java.util.List;

import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.iafa.IaFaModel;
import statemachine.model.fsm.iafa.IaFaNonDetModel;
import statemachine.util.IO;

public class Import {



    static public IaFaNonDetModel dot2IaFaModel(String filename) {
       // statemachine.model.fsm.iafa.format.Dot.import(filename);
        //IO.writeSmallTextFile( lines, filename);

        List<String> lines=IO.readSmallTextFile(filename);
        IaFaNonDetModel model= statemachine.model.fsm.iafa.format.Dot.importIaFaModel(lines);
        return model;
    }


}
