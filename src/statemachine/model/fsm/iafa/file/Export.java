package statemachine.model.fsm.iafa.file;

import java.util.List;

import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.conversion.LtsZip;
import statemachine.model.fsm.ia.IaModel;
import statemachine.model.fsm.iafa.IaFaModel;
import statemachine.model.fsm.iafa.conversion.IaFaZip;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.util.IO;

public class Export {

    // LOSSNESS :
    //------------

	   static public void  dot(IaFaModel model, String filename){
		   List<String> lines=statemachine.model.fsm.iafa.format.Dot.export(model);
 		   IO.writeSmallTextFile( lines, filename);
	   }

	   /*

	   static public void  gml(FaModel model, String filename){
		   //TODO => also add extra method where you can give specific
		   //       location and transition formatters instead of default
		   //     => see statemachine.model.fsm.fa.conversion.LtsZip class

		  // LtsNonDetModel lts=LtsZip.zipFa2Lts(model);
		  /// statemachine.model.lts.file.Export.gml(lts,filename);
	   }
	   */

	   /*  no aldebaran export because locations can only be represented by numbers in aldebaran format
	    *  => you can transform  dfa/moore to mealy machine and export that to aldebaran
		*/
	   // TODO: export to aldebaran using dfa2mealy






      // LOSSY : below exports loose active states
      //-------------------------------------------

         static public void  dotAsIa(IaFaModel model, String filename){
             LtsNonDetModel lts=IaFaZip.zipIaFa2Lts(model);
             statemachine.model.fsm.lts.file.Export.dot(lts,filename);
         }

	    static public void  gmlAsIa(IaFaModel model, String filename){
	           LtsNonDetModel lts=IaFaZip.zipIaFa2Lts(model);
	           statemachine.model.fsm.lts.file.Export.gml(lts,filename);
	       }

	    static public void  aldebaranAsIa(IaFaModel model, String filename){
	           LtsNonDetModel lts=IaFaZip.zipIaFa2Lts(model);
	           statemachine.model.fsm.lts.file.Export.aldebaran(lts,filename);
	       }
}
