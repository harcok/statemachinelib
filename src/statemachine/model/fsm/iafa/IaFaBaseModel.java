package statemachine.model.fsm.iafa;

import java.util.LinkedList;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.iamoore.IaMooreBaseModel;

/**
 * IaFaBaseModel is implemented as an IaMooreModel
 * where the Moore outputs are only ACCEPT and REJECT
 * and where by default a state is REJECT and
 * we can make it ACCCEPT with the setAccepting builder method
 * If a state is ACCEPT or REJECT can be inspected
 * with the isAccepting(location) method.
 *
 * Because IaFa is implemented on top of a IaMooreModel
 * we can still fetch ACCEPT and REJECT with getOutput from
 * a location. However we don't allow to set the output
 * within the builder with a setOutput method, because
 * that only confuses the user. It must be clear that
 * the user can only setAccepting or not.
 *
 * The outputs ACCEPT and REJECT  are not in the output alphabet,
 * because the output alphabet only contains outputs which
 * occur on transitions.
 *

 *
 * @author harcok
 *
 */
public abstract class IaFaBaseModel extends IaMooreBaseModel implements IaFaModel {
    // note: IaMooreBaseModel already implements IaMooreModel
    public static OutputAction ACCEPT = new OutputAction("ACCEPT");
    public static OutputAction REJECT = new OutputAction("REJECT");

    //Builder class
    public static class ImmutableBuilder extends IaMooreBaseModel.ImmutableBuilder {

        public void setAccepting(LocationState location) {
            loc2outputBuilder.put(location, ACCEPT);
        }

        public ImmutableBuilder addFromIaFaModel(IaFaModel model) {

            for (InputTransition trans : model.getModelInputTransitions()) {
                this.addInputTransition(trans.getSource(), trans.getDestination(), trans.getInput());
            }
            for (OutputTransition trans : model.getModelOutputTransitions()) {
                this.addOutputTransition(trans.getSource(), trans.getDestination(), trans.getOutput());
            }
            for (LocationState location : model.getLocations()) {
                OutputAction output;// = model.getOutputAt(location);
                if (model.isAccepting(location)) {
                    output = ACCEPT;
                } else {
                    output = REJECT;
                }
                this.addOutput(location, output);
            }
            this.setStartLocation(model.getStartLocation());
            return this;
        }

        protected void baseBuild() {
            // set fixed output alphabet of a FA
            outputalphabetBuilder.add(REJECT);
            outputalphabetBuilder.add(ACCEPT);


            // get so far set locations and corresponding outputs set
            ImmutableSet<LocationState> local_locations = locationsBuilder.build();
            ImmutableMap<LocationState, OutputAction> local_loc2output = loc2outputBuilder.build();

            // for locations where no accepting output is set, we set REJECT output
            for (LocationState location : local_locations) {
                if (!local_loc2output.containsKey(location)) {
                    this.addOutput(location, REJECT);
                }
            }
            // do build into final fields
            super.baseBuild();

            // remove  REJECT ACCEPT from output alphabet  ( only outputs on transitions in output alphabet)
            LinkedList<OutputAction> trans_outputs = new LinkedList<>(this.outputalphabet);
            trans_outputs.remove(REJECT);
            trans_outputs.remove(ACCEPT);
            this.outputalphabet =  ImmutableSet.copyOf(trans_outputs);
        }
    }

    // Constructor
    protected IaFaBaseModel(ImmutableBuilder builder) {
        super(builder);
    }

    public boolean isAccepting(LocationState location) {
        return loc2output.get(location) == ACCEPT;
    }

}
