package statemachine.model.fsm.iafa.format;

import java.util.LinkedList;
import java.util.List;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.iafa.IaFaModel;
import statemachine.model.fsm.iafa.IaFaNonDetModel;

/*

IaFa

 digraph g {

    __start0 [label="" shape="none"]

    1 [shape="circle" label="1"];                   => rejection state
    2 [shape="doublecircle" label="2"];             => accepting state
    1 -> 2 [label="?a"];
    1 -> 2 [label="!x"];
    2 -> 1 [label="?b"];
    1 -> 1 [label="!y"];

    __start0 -> 1;
}


MealyFa

 digraph g {

    __start0 [label="" shape="none"]

    1 [shape="circle" label="1"];                   => rejection state
    2 [shape="doublecircle" label="2"];             => accepting state
    1 -> 2 [label="a/x"];
    1 -> 2 [label="b/x"];
    2 -> 1 [label="a/y"];
    1 -> 1 [label="a/x"];

    __start0 -> 1;
}


 */

public class Dot {


    public static IaFaNonDetModel importIaFaModel(List<String> lines) {
        IaFaNonDetModel.ImmutableBuilder builder =new IaFaNonDetModel.ImmutableBuilder();
        for(String line: lines){

            if(line.contains("__start0") && !line.contains("label")) {
                int e1 = line.indexOf('-');
                int e2 = line.indexOf(';');
                String start = line.substring(e1+2, e2).trim();
                LocationState startLocation = new LocationState(start);
                builder.setStartLocation(startLocation);
            }

            if(!line.contains("label")) continue;

            if(line.contains("->")){
                int e1 = line.indexOf('-');
                int e2 = line.indexOf('[');
                int b3 = line.indexOf('"');
                int e3 = line.lastIndexOf('"');

                String from = line.substring(0, e1).trim();
                String to = line.substring(e1+2, e2).trim();
                String label = line.substring(b3+1, e3).trim();

                LocationState src = new LocationState(from);
                LocationState dst = new LocationState(to);

                if ( label.trim().startsWith("?")) {
                    InputAction input =  new InputAction(label.trim().substring(1).trim());
                    builder.addInputTransition(src, dst, input);
                } else if ( label.trim().startsWith("!")) {
                    OutputAction output =  new OutputAction(label.trim().substring(1).trim());
                    builder.addOutputTransition(src, dst, output);
                } else {
                    throw new RuntimeException("cannot unzip label: " + label );
                }
            }
            if (line.contains("doublecircle")) {
                int e1 = line.indexOf('[');
                String locationStr = line.substring(0, e1).trim();
                LocationState location = new LocationState(locationStr);
                builder.setAccepting(location);
            }

        }
        return builder.build();
    }

	static public List<String> export(IaFaModel model) {
		String indent="    ";
		LinkedList<String> lines= new LinkedList<>();
		lines.add("digraph g {");
		lines.add("");
		lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
		lines.add("");


		for ( LocationState location: model.getLocations()) {
			if (model.isAccepting(location)) {
				lines.add(indent + location + " [shape=\"doublecircle\" label=\"" + location + "\"];");
			} else {
				lines.add(indent + location + " [shape=\"circle\" label=\"" + location + "\"];");
			}
		}
		for (InputTransition transition: model.getModelInputTransitions() ) {
			lines.add(indent + transition.getSource() + " -> " + transition.getDestination() + " [label=\"" + "?"+ transition.getInput().getName() + "\"];");
		}
		for (OutputTransition transition: model.getModelOutputTransitions() ) {
			lines.add(indent + transition.getSource() + " -> " + transition.getDestination() + " [label=\"" + "!"+ transition.getOutput().getName() + "\"];");
		}
	  	lines.add("");
		lines.add(indent + "__start0 -> " + model.getStartLocation() + ";");
		lines.add("}");
		return lines;
	}



}
