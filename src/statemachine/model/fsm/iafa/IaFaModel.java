package statemachine.model.fsm.iafa;

import statemachine.interfaces.model.types.ModelInterfaceAutomatonFiniteAcceptor;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.HiddenTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.elements.transition.BaseTransition;

public interface IaFaModel extends  ModelInterfaceAutomatonFiniteAcceptor
                            <InputAction,OutputAction,HiddenAction,LocationState,
                             BaseTransition<LocationState>,InputTransition,OutputTransition,HiddenTransition>,
                            InitialState<LocationState,LocationState>
{}
