package statemachine.model.fsm.iafa.walker;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.Accepting;
import statemachine.interfaces.system.walker.CurrentState;
import statemachine.interfaces.system.walker.ResetState;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.iafa.IaFaModel;

@NonNullByDefault
public class IaFaBaseWalker<M extends IaFaModel> implements CurrentState<LocationState>, ResetState<LocationState>,Accepting {

    protected LocationState currentLocation;
    protected M model;

    // Constructor
    public IaFaBaseWalker(M model) {
        this.model = model;
        this.currentLocation = model.getStartLocation();
    }

    // getters

    public LocationState getLocation() {
        return currentLocation;
    }

    public @NonNull M getModel() {
        return model;
    }

    // interface CurrentState

    @Override
    public LocationState getState() {
        return currentLocation;
    }

    @Override
    public void setState(LocationState state) {
        this.currentLocation = state;
    }



    // interface ResetState

    @Override
    public void resetState() {
        this.currentLocation = model.getStartLocation();

    }

    // interface Accepting
    @Override
    public boolean isAccepting() {
        return model.isAccepting(currentLocation);
    }
}
