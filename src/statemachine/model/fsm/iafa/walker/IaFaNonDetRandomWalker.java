package statemachine.model.fsm.iafa.walker;

import java.util.List;
import java.util.Random;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.interfaces.system.walker.StepInputOrOutputOrHidden;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.HiddenTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.iafa.IaFaNonDetModel;





@NonNullByDefault
public class IaFaNonDetRandomWalker extends IaFaBaseWalker<IaFaNonDetModel> implements StepInputOrOutputOrHidden<LocationState,InputAction,OutputAction,HiddenAction> {

	private Random random;

	public IaFaNonDetRandomWalker(IaFaNonDetModel model, Random random ) {
		super(model);
		this.random = random;
	}

	// interface StepInputOrOutputOrHidden

	@Override
	public boolean checkInputPossible(InputAction action) {
   	     ImmutableSet<InputTransition> possibleTransitions=model.getModelInputTransitions(currentLocation,action);
   	     return possibleTransitions.size() > 0;
	}



	@Override
	public boolean checkOutputPossible(OutputAction action) {
  	     ImmutableSet<OutputTransition> possibleTransitions=model.getModelOutputTransitions(currentLocation,action);
  	     return possibleTransitions.size() > 0;
	}

	@Override
	public boolean checkHiddenPossible(HiddenAction action) {
   	     ImmutableSet<HiddenTransition> possibleTransitions=model.getModelHiddenTransitions(currentLocation,action);
   	     return possibleTransitions.size() > 0;
	}

	 /**
	  *  if more then one path possible randomly chooses one
	  * @param action
	  */
	@Override
    public void stepInput(InputAction action){
   	 ImmutableSet<InputTransition> possibleTransitions=model.getModelInputTransitions(currentLocation,action);
   	 int numTrans=possibleTransitions.size();
   	 if ( numTrans > 1 ) {
   		 List<InputTransition> list =possibleTransitions.asList();
   		int index=random.nextInt(numTrans);
   		currentLocation=list.get(index).getDestination();
   	 }
   	 if (numTrans == 1 ) {
   		InputTransition t=possibleTransitions.iterator().next();
   		 currentLocation = t.getDestination();
   	 }
   	 // if ( numTrans == 0 )     DO NOTHING
    }

	@Override
    public void stepOutput(OutputAction action){
      	 ImmutableSet<OutputTransition> possibleTransitions=model.getModelOutputTransitions(currentLocation,action);
      	 int numTrans=possibleTransitions.size();
      	 if ( numTrans > 1 ) {
      		 List<OutputTransition> list =possibleTransitions.asList();
      		int index=random.nextInt(numTrans);
      		currentLocation=list.get(index).getDestination();
      	 }
      	 if (numTrans == 1 ) {
      		OutputTransition t=possibleTransitions.iterator().next();
      		 currentLocation = t.getDestination();
      	 }
      	 // if ( numTrans == 0 )     DO NOTHING
       }

	@Override
    public void stepHidden(HiddenAction action){
      	 @NonNull ImmutableSet<@NonNull HiddenTransition> possibleTransitions=model.getModelHiddenTransitions(currentLocation,action);
      	 int numTrans=possibleTransitions.size();
      	 if ( numTrans > 1 ) {
      		 List<@NonNull HiddenTransition> list =possibleTransitions.asList();
      		int index=random.nextInt(numTrans);
      		currentLocation=list.get(index).getDestination();
      	 }
      	 if (numTrans == 1 ) {
      		@NonNull HiddenTransition t=possibleTransitions.iterator().next();
      		 currentLocation = t.getDestination();
      	 }
      	 // if ( numTrans == 0 )     DO NOTHING
       }
}
