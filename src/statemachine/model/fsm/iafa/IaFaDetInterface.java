package statemachine.model.fsm.iafa;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.interfaces.system.properties.OutputDeterministic;
import statemachine.interfaces.system.properties.StateDeterministic;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

public interface IaFaDetInterface
        extends
            IaFaModel,
            StateDeterministic<LocationState, InputAction, OutputAction>,
            InputDeterministic<LocationState, InputAction, InputTransition>,
            OutputDeterministic<LocationState, OutputAction, OutputTransition> {

}
