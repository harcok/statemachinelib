package statemachine.model.fsm.moore.file;

import java.util.List;

import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.moore.MooreNonDetModel;
import statemachine.util.IO;

public class Import {

    public static MooreNonDetModel dot2MooreModel(String filename) {
        List<String> lines=IO.readSmallTextFile(filename);
        MooreNonDetModel model= statemachine.model.fsm.moore.format.Dot.importMooreModel(lines);
        return model;
    }

}
