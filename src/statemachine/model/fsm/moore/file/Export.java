package statemachine.model.fsm.moore.file;

import java.util.List;

import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.moore.MooreModel;
import statemachine.model.fsm.moore.conversion.LtsZip;
import statemachine.util.IO;


public class Export {

    static public void dot(MooreModel model, String filename) {
            List<String> lines=statemachine.model.fsm.moore.format.Dot.export(model);
            IO.writeSmallTextFile( lines, filename);
    }

//    static public void dot(MooreModel model, String filename) {
//        LtsNonDetModel lts = LtsZip.zipMoore2Lts(model);
//        statemachine.model.fsm.lts.file.Export.dot(lts, filename);
//    }

    //
    //static public void gml(MealyModel model, String filename) {
    //    LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
    //    statemachine.model.fsm.lts.file.Export.gml(lts, filename);
    //}
    //
    //static public void aldebaran(MealyModel model, String filename) {
    //    LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
    //    statemachine.model.fsm.lts.file.Export.aldebaran(lts, filename);
    //}
}
