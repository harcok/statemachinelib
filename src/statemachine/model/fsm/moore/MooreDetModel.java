package statemachine.model.fsm.moore;

import java.util.ArrayList;
import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.moore.MooreBaseModel.ImmutableBuilder;

public class MooreDetModel extends MooreBaseModel implements InputDeterministic<LocationState,InputAction,InputTransition>{

    //Builder class
    public static final class ImmutableBuilder extends MooreBaseModel.ImmutableBuilder {


    	
        public ImmutableBuilder addOutput(LocationState src, OutputAction output){
             super.addOutput(src,output);
             return this;
        }
    	
        public MooreDetModel build(){
    		super.baseBuild();
            return new MooreDetModel(this);
        }
        

    }
    
	// Constructor (called by builder)
	private MooreDetModel(ImmutableBuilder builder) {
		super(builder);
		
		// check for none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2trans.keySet()) {
			for ( InputAction action : this.inputalphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}
	}
	
	// static factory from MooreModel
	static public MooreDetModel fromMooreModel(MooreModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromMooreModel(model);
		return builder.build();
	}
	
    // InputDeterministic interface

	@Override
	public Optional<InputTransition> getTriggeredTransition(LocationState location, InputAction action) {
		ImmutableSet<@NonNull InputTransition> transitions = this.getModelTransitions(location, action);
		return Util.getOptionalTransition(transitions);
	}


}