package statemachine.model.fsm.moore;

import java.util.HashSet;
import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.moore.MooreNonDetModel.ImmutableBuilder;

@NonNullByDefault
public abstract class MooreBaseModel implements MooreModel {


	protected final ImmutableSet<InputAction> inputalphabet;
	protected final ImmutableSet<OutputAction> outputalphabet;
	private   final LocationState startLocation;
	private   final ImmutableSet<LocationState> locations;
	private   final ImmutableSet<InputTransition> transitions;
	protected final ImmutableSetMultimap<LocationState,  InputTransition> loc2trans;
	protected ImmutableMap<LocationState,  OutputAction> loc2output;


    // Builder class
    public static abstract class ImmutableBuilder{

    	protected ImmutableSet<InputAction> inputalphabet;
    	protected ImmutableSet<OutputAction> outputalphabet;
    	protected LocationState startLocation;
    	protected ImmutableSet<LocationState> locations;
    	protected ImmutableSet<InputTransition> transitions;
    	protected ImmutableSetMultimap<LocationState,  InputTransition> loc2trans;
    	protected ImmutableMap<LocationState,  OutputAction> loc2output;


    	// builders
    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<LocationState> locationsBuilder=ImmutableSet.builder();

    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<InputTransition> transitionsBuilder=ImmutableSet.builder();

    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<InputAction> inputalphabetBuilder=ImmutableSet.builder();
       	@SuppressWarnings("null")
    	protected ImmutableSet.Builder<OutputAction> outputalphabetBuilder=ImmutableSet.builder();


		// store transitions and outputs per location
		@SuppressWarnings("null")
		protected ImmutableSetMultimap.Builder<LocationState,  InputTransition> loc2transBuilder = ImmutableSetMultimap.builder();
		@SuppressWarnings("null")
		protected ImmutableMap.Builder<LocationState,  OutputAction> loc2outputBuilder = ImmutableMap.builder();

		HashSet<LocationState> locationsDone = new HashSet<LocationState>();

        public ImmutableBuilder addTransition(LocationState src,LocationState dst, InputAction input){
            this.locationsBuilder.add(src,dst);
            this.inputalphabetBuilder.add(input);
            InputTransition transition = new InputTransition(src,dst,input);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src,transition);
            return this;
        }


        public ImmutableBuilder setStartLocation(LocationState startLocation){
            this.startLocation =startLocation;
            return this;
        }

        protected ImmutableBuilder addOutput(LocationState src, OutputAction output){
          	this.locationsBuilder.add(src);
            this.outputalphabetBuilder.add(output);
            if ( ! locationsDone.contains(src) )   {
               // https://google.github.io/guava/releases/21.0/api/docs/com/google/common/collect/ImmutableMap.Builder.html
               //  Duplicate keys are not allowed, and will cause build() to fail.
               this.loc2outputBuilder.put(src,output);
               locationsDone.add(src);
            }
            return this;
        }


    	protected ImmutableBuilder  addFromMooreModel(MooreModel model) {
    		for (InputTransition trans: model.getModelTransitions() ) {
    		   this.addTransition(trans.getSource(), trans.getDestination(), trans.getInput());
    		}
    		for (LocationState location: model.getLocations()) {
    			this.addOutput(location,model.getOutputAt(location));
    		}
    		this.setStartLocation(model.getStartLocation());
    		return this;
    	}



        @SuppressWarnings("null")
		protected void baseBuild(){
    		this.inputalphabet = inputalphabetBuilder.build();
    		this.outputalphabet = outputalphabetBuilder.build();
    		//this.startLocation = ...; // no builder for startlocation
    		this.locations = locationsBuilder.build();
    		this.transitions = transitionsBuilder.build();
    		this.loc2trans = loc2transBuilder.build();
    		this.loc2output = loc2outputBuilder.build();
        }
    }




	// Constructor

	protected MooreBaseModel(ImmutableBuilder builder) {
		this.inputalphabet = builder.inputalphabet;
		this.outputalphabet = builder.outputalphabet;
		this.startLocation = builder.startLocation;
		this.locations = builder.locations;
		this.transitions = builder.transitions;
		this.loc2trans = builder.loc2trans;
		this.loc2output = builder.loc2output;
	}





    // interface InitialState

    @Override
    public @NonNull LocationState getStartLocation() {
        return startLocation;
    }

    @Override
    public @NonNull LocationState getStartState() {
        return startLocation;
    }


    // interface ModelAutomaton

	@Override
	public ImmutableSet<LocationState> getLocations() {
		return locations;
	}

	@Override
	public ImmutableSet<InputTransition> getModelTransitions() {
		return transitions;
	}

	@Override
	public ImmutableSet<InputTransition> getModelTransitions(LocationState location) {
		return loc2trans.get(location);
	}



	// interface ModelMoore

	@Override
	public ImmutableSet<InputAction> getInputAlphabet() {
		return inputalphabet;
	}

	@Override
	public ImmutableSet<OutputAction> getOutputAlphabet() {
		return outputalphabet;
	}



	@Override
	public ImmutableSet<InputTransition> getModelTransitions(LocationState location,InputAction action) {
		ImmutableSet.Builder<InputTransition> transitionsBuilder=ImmutableSet.builder();
		ImmutableSet<InputTransition> transitions= loc2trans.get(location);

   	    for ( InputTransition transition : transitions ) {
   		   if (transition.getInput().equals(action) ) {
   			  transitionsBuilder.add(transition);
   		   }
   	    }
   	    return transitionsBuilder.build();
	}

	@Override
	public OutputAction getOutputAt(LocationState location) {
		return loc2output.get(location);
	}



	// for debugging

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner("\n");
		joiner.add("start: " + startLocation);
		joiner.add("locations:");
		for ( LocationState location: getLocations()) {
			joiner.add(location.toString() + ": !" +    // dfa is specialized moore where ACCEPT and REJECT are outputs from location!
		               loc2output.get(location)
			.getName());
		}
		joiner.add("transitions:");
		for (InputTransition transition: getModelTransitions() ) {
			joiner.add(transition.toString());
		}
		joiner.add("");
		return joiner.toString();
	}

}
