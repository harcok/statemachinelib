package statemachine.model.fsm.moore.format;


import java.util.ArrayList;

/*




*/

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

import org.javatuples.Pair;

import com.google.common.collect.ComparisonChain;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.moore.MooreModel;
import statemachine.model.fsm.moore.MooreNonDetModel;


/**

 digraph g {
        __start0 [label="" shape="none"];
        __start0 -> A;

        A [shape="record", style="rounded", label="{ A | 0 }"];
        B [shape="record", style="rounded", label="{ B | 0 }"];
        C [shape="record", style="rounded", label="{ C | 0 }"];
        D [shape="record", style="rounded", label="{ D | 0 }"];
        E [shape="record", style="rounded", label="{ E | 0 }"];
        F [shape="record", style="rounded", label="{ F | 0 }"];
        G [shape="record", style="rounded", label="{ G | 0 }"];
        H [shape="record", style="rounded", label="{ H | 0 }"];
        I [shape="record", style="rounded", label="{ I | 1 }"];


        A -> D [label="0"];
        A -> B [label="1"];
        B -> E [label="0"];
        B -> C [label="1"];
        C -> F [label="0"];
        C -> C [label="1"];
        D -> G [label="0"];
        D -> E [label="1"];
        E -> H [label="0"];
        E -> F [label="1"];
        F -> I [label="0"];
        F -> F [label="1"];
        G -> G [label="0"];
        G -> H [label="1"];
        H -> H [label="0"];
        H -> I [label="1"];
        I -> I [label="0"];
        I -> I [label="1"];

    }

 */

public class Dot {
    static public MooreNonDetModel importMooreModel( List<String> lines) {


        MooreNonDetModel.ImmutableBuilder builder =new MooreNonDetModel.ImmutableBuilder();


        for(String line: lines){

            // note: dot only allows lower case keywords so we can just match on lowercase "label"!

            // fetch start location
            // match transition from invisible __start0 node without label to the start node
            // note: we assume that this transition has no  attribute list
            if(line.contains("__start0") && !line.contains("label")) {
                int arrow = line.indexOf('-');
                String start = line.substring(arrow+2).trim();
                LocationState startLocation = new LocationState(start);
                builder.setStartLocation(startLocation);
            }

            // we match only transitions and locations with labels;  locations we fetch from transitions, so they don't need to be declared
            if(!line.contains("label")) continue;

            // match location with  output in label
            // for moore location line looks like:
            //     A [shape="record", style="rounded", label="{ A | 0 }"];
            if (line.contains("record")) {
                    int labelindex = line.indexOf("label");
                    int labelStart=line.indexOf('"',labelindex);
                    int labelEnd=line.indexOf('"',labelStart+1);
                    String label = line.substring(labelStart+1, labelEnd).trim();

                    Pair<LocationState,OutputAction> loc_output=statemachine.model.fsm.moore.conversion.LtsZip.defaultLocationLabelParser.apply(label);
                    builder.addOutput(loc_output.getValue0(), loc_output.getValue1());
            }

            // transition
            if(line.contains("->")){
                int arrow = line.indexOf('-');
                int attrlist = line.indexOf('[');
                int labelattr= line.indexOf("label",attrlist);
                int labelbegin = line.indexOf('"',labelattr);
                int labelend = line.lastIndexOf('"');

                String from = line.substring(0, arrow).trim();
                String to = line.substring(arrow+2, attrlist).trim();
                String label = line.substring(labelbegin+1, labelend).trim();

                LocationState src = new LocationState(from);
                LocationState dst = new LocationState(to);
                InputAction action =  new InputAction(label);
                builder.addTransition(src, dst, action);
            }



        }
        return builder.build();

    }

    static private String formatID(String id) {
         return id.contains(" ") ?  "\"" + id +  "\""  : id;
    }

    static public List<String> export(MooreModel model) {

        // get locations sorted
        ArrayList<LocationState> locations = new  ArrayList<>(model.getLocations());
        locations.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));


        // get transitions
        ArrayList<InputTransition> transitions= new  ArrayList<>(model.getModelTransitions());
        // sort transitions
        BiFunction<InputTransition,InputTransition,Integer> comparator =  (trans1,trans2) -> {
            return ComparisonChain.start()
            .compare(trans1.getSource().getName(),      trans2.getSource().getName())
            .compare(trans1.getDestination().getName(), trans2.getDestination().getName())
            .compare(trans1.getInput().getName(),       trans2.getInput().getName())
            .result();
        };
        transitions.sort(comparator::apply);


        // header
        String indent="    ";
        LinkedList<String> lines= new LinkedList<>();
        lines.add("digraph g {");
        lines.add("");
        lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
        lines.add(indent + "__start0 -> " +   formatID(model.getStartLocation().getName()) );
        lines.add("");

        // content
        for ( LocationState location: locations) {
            OutputAction output= model.getOutputAt(location);
            String id =  formatID(location.getName());
            String label=statemachine.model.fsm.moore.conversion.LtsZip.defaultLocationLabelFormatter.apply(location,output);
            String format = "%s%s [label=\"%s\" shape=\"record\" style=\"rounded\"]";
            lines.add(String.format(format,indent,id,label));
        }
        lines.add("");
        for (InputTransition transition: transitions ) {
            String src= formatID(transition.getSource().getName());
            String dst= formatID(transition.getDestination().getName());
            String label = transition.getInput().getName();
            String format = "%s%s -> %s  [label=\"%s\"]";
            lines.add(String.format(format,indent,src,dst,label));
        }

        // footer
        lines.add("}");
        return lines;
    }
}
