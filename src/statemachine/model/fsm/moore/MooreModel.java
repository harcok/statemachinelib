package statemachine.model.fsm.moore;

import statemachine.interfaces.model.types.ModelMoore;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;

public interface MooreModel extends ModelMoore<InputAction, OutputAction, OutputAction, LocationState, InputTransition>,
        InitialState<LocationState, LocationState> {

}
