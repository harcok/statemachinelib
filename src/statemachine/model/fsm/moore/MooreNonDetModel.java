package statemachine.model.fsm.moore;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.moore.MooreDetModel.ImmutableBuilder;

public class MooreNonDetModel extends MooreBaseModel{

    //Builder class
    public static final class ImmutableBuilder extends MooreBaseModel.ImmutableBuilder {


        public ImmutableBuilder addOutput(LocationState src, OutputAction output){
             super.addOutput(src,output);
             return this;
        }

        public MooreNonDetModel build(){
    		super.baseBuild();
            return new MooreNonDetModel(this);
        }


    }

	// Constructor
	private MooreNonDetModel(ImmutableBuilder builder) {
		super(builder);
	}


	// static factory from MooreModel
	static public MooreNonDetModel fromMooreModel(MooreModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromMooreModel(model);
		return builder.build();
	}


    static public void printSimpleStatistics(String filename) {
        MooreNonDetModel nfaModel=statemachine.model.fsm.moore.file.Import.dot2MooreModel(filename);
        int num_inputs=nfaModel.getInputAlphabet().size();
        int num_locations=nfaModel.getLocations().size();
        int num_outputs=nfaModel.getOutputAlphabet().size();
        int num_trans=nfaModel.getModelTransitions().size();
        String format= "states:%1$-10.10s  inputs:%2$-10.10s outputs:%3$-10.10s  transitions:%4$-10.10s";
        System.out.println(String.format(format,num_locations,num_inputs,num_outputs,num_trans));
    }


}

