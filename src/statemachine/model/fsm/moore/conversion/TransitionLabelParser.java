package statemachine.model.fsm.moore.conversion;


import statemachine.model.elements.action.InputAction;

@FunctionalInterface
public interface TransitionLabelParser {
	public InputAction apply (String label);
}
