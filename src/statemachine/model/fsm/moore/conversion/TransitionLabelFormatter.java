package statemachine.model.fsm.moore.conversion;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;


@FunctionalInterface
public interface TransitionLabelFormatter {
	public String apply (InputAction input);
}
