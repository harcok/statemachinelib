package statemachine.model.fsm.moore.conversion;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

@FunctionalInterface
public interface LocationLabelFormatter {
    public String apply (LocationState location, OutputAction output);
}

