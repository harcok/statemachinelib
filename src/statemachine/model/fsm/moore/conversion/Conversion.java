package statemachine.model.fsm.moore.conversion;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.moore.MooreModel;

public class Conversion {

    public static MealyNonDetModel Moore2MealyNonDetModel(MooreModel moore) {
        MealyNonDetModel.ImmutableBuilder builder =new MealyNonDetModel.ImmutableBuilder();
        builder.setStartLocation(moore.getStartLocation());
        for ( InputTransition transition :moore.getModelTransitions()) {
            OutputAction output= moore.getOutputAt(transition.getDestination());
            builder.addTransition(transition.getSource(), transition.getDestination(), transition.getInput(), output);
        }
        return builder.build();
    }

}
