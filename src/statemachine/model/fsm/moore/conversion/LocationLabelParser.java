package statemachine.model.fsm.moore.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

public interface LocationLabelParser {
    public Pair<LocationState,OutputAction> apply (String label);

}
