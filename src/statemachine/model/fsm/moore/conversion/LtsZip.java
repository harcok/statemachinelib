package statemachine.model.fsm.moore.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.moore.conversion.TransitionLabelFormatter;
import statemachine.model.fsm.moore.conversion.TransitionLabelParser;
import statemachine.model.fsm.moore.conversion.LocationLabelFormatter;
import statemachine.model.fsm.moore.conversion.LocationLabelParser;
import statemachine.model.fsm.moore.MooreModel;

public class LtsZip {


    static public LocationLabelFormatter defaultLocationLabelFormatter=
            (location ,output ) -> "{ " + location.getName() + " | " + output.getName() + " }";

    static public LocationLabelParser defaultLocationLabelParser =
            label -> {
                 int start = label.indexOf('{');
                 int mid = label.indexOf('|');
                 int end = label.lastIndexOf('}');
                 String location = label.substring(start+1,mid).trim();
                 String output = label.substring(mid+1,end).trim();
                 return new  Pair<LocationState,OutputAction>(new LocationState(location), new OutputAction(output));
            };


    // note: we use getName() to that it doesn't display ? or !
    //       because these are printed by the toString method of input and output)
    static public TransitionLabelFormatter defaultTransitionLabelFormatter =
             input ->   input.getName() ;


    static public TransitionLabelParser defaultTransitionLabelParser =
        inputlabel -> new InputAction(inputlabel) ;


    static public LtsNonDetModel zipMoore2Lts(MooreModel model,LocationLabelFormatter locationFormatter, TransitionLabelFormatter transitionFormatter) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();


        for (InputTransition trans : model.getModelTransitions()) {
            String label = transitionFormatter.apply(trans.getInput());
            Action action = new Action(label);
            String src = locationFormatter.apply(trans.getSource(), model.getOutputAt(trans.getSource()));
            String dest = locationFormatter.apply(trans.getDestination(), model.getOutputAt(trans.getDestination()));
            builder.addTransition(new LocationState(src), new LocationState(dest), action);
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
    }

    static public LtsNonDetModel zipMoore2Lts(MooreModel model) {
        return zipMoore2Lts(model, defaultLocationLabelFormatter, defaultTransitionLabelFormatter);
    }
}
