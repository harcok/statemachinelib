package statemachine.model.fsm.ia.conversion;

import java.util.function.Function;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.ia.IaModel;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsZip {

	static public LtsNonDetModel zipIa2Lts(IaModel model) {
	    return zipIa2Lts(model,i-> "?" + i.getName(), o-> "?" + o.getName());
	}

    static public LtsNonDetModel zipIa2Lts(IaModel model, Function<InputAction,String> formatInput,Function<OutputAction,String> formatOutput) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

        for (InputTransition trans: model.getModelInputTransitions() ) {
            Action action = new Action(formatInput.apply(trans.getInput()));
            builder.addTransition(trans.getSource(), trans.getDestination(), action);
        }
        for (OutputTransition trans: model.getModelOutputTransitions() ) {
            Action action = new Action(formatOutput.apply(trans.getOutput()));
            builder.addTransition(trans.getSource(), trans.getDestination(), action);
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
    }

	static public IaNonDetModel unzipLts2IaS(LtsModel model){
		    IaNonDetModel.ImmutableBuilder builder = new IaNonDetModel.ImmutableBuilder();

	  		for (ActionTransition trans: model.getModelTransitions() ) {
	  			String label=trans.getAction().getName();
	  			if ( label.trim().startsWith("?")) {
	  				InputAction input =  new InputAction(label.trim().substring(1).trim());
	  			    builder.addInputTransition(trans.getSource(), trans.getDestination(), input);
	  			} else if ( label.trim().startsWith("!")) {
	  				OutputAction output =  new OutputAction(label.trim().substring(1).trim());
	  			    builder.addOutputTransition(trans.getSource(), trans.getDestination(), output);
	  			} else {
	  				throw new RuntimeException("cannot unzip label: " + label );
	  			}
	 		}
	  		builder.setStartLocation(model.getStartLocation());
	  		return builder.build();
	  }

}
