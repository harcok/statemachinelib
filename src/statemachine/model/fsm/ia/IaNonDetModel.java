package statemachine.model.fsm.ia;

public class IaNonDetModel  extends IaBaseModel implements IaModel {


    //Builder class
    public static final class ImmutableBuilder extends IaBaseModel.ImmutableBuilder {
        
        public IaNonDetModel build(){
            super.baseBuild();
            return new IaNonDetModel(this);
        }
    }
    
    // Constructor
    private IaNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }
    
    // static factory from Model
    static public IaNonDetModel fromIAModel(IaModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromIAModel(model);
        return builder.build();
    }


}
