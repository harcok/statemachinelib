package statemachine.model.fsm.ia.file;

import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.ia.conversion.LtsZip;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class Import {

	static public IaNonDetModel  dot2IaModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.dot2LtsModel( filename);
	       return LtsZip.unzipLts2IaS(lts);
	}
	
	static public IaNonDetModel  aldebaran2IaModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel( filename);
	       return LtsZip.unzipLts2IaS(lts);
	}
	
	static public IaNonDetModel  gml2IaModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel( filename);
	       return LtsZip.unzipLts2IaS(lts);
	}

}
