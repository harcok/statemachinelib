package statemachine.model.fsm.ia.walker;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.StepInputOrOutput;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

import statemachine.model.fsm.ia.IaDetModelInterface;


@NonNullByDefault
public class IaDetWalker extends IaBaseWalker<IaDetModelInterface> implements StepInputOrOutput<LocationState,InputAction,OutputAction> {


	public IaDetWalker(IaDetModelInterface model ) {
		super(model);
	}

	// interface StepInputOrOutput

	@Override
	public boolean checkInputPossible(InputAction action) {
		Optional<InputTransition> optTransition = model.getTriggeredTransition(currentLocation, action);
		return optTransition.isPresent();
	}

	@Override
	public boolean checkOutputPossible(OutputAction action) {
		Optional<OutputTransition> optTransition = model.getSystemTransition(currentLocation, action);
		return optTransition.isPresent();
	}


	 /**
	  *  if path possible choose it, else do nothing
	  * @param action
	  */
	@Override
    public void stepInput(InputAction action){
	   	 Optional<InputTransition> optTransition=model.getTriggeredTransition(currentLocation, action);
	   	 if (optTransition.isPresent() ){
	   		 currentLocation = optTransition.get().getDestination();
	   	 }
	   	 // else do nothing
    }

	@Override
    public void stepOutput(OutputAction action){
	   	 Optional<OutputTransition> optTransition=model.getSystemTransition(currentLocation, action);
	   	 if (optTransition.isPresent() ){
	   		 currentLocation = optTransition.get().getDestination();
	   	 }
	   	 // else do nothing
	}

}