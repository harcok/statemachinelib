package statemachine.model.fsm.ia;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.interfaces.system.properties.OutputDetermined;
import statemachine.interfaces.system.properties.OutputDeterministic;
import statemachine.interfaces.system.properties.StateDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;

/**  IaBehaviorDetModel is an IaDetModel which is also OutputDetermined
 *
 * @author harcok
 *
 */
public class IaBehaviorDetModel  extends IaBaseModel
implements IaDetModelInterface,OutputDetermined<LocationState, OutputTransition>
/*
implements IaModel,
           StateDeterministic<LocationState,InputAction,OutputAction>,
           InputDeterministic<LocationState, InputAction, InputTransition>,
           OutputDeterministic<LocationState, OutputAction, OutputTransition>,
           OutputDetermined<LocationState, OutputTransition>*/
{

    //Builder class
    public static final class ImmutableBuilder extends IaBaseModel.ImmutableBuilder {

        public IaBehaviorDetModel build() {
            super.baseBuild();
            return new IaBehaviorDetModel(this);
        }

        // interface NoHiddenBehavior  ( StateDeterministic extends NoHiddenBehavior)

        @Override
        public ImmutableBuilder addHiddenTransition(LocationState src, LocationState dst, HiddenAction action) {

            throw new UnsupportedOperationException();
        }
    }

    // Constructor
    private IaBehaviorDetModel(ImmutableBuilder builder) {
        super(builder);

        // check for input none-determinism ; if found it throws exception
        for (LocationState location : this.loc2inputtrans.keySet()) {
            for (InputAction action : this.inputalphabet) {
                getTriggeredTransition(location, action); // throws exception when none-determinism is detected!
            }
        }

        // check for output none-determinism ; if found it throws exception
        for (LocationState location : this.loc2outputtrans.keySet()) {
            for (OutputAction action : this.outputalphabet) {
                getSystemTransition(location, action); // throws exception when none-determinism is detected!
            }
        }

        // check for output-determined holds ; if violation found it throws exception
        for (LocationState location : this.loc2outputtrans.keySet()) {
            getSystemTriggeredOutputTransition(location); // throws exception when violation of  output-determined is detected!
            ImmutableSet<@NonNull OutputTransition> output_transitions = this.getModelOutputTransitions(location);
        }
    }

    // static factory from model
    static public IaBehaviorDetModel fromIAModel(IaModel model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromIAModel(model);
        return builder.build();
    }

    // interface InputDeterministic

    @Override
    public Optional<InputTransition> getTriggeredTransition(LocationState location, InputAction action) {
        ImmutableSet<@NonNull InputTransition> transitions = this.getModelInputTransitions(location, action);
        return Util.getOptionalTransition(transitions);
    }

    // interface OutputDeterministic

    @Override
    public Optional<OutputTransition> getSystemTransition(LocationState location, OutputAction action) {
        ImmutableSet<@NonNull OutputTransition> transitions = this.getModelOutputTransitions(location, action);
        return Util.getOptionalTransition(transitions);
    }

    // interface StateDeterministic
    @Override
    public LocationState getSystemsNextState(LocationState state, InputAction input) {
        Optional<InputTransition> optTrans = getTriggeredTransition(state, input);
        if (optTrans.isPresent()) {
            return optTrans.get().getDestination();
        }
        return state;
    }

    @Override
    public LocationState getSystemsNextState(LocationState state, OutputAction output) {
        Optional<OutputTransition> optTrans = getSystemTransition(state, output);
        if (optTrans.isPresent()) {
            return optTrans.get().getDestination();
        }
        return state;
    }

    // interface OutputDeterministic

    @Override
    public Optional<OutputTransition> getSystemTriggeredOutputTransition(LocationState state) {

        ImmutableSet<@NonNull OutputTransition> output_transitions = this.getModelOutputTransitions(state);
        //  output-determined requires maximal one output transition per location
        //  next line will throw exception if location has more then one output transitions
        return Util.getOptionalTransition(output_transitions);

    }

}
