package statemachine.model.fsm.ia.genericwalkertest;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.walker.CurrentState;
import statemachine.interfaces.system.walker.ResetState;

@NonNullByDefault
public class IaGenericBaseWalker<L extends SystemState & ModelLocation, T extends ModelTransition<L>, M extends ModelAutomaton<L, T> & InitialState<L, L>>
        implements
            CurrentState<L>,
            ResetState<L> {

    protected L currentLocation;
    protected M model;

    // Constructor
    public IaGenericBaseWalker(M model) {
        this.model = model;
        this.currentLocation = model.getStartLocation();
    }

    // getters
    public L getLocation() {
        return currentLocation;
    }

    public @NonNull M getModel() {
        return model;
    }

    // interface CurrentState

    @Override
    public void resetState() {
        this.currentLocation = model.getStartLocation();

    }

    @Override
    public L getState() {
        return currentLocation;
    }

    @Override
    public void setState(L state) {
        this.currentLocation = state;
    }
}
