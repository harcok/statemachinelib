package statemachine.model.fsm.ia.genericwalkertest;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.elements.transition.BaseTransition;
import statemachine.model.fsm.ia.IaDetModel;

public class IaDetWalker extends IaGenericDetWalker
  <InputAction,OutputAction,LocationState,BaseTransition<LocationState>,InputTransition,OutputTransition,IaDetModel >
{

	public IaDetWalker(IaDetModel model) {
		super(model);
	}
};


