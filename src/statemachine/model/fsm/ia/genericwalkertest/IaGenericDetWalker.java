package statemachine.model.fsm.ia.genericwalkertest;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.actionmarkers.ModelInput;
import statemachine.interfaces.model.actionmarkers.ModelOutput;
import statemachine.interfaces.model.actiontypemarkers.ModelInputType;
import statemachine.interfaces.model.actiontypemarkers.ModelOutputType;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelInputTransition;
import statemachine.interfaces.model.transitions.ModelOutputTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.interfaces.system.actionmarkers.SystemInput;
import statemachine.interfaces.system.actionmarkers.SystemOutput;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.interfaces.system.properties.NoHiddenBehavior;
import statemachine.interfaces.system.properties.OutputDeterministic;
import statemachine.interfaces.system.statemarkers.SystemState;
import statemachine.interfaces.system.transitionmarkers.SystemTriggeredOutputTransition;
import statemachine.interfaces.system.walker.StepInputOrOutput;

@NonNullByDefault
public class IaGenericDetWalker<I extends ModelInputType & ModelInput & SystemInput,
        O extends ModelOutputType & ModelOutput & SystemOutput, L extends SystemState & ModelLocation,
        T extends ModelDirectedTransition<L>, IT extends ModelInputTransition<?, L>,
        OT extends ModelOutputTransition<?, L> & SystemTriggeredOutputTransition,
        M extends ModelAutomaton<L, T> & InitialState<L, L> & NoHiddenBehavior & InputDeterministic<L, I,
                IT> & OutputDeterministic<L, O, OT>>
        extends
            IaGenericBaseWalker<L, T, M>
        implements
            StepInputOrOutput<L, I, O> {

    public IaGenericDetWalker(M model) {
        super(model);
        resetState();
    }

    // interface StepInputOrOutput

    @Override
    public boolean checkInputPossible(I action) {
        Optional<IT> optTransition = model.getTriggeredTransition(currentLocation, action);
        return optTransition.isPresent();
    }

    @Override
    public boolean checkOutputPossible(O action) {
        Optional<OT> optTransition = model.getSystemTransition(currentLocation, action);
        return optTransition.isPresent();
    }

    /**
     * if more then one path possible randomly chooses one
     *
     * @param action
     */
    @Override
    public void stepInput(I action) {
        Optional<IT> optTransition = model.getTriggeredTransition(currentLocation, action);
        if (optTransition.isPresent()) {
            currentLocation = optTransition.get().getDestination();
        }
        // else do nothing
    }

    @Override
    public void stepOutput(O action) {
        Optional<OT> optTransition = model.getSystemTransition(currentLocation, action);
        if (optTransition.isPresent()) {
            currentLocation = optTransition.get().getDestination();
        }
        // else do nothing
    }

}
