package statemachine.model.fsm.ia;

import java.util.HashSet;
import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.HiddenAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.HiddenTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.elements.transition.BaseTransition;

public abstract class IaBaseModel implements IaModel{

	protected final ImmutableSet<InputAction> inputalphabet;
	protected final ImmutableSet<OutputAction> outputalphabet;
	protected final ImmutableSet<HiddenAction> hiddenalphabet;
	private   final LocationState startLocation;
	private   final ImmutableSet<LocationState> locations;
	private   final ImmutableSet<InputTransition> inputTransitions;
	private   final ImmutableSet<OutputTransition> outputTransitions;
	protected final ImmutableSet<HiddenTransition> hiddenTransitions;
	protected final ImmutableSetMultimap<LocationState,  InputTransition> loc2inputtrans;
	protected final ImmutableSetMultimap<LocationState,  OutputTransition> loc2outputtrans;
	protected final ImmutableSetMultimap<LocationState,  HiddenTransition> loc2hiddentrans;

    // Builder class
    public static abstract class ImmutableBuilder{

	    	protected ImmutableSet<InputAction> inputalphabet;
	    	protected ImmutableSet<OutputAction> outputalphabet;
	    	protected ImmutableSet<HiddenAction> hiddenalphabet;
	    	protected LocationState startLocation;
	    	protected ImmutableSet<LocationState> locations;
	    	protected ImmutableSet<InputTransition> inputTransitions;
	    	protected ImmutableSet<OutputTransition> outputTransitions;
	    	protected ImmutableSet<HiddenTransition> hiddenTransitions;
	    	protected ImmutableSetMultimap<LocationState,  InputTransition> loc2inputtrans;
	    	protected ImmutableSetMultimap<LocationState,  OutputTransition> loc2outputtrans;
	    	protected ImmutableSetMultimap<LocationState,  HiddenTransition> loc2hiddentrans;

	    	// builders
	    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<LocationState> locationsBuilder=ImmutableSet.builder();

	    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<InputTransition> inputTransitionsBuilder=ImmutableSet.builder();
	    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<OutputTransition> outputTransitionsBuilder=ImmutableSet.builder();
	    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<HiddenTransition> hiddenTransitionsBuilder=ImmutableSet.builder();

	    	@SuppressWarnings("null")
		protected ImmutableSet.Builder<InputAction> inputalphabetBuilder=ImmutableSet.builder();
	    @SuppressWarnings("null")
	    	protected ImmutableSet.Builder<OutputAction> outputalphabetBuilder=ImmutableSet.builder();
	    @SuppressWarnings("null")
	    	protected ImmutableSet.Builder<HiddenAction> hiddenalphabetBuilder=ImmutableSet.builder();

		// store transitions per location
		@SuppressWarnings("null")
		protected ImmutableSetMultimap.Builder<LocationState,  InputTransition> loc2inputransBuilder = ImmutableSetMultimap.builder();
		@SuppressWarnings("null")
		protected ImmutableSetMultimap.Builder<LocationState,  OutputTransition> loc2outputransBuilder = ImmutableSetMultimap.builder();
		@SuppressWarnings("null")
		protected ImmutableSetMultimap.Builder<LocationState,  HiddenTransition> loc2hiddentransBuilder = ImmutableSetMultimap.builder();



        public ImmutableBuilder addInputTransition(LocationState src,LocationState dst, InputAction action){
            this.locationsBuilder.add(src,dst);
            this.inputalphabetBuilder.add(action);
            InputTransition transition = new InputTransition(src,dst,action);
            this.inputTransitionsBuilder.add(transition);
            this.loc2inputransBuilder.put(src,transition);
            return this;
        }


        public ImmutableBuilder addOutputTransition(LocationState src,LocationState dst, OutputAction action){
            this.locationsBuilder.add(src,dst);
            this.outputalphabetBuilder.add(action);
            OutputTransition transition = new OutputTransition(src,dst,action);
            this.outputTransitionsBuilder.add(transition);
            this.loc2outputransBuilder.put(src,transition);
            return this;
        }

        public ImmutableBuilder addHiddenTransition(LocationState src,LocationState dst, HiddenAction action){
            this.locationsBuilder.add(src,dst);
            this.hiddenalphabetBuilder.add(action);
            HiddenTransition transition = new HiddenTransition(src,dst,action);
            this.hiddenTransitionsBuilder.add(transition);
            this.loc2hiddentransBuilder.put(src,transition);
            return this;
        }

        public ImmutableBuilder setStartLocation(LocationState startLocation){
            this.startLocation =startLocation;
            return this;
        }


    	protected ImmutableBuilder  addFromIAModel(IaModel model) {
    		for (InputTransition trans: model.getModelInputTransitions() ) {
    		   this.addInputTransition(trans.getSource(), trans.getDestination(), trans.getInput());
    		}
    		for (OutputTransition trans: model.getModelOutputTransitions() ) {
     		   this.addOutputTransition(trans.getSource(), trans.getDestination(), trans.getOutput());
     	}
    		for (HiddenTransition trans: model.getModelHiddenTransitions() ) {
      		   this.addHiddenTransition(trans.getSource(), trans.getDestination(), trans.getHiddenAction());
      	}
    		this.setStartLocation(model.getStartLocation());
    		return this;
    	}



        @SuppressWarnings("null")
		protected void baseBuild(){
    		this.inputalphabet = inputalphabetBuilder.build();
    		this.outputalphabet = outputalphabetBuilder.build();
    		this.hiddenalphabet = hiddenalphabetBuilder.build();
    		//this.startLocation = ...; // no builder for startlocation
    		this.locations = locationsBuilder.build();
    		this.inputTransitions = inputTransitionsBuilder.build();
    		this.outputTransitions = outputTransitionsBuilder.build();
    		this.hiddenTransitions = hiddenTransitionsBuilder.build();
    		this.loc2outputtrans = loc2outputransBuilder.build();
    		this.loc2inputtrans = loc2inputransBuilder.build();
    		this.loc2hiddentrans = loc2hiddentransBuilder.build();
        }
    }




	// Constructor

	protected IaBaseModel(ImmutableBuilder builder) {
		this.inputalphabet = builder.inputalphabet;
		this.outputalphabet = builder.outputalphabet;
		this.hiddenalphabet = builder.hiddenalphabet;
		this.startLocation = builder.startLocation;
		this.locations = builder.locations;
		this.inputTransitions = builder.inputTransitions;
		this.outputTransitions = builder.outputTransitions;
		this.hiddenTransitions = builder.hiddenTransitions;
		this.loc2inputtrans = builder.loc2inputtrans;
		this.loc2outputtrans = builder.loc2outputtrans;
		this.loc2hiddentrans = builder.loc2hiddentrans;
	}




    // interface InitialState

    @Override
    public @NonNull LocationState getStartLocation() {
        return startLocation;
    }

    @Override
    public @NonNull LocationState getStartState() {
        return startLocation;
    }


    // interface ModelAutomaton

	@Override
	public ImmutableSet<LocationState> getLocations() {
		return locations;
	}

	@Override
	public ImmutableSet<BaseTransition<LocationState>> getModelTransitions() {
		HashSet<BaseTransition<LocationState>> set = new HashSet<>();
		set.addAll(inputTransitions);
		set.addAll(outputTransitions);
		set.addAll(hiddenTransitions);
		return ImmutableSet.copyOf(set);
	}

	@Override
	public ImmutableSet<BaseTransition<LocationState>> getModelTransitions(LocationState location) {
		ImmutableSet.Builder<BaseTransition<LocationState>> transitionsBuilder=ImmutableSet.builder();
		transitionsBuilder.addAll(loc2inputtrans.get(location));
		transitionsBuilder.addAll(loc2outputtrans.get(location));
		transitionsBuilder.addAll(loc2hiddentrans.get(location));
		return transitionsBuilder.build();
	}



	// interface ModelInterfaceAutomata

	@Override
	public ImmutableSet<InputAction> getInputAlphabet() {
		return inputalphabet;
	}

	@Override
	public ImmutableSet<OutputAction> getOutputAlphabet() {
		return outputalphabet;
	}

	@Override
	public ImmutableSet<HiddenAction> getHiddenAlphabet() {
		return hiddenalphabet;
	}

	@Override
	public @NonNull ImmutableSet<@NonNull InputTransition> getModelInputTransitions() {
		return inputTransitions;
	}

	@Override
	public @NonNull ImmutableSet<@NonNull OutputTransition> getModelOutputTransitions() {
		return outputTransitions;
	}

	@Override
	public @NonNull ImmutableSet<@NonNull HiddenTransition> getModelHiddenTransitions() {
		return hiddenTransitions;
	}

	@Override
	public @NonNull ImmutableSet<@NonNull InputTransition> getModelInputTransitions(@NonNull LocationState location) {
		return loc2inputtrans.get(location);
	}

	@Override
	public @NonNull ImmutableSet<@NonNull OutputTransition> getModelOutputTransitions(@NonNull LocationState location) {
		return loc2outputtrans.get(location);
	}

	@Override
	public @NonNull ImmutableSet<@NonNull HiddenTransition> getModelHiddenTransitions(@NonNull LocationState location) {
		return loc2hiddentrans.get(location);
	}



	@Override
	public @NonNull ImmutableSet<@NonNull InputTransition> getModelInputTransitions(@NonNull LocationState location, @NonNull InputAction action) {
		ImmutableSet.Builder<InputTransition> transitionsBuilder=ImmutableSet.builder();
		@NonNull ImmutableSet<@NonNull InputTransition> transitions= loc2inputtrans.get(location);

   	    for ( InputTransition transition : transitions ) {
   		   if (transition.getInput().equals(action) ) {
   			  transitionsBuilder.add(transition);
   		   }
   	    }
   	    return transitionsBuilder.build();
	}

	@Override
	public @NonNull ImmutableSet<@NonNull OutputTransition> getModelOutputTransitions(@NonNull LocationState location, @NonNull OutputAction action) {
		ImmutableSet.Builder<OutputTransition> transitionsBuilder=ImmutableSet.builder();
		@NonNull ImmutableSet<@NonNull OutputTransition> transitions= loc2outputtrans.get(location);

   	    for ( OutputTransition transition : transitions ) {
   		   if (transition.getOutput().equals(action) ) {
   			  transitionsBuilder.add(transition);
   		   }
   	    }
   	    return transitionsBuilder.build();
	}


	@Override
	public @NonNull ImmutableSet<@NonNull HiddenTransition> getModelHiddenTransitions(@NonNull LocationState location, @NonNull HiddenAction action) {
		ImmutableSet.Builder<HiddenTransition> transitionsBuilder=ImmutableSet.builder();
		@NonNull ImmutableSet<@NonNull HiddenTransition> transitions= loc2hiddentrans.get(location);

   	    for ( HiddenTransition transition : transitions ) {
   		   if (transition.getHiddenAction().equals(action) ) {
   			  transitionsBuilder.add(transition);
   		   }
   	    }
   	    return transitionsBuilder.build();
	}





	// for debugging

	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner("\n");
		joiner.add("start: " + startLocation);
		joiner.add("locations:");
		for ( LocationState location: getLocations()) {
			joiner.add(location.toString());
		}
		joiner.add("input transitions:");
		for (InputTransition transition: getModelInputTransitions() ) {
			joiner.add(transition.toString());
		}
		joiner.add("output transitions:");
		for (OutputTransition transition: getModelOutputTransitions() ) {
			joiner.add(transition.toString());
		}
		joiner.add("hidden transitions:");
		for (HiddenTransition transition: getModelHiddenTransitions() ) {
			joiner.add(transition.toString());
		}
		joiner.add("");
		return joiner.toString();
	}
}
