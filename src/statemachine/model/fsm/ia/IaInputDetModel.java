package statemachine.model.fsm.ia;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;


public class IaInputDetModel  extends IaBaseModel
         implements IaModel,
                    InputDeterministic<LocationState,InputAction,InputTransition> {

    //Builder class
    public static final class ImmutableBuilder extends IaBaseModel.ImmutableBuilder {

        public IaInputDetModel build(){
            super.baseBuild();
            return new IaInputDetModel(this);
        }
    }

    // Constructor
    private IaInputDetModel(ImmutableBuilder builder) {
        super(builder);

		// check for input none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2inputtrans.keySet()) {
			for ( InputAction action : this.inputalphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}

    }

    // static factory from LTSModel
    static public IaInputDetModel fromIAModel(IaModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromIAModel(model);
        return builder.build();
    }



	// interface InputDeterministic

    @Override
	public Optional<InputTransition> getTriggeredTransition(LocationState location,InputAction action) {
		ImmutableSet<@NonNull InputTransition> transitions = this.getModelInputTransitions(location, action);
		return Util.getOptionalTransition(transitions);
	}


}