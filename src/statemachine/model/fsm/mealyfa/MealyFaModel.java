package statemachine.model.fsm.mealyfa;

import statemachine.interfaces.model.types.ModelMealyFiniteAcceptor;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;

public interface MealyFaModel
        extends ModelMealyFiniteAcceptor<InputAction, OutputAction, LocationState, MealyTransition>,
        InitialState<LocationState, LocationState> {
}
