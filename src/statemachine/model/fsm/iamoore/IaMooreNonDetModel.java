package statemachine.model.fsm.iamoore;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.ia.IaBaseModel;
import statemachine.model.fsm.ia.IaModel;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.ia.IaNonDetModel.ImmutableBuilder;

public class IaMooreNonDetModel extends IaMooreBaseModel {

 
    //Builder class
    public static final class ImmutableBuilder extends IaMooreBaseModel.ImmutableBuilder {
    	
        public ImmutableBuilder addOutput(LocationState src, OutputAction output){
             super.addOutput(src,output);
             return this;
        }
    	
        public IaMooreNonDetModel build(){
            super.baseBuild();
            return new IaMooreNonDetModel(this);
        }
    }
    
    // Constructor
    private IaMooreNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }
    
    // static factory from LTSModel
    static public IaMooreNonDetModel fromIAModel(IaMooreModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromIaMooreModel(model);
        return builder.build();
    } 
 


}
