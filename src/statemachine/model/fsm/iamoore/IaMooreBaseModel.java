package statemachine.model.fsm.iamoore;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.elements.transition.OutputTransition;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.FaBaseModel.ImmutableBuilder;
import statemachine.model.fsm.ia.IaBaseModel;
import statemachine.model.fsm.ia.IaModel;
import statemachine.model.fsm.moore.MooreBaseModel;

public abstract class IaMooreBaseModel extends IaBaseModel implements IaMooreModel {

    protected ImmutableMap<LocationState, OutputAction> loc2output;

    //Builder class
    public static class ImmutableBuilder extends IaBaseModel.ImmutableBuilder {
        protected ImmutableMap<LocationState, OutputAction> loc2output;

        @SuppressWarnings("null")
        protected ImmutableMap.Builder<LocationState, OutputAction> loc2outputBuilder = ImmutableMap.builder();

        public ImmutableBuilder addFromIaMooreModel(IaMooreModel model) {
            for (InputTransition trans : model.getModelInputTransitions()) {
                this.addInputTransition(trans.getSource(), trans.getDestination(), trans.getInput());
            }
            for (OutputTransition trans : model.getModelOutputTransitions()) {
                this.addOutputTransition(trans.getSource(), trans.getDestination(), trans.getOutput());
            }
            this.setStartLocation(model.getStartLocation());
            for (LocationState location : model.getLocations()) {
                OutputAction output = model.getOutputAt(location);
                this.addOutput(location, output);
            }
            return this;
        }

        protected ImmutableBuilder addOutput(LocationState src, OutputAction output) {
            this.locationsBuilder.add(src);
            this.outputalphabetBuilder.add(output);
            this.loc2outputBuilder.put(src, output);
            return this;
        }

        protected void baseBuild() {

            // build  locations to corresponding outputs map
            this.loc2output = loc2outputBuilder.build();

            // do build into final fields
            super.baseBuild();
        }
    }

    // Constructor
    protected IaMooreBaseModel(ImmutableBuilder builder) {
        super(builder);
        this.loc2output = builder.loc2output;
    }

    @Override
    public OutputAction getOutputAt(LocationState location) {
        return loc2output.get(location);
    }

}
