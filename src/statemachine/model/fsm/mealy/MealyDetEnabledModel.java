package statemachine.model.fsm.mealy;



import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;


import statemachine.interfaces.system.properties.InputEnabledDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.mealy.util.IosStep;

@NonNullByDefault
public class MealyDetEnabledModel extends MealyBaseModel
      implements MealyModel, InputEnabledDeterministic<LocationState,InputAction,MealyTransition>
{


    //Builder class
    public static final class ImmutableBuilder extends MealyBaseModel.ImmutableBuilder {

        public MealyDetEnabledModel build(){
            super.baseBuild();
            return new MealyDetEnabledModel(this);
        }
    }

    // Constructor
    private MealyDetEnabledModel(ImmutableBuilder builder) {
        super(builder);

        // verify input-determinism and verify model is input enabled; if verification fails it throws exception
        for (LocationState location:  this.loc2trans.keySet()) {
            for ( InputAction action : this.inputalphabet ) {
                getTriggeredTransition(location,action);  // throws exception when none-determinism is detected or input not enabled!
            }
        }
    }

    // static factory from MealyModel
    static public MealyDetEnabledModel fromMealyModel(MealyModel model) {
        ImmutableBuilder builder =  new ImmutableBuilder();
        builder.addFromMealyModel(model);
        return builder.build();
    }

    // interface InputEnabledDeterministic

    @Override
    public MealyTransition getTriggeredTransition(LocationState location,InputAction action) {
        ImmutableSet<MealyTransition> transitions = this.getModelTransitions(location, action);
        return Util.getTheTransition(transitions,location.getName(),action.getName());
    }

    public List<IosStep> getTrace(List<InputAction> inputs) {
        List<IosStep> trace = new LinkedList<>();
        LocationState curloc = this.getStartLocation();
        trace.add(new IosStep(null, null, curloc));
        for (InputAction input :inputs) {
             MealyTransition trans = this.getTriggeredTransition(curloc, input);
             LocationState dest = trans.getDestination();
             OutputAction output = trans.getOutput();
             trace.add(new IosStep(input, output, dest));
             curloc=dest;
        }
        return trace;
    }
    public List<IosStep> getTraceFrom(LocationState beginLocation,List<InputAction> inputs) {
        List<IosStep> trace = new LinkedList<>();
     //   LocationState curloc = this.getStartLocation();
        LocationState curloc = beginLocation;
        trace.add(new IosStep(null, null, curloc));
        for (InputAction input :inputs) {
             MealyTransition trans = this.getTriggeredTransition(curloc, input);
             LocationState dest = trans.getDestination();
             OutputAction output = trans.getOutput();
             trace.add(new IosStep(input, output, dest));
             curloc=dest;
        }
        return trace;
    }
}