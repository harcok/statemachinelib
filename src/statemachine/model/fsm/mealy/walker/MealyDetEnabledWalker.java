package statemachine.model.fsm.mealy.walker;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.StepInputGetOutput;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;


@NonNullByDefault
public class MealyDetEnabledWalker extends MealyBaseWalker<MealyDetEnabledModel> implements StepInputGetOutput<LocationState,InputAction,OutputAction> {


    public MealyDetEnabledWalker(MealyDetEnabledModel model ) {
        super(model);
    }

    // interface StepInputGetOutput

    @Override
    public boolean checkInputPossible(InputAction action) {
        return true;  // because model is input enable ; implements InputEnabledDeterministic interface
    }




     /**
      *  step for always enabled input
      *
      * @param action
     * @return
      */
    @Override
    public OutputAction stepInput(InputAction action){
         MealyTransition transition=model.getTriggeredTransition(currentLocation, action);
         currentLocation = transition.getDestination();
         return transition.getOutput();
    }


}
