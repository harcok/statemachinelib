package statemachine.model.fsm.mealy.walker;



import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.StepInputGetOptionalOutput;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.mealy.MealyDetModel;


@NonNullByDefault
public class MealyDetWalker extends MealyBaseWalker<MealyDetModel> implements StepInputGetOptionalOutput<LocationState,InputAction,OutputAction> {


    public MealyDetWalker(MealyDetModel model ) {
        super(model);
    }

    // interface StepInputOrOutput

    @Override
    public boolean checkInputPossible(InputAction action) {
        Optional<MealyTransition> optTransition = model.getTriggeredTransition(currentLocation, action);
        return optTransition.isPresent();
    }




     /**
      *  if path possible choose it and return Output, else do nothing and return empty result
      *
      * @param action
     * @return
      */
    @Override
    public Optional<OutputAction> stepInput(InputAction action){
         Optional<MealyTransition> optTransition=model.getTriggeredTransition(currentLocation, action);
         if (optTransition.isPresent() ){
             MealyTransition transition=optTransition.get();
             currentLocation = transition.getDestination();
             return Optional.of(transition.getOutput());
         } else {
             return Optional.empty();
         }
    }



}
