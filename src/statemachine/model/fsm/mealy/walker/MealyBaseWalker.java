package statemachine.model.fsm.mealy.walker;



import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.CurrentState;
import statemachine.interfaces.system.walker.ResetState;
import statemachine.model.elements.location.LocationState;

import statemachine.model.fsm.mealy.MealyModel;

@NonNullByDefault
public class MealyBaseWalker<M extends MealyModel> implements CurrentState<LocationState>, ResetState<LocationState> {

    protected LocationState currentLocation;
    protected M model;

    // Constructor
    public MealyBaseWalker(M model) {
        this.model = model;
        this.currentLocation = model.getStartLocation();
    }

    // getters

    public LocationState getLocation() {
        return currentLocation;
    }

    public @NonNull M getModel() {
        return model;
    }

    // interface CurrentState

    @Override
    public LocationState getState() {
        return currentLocation;
    }

    @Override
    public void setState(LocationState state) {
        this.currentLocation = state;
    }

    // interface ResetState

    @Override
    public void resetState() {
        this.currentLocation = model.getStartLocation();
    }
}
