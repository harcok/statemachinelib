package statemachine.model.fsm.mealy;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;

@NonNullByDefault
public class MealyDetModel extends MealyBaseModel
      implements MealyModel, InputDeterministic<LocationState,InputAction,MealyTransition>
{


    //Builder class
    public static final class ImmutableBuilder extends MealyBaseModel.ImmutableBuilder {

        public MealyDetModel build(){
    		    super.baseBuild();
            return new MealyDetModel(this);
        }
    }

	// Constructor
	private MealyDetModel(ImmutableBuilder builder) {
		super(builder);

		// check for input none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2trans.keySet()) {
			for ( InputAction action : this.inputalphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}
	}

	// static factory from MealyModel
	static public MealyDetModel fromMealyModel(MealyModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromMealyModel(model);
		return builder.build();
	}

	// interface InputDeterministic

    @Override
	public Optional<MealyTransition> getTriggeredTransition(LocationState location,InputAction action) {
		ImmutableSet<MealyTransition> transitions = this.getModelTransitions(location, action);
		return Util.getOptionalTransition(transitions);
	}
}