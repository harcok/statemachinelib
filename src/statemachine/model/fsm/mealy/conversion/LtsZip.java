package statemachine.model.fsm.mealy.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.mealy.MealyModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;

public class LtsZip {

    // note: we use getName() to that it doesn't display ? or !
    //       because these are printed by the toString method of input and output)
    static public TransitionLabelFormatter defaultTransitionLabelFormatter =
            ( input, output) ->  input.getName() + "/" + output.getName();


    static public TransitionLabelParser defaultTransitionLabelParser = new TransitionLabelParser() {
        public Pair<InputAction, OutputAction> apply(String label) {
            String[] parts = label.split("/");
            InputAction input = new InputAction(parts[0].trim());
            OutputAction output = new OutputAction(parts[1].trim());
            return new Pair<InputAction, OutputAction>(input, output);
        }
    };

    static public LtsNonDetModel zipMealy2Lts(MealyModel model, TransitionLabelFormatter transitionFormatter) {
        LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

        for (MealyTransition trans : model.getModelTransitions()) {
            String label = transitionFormatter.apply(trans.getInput(),trans.getOutput());
            Action action = new Action(label);
            builder.addTransition(trans.getSource(), trans.getDestination(), action);
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
    }

    static public LtsNonDetModel zipMealy2Lts(MealyModel model) {
        return zipMealy2Lts(model, defaultTransitionLabelFormatter);
    }

    static public MealyNonDetModel unzipLts2Mealy(LtsModel model, TransitionLabelParser labelParser) {
        MealyNonDetModel.ImmutableBuilder builder = new MealyNonDetModel.ImmutableBuilder();

        for (ActionTransition trans : model.getModelTransitions()) {
            String label = trans.getAction().getName();
            Pair<InputAction, OutputAction> input_output = labelParser.apply(label);
            InputAction input = input_output.getValue0();
            OutputAction output = input_output.getValue1();
            builder.addTransition(trans.getSource(), trans.getDestination(), input, output);
        }
        builder.setStartLocation(model.getStartLocation());
        return builder.build();
    }

    static public MealyNonDetModel unzipLts2Mealy(LtsModel model) {
        return unzipLts2Mealy(model, defaultTransitionLabelParser);
    }

}

