package statemachine.model.fsm.mealy.conversion;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;

/** TransitionLabelFormatter
 *
 *   Defines how to format input and output of a mealy machine to a string label.
 *   This label can be used to zip an Mealy model to a LTS model.
 *   We use this to export a Mealy model to one of the the lts file formats. (eg. graphviz dot file)
 *

 example:

	  TransitionLabelFormatter formatter = new TransitionLabelFormatter() {
		public String apply(InputAction input,OutputAction output) {
			return  input.toString() + "/" + output.toString();
		}
	  }

  Note: when using a lambda function notation it becomes very lightweight and clear to define a formatter :

  	  TransitionLabelFormatter formatter = (input, output) ->  input.getName() + "/" + output.getName();

*/
@FunctionalInterface
public interface TransitionLabelFormatter {
	public String apply (InputAction input,OutputAction output);
}
