package statemachine.model.fsm.mealy.conversion;

import java.util.HashSet;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.ListMultimap;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.mealy.MealyModel;
import statemachine.model.fsm.moore.MooreNonDetModel;

public class Conversion {

    public static MooreNonDetModel Mealy2MooreNonDetModel(MealyModel mealy) {
        MooreNonDetModel.ImmutableBuilder builder =  new MooreNonDetModel.ImmutableBuilder();

        LocationState mooreStartLocation,mealyStartLocation;

        mealyStartLocation=mealy.getStartLocation();

        //ListMultimap<LocationState, LocationState> mealyloc2mooreloc = ArrayListMultimap.create();

        HashMultimap<LocationState, LocationState> mealyloc2mooreloc =  HashMultimap.create();
       // ImmutableSetMultimap.Builder<LocationState, LocationState> mealyloc2moorelocBuilder = ImmutableSetMultimap.builder();



        // from incoming arrows derive all moore states
        //    --a/x--> q   =>   --a--> (q,x)
        for ( MealyTransition transition :mealy.getModelTransitions()) {
            //String locationname="(" + transition.getDestination().getName() + "," + transition.getOutput().getName() + ")";
            String locationname= "_" + transition.getDestination().getName() + "_" + transition.getOutput().getName();

            LocationState mooreloc=new LocationState(locationname);
            mealyloc2mooreloc.put(transition.getDestination(),mooreloc);

            // add output to moore location
            builder.addOutput(mooreloc, transition.getOutput() );
        }
       // ImmutableSetMultimap<LocationState, LocationState> mealyloc2mooreloc=  mealyloc2moorelocBuilder.build();

        // set start moore state
        //mooreStartLocation=mealyloc2mooreloc.get(mealyStartLocation).get(0);
        mooreStartLocation=mealyloc2mooreloc.get(mealyStartLocation).iterator().next();
        builder.setStartLocation(mooreStartLocation);

        /*
           from outgoing arrows derive all moore transitions
           for each moore state (q,o)  take all outgoing transition of mealy model from  q

              a/x                                          a
            q -> q'      =>  gives you transition    (q,o) -> (q',x)

              b/y                                          b
            q -> q''     =>  gives you transition    (q,o) -> (q'',y)

         */
        for ( MealyTransition transition :mealy.getModelTransitions()) {
            //String locationname="(" + transition.getDestination().getName() + "," + transition.getOutput().getName() + ")";
            String locationname= "_" +transition.getDestination().getName() + "_" + transition.getOutput().getName();
            LocationState dst=new LocationState(locationname);
            for ( LocationState src :mealyloc2mooreloc.get(transition.getSource())) {
                builder.addTransition(src, dst, transition.getInput());
            }
        }


        return builder.build();
    }


    public static IaNonDetModel Mealy2IaNonDetModel(MealyModel mealy) {
        IaNonDetModel.ImmutableBuilder builder =  new IaNonDetModel.ImmutableBuilder();
        builder.setStartLocation(mealy.getStartLocation());

        HashSet<String> seen= new HashSet<>(); // to merge similar transitions !!

        for ( MealyTransition transition :mealy.getModelTransitions()) {
            InputAction input=transition.getInput();
            OutputAction output=transition.getOutput();
            LocationState startLocation = transition.getSource();
            LocationState endLocation = transition.getDestination();
            //LocationState midLocation =  new LocationState("("+output.getName() +"," + endLocation.getName() + ")");
            LocationState midLocation =  new LocationState(output.getName() +"_" + endLocation.getName());

            String inputkey= midLocation.getName()+ "--" + endLocation.getName()+ "--?" + input.getName();
            if (! seen.contains(inputkey)) {
                builder.addInputTransition(startLocation, midLocation, input);
                seen.add(inputkey);
            }

            String outputkey= midLocation.getName()+ "--" + endLocation.getName()+ "--!" + output.getName();
            if (! seen.contains(outputkey)) {
              builder.addOutputTransition(midLocation, endLocation, output);
              seen.add(outputkey);
            }
        }

        return builder.build();
    }


}
