package statemachine.model.fsm.mealy.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;

@FunctionalInterface
public interface TransitionLabelParser {
	public Pair<InputAction,OutputAction> apply (String label);
}
