package statemachine.model.fsm.mealy.file;

import statemachine.model.fsm.mealy.conversion.LtsZip;


import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;

public class Import {

	static public MealyNonDetModel  dot2MealyModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.dot2LtsModel( filename);
	       return LtsZip.unzipLts2Mealy(lts);
	}

//  enduser must do this themselves => keep library light!  => document this instead!!
// ->  general approach:  <moreSpecificModel> model=<moreSpecificModel>.fromMealyModel(<basenondetmodel);
//
//    static public MealyDetModel importMealyDetModel(String filename) {
//        return MealyDetModel.fromMealyModel(dot2MealyModel(filename));
//    }

	static public MealyNonDetModel  aldebaran2MealyModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.aldebaran2LtsModel( filename);
	       return LtsZip.unzipLts2Mealy(lts);
	}

	static public MealyNonDetModel  gml2MealyModel( String filename){
		   LtsNonDetModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel( filename);
	       return LtsZip.unzipLts2Mealy(lts);
	}

}
