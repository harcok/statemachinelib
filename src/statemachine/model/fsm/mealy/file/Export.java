package statemachine.model.fsm.mealy.file;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import nl.ru.cs.tree.Tree;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.format.NuSMV;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;
import statemachine.model.fsm.mealy.MealyModel;
import statemachine.model.fsm.mealy.conversion.Conversion;
import statemachine.model.fsm.mealy.conversion.LtsZip;
import statemachine.model.fsm.mealy.util.SplittingTreeData;
import statemachine.model.fsm.mealy.util.Util;
import statemachine.util.IO;
import org.javatuples.Triplet;

public class Export {

	// mealy
	static public void dot(MealyModel model, String filename) {
		LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
		statemachine.model.fsm.lts.file.Export.dot(lts, filename);
	}

	static public void gml(MealyModel model, String filename) {
		LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
		statemachine.model.fsm.lts.file.Export.gml(lts, filename);
	}

	static public void aldebaran(MealyModel model, String filename) {
		LtsNonDetModel lts = LtsZip.zipMealy2Lts(model);
		statemachine.model.fsm.lts.file.Export.aldebaran(lts, filename);
	}
    static public void nusmvAsLts(MealyModel model, String filename) {
        IaNonDetModel iamodel = Conversion.Mealy2IaNonDetModel(model);
        statemachine.model.fsm.ia.file.Export.nusmv(iamodel, filename);
    }

//    static public void nusmv(MealyModel model, String filename) {
//     //   IaNonDetModel iamodel = Conversion.Mealy2IaNonDetModel(model);
//        statemachine.model.fsm.mealy.file.Export.nusmv(model, filename);
//    }

    static public void  nusmv(MealyModel model, String filename){
        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);
        IO.writeSmallTextFile( lines, filename);
    }

    static public void  nusmvWithSpecs(MealyDetEnabledModel model, String filename){

        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);

        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);

        ImmutableSet<MealyTransition> transitions = model.getModelTransitions();
        for ( MealyTransition transition : transitions ) {
            LocationState src = transition.getSource();
            LocationState dst = transition.getDestination();
            InputAction input = transition.getInput();
            OutputAction output = transition.getOutput();

            if (output.getName().equals("error")) continue;

            // SPEC  prefix for nusmv
            //String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";
            String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";

            lines.add("--transition: "+transition.toString());
            System.out.println("--transition: "+transition.toString());
            // System.out.println("--ACTL: "+formula);
            lines.add("--ACTL: "+formula);
            System.err.println("--ACTL: "+formula);

            String nusmvFormula = Util.parseFormula(formula);
            //System.out.println("SPEC " + nusmvFormula);
            lines.add("SPEC " + nusmvFormula);
        }

        IO.writeSmallTextFile( lines, filename);
    }


    static public void  nusmvWithSpecs_State2State(MealyDetEnabledModel model, String filename){
        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);

        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);
        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);
        Map<LocationState, String> state2distformulaMutated = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        ImmutableSet<LocationState> locations = model.getLocations();

        for ( LocationState src : locations) {
            for ( LocationState dst : locations ) {
                lines.add("--reachable: "+ src.getName() + " => " + dst.getName() );
               // String formula= "AG ( ("+ state2distformula.get(src) + ") => EF (" + state2distformula.get(dst) + ") )";
              //  String formula= "AG ( ("+ state2distformulaMutated.get(src) + ") => EF (" + state2distformula.get(dst) + ") )";
                String formula= "AG ( ("+ state2distformula.get(src) + ") => EF (" + state2distformulaMutated.get(dst) + ") )";
                String nusmvFormula = Util.parseFormula2(formula);
                lines.add("SPEC " + nusmvFormula);
            }
        }
        IO.writeSmallTextFile( lines, filename);
    }


    static public Random randomGenerator= new Random(43241);
 //--------------------------------------------------------------------------------------
    static public void  nusmvWithSpecsTrueAndFalse(MealyDetEnabledModel model, String filename){


        /*
         *
        int numberGood=8;
        int numberBad=8;
        int numberToGenerate=8;//50;
       // int numberToGenerate=50;
        int numberToGenerateConnecting=2;
        int numberGoodConnect=2;
        int numberBadConnect=2;

         */
       // Random
        // n=3,4,5 connected formulas, waarvan  1 tot n-1 good ( rest bad)
        int totalConnectedFormulas =  Util.getRandomNumberInRange(3,5,randomGenerator);  // [3:5]
        int numberGoodConnect  =  Util.getRandomNumberInRange(1,totalConnectedFormulas-1,randomGenerator); // [1,n-1]
        int numberBadConnect = totalConnectedFormulas - numberGoodConnect;

//  HACK: to still get rightly predicted formulas for m27 m201  =>  somehow  my good connected formulas don't work there => model not fully connected probably??
//        // disable good connect  for m27 and m201  => something wrong there
//        totalConnectedFormulas=2;
//        numberGoodConnect=0;
//        numberBadConnect=totalConnectedFormulas;

        // rest is transition formulas (15,16,17) waarvan  7,8,9  good , en (8,9,10),(7,8,9),(6,7,8) bad [6-10]
        int totalTransFormulas = 20-totalConnectedFormulas;
        int numberGood=Util.getRandomNumberInRange(7,9,randomGenerator); // [7:9]  good
        int numberBad=totalTransFormulas-numberGood;

        System.out.println("totalTransFormulas: " + totalTransFormulas + "  totalConnectedFormulas:"+ totalConnectedFormulas);
        System.out.println("numberGood:"+numberGood + "  numberBad:" + numberBad + "  numberGoodConnect:"+numberGoodConnect + "  numberBadConnect:" + numberBadConnect);

        LinkedList<Triplet<Boolean,String,String>> result_formulas= new LinkedList<>();


        List<String>  formula_lines = new ArrayList<>();
        List<String>  answeredformula_lines = new ArrayList<>();

        System.out.println("export model");
        List<String> nusmv_lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);
        System.out.println("importing separating family");
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);
        System.out.println("generating formulas");

        // get random  list of transitions
        List<MealyTransition> transitions = new LinkedList<>(model.getModelTransitions());
        Collections.shuffle(transitions, new Random(3));



        // add BAD formulas
        //====================

        // add 8 bad transition formulas
        // -----------------------------
        System.out.println(filename);
        System.out.println("--------------------------");
        System.out.println(" bad (good)");
        HashMap<String,List<MealyTransition>> formula2transitions =null;

        // first try good bad
        String marker= "bad(good)";
        formula2transitions =  Util.getBadFormulasForTransitions(model,separatingFamily,transitions,numberBad,numberBad,true);

        // else try semi bad
        if ( formula2transitions.entrySet().size() < 5 ) {
            marker= "bad(semi-good)";
            formula2transitions =  Util.getBadFormulasForTransitions(model,separatingFamily,transitions,numberBad,numberBad,false);
        }
        for ( String formula : formula2transitions.keySet() ) {

            MealyTransition transition1 = formula2transitions.get(formula).get(0);
            MealyTransition transition2 = formula2transitions.get(formula).get(1);
            String comment=marker +
                     " -- ACTL: \""+formula +"\"" +
                     " -- transition matching both src, io and dest: " + transition1.toString() +
                     " -- transition matching only src, io but just not dest: " + transition2.toString() ;

            result_formulas.add(new Triplet<>(false,formula,comment));
        }


        // exclude some transitions for other formulas
        LinkedList<MealyTransition> excludeTransitions =  new LinkedList<>();
        for (List<MealyTransition> transits:formula2transitions.values() ) {
            excludeTransitions.addAll(transits);
        }


        // if needed  add  extra simple bad weak transitions (not enough bad/semi-bad transition formulas are generated)
        if ( formula2transitions.size() < numberBad ) {
            int numberExtra = numberBad-formula2transitions.size();
            System.out.println(" bad (weak) " +  numberExtra);
            HashMap<String,MealyTransition> formula2transition =  Util.getSimpleBadFormulasForTransitions(model,separatingFamily,transitions,excludeTransitions,numberExtra);
            for ( String formula : formula2transition.keySet() ) {
                  String comment=" -- bad (weak)" +
                  " -- transition src matches multiple states, io and good sdt for dest         : "+formula2transition.get(formula).toString() +
                  " -- ACTL: "+formula;
                  result_formulas.add(new Triplet<>(false,formula,comment));
            }

            // if numberExtra not reach  then just add some good
            numberGood= numberGood + (numberExtra-formula2transition.size());
        }



        Collections.shuffle(transitions, new Random(3));

        // add 2 bad  connection formulas
        //-------------------------------
        System.out.println(" bad connection");
        LocationState errorState = Util.getErrorState(model);
        HashMap<String,LocationState> badformula2state =  Util.getBadConnectionFormulasForTransitions(model,separatingFamily,errorState,numberBadConnect,numberBadConnect);
        for ( String formula : badformula2state.keySet() ) {
            String comment=" -- connection" + " -- ACTL: "+formula;
            result_formulas.add(new Triplet<>(false,formula,comment));
        }

        // add GOOD formulas
        //====================

        // add 8 good transition formulas
        //-------------------------------
        System.out.println(" good transitions");
        HashMap<String,MealyTransition> formula2transition =  Util.getGoodFormulasForTransitions(model,separatingFamily,transitions,excludeTransitions,numberGood,numberGood);
        for ( String formula : formula2transition.keySet() ) {
            String comment= " -- good" + " -- transition  : "+formula2transition.get(formula).toString() +  " -- ACTL: "+ formula;
            result_formulas.add(new Triplet<>(true,formula,comment));
        }


        // add 2 good connection formulas
        //-------------------------------
        System.out.println(" good connection");
        HashMap<String,LocationState> formula2state =  Util.getGoodConnectionFormulasForTransitions(model,separatingFamily,errorState, numberGoodConnect,numberGoodConnect);
        for ( String formula : formula2state.keySet() ) {
            String comment=" -- connection" + " -- ACTL: "+formula;
            result_formulas.add(new Triplet<>(true,formula,comment));
        }


        // store found formulas
        //====================

        // shuffle the formula's so the true and false ones are mixed
        Collections.shuffle(result_formulas, new Random(3));

        for ( Triplet<Boolean, String, String> answer_formula_comment : result_formulas) {
            String answer=answer_formula_comment.getValue0().toString();
            String formula=answer_formula_comment.getValue1();
            String comment=answer_formula_comment.getValue2();

            formula_lines.add(formula );

            answeredformula_lines.add(answer+ ": " + formula );

            nusmv_lines.add("-- "+ answer);
            String[] commentlines = comment.split(" -- ");
            for ( String commentline : commentlines) {
                nusmv_lines.add("-- "+ commentline);
            }
            if ( comment.contains(" -- connection" )) {
                String nusmvFormula = Util.parseFormula2(formula);
                nusmv_lines.add("SPEC " + nusmvFormula);
            } else {
                String nusmvFormula = Util.parseFormula(formula);
                nusmv_lines.add("SPEC " + nusmvFormula);
            }
        }

        IO.writeSmallTextFile( formula_lines, filename+".formulas.txt");
        IO.writeSmallTextFile( answeredformula_lines, filename+".answeredformulas.txt");
        IO.writeSmallTextFile( nusmv_lines, filename+".nusmv");
    }

    static public void  nusmvWithSpecsTrueAndFalseOLD(MealyDetEnabledModel model, String filename){
        int numberGood=8;
        int numberBad=8;
        int numberToGenerate=8;//50;
       // int numberToGenerate=50;
        int numberToGenerateConnecting=2;
        int numberGoodConnect=2;
        int numberBadConnect=2;

        List<String>  formula_lines = new ArrayList<>();
        List<String>  answeredformula_lines = new ArrayList<>();

        System.out.println("export model");
        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);
        System.out.println("importing separating family");
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);
        System.out.println("generating formulas");

        // get random  list of transitions
        List<MealyTransition> transitions = new LinkedList<>(model.getModelTransitions());
        Collections.shuffle(transitions, new Random(3));

        System.out.println(filename);
        System.out.println("--------------------------");
        System.out.println(" bad (good)");
        HashMap<String,List<MealyTransition>> formula2transitions =null;

        String marker= "bad(good)";
        formula2transitions =  Util.getBadFormulasForTransitions(model,separatingFamily,transitions,numberGood,numberToGenerate,true);

        if ( formula2transitions.entrySet().size() < 5 ) {
            marker= "bad(semi-good)";
            formula2transitions =  Util.getBadFormulasForTransitions(model,separatingFamily,transitions,numberGood,numberToGenerate,false);
        }
        for ( String formula : formula2transitions.keySet() ) {
            //String formula=trans2formula.get(transition);
            MealyTransition transition1 = formula2transitions.get(formula).get(0);
            MealyTransition transition2 = formula2transitions.get(formula).get(1);
        //for ( MealyTransition transition : trans2formula.keySet() ) {
        //    String formula=trans2formula.get(transition);
            lines.add("-- false");
            lines.add("-- " + marker);
            lines.add("--transition matching both src, io and dest         : "+transition1.toString());
            lines.add("--transition matching only src, io but just not dest: "+transition2.toString());
            //System.out.println("--transition: "+transition1.toString());
            // System.out.println("--ACTL: "+formula);
            lines.add("--ACTL: "+formula);
            answeredformula_lines.add("false: " + formula);
            formula_lines.add(formula);

         //   System.err.println("--ACTL: "+formula);

            String nusmvFormula = Util.parseFormula(formula);
            //System.out.println("SPEC " + nusmvFormula);
            lines.add("SPEC " + nusmvFormula);
        }

        LinkedList<MealyTransition> excludeTransitions =  new LinkedList<>();
        for (List<MealyTransition> transits:formula2transitions.values() ) {
            excludeTransitions.addAll(transits);
        }


        // if needed  add  extra simple bad
        if ( formula2transitions.size() < numberGood ) {
            int numberExtra = numberBad-formula2transitions.size();
            System.out.println(" bad (weak) " +  numberExtra);
            HashMap<String,MealyTransition> formula2transition =  Util.getSimpleBadFormulasForTransitions(model,separatingFamily,transitions,excludeTransitions,numberExtra);
            for ( String formula : formula2transition.keySet() ) {
                lines.add("-- false");
                lines.add("-- bad (weak)");
                lines.add("--transition src matches multiple states, io and good sdt for dest         : "+formula2transition.get(formula).toString());
                lines.add("--ACTL: "+formula);
                answeredformula_lines.add("false: " + formula);
                formula_lines.add(formula);
                String nusmvFormula = Util.parseFormula(formula);
                lines.add("SPEC " + nusmvFormula);

            }

            // if numberExtra not reach  then just add some good
            numberGood= numberGood + (numberExtra-formula2transition.size());
        }

        Collections.shuffle(transitions, new Random(3));

        // add 2 bad  connection formulas
        System.out.println(" bad connection");
        LocationState errorState = Util.getErrorState(model);
        HashMap<String,LocationState> badformula2state =  Util.getBadConnectionFormulasForTransitions(model,separatingFamily,errorState,numberBadConnect,numberToGenerateConnecting);
        for ( String formula : badformula2state.keySet() ) {
            lines.add("-- false");
            lines.add("-- connection");
            lines.add("--ACTL: "+formula);
            answeredformula_lines.add("false: " + formula);
            formula_lines.add(formula);
            String nusmvFormula = Util.parseFormula2(formula);
            lines.add("SPEC " + nusmvFormula);
        }

        // add 8 good transition formulas
        System.out.println(" good transitions");
        HashMap<String,MealyTransition> formula2transition =  Util.getGoodFormulasForTransitions(model,separatingFamily,transitions,excludeTransitions,numberGood,numberToGenerate);
        for ( String formula : formula2transition.keySet() ) {
            lines.add("-- true");
            lines.add("-- good");
            lines.add("--transition          : "+formula2transition.get(formula).toString());
            lines.add("--ACTL: "+formula);
            answeredformula_lines.add("true: " + formula);
            formula_lines.add(formula);
            String nusmvFormula = Util.parseFormula(formula);
            lines.add("SPEC " + nusmvFormula);

        }


        // add 2 good connection formulas
        System.out.println(" good connection");
        HashMap<String,LocationState> formula2state =  Util.getGoodConnectionFormulasForTransitions(model,separatingFamily,errorState, numberGoodConnect,numberToGenerateConnecting);
        for ( String formula : formula2state.keySet() ) {
            lines.add("-- true");
            lines.add("-- connection");
            lines.add("--ACTL: "+formula);
            answeredformula_lines.add("true: " + formula);
            formula_lines.add(formula);
            String nusmvFormula = Util.parseFormula2(formula);
            lines.add("SPEC " + nusmvFormula);
        }

        IO.writeSmallTextFile( formula_lines, filename+".formulas.txt");
        IO.writeSmallTextFile( answeredformula_lines, filename+".answeredformulas.txt");
        IO.writeSmallTextFile( lines, filename+".nusmv");
    }


    static public void  nusmvWithSpecsPlukked_bothsides(MealyDetEnabledModel model, String filename){

        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);

     //   Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);
        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        ImmutableSet<MealyTransition> transitions = model.getModelTransitions();
        for ( MealyTransition transition : transitions ) {
            LocationState src = transition.getSource();
            LocationState dst = transition.getDestination();
            InputAction input = transition.getInput();
            OutputAction output = transition.getOutput();

            if (output.getName().equals("error")) continue;

            // SPEC  prefix for nusmv
            //String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";
            String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";

            lines.add("--transition: "+transition.toString());
            System.out.println("--transition: "+transition.toString());
            // System.out.println("--ACTL: "+formula);
            lines.add("--ACTL: "+formula);
            System.err.println("--ACTL: "+formula);

            String nusmvFormula = Util.parseFormula(formula);
            //System.out.println("SPEC " + nusmvFormula);
            lines.add("SPEC " + nusmvFormula);
        }

        IO.writeSmallTextFile( lines, filename);
    }

    static public void  nusmvWithSpecsPlukked_sourcesideonly(MealyDetEnabledModel model, String filename){
        List<String> lines=statemachine.model.fsm.mealy.format.NuSMV.export(model);

        Tree<SplittingTreeData> splittingTree = Util.getSplittingTreeMealy(model);
        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.getSeparatingFamily(splittingTree);

   //     Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(model,3);

        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);
        Map<LocationState, String> state2distformulaMutated = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        ImmutableSet<MealyTransition> transitions = model.getModelTransitions();
        for ( MealyTransition transition : transitions ) {
            LocationState src = transition.getSource();
            LocationState dst = transition.getDestination();
            InputAction input = transition.getInput();
            OutputAction output = transition.getOutput();

            if (output.getName().equals("error")) continue;

            // SPEC  prefix for nusmv
            //String formula= "AG ( ("+ state2distformula.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";
            String formula= "AG ( ("+ state2distformulaMutated.get(src) + ") => <" + input.getName() +"><" +output.getName() + ">("  + state2distformula.get(dst) + ") )";

            lines.add("--transition: "+transition.toString());
            System.out.println("--transition: "+transition.toString());
            // System.out.println("--ACTL: "+formula);
            lines.add("--ACTL: "+formula);
            System.err.println("--ACTL: "+formula);

            String nusmvFormula = Util.parseFormula(formula);
            //System.out.println("SPEC " + nusmvFormula);
            lines.add("SPEC " + nusmvFormula);
        }

        IO.writeSmallTextFile( lines, filename);
    }


}
