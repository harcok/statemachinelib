package statemachine.model.fsm.mealy.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.javatuples.Triplet;

import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

public class ADSdata implements EdgeNode<OutputAction,ADSnode>{

    private InputAction input;
    private LinkedList<LocationState> initialStates;
    private LinkedList<LocationState> currentStates;
    private OutputAction output;


    public ADSdata(OutputAction output, Collection<LocationState> initialStates, Collection<LocationState> currentStates, InputAction input) {
        super();
        this.input = input;
        this.initialStates = new LinkedList<LocationState>(initialStates);
        this.currentStates =  new LinkedList<LocationState>(currentStates);
        this.output = output;
    }

    @Override
    public OutputAction getEdge() {
        return output;
    }

//    @Override
//    public Triplet<InputAction, List<LocationState>, List<LocationState>> getNode() {
//       return new Triplet<>(input,initialStates,currentStates);
//    }

    @Override
    public ADSnode getNode() {
        return new ADSnode(input,initialStates,currentStates);  // has specialize to String for easy tree representation
    }

    public InputAction getInput() {
        return input;
    }

    public LinkedList<LocationState> getInitialStates() {
        return initialStates;
    }

    public LinkedList<LocationState> getCurrentStates() {
        return currentStates;
    }

    public OutputAction getOutput() {
        return output;
    }

    public void setInput(InputAction input) {
        this.input = input;
    }

    @Override
    public String toString() {
        return "ADSdata [input=" + input + ", initialStates=" + initialStates + ", currentStates=" + currentStates + ", output="
                + output + "]";
    }



}
