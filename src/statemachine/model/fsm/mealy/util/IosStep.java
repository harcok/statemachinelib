package statemachine.model.fsm.mealy.util;

import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

public class IosStep implements EdgeNode<IoStep,LocationState> {

    private InputAction input;
    private OutputAction output;
    private LocationState location;

    public IosStep(InputAction input, OutputAction output, LocationState location) {
        super();
        this.input = input;
        this.output = output;
        this.location = location;
    }
    public InputAction getInput() {
        return input;
    }
    public OutputAction getOutput() {
        return output;
    }
    public LocationState getLocation() {
        return location;
    }

    public IoStep getIoStep() {
        return new IoStep(input,output);
    }

    @Override
    public IoStep getEdge() {
        return getIoStep();
    }
    @Override
    public LocationState getNode() {
        return location;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((input == null) ? 0 : input.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((output == null) ? 0 : output.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IosStep other = (IosStep) obj;
        if (input == null) {
            if (other.input != null)
                return false;
        } else if (!input.equals(other.input))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (output == null) {
            if (other.output != null)
                return false;
        } else if (!output.equals(other.output))
            return false;
        return true;
    }
    @Override
    public String toString() {
        return "IosStep [input=" + input + ", output=" + output + ", location=" + location + "]";
    }

}
