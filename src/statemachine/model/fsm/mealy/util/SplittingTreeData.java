package statemachine.model.fsm.mealy.util;



import org.javatuples.Pair;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import nl.ru.cs.tree.format.EdgeNode;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;

public class SplittingTreeData implements EdgeNode<OutputAction, Pair<  ImmutableSet<LocationState> , ImmutableList<InputAction> > > {

    private OutputAction distinguishingOutput;
    private ImmutableSet<LocationState> states;
    private ImmutableList<InputAction> separatingSequence;


    public SplittingTreeData(OutputAction distinguishingOutput,
            Iterable<LocationState> states,Iterable<InputAction> separatingSequence) {
        super();
        this.distinguishingOutput = distinguishingOutput;
        this.states = ImmutableSet.copyOf(states);
        this.separatingSequence = ImmutableList.copyOf(separatingSequence);
    }

    public SplittingTreeData(OutputAction distinguishingOutput,
            Iterable<LocationState> states) {
        super();
        this.distinguishingOutput = distinguishingOutput;
        this.states = ImmutableSet.copyOf(states);
    }


    public OutputAction getDistinguishingOutput() {
        return distinguishingOutput;
    }


    public ImmutableSet<LocationState> getStates() {
        return states;
    }


    public ImmutableList<InputAction> getSeparatingSequence() {
        return separatingSequence;
    }

    public void setSeparatingSequence(Iterable<InputAction> separatingSequence) {
        this.separatingSequence = ImmutableList.copyOf(separatingSequence);
    }
    @Override
    public OutputAction getEdge() {
        return distinguishingOutput;
    }
    @Override
    public Pair<ImmutableSet<LocationState>,ImmutableList<InputAction>> getNode() {
        return new Pair<>(states,separatingSequence);
    }

    public String toString() {
        //return "output '" + distinguishingOutput +"' distinguishes states " + states + " which again can be split with separatingSequence:" +separatingSequence;
        return "{distinguishingOutput:" + distinguishingOutput +",states:" + states + ",separatingSequence:" +separatingSequence + "}";
    }

}
