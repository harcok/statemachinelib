package statemachine.model.fsm.mealy.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.MessageLoggers;
import org.zeroturnaround.exec.ProcessExecutor;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

import exceptionslib.BugException;
import exceptionslib.DependencyException;
import exceptionslib.FileIOException;
import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeNode;
import nl.ru.cs.tree.TreeUtils;
import nl.ru.cs.tree.format.EdgeNode;
import nl.ru.cs.tree.format.EdgeNodeImpl;
import nl.ru.cs.tree.iterator.TreeIterator;
import nl.ru.cs.tree.iterator.TreeIteratorBreadthFirst;
import nl.ru.cs.tree.iterator.TreeIteratorUpwards;
import ru.yandex.lc.jbd.Dumper;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.util.IO;
import statemachine.util.LocationPair;

public class Util {

    // note: getSplittingTreeMealyFirst is not extensive enough => should retry leaves splitting => testSplittingTreeRick not fully split tree (but ok so far!!)
    //static public Tree<SplittingTreeData> getSplittingTreeMealy(MealyDetEnabledModel mealy) {
    static public Tree<SplittingTreeData> getSplittingTreeMealyFIRSTIMP(MealyDetEnabledModel mealy) {
        ImmutableSet<LocationState> locations = mealy.getLocations();
        ImmutableSet<InputAction> inputs = mealy.getInputAlphabet();

        SplittingTreeData initialData= new SplittingTreeData(null,locations);
        TreeNode<SplittingTreeData> root = new TreeNode<SplittingTreeData>(initialData);
        Tree<SplittingTreeData> tree = new Tree<SplittingTreeData>(root);

        // immediately quit if nothing to split
        if (locations.size() <= 1)  return  tree;

        LinkedList<TreeNode<SplittingTreeData>> leafBlocks = new LinkedList<>();
        leafBlocks.add(root);
        LinkedList<TreeNode<SplittingTreeData>> newLeafBlocks = new LinkedList<>();
        LinkedList<TreeNode<SplittingTreeData>> removeLeafBlocks = new LinkedList<>();

        // split on output
        for ( InputAction input : inputs) {


            System.out.println("\n\ntry to split on input: " + input );
            printBlocks("current leafblocks (actual partioning)",leafBlocks);


            for ( TreeNode<SplittingTreeData> leafBlock : leafBlocks) {

                ImmutableSet<LocationState> blockStates = leafBlock.getData().getStates();
                if (blockStates.size() <=1 )  continue; // nothing to split

                System.out.println("current leafBlock: " +blockStates );
                SetMultimap<OutputAction,LocationState> out2states = splitOnOutput(mealy,blockStates,input);

                Set<OutputAction> splittingOutputs = out2states.keySet();
                System.out.println("out2states.size(): " + splittingOutputs.size());

                if (splittingOutputs.size() == 1 ) continue;  // no split reached
                SplittingTreeData data = leafBlock.getData();
                // set separating sequence in current node
                data.setSeparatingSequence(Arrays.asList(input));
                // add child node for each sub partition found
                for ( OutputAction output : splittingOutputs ) {
                    System.out.println("output: "  + output );
                    Set<LocationState> partition = out2states.get(output);
                    SplittingTreeData childData = new SplittingTreeData(output,partition );
                    System.out.println("leafBlock: "  + partition );

                    System.out.println(childData);

                    TreeNode<SplittingTreeData> childNode = new TreeNode<>(childData);
                    leafBlock.addChild(childNode );

                    // add new childs to be process. Note: childs with only one child will automatically be skipped.
                    newLeafBlocks.add(childNode);

                }
                // remove block,  did just split that block
                removeLeafBlocks.add(leafBlock);
            }
            // remove blocks after looping over blocks
            leafBlocks.removeAll(removeLeafBlocks);
            // add newTodoBlocks after all todoBlocks are processed for that specific input and we switch to next input,
            // because the children of a split block cannot be split again by the same input
            leafBlocks.addAll(newLeafBlocks);

 //           printBlocks("new leaf blocks",newLeafBlocks);

            removeLeafBlocks.clear();
            newLeafBlocks.clear();
        }


//        printBlocks("\n\n leafblocks (actual partioning)",leafBlocks); // actual partitioning

 //       System.out.println("\n\n\nsplit on blocks\n\n\n");

        // get map which maps state to block. To easily fetch  the block for a dest state.
        Map<LocationState,TreeNode<SplittingTreeData>> state2block = getState2blockMapping(leafBlocks);

        // split on dest state in different blocks
        newLeafBlocks.clear();
        for ( InputAction input : inputs) {
 //           System.out.println("\n\ntry to split on input: " + input );
 //           printBlocks("current leaf blocks",leafBlocks);

            for ( TreeNode<SplittingTreeData> leafBlock : leafBlocks) {

                ImmutableSet<LocationState> blockStates = leafBlock.getData().getStates();
                if (blockStates.size() <=1 )  continue; // nothing to split

//                System.out.println("current leafBlock: " + blockStates );

                // find dest blocks for block
                Set<LocationState> destStates= new HashSet<>();
                Set<TreeNode<SplittingTreeData>> destBlocks= new HashSet<>();
                for ( LocationState state : blockStates) {
                    MealyTransition trans=mealy.getTriggeredTransition(state, input);
                    LocationState dest = trans.getDestination();
                    destStates.add(dest);
                    destBlocks.add(state2block.get(dest));
                }
                if (destBlocks.size() <= 1 ) continue; // no split on different dest blocks possible

                // get destLcaBlock
                TreeNode<SplittingTreeData> destLcaBlock=getDestLcaBlock(tree,destBlocks,destStates);

                // set separating sequence in current block
                ImmutableList<InputAction> separatingSequence = destLcaBlock.getData().getSeparatingSequence();
                LinkedList<InputAction> newSeparatingSequence = new LinkedList<>(separatingSequence);
                newSeparatingSequence.addFirst(input);
                leafBlock.getData().setSeparatingSequence(newSeparatingSequence);
//                System.out.println("newSeparatingSequence: "  + newSeparatingSequence );


                //  map each deststate to its destLcaChildBlock
                List<TreeNode<SplittingTreeData> > destLcaBlock_Children = destLcaBlock.getChildren();
                Map<LocationState,TreeNode<SplittingTreeData>> deststate2DestLcaChildBlock = new HashMap<>();
                for ( TreeNode<SplittingTreeData> destLcaChildBlock : destLcaBlock_Children) {
                    for ( LocationState state : destLcaChildBlock.getData().getStates() ) {
                        deststate2DestLcaChildBlock.put(state, destLcaChildBlock);
                    }
                }

                // partition original block by different destLcaChildBlock destination
                //
                // find the source states for each destLcaChildBlock  => new sub partition of source states!
                // -> we use multimap to find source states for each destLcaChildBlock
                HashMultimap<TreeNode<SplittingTreeData>,LocationState> destLcaChildBlock2sourceStates = HashMultimap.create();
                for ( LocationState state : blockStates ) {
                    LocationState dest= mealy.getTriggeredTransition(state, input).getDestination();
                    destLcaChildBlock2sourceStates.put(deststate2DestLcaChildBlock.get(dest), state);
                }
                System.out.println( destLcaChildBlock2sourceStates.keySet());
                for (  TreeNode<SplittingTreeData> destLcaChildBlock : destLcaChildBlock2sourceStates.keySet() ) {
                    // get new partition and its distinguishing output from the other partitions
                    Set<LocationState> partition = destLcaChildBlock2sourceStates.get(destLcaChildBlock);
                    OutputAction distOutput = destLcaChildBlock.getData().getDistinguishingOutput();

                    // create new child node in tree to store this new subpartition with its distinguishing output
                    SplittingTreeData childData = new SplittingTreeData(distOutput,partition );
                    TreeNode<SplittingTreeData> childNode = new TreeNode<>(childData);
                    leafBlock.addChild(childNode );

//                    System.out.println("partition: "  + partition );

                    // add new childs to be process. Note: childs with only one child will automatically be skipped.
                    newLeafBlocks.add(childNode);
                }
                // remove block,  did just split that block
                removeLeafBlocks.add(leafBlock);

            }
            // remove blocks after looping over blocks
            leafBlocks.removeAll(removeLeafBlocks);
            removeLeafBlocks.clear();

//            // only add  new found blocks when input is tried on all current blocks
//            // note: new found blocks won't split on input anymore, so no need to try to split them on input
            leafBlocks.addAll(newLeafBlocks);
 //           printBlocks("new leaf blocks",newLeafBlocks);
            newLeafBlocks.clear();
            printBlocks("leaf blocks",leafBlocks);
        }

        return tree;
    }

    static public Tree<SplittingTreeData> getSplittingTreeMealy(MealyDetEnabledModel mealy) {


        ImmutableSet<LocationState> locations = mealy.getLocations();
        ImmutableSet<InputAction> inputs = mealy.getInputAlphabet();

        SplittingTreeData initialData= new SplittingTreeData(null,locations);
        TreeNode<SplittingTreeData> root = new TreeNode<SplittingTreeData>(initialData);
        Tree<SplittingTreeData> tree = new Tree<SplittingTreeData>(root);

        // immediately quit if nothing to split
        if (locations.size() <= 1)  return  tree;

        LinkedList<TreeNode<SplittingTreeData>> leafBlocks = new LinkedList<>();
        leafBlocks.add(root);
        LinkedList<TreeNode<SplittingTreeData>> newLeafBlocks = new LinkedList<>();
        LinkedList<TreeNode<SplittingTreeData>> removeLeafBlocks = new LinkedList<>();

        // split on output
        for ( InputAction input : inputs) {


 //           System.out.println("\n\ntry to split on input: " + input );
//            printBlocks("current leafblocks (actual partioning)",leafBlocks);


            for ( TreeNode<SplittingTreeData> leafBlock : leafBlocks) {

                ImmutableSet<LocationState> blockStates = leafBlock.getData().getStates();
                if (blockStates.size() <=1 )  continue; // nothing to split

                System.out.println("current leafBlock: " +blockStates );
                SetMultimap<OutputAction,LocationState> out2states = splitOnOutput(mealy,blockStates,input);

                Set<OutputAction> splittingOutputs = out2states.keySet();
                System.out.println("out2states.size(): " + splittingOutputs.size());

                if (splittingOutputs.size() == 1 ) continue;  // no split reached
                SplittingTreeData data = leafBlock.getData();
                // set separating sequence in current node
                data.setSeparatingSequence(Arrays.asList(input));
                // add child node for each sub partition found
                for ( OutputAction output : splittingOutputs ) {
                    System.out.println("output: "  + output );
                    Set<LocationState> partition = out2states.get(output);
                    SplittingTreeData childData = new SplittingTreeData(output,partition );
                    System.out.println("leafBlock: "  + partition );

                    System.out.println(childData);

                    TreeNode<SplittingTreeData> childNode = new TreeNode<>(childData);
                    leafBlock.addChild(childNode );

                    // add new childs to be process. Note: childs with only one child will automatically be skipped.
                    newLeafBlocks.add(childNode);

                }
                // remove block,  did just split that block
                removeLeafBlocks.add(leafBlock);
            }
            // remove blocks after looping over blocks
            leafBlocks.removeAll(removeLeafBlocks);
            // add newTodoBlocks after all todoBlocks are processed for that specific input and we switch to next input,
            // because the children of a split block cannot be split again by the same input
            leafBlocks.addAll(newLeafBlocks);

            printBlocks("new leaf blocks",newLeafBlocks);

            removeLeafBlocks.clear();
            newLeafBlocks.clear();
        }


        printBlocks("\n\n leafblocks (actual partioning)",leafBlocks); // actual partitioning

        System.out.println("\n\n\nsplit on blocks\n\n\n");


        // get map which maps state to block. To easily fetch  the block for a dest state.
        Map<LocationState,TreeNode<SplittingTreeData>> state2block = getState2blockMapping(leafBlocks);

      // split on dest state in different blocks

//        newLeafBlocks.clear();

        Map <Integer,  List<TreeNode<SplittingTreeData>> > k2blocks = new HashMap<>();
        k2blocks.put(0, Arrays.asList( root) );  // k = 0 splitting is initial set of states before splitting starts

        int k=1;
        List<TreeNode<SplittingTreeData>> k_blocks = new LinkedList<>(leafBlocks);  // leafs from output splitting is k=1 partition
        k2blocks.put(k, k_blocks);

        // note: k_blocks and   k_plus_1_blocks only contain blocks with more then one state  ( singleton nodes already removed )
        while (true) {
            List<TreeNode<SplittingTreeData>> k_plus_1_blocks = do_partition_k(mealy,k,tree,k_blocks);

            // quick check for stable partition
            if ( k_blocks.size() == mealy.getLocations().size() ) break;

            printBlocks("k_blocks",k_blocks);
            printBlocks("k_plus_1_blocks",k_plus_1_blocks);
            if (k_plus_1_blocks.equals(k_blocks)) break; // no split succeeded anymore
       //     if (k_plus_1_blocks.stream().filter(b -> (b.getData().getStates().size() > 1)).collect(toList()).size() ==  0 ) break;  // no blocks to split left!
            k2blocks.put(k+1, k_plus_1_blocks);
            k=k+1;
            k_blocks= k_plus_1_blocks;
        }

        // print blocks per k:
        for ( int i :k2blocks.keySet()) {
            printBlocks(i+" ",k2blocks.get(i));
        }

        return tree;
    }


    private static List<TreeNode<SplittingTreeData>> do_partition_k(
            MealyDetEnabledModel mealy, int k, Tree<SplittingTreeData> tree, List<TreeNode<SplittingTreeData>> k_blocks) {


        LinkedList<TreeNode<SplittingTreeData>> leafBlocks = new LinkedList<>(k_blocks);
        LinkedList<TreeNode<SplittingTreeData>> newLeafBlocks = new LinkedList<>();
        LinkedList<TreeNode<SplittingTreeData>> removeLeafBlocks = new LinkedList<>();

        ImmutableSet<InputAction> inputs = mealy.getInputAlphabet();

        // get map which maps state to block. To easily fetch  the block for a dest state.
        Map<LocationState,TreeNode<SplittingTreeData>> state2block = getState2blockMapping(leafBlocks);

        // split on dest state in different blocks
        for ( InputAction input : inputs) {
            System.out.println("\n\ntry to split on input: " + input );
            printBlocks("current leaf blocks",leafBlocks);

            for ( TreeNode<SplittingTreeData> leafBlock : leafBlocks) {

                ImmutableSet<LocationState> blockStates = leafBlock.getData().getStates();
                if (blockStates.size() <=1 )  continue; // nothing to split

                System.out.println("current leafBlock: " + blockStates );

                // find dest blocks for block
                Set<LocationState> destStates= new HashSet<>();
                Set<TreeNode<SplittingTreeData>> destBlocks= new HashSet<>();
                for ( LocationState state : blockStates) {
                    MealyTransition trans=mealy.getTriggeredTransition(state, input);
                    LocationState dest = trans.getDestination();
                    destStates.add(dest);
                    destBlocks.add(state2block.get(dest));
                }
                if (destBlocks.size() <= 1 ) continue; // no split on different dest blocks possible

                // get destLcaBlock
                TreeNode<SplittingTreeData> destLcaBlock=getDestLcaBlock(tree,destBlocks,destStates);

                // set separating sequence in current block
                ImmutableList<InputAction> separatingSequence = destLcaBlock.getData().getSeparatingSequence();
                LinkedList<InputAction> newSeparatingSequence = new LinkedList<>(separatingSequence);
                newSeparatingSequence.addFirst(input);
                leafBlock.getData().setSeparatingSequence(newSeparatingSequence);
                System.out.println("newSeparatingSequence: "  + newSeparatingSequence );


                //  map each deststate to its destLcaChildBlock
                List<TreeNode<SplittingTreeData> > destLcaBlock_Children = destLcaBlock.getChildren();
                Map<LocationState,TreeNode<SplittingTreeData>> deststate2DestLcaChildBlock = new HashMap<>();
                for ( TreeNode<SplittingTreeData> destLcaChildBlock : destLcaBlock_Children) {
                    for ( LocationState state : destLcaChildBlock.getData().getStates() ) {
                        deststate2DestLcaChildBlock.put(state, destLcaChildBlock);
                    }
                }

                // partition original block by different destLcaChildBlock destination
                //
                // find the source states for each destLcaChildBlock  => new sub partition of source states!
                // -> we use multimap to find source states for each destLcaChildBlock
                HashMultimap<TreeNode<SplittingTreeData>,LocationState> destLcaChildBlock2sourceStates = HashMultimap.create();
                for ( LocationState state : blockStates ) {
                    LocationState dest= mealy.getTriggeredTransition(state, input).getDestination();
                    destLcaChildBlock2sourceStates.put(deststate2DestLcaChildBlock.get(dest), state);
                }
                System.out.println( destLcaChildBlock2sourceStates.keySet());
                for (  TreeNode<SplittingTreeData> destLcaChildBlock : destLcaChildBlock2sourceStates.keySet() ) {
                    // get new partition and its distinguishing output from the other partitions
                    Set<LocationState> partition = destLcaChildBlock2sourceStates.get(destLcaChildBlock);
                    OutputAction distOutput = destLcaChildBlock.getData().getDistinguishingOutput();

                    // create new child node in tree to store this new subpartition with its distinguishing output
                    SplittingTreeData childData = new SplittingTreeData(distOutput,partition );
                    TreeNode<SplittingTreeData> childNode = new TreeNode<>(childData);
                    leafBlock.addChild(childNode );

                    System.out.println("partition: "  + partition );

                    // add new childs to be process. Note: childs with only one child will automatically be skipped.
                    newLeafBlocks.add(childNode);
                }
                // remove block,  did just split that block
                removeLeafBlocks.add(leafBlock);

            }

            printBlocks("new leaf blocks",newLeafBlocks);
            printBlocks("remove leaf blocks",removeLeafBlocks);

            // remove/add blocks after looping over blocks
            leafBlocks.removeAll(removeLeafBlocks);
            removeLeafBlocks.clear();
            leafBlocks.addAll(newLeafBlocks);
            newLeafBlocks.clear();

            printBlocks("leaf blocks",leafBlocks);
        }

        return leafBlocks;
    }

    private static void printBlocks(String label, List<TreeNode<SplittingTreeData>> newTodoBlocks) {
        System.out.print(label + ": ");
        newTodoBlocks.stream().forEach(v-> System.out.print(v.getData().getStates()) );
        System.out.println("");
    }

    private static TreeNode<SplittingTreeData> getDestLcaBlock(
            Tree<SplittingTreeData> tree, Set<TreeNode<SplittingTreeData>> destBlocks, Set<LocationState> destStates) {

        // get destLcaBlock        tree, destBlocks, destStates
        // - by default take root node
        TreeNode<SplittingTreeData> destLcaBlock=tree.getRoot();
        // - take one dest block
        TreeNode<SplittingTreeData> destBlock = destBlocks.iterator().next();
        // - walk upwards until you get first node containing all deststates => that is destLcaBlock
        Tree<SplittingTreeData> treeWithUpwardsIterator = tree.getShallowCopy(new TreeIteratorUpwards<>(destBlock));
        for(TreeNode<SplittingTreeData> node : treeWithUpwardsIterator) {
            ImmutableSet<LocationState> nodeStates = node.getData().getStates();
            if  (nodeStates.containsAll(destStates)) {
                destLcaBlock= node;
                break;
            }
        }
        return destLcaBlock;
    }

    private static Map<LocationState, TreeNode<SplittingTreeData>> getState2blockMapping(List<TreeNode<SplittingTreeData>> leafblocks) {
        Map<LocationState,TreeNode<SplittingTreeData>> state2block = new HashMap<>();
        for ( TreeNode<SplittingTreeData> leafblock : leafblocks) {
            for ( LocationState state : leafblock.getData().getStates() ) {
                state2block.put(state, leafblock);
            }
        }
        return state2block;
    }

    static public void exportSplittingTree(String filepath, Tree<SplittingTreeData> splittingTree) {
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(filepath, splittingTree,
                e -> e.toString(), n -> {
                    String sepseq = ( n.getValue1()  == null ) ? "" : "\n" + n.getValue1().toString();
                    return n.getValue0().toString() + sepseq; } );
    }



    /**  Split states on output. Each set of states in split has same output under given input.
     *
     * @param mealy model
     * @param states: set of states to be split
     * @param input: input to use for split
     * @return SetMultimap which maps output to its split subset
     */
    public static SetMultimap<OutputAction,LocationState> splitOnOutput(MealyDetEnabledModel mealy,ImmutableSet<LocationState> states, InputAction input) {

        HashMultimap<OutputAction,LocationState>  out2states =  HashMultimap.create();
        for ( LocationState state : states ) {
            MealyTransition trans=mealy.getTriggeredTransition(state, input);
            //out2states.put( trans.getOutput(),trans.getDestination() );
            out2states.put( trans.getOutput(), state );
        }
        return out2states;
    }

    public static SetMultimap<OutputAction,LocationState> splitOnDestStateBlock(MealyDetEnabledModel mealy,ImmutableSet<LocationState> states, InputAction input) {

        HashMultimap<OutputAction,LocationState>  block2states =  HashMultimap.create();
        for ( LocationState state : states ) {
            MealyTransition trans=mealy.getTriggeredTransition(state, input);
            block2states.put( trans.getOutput(), state );
        }
        return block2states;
    }

    static public Map<LocationState,List<InputAction>> getAccessSequences(MealyDetModel detlts) {

        Map<LocationState,List<InputAction>> location2accessSequence = new HashMap<>();

        // first get spanning tree
        Tree<EdgeNode<InputAction,LocationState>> tree=getSpanningTreeMealy(detlts);
        // for each node in spanning tree, represented by a state, get its access sequence
        for ( TreeNode<EdgeNode<InputAction,LocationState>> node : tree ) {
            LocationState location=node.getData().getNode();
            if (node.getParent() == null) {
                // root node
                location2accessSequence.put(location, new LinkedList<>());
            } else {
                List<TreeNode<EdgeNode<InputAction,LocationState>>> trace = TreeUtils.getTraceFromRootToNode(node, tree);
                // remove root node with only start location and no incoming edge
                trace.remove(0);
                List<InputAction> inputs=new LinkedList<>();
                for ( TreeNode<EdgeNode<InputAction,LocationState>> nodeedge :trace) {
                    inputs.add(nodeedge.getData().getEdge());
                }
                location2accessSequence.put(location,inputs );
            }
        }
        return location2accessSequence;
    }




    // makes spanning tree using  names of locations and actions
    static public   Tree<EdgeNode<InputAction,LocationState>> getSpanningTreeMealy(MealyDetModel detlts) {

        HashSet<LocationState> locationsSeen = new HashSet<LocationState>();
        Queue<LocationState> locationsTodo = new LinkedList<LocationState>();
        Map<LocationState, TreeNode<EdgeNode<InputAction,LocationState>>> location2node = new HashMap<>();

        LocationState startLocation = detlts.getStartLocation();
        ImmutableSet<InputAction> actions = detlts.getInputAlphabet();

        TreeNode<EdgeNode<InputAction,LocationState>> root = new TreeNode<EdgeNode<InputAction,LocationState>>(new EdgeNodeImpl<>(null, startLocation));
        location2node.put(startLocation, root);

        locationsSeen.add(startLocation);
        locationsTodo.add(startLocation);

        while (!locationsTodo.isEmpty()) {
            LocationState location = locationsTodo.remove();

            TreeNode<EdgeNode<InputAction,LocationState>> parentnode = location2node.get(location);
            for (InputAction action : actions) {
                Optional<MealyTransition> optTrans = detlts.getTriggeredTransition(location, action);
                if (optTrans.isPresent()) {
                    LocationState dest = optTrans.get().getDestination();
                    if (!locationsSeen.contains(dest)) {
                        // we have seen this location
                        locationsSeen.add(dest);
                        // add tree node going to destination
                        TreeNode<EdgeNode<InputAction,LocationState>> childnode = new TreeNode<>(new EdgeNodeImpl<>(action, dest));
                        parentnode.addChild(childnode);
                        location2node.put(dest, childnode);
                        // process location's outgoing transitions later
                        locationsTodo.add(dest);
                    }
                }
            }
        }

        Tree<EdgeNode<InputAction,LocationState>> tree = new Tree<>(root);
        return tree;

    }

    /**
     *  compare whether two mealy machines are observational equivalent
     *
     *  note: the mealy machines must be deterministic but not necessarily input enabled
     *
     * @param machine1
     * @param machine2
     */
    public static TraceDiff compare(MealyDetModel machine1, MealyDetModel machine2) {
        return compare(machine1, machine2, machine1.getInputAlphabet());
    }

    /**
     *  compare whether two mealy machines are observational equivalent
     *
     *  note: the mealy machines must be deterministic but not necessarily input enabled
     *
     * @param machine1
     * @param machine2
     * @param inputAlphabet
     */
    public static TraceDiff compare(MealyDetModel machine1, MealyDetModel machine2, ImmutableSet<InputAction> inputAlphabet) {

        LocationState startLoc1 = machine1.getStartLocation();
        LocationState startLoc2 = machine2.getStartLocation();
        LocationPair startPair = new LocationPair(startLoc1, startLoc2);

        Map<LocationPair, Link> statePair2actionOnIncomingTransition = new HashMap<>();
        Set<LocationPair> seenStatePairs = new HashSet<>();
        Queue<LocationPair> locationPairsTodo = new LinkedList<LocationPair>();

        seenStatePairs.add(startPair);
        locationPairsTodo.add(startPair);

        while (!locationPairsTodo.isEmpty()) {
            LocationPair currentLocPair = locationPairsTodo.remove();
            for (InputAction input : inputAlphabet) {

                Optional<MealyTransition> trans1 = machine1.getTriggeredTransition(currentLocPair.left, input);
                Optional<MealyTransition> trans2 = machine2.getTriggeredTransition(currentLocPair.right, input);

                OutputAction out1, out2;
                if (trans1.isPresent() != trans2.isPresent()) {
                    if (trans1.isPresent()) {
                        out1 = trans1.get().getOutput();
                        out2 = null;
                    } else {
                        out2 = trans2.get().getOutput();
                        out1 = null;
                    }
                    return getTraceDiff(input, out1, out2, machine1, statePair2actionOnIncomingTransition, startPair, currentLocPair);
                }
                if (!trans1.isPresent()) {
                    // both transitions not possible  => machines equal
                    continue;
                }

                out1 = trans1.get().getOutput();
                out2 = trans2.get().getOutput();

                if (out1.equals(out2)) {
                    LocationPair destinationPair = new LocationPair(trans1.get().getDestination(), trans2.get().getDestination());
                    if (!seenStatePairs.contains(destinationPair)) {
                        seenStatePairs.add(destinationPair);
                        locationPairsTodo.add(destinationPair);
                        statePair2actionOnIncomingTransition.put(destinationPair, new Link(currentLocPair, input));
                    }
                    // else already seen pair which is already handled or on todo list to be handled
                } else {
                    return getTraceDiff(input, out1, out2, machine1, statePair2actionOnIncomingTransition, startPair, currentLocPair);
                }
            }
        }
        return null;

    }

    static public List<InputAction> getInputs(
            Map<LocationPair, Link> statePair2actionOnIncomingTransition, LocationPair startPair, LocationPair endPair) {
        List<InputAction> inputs = new ArrayList<InputAction>();

        LocationPair currentPair = endPair;
        while (!currentPair.equals(startPair)) {
            Link prevPairAndInput = statePair2actionOnIncomingTransition.get(currentPair);
            inputs.add(prevPairAndInput.input);
            currentPair = prevPairAndInput.previousLocationPair;
        }
        Collections.reverse(inputs);
        return inputs;
    }

    static public List<OutputAction> getOutputs(MealyDetModel machine, List<InputAction> inputs) {
        List<OutputAction> outputs = new ArrayList<OutputAction>();
        LocationState location = machine.getStartLocation();
        for (InputAction input : inputs) {
            // TODO: handle optional  transitions better
            MealyTransition transition = machine.getTriggeredTransition(location, input).get();
            outputs.add(transition.getOutput());
            location = transition.getDestination();
        }
        return outputs;
    }

    static public TraceDiff getTraceDiff(
            InputAction input, OutputAction out1, OutputAction out2, MealyDetModel machine,
            Map<LocationPair, Link> statePair2actionOnIncomingTransition, LocationPair startPair, LocationPair endPair) {
        List<InputAction> inputs = getInputs(statePair2actionOnIncomingTransition, startPair, endPair);
        List<OutputAction> outputs = getOutputs(machine, inputs);
        return new TraceDiff(inputs, outputs, input, out1, out2);
    }

    static class Link {
        public LocationPair previousLocationPair;
        public InputAction input;

        public Link(LocationPair previousLocationPair, InputAction input) {
            super();
            this.previousLocationPair = previousLocationPair;
            this.input = input;
        }
    }

    public static Map<LocationState,String> getState2DistinguishingFormula( MealyDetEnabledModel mealy,Map<LocationState,Set<List<InputAction>>> separatingFamily) {
        Map<LocationState,String> state2formula = new HashMap<>();
        for ( LocationState state : separatingFamily.keySet()) {
            Tree<IosStep> sdt = Util.getStateDistinguishingTree(mealy, state, separatingFamily.get(state));

     //       sdt=stripSdt(mealy,sdt);

          //  nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile("sdt_state_"+state.getName(), sdt);
            String formula = Util.sdt2ctlFormula(sdt);
            state2formula.put(state, formula);
        }
        return state2formula;
    }

    private static String getStrippedStateFormula(
            MealyDetEnabledModel model, Map<LocationState, Set<List<InputAction>>> separatingFamily, LocationState state) {
        Tree<IosStep> sdt = Util.getStrippedStateDistinguishingTree(model, state, separatingFamily.get(state));
       // sdt=stripSdt(model,sdt);
        String formula = Util.sdt2ctlFormula(sdt);
        return formula;
    }



    /**  Find a mutated sdt by stripping state indentifier sdts for the given state identifiers.
     * The returned mutated sdt depends on the boolean distinguishing parameter.
     *
     *  With distinguishing==true the function returns the sdt stripped so far possible that it still distinguishes the states.
     *  With distinguishing==false the function returns the sdt stripped so far that it is the first time it distinguishes the states.
     *
     * @param states
     * @param mealy
     * @param separatingFamily
     * @param intersectionList
     * @param distinguishing
     * @return
     */
    public static Tree<IosStep> findMutatedSdt(List<LocationState> states,MealyDetEnabledModel mealy,Map<LocationState,Set<List<InputAction>>> separatingFamily, List<LocationState> intersectionList,boolean distinguishing,boolean strip) {
        Set<LocationState> intersection = null;
        Tree<IosStep> foundsdt = null;


        HashMap<LocationState,Tree<IosStep>> source2sdt1 = new HashMap<>();
        HashMap<LocationState,Tree<IosStep>> source2sdt2 = new HashMap<>();
        for ( LocationState state : states ) {
            //Tree<IosStep> sdt = Util.getStrippedStateDistinguishingTree(mealy, state, separatingFamily.get(state));
            Tree<IosStep> sdt= null;
            if ( strip ) {
               sdt = Util.getStrippedStateDistinguishingTree(mealy, state, separatingFamily.get(state));
            } else {
               sdt = Util.getStateDistinguishingTree(mealy, state, separatingFamily.get(state));
            }

            // strip state identifier specific for model
          //  sdt = Util.stripSdtLeftRight(mealy, sdt);

        //    System.out.println(state);
         //   System.out.println(sdt);
          //  sdt=stripSdt(mealy,sdt);
            source2sdt1.put(state, sdt);
            source2sdt2.put(state, sdt);
        }



        boolean workdone=true;
        LocationState breakState=null;
        search: while ( workdone ) {
            workdone = false;
            for ( LocationState state : states ) {

                Tree<IosStep> sdt1 =  source2sdt1.get(state);

                if ( sdt1.size() > 2 )
                {
                    workdone=true;
                    Tree<IosStep> prev_sdt1 = sdt1;
                    sdt1=Util.plukLeaf(sdt1,false);  // pluk left deepest
                    source2sdt1.put(state,sdt1);
                    Set<LocationState> matchingStates = matchDistTree(mealy,sdt1);
                    if (matchingStates.size() > 1 ) {
                        intersection = Sets.intersection(Sets.newHashSet(states), Sets.newHashSet(matchingStates));
                        if (intersection.size() > 1 ) {
                            if (distinguishing) {
                                foundsdt=prev_sdt1;  // the previous sdt is last distinguishing
                            } else {
                                foundsdt=sdt1;   // the first not distinguishing
                            }
                            breakState=state;
                            break search;
                        }
                    }
                }

                Tree<IosStep> sdt2 =  source2sdt2.get(state);
                if ( sdt2.size() > 2 )
                {
                    workdone=true;
                    Tree<IosStep> prev_sdt2 = sdt2;
                    sdt2=Util.plukLeaf(sdt2,true);  // pluk  right deepest
                    source2sdt2.put(state,sdt2);
                    Set<LocationState> matchingStates = matchDistTree(mealy,sdt2);
                    if (matchingStates.size() > 1 ) {
                        intersection = Sets.intersection(Sets.newHashSet(states), Sets.newHashSet(matchingStates));
                        if (intersection.size() > 1 ) {
                            if (distinguishing) {
                                foundsdt=prev_sdt2;  // the previous sdt is last distinguishing
                            } else {
                                foundsdt=sdt2;   // the first not distinguishing
                            }
                            breakState=state;
                            break search;
                        }
                    }
                }


            }
        }

        if ( workdone ) { // while loop can only end with workdone==true if intersection found (else ends with workdone==false)
            intersectionList.addAll(intersection);
            if (distinguishing) {
               // make not distinguishing state the first in list
               intersectionList.remove(breakState);
              // intersectionList.add(breakState);
               intersectionList.add(0, breakState);
            }
            //System.out.println("found:" + foundsdt);
            return foundsdt;
        }


        return null;

    }



    public static HashMap<String,List<MealyTransition>> getBadFormulasForTransitions( MealyDetEnabledModel mealy,
            Map<LocationState,Set<List<InputAction>>> separatingFamily, Collection<MealyTransition> mealyTransitions,int number, int numberToGenerate, boolean strip) {

        HashMap<String,List<MealyTransition>> formula2transitions = new  HashMap<>(); // first matches formula, second doesn't because destination state identifier just doesn't match

        // collect transitions with same  input/output pair for an normal transition ( normal means not a transition to the error state)
        HashMultimap<String,MealyTransition> ioPair2transitions = HashMultimap.create();
        for ( MealyTransition transition : mealyTransitions ) {
            if ( transition.getOutput().getName().equals("error")) continue;
            ioPair2transitions.put(transition.getInput().getName() + "/"+transition.getOutput().getName(), transition);
        }

//        for (String ioPair:ioPair2transitions.keySet()) {
//            Set<MealyTransition> transitions = ioPair2transitions.get(ioPair);
//            if (transitions.size() == 1 ) ioPair2transitions.re
//                //ioPair2transitions.removeAll(ioPair);
//
//        }
        System.out.println(ioPair2transitions.keySet().size());


        // per ioPair try to find a almost true formula which is false!
        // That is just pluk one extra leaf on destination state and the formula would hold.
        for (String ioPair:ioPair2transitions.keySet()) {


     //     System.out.println(formula2transitions.size());
          if( formula2transitions.size() == numberToGenerate ) break;

            Set<MealyTransition> transitions = ioPair2transitions.get(ioPair);
            if (transitions.size() > 1 ) {

                System.out.println("found one: " + transitions);

                // get different sources
                List<LocationState> sources = transitions.stream().map(t -> t.getSource()).collect(Collectors.toList());
                // note: each transition has unique source because  two transitions with same i/o and therefore
                //       same input cannot have the same source for a deterministic mealy machine


                // search for mutate sdt which matches multiple states in sources
                // we do this by mutating  the sources's sdts until hopefully some match each other
                List<LocationState> intersection = new LinkedList<>();
                Tree<IosStep> foundSourceSdt=findMutatedSdt(sources, mealy,separatingFamily,intersection, false,strip);
                // if intersection is not empty we found a mutated source sdt which matches the states in the intersection list
                if (! intersection.isEmpty()) {
                     System.out.println("intersection: " + intersection);
//                      System.out.println(transitions);
//                      System.out.println("size: " + foundSourceSdt.size() + " depth: " + TreeUtils.getDepth(foundSourceSdt) );
//                      System.out.println(foundSourceSdt);

                      // just take first two transitions from intersection
                      List<LocationState> inter =  new ArrayList<> (intersection);
                      LocationState src1= inter.get(0);
                      MealyTransition transition1 = transitions.stream()
                              .filter(t -> t.getSource().equals(src1))
                              .findFirst().get();
                      LocationState dst1=transition1.getDestination();
                      LocationState src2= inter.get(1);
                      MealyTransition transition2 = transitions.stream()
                              .filter(t -> t.getSource().equals(src2))
                              .findFirst().get();
                      LocationState dst2= transition2.getDestination();

//                      System.out.println("transition1: " + transition1);
//                      System.out.println("transition2: " + transition2);



                      List<LocationState> destinations = Arrays.asList(dst1,dst2);
                      List<LocationState> intersectionDest = new LinkedList<>();
                      // search for mutate sdt which matches multiple states in destinations
                      // we do this by mutating  the destinations sdts until hopefully some match each other.
                      // Instead of returning the sdt which matches all destinations(last param false) instead we return the previoust,
                      // before the last mutation, which just not matches all destination.(last param true) A single pluk to it would make
                      // match all destinations but it just not does match.
                      Tree<IosStep> foundsdtdest=findMutatedSdt(destinations, mealy,separatingFamily,intersectionDest, true,strip);  // RIGHT
                      // note: don't know which of the two transition the found sdt matches
                      //Tree<IosStep> foundsdtdest=findMutatedSdt(destinations, mealy,separatingFamily,intersectionDest, false);  // WRONG

                      if (! intersectionDest.isEmpty()) {
                        //  System.out.println("Dest sdt found, size: " + foundsdtdest.size() + " depth: " + TreeUtils.getDepth(foundsdtdest) );

                          // the first entry in the intersection is the state which not matches sdt, and the other is matching the sdt
                          LocationState notMatchingDestState = intersectionDest.get(0);


                          ArrayList<MealyTransition> theTransitions= new ArrayList<>();
                          if ( transition2.getDestination().equals(notMatchingDestState)) {
                              theTransitions.add(transition1);
                              theTransitions.add(transition2);
                          } else {
                              theTransitions.add(transition2);
                              theTransitions.add(transition1);
                          }

                          MealyTransition transition = transitions.iterator().next();
                          InputAction input = transition.getInput();
                          OutputAction output = transition.getOutput();
                          String formulaSource =  Util.sdt2ctlFormula(foundSourceSdt);
                          String formulaDest = Util.sdt2ctlFormula(foundsdtdest);

                          String formula= "AG ( ("+ formulaSource + ") => <" + input.getName() +"><" +output.getName() + ">("  + formulaDest + ") )";
                    //      System.out.println(formula);

                          formula2transitions.put(formula, theTransitions);
                          System.out.println(formula2transitions.size());
                      }

                }

            }
        }

        HashMap<String, List<MealyTransition>> subset_formula2transitions = getSmallestFormulas(formula2transitions,number);
        return subset_formula2transitions;
    }

    public static <V> HashMap<String,V> getSmallestFormulas(HashMap<String,V> formula2value , int number ) {
        LinkedList<String> formulas = new LinkedList<>(formula2value.keySet());
        Collections.sort(formulas, Comparator.comparing(String::length));
        int max = formulas.size() < number ?   formulas.size() : number ;
        List<String> subformulas = formulas.subList(0,  max);
        HashMap<String,V> subset_formula2value = new  HashMap<>(); // first matches formula, second doesn't because destination state identifier just doesn't match
        for (String aformula : subformulas) {
            subset_formula2value.put(aformula, formula2value.get(aformula));
        }
       // System.out.println("sizeeeeee: " +subset_formula2value.size() );
        return subset_formula2value;
    }


    public static LocationState getErrorState(MealyDetEnabledModel mealy) {

        for ( MealyTransition transition : mealy.getModelTransitions()) {
            if ( transition.getOutput().getName().equals("error")) {
                LocationState errorState = transition.getDestination();
                return errorState;
            }
        }
        throw new BugException("missing error state");
    }

    /**   take transition and mutate source so that it matches also other sources which do no
     *    not support the same i/o ( all transitions with same i/o are already excluded by excludeTransitions)
     *    and therefore the formula becomes false
     *
     *    for dest we take the real sdt
     *
     *    we only use matched sources  which are not seen before as source in  excludeTransitions
     *
     * @param mealy
     * @param separatingFamily
     * @param transitions
     * @param excludeTransitions
     * @param number
     * @return
     */
    public static HashMap<String, MealyTransition> getSimpleBadFormulasForTransitions(
            MealyDetEnabledModel mealy, Map<LocationState, Set<List<InputAction>>> separatingFamily,
            List<MealyTransition> transitions,
            List<MealyTransition> excludeTransitions,
            int number) {



        HashMap<String,MealyTransition> formula2transition = new  HashMap<>();

        int count=0;
        List<LocationState> excludeSources = excludeTransitions.stream().map(t -> t.getSource()).collect(Collectors.toList());

 //       Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(mealy,separatingFamily);

        for ( MealyTransition transition : transitions) {
            LocationState source = transition.getSource();
            if ( transition.getOutput().getName().equals("error")) continue;
            if (excludeSources.contains(source) ) continue;

            Tree<IosStep> sdt = Util.getStateDistinguishingTree(mealy, source, separatingFamily.get(source));
            Set<LocationState> matchingStates = matchDistTree(mealy,sdt); // initially   matchingStates= {state}!!
            // System.out.println(state + " : " + matchingStates + " depth : " + TreeUtils.getDepth(sdt) );


            // pluk source's sdt until more then one matching state
            Tree<IosStep> resultSdt = sdt;
            while ( matchingStates.size() == 1 ) {
                if ( resultSdt.size() == 2) break;  // sdt of size cannot be plukked anymore, so break
                resultSdt=Util.plukLeaf(resultSdt,false);  //TODO: try ones with error
                matchingStates = matchDistTree(mealy,resultSdt);
                matchingStates.removeAll(excludeSources); // must at least have 'source' in result because 'source' not in excludeSources
              //  System.out.println(source + " : " + matchingStates + " depth : " + TreeUtils.getDepth(resultSdt) );
            }

            if (  matchingStates.size() > 1 ) {
                count++;
                String srcFormula = Util.sdt2ctlFormula(resultSdt);
                InputAction input = transition.getInput();
                OutputAction output = transition.getOutput();
                LocationState dst = transition.getDestination();
                String dstFormula = Util.getStrippedStateFormula(mealy, separatingFamily, dst);
                String formula= "AG ( ("+ srcFormula + ") => <" + input.getName() +"><" +output.getName() + ">("  + dstFormula + ") )";

        //        System.out.println(source + " : " + matchingStates + " depth : " + TreeUtils.getDepth(resultSdt) );

                formula2transition.put(formula, transition);
                excludeSources.add(source);

               // state2formula.put(state, formula);
                if (count==number ) break;
            }


        }

        return formula2transition;
    }

    public static HashMap<String, MealyTransition> getGoodFormulasForTransitions(
            MealyDetEnabledModel model, Map<LocationState, Set<List<InputAction>>> separatingFamily,
            List<MealyTransition> transitions,
            List<MealyTransition> excludeTransitions,
            int number, int numberToGenerate) {

        if ( number > numberToGenerate) numberToGenerate=number;

        HashMap<String,MealyTransition> formula2transition = new  HashMap<>();

 //       Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);

        int count=0;
        for ( MealyTransition transition : transitions ) {
            LocationState src = transition.getSource();
            LocationState dst = transition.getDestination();
            InputAction input = transition.getInput();
            OutputAction output = transition.getOutput();

            if (output.getName().equals("error")) continue;

            count++;

            String srcFormula = Util.getStrippedStateFormula(model, separatingFamily, src);
            String dstFormula = Util.getStrippedStateFormula(model, separatingFamily, dst);

            // SPEC  prefix for nusmv
            String formula= "AG ( ("+ srcFormula + ") => <" + input.getName() +"><" +output.getName() + ">("  + dstFormula + ") )";
            formula2transition.put(formula, transition);

            System.out.println(count);
            if( count == numberToGenerate ) break;
           // if (count==number ) break;
        }

        HashMap<String,MealyTransition> subset_formula2transition = getSmallestFormulas(formula2transition,number);
        return subset_formula2transition;
     //   return formula2transition;
    }



    public static HashMap<String, LocationState> xxxxgetBadConnectionFormulasForTransitionsNEW(
            MealyDetEnabledModel model, Map<LocationState, Set<List<InputAction>>> separatingFamily, LocationState errorState,
            int number, int numberToGenerate) {

        HashMap<String,LocationState> formula2srcstate = new  HashMap<>();

        System.out.println("getState2DistinguishingFormula");
 //       Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);

        //System.out.println("getState2DistinguishingFormulaPlukked");
        //Map<LocationState, String> state2distformulaMutated = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        System.out.println("get locations");
        ImmutableSet<LocationState> locations = model.getLocations();

        List<LocationState>  srcLocations= new LinkedList<>(locations);
        Collections.shuffle(srcLocations, new Random(3));
        List<LocationState>  destLocations= new LinkedList<>(locations);
        Collections.shuffle(destLocations, new Random(4));

//      // only few results
//        for ( LocationState src : locations) {
//            Tree<IosStep> sdt = Util.getStateDistinguishingTree(model, src, separatingFamily.get(src));
//            Tree<IosStep> strippedSdt=stripSdt(model,sdt);
//            TreeNode<IosStep> matchNode = TreeUtils.findMatch(strippedSdt, ios -> { if (ios.getOutput() ==null) return false; return ios.getOutput().getName().equals("error"); });
//            if ( matchNode != null ) {
//                System.out.println("having error output: " + src);
//            }
//        }


        // strip error state until matches other states which we take as src
        // then randomly take another state as dest (but may not be in "other states")



        System.out.println("start loop");
        ArrayList<LocationState> pickedStates = new ArrayList<>();
      //  ArrayList<LocationState> pickedSrcs = new ArrayList<>();
        int count=0;
        outer:for ( LocationState src : srcLocations) {
            if (pickedStates.contains(src)) continue;
            for ( LocationState dst : destLocations ) {
                if (src.equals(dst)) continue;
                if (pickedStates.contains(dst)) continue;


                if (src.equals(errorState)) continue;
                if (dst.equals(errorState)) continue;

                // pluk source sdt until it also matches errorState   => error state cannot reach dst=> formula false

                // Tree<IosStep> original_srcSdt = Util.getStateDistinguishingTree(model, src, separatingFamily.get(src));

                // alternative take errorstate as source:
                Tree<IosStep> original_srcSdt = Util.getStrippedStateDistinguishingTree(model, errorState, separatingFamily.get(errorState));
               // original_srcSdt=stripSdt(model,original_srcSdt);
                Tree<IosStep> srcSdt= plukLeafFromSdtRepeatinglyUntilMatchesState( model, original_srcSdt,  errorState) ;
                if ( srcSdt == null ) continue;  // not succeeded


                String src_formula =  Util.sdt2ctlFormula(srcSdt);

                String dst_formula = Util.getStrippedStateFormula(model, separatingFamily, dst);

                count++;
                System.out.println(count);
                String formula= "AG ( ("+ src_formula + ") => EF (" + dst_formula + ") )";
                formula2srcstate.put(formula, src);

                pickedStates.add(src);
                pickedStates.add(dst);
                //if (count==number ) break outer;
                if( count == numberToGenerate ) break outer;

            }
        }
        HashMap<String,LocationState> subset_formula2srcstate = getSmallestFormulas(formula2srcstate,number);
        return subset_formula2srcstate;
    }
    public static HashMap<String, LocationState> getBadConnectionFormulasForTransitions(
            MealyDetEnabledModel model, Map<LocationState, Set<List<InputAction>>> separatingFamily, LocationState errorState,
            int number, int numberToGenerate) {

        HashMap<String,LocationState> formula2srcstate = new  HashMap<>();

        System.out.println("getState2DistinguishingFormula");
 //       Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);

        //System.out.println("getState2DistinguishingFormulaPlukked");
        //Map<LocationState, String> state2distformulaMutated = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        System.out.println("get locations");
        ImmutableSet<LocationState> locations = model.getLocations();

        List<LocationState>  srcLocations= new LinkedList<>(locations);
        Collections.shuffle(srcLocations, new Random(3));
        List<LocationState>  destLocations= new LinkedList<>(locations);
        Collections.shuffle(destLocations, new Random(4));

        System.out.println("start loop");
        int count=0;
        ArrayList<LocationState> pickedStates = new ArrayList<>();
        outer:for ( LocationState src : srcLocations) {
            if (pickedStates.contains(src)) continue outer;
            if (src.equals(errorState)) continue outer;

            //1. pluk source sdt until it also matches errorState   => error state cannot reach dst=> formula false
            Tree<IosStep> original_srcSdt = Util.getStateDistinguishingTree(model, src, separatingFamily.get(src));
            Tree<IosStep> srcSdt= plukLeafFromSdtRepeatinglyUntilMatchesState( model, original_srcSdt,  errorState);

            inner: for ( LocationState dst : destLocations ) {
                if (src.equals(dst)) continue inner;
                if (pickedStates.contains(dst)) continue inner;
                if (pickedStates.contains(src)) continue outer;
                if (src.equals(errorState)) continue outer;
                if (dst.equals(errorState)) continue inner;


                // 2. take errorstate  and strip it until it matches another state
              //  Tree<IosStep> original_srcSdt = Util.getStateDistinguishingTree(model, errorState, separatingFamily.get(errorState));
              //  original_srcSdt=stripSdt(model,original_srcSdt);
              //  Tree<IosStep> srcSdt= plukLeaf(original_srcSdt,false);
                //Tree<IosStep> srcSdt= plukLeafFromSdtRepeatinglyUntilMatchesState( model, original_srcSdt,  src) ;

                if ( srcSdt == null ) continue;  // not succeeded


                String src_formula =  Util.sdt2ctlFormula(srcSdt);

                String dst_formula = Util.getStrippedStateFormula(model, separatingFamily, dst);

                count++;
                System.out.println(count);
                String formula= "AG ( ("+ src_formula + ") => EF (" + dst_formula + ") )";
                formula2srcstate.put(formula, src);

                pickedStates.add(src);
                pickedStates.add(dst);

                //if (count==number ) break outer;
                if( count == numberToGenerate ) break outer;

            }
        }
        HashMap<String,LocationState> subset_formula2srcstate = getSmallestFormulas(formula2srcstate,number);
        return subset_formula2srcstate;
    }



    public static HashMap<String, LocationState> getGoodConnectionFormulasForTransitions(
            MealyDetEnabledModel model, Map<LocationState, Set<List<InputAction>>> separatingFamily, LocationState errorState,
            int number,int numberToGenerate) {

        HashMap<String,LocationState> formula2srcstate = new  HashMap<>();

        System.out.println("getState2DistinguishingFormula");
//        Map<LocationState, String> state2distformula = Util.getState2DistinguishingFormula(model,separatingFamily);

        //System.out.println("getState2DistinguishingFormulaPlukked");
        //Map<LocationState, String> state2distformulaMutated = Util.getState2DistinguishingFormulaPlukked(model,separatingFamily);

        System.out.println("get locations");
        ImmutableSet<LocationState> locations = model.getLocations();

        List<LocationState>  srcLocations= new LinkedList<>(locations);
        Collections.shuffle(srcLocations, new Random(3));
        List<LocationState>  destLocations= new LinkedList<>(locations);
        Collections.shuffle(destLocations, new Random(4));

        System.out.println("start loop");
        int count=0;
        ArrayList<LocationState> pickedStates = new ArrayList<>();
        outer:for ( LocationState src : srcLocations) {
            inner: for ( LocationState dst : destLocations ) {
                if (src.equals(dst)) continue inner;
                if (pickedStates.contains(dst)) continue inner;
                if (pickedStates.contains(src)) continue outer;
                if (dst.equals(errorState)) continue inner;
                if (src.equals(errorState)) continue outer;

//                // pluk destinition until it matches  multiple states   =>  then still holds
//                Tree<IosStep> sdt = Util.getStateDistinguishingTree(model, dst, separatingFamily.get(dst));
//                Tree<IosStep> resultSdt=plukLeafFromSdtRepeatinglyUntilMatchesMultipleStates(model,sdt);
//                Set<LocationState> matchingStates = matchDistTree(model,resultSdt);
//                if (matchingStates.size() == 1) continue;
//                String dst_formula = Util.sdt2ctlFormula(resultSdt);

                count++;


                String srcFormula = Util.getStrippedStateFormula(model, separatingFamily, src);
                String dstFormula = Util.getStrippedStateFormula(model, separatingFamily, dst);


                String formula= "AG ( ("+ srcFormula + ") => EF (" + dstFormula + ") )";
              //  String formula= "AG ( ("+ state2distformulaMutated.get(src) + ") => EF (" + state2distformula.get(dst) + ") )";
               // String formula= "AG ( ("+ state2distformula.get(src) + ") => EF (" + dst_formula + ") )";
                formula2srcstate.put(formula, src);

                pickedStates.add(src);
                pickedStates.add(dst);
                System.out.println(count + "  " + src + "->" + dst + " "+pickedStates);


                //if (count==number ) break outer;
                if( count == numberToGenerate ) break outer;

            }
        }
        HashMap<String,LocationState> subset_formula2srcstate = getSmallestFormulas(formula2srcstate,number);
        return subset_formula2srcstate;
      //  return formula2srcstate;
    }

     public static Tree<IosStep> plukLeafFromSdtRepeatinglyUntilMatchesMultipleStates(MealyDetEnabledModel mealy, Tree<IosStep> sdt) {
         Set<LocationState> matchingStates = matchDistTree(mealy,sdt);
    //     System.out.println(state + " : " + matchingStates + " depth : " + TreeUtils.getDepth(sdt) );
     //    System.out.println(state + " : " + matchingStates);
     //    System.out.println( "sdt : " + sdt);
    //     System.out.println( "depth : " + TreeUtils.getDepth(sdt));

         // pluk until more  then one matching state
         Tree<IosStep> resultSdt = sdt;
         while ( matchingStates.size() == 1 ) {
             if ( resultSdt.size() == 2) break;  // sdt of size cannot be plukked anymore, so break
             resultSdt=Util.plukLeaf(resultSdt,false);
             matchingStates = matchDistTree(mealy,resultSdt);
        //     System.out.println(state + " : " + matchingStates + " depth : " + TreeUtils.getDepth(resultSdt) );
       //      System.out.println( "sdt : " + resultSdt);
        //     System.out.println( "depth : " + TreeUtils.getDepth(resultSdt));
         }
         return resultSdt;
     }

     public static Tree<IosStep> plukLeafFromSdtRepeatinglyUntilMatchesState(MealyDetEnabledModel mealy, Tree<IosStep> sdt, LocationState state) {
         // pluk until matching state
         Tree<IosStep> resultSdt = sdt;
         Set<LocationState> matchingStates = matchDistTree(mealy,sdt);
         int size = sdt.size();
         int prev_size = size;
         while ( ! matchingStates.contains(state)  ) {
             if ( size  == 2)  return null;  // sdt of size cannot be plukked anymore, so break

             prev_size = size;
             resultSdt=Util.plukLeaf(resultSdt,false);
             size = resultSdt.size();
             if ( size == prev_size ) return null;  // sdt of size cannot be plukked anymore, so break
             matchingStates = matchDistTree(mealy,resultSdt);
         }
         return resultSdt;
     }

     // strip state sdt to minimal one for this specific model!!
     public static Tree<IosStep> stripSdtLeftRight(MealyDetEnabledModel mealy, Tree<IosStep> sdt) {


         // pluk std until it matches more then one state
         Tree<IosStep> resultSdt = sdt;
         Tree<IosStep> prevSdt = sdt;
         Set<LocationState> matchingStates = matchDistTree(mealy,sdt);
         while (  matchingStates.size() ==1  ) {
             if ( resultSdt.size() == 2)  { // sdt of size cannot be plukked anymore, so break
                 prevSdt = resultSdt;
                 break;
             }
             prevSdt = resultSdt;
             resultSdt=Util.plukLeaf(resultSdt,false);
             matchingStates = matchDistTree(mealy,resultSdt);
         }

         // strip from other side of tree
         resultSdt = prevSdt;
         matchingStates = matchDistTree(mealy,resultSdt);
         while (  matchingStates.size() ==1  ) {
             if ( resultSdt.size() == 2)  { // sdt of size cannot be plukked anymore, so break
                 prevSdt = resultSdt;
                 break;
             }
             prevSdt = resultSdt;
             resultSdt=Util.plukLeaf(resultSdt,true);
             matchingStates = matchDistTree(mealy,resultSdt);
         }

         // return previous sdt where it last only matched only one state
         return prevSdt;
     }
     public static int countstrips=0;
     public static Tree<IosStep> stripSdt(MealyDetEnabledModel mealy, Tree<IosStep> sdt) {
        // return stripSdtLeftRight(mealy,sdt);

//         countstrips++;
//         System.out.println("countstrips "+ countstrips);

         return stripSdtToMinimal(mealy,sdt);
       //  return sdt;
     }

     // strip state sdt to minimal one for this specific model!!
     public static Tree<IosStep> stripSdtToMinimal(MealyDetEnabledModel mealy, Tree<IosStep> sdt) {

         // size == 2 is the smallest size, which is just a single transition as distinguishing sequence.
         // If we cut that then we don't have a tree anymore.  => in that case don't do anything.
         // note: if there are other leaves then it is ok to remove a branch of just only one transition
         if ( sdt.size() == 2 ) return sdt;

         Tree<IosStep> localSdt = sdt.getDeepCopy(); // don't change original sdt

         // get the  leaf nodes
         LinkedList<TreeNode<IosStep>> todo_leaves = new LinkedList<>(TreeUtils.getLeaves(localSdt));

         List<TreeNode<IosStep>> todo_error_leaves = todo_leaves.stream().filter(l->l.getData().getOutput().getName().equals("error")).collect(Collectors.toList());

         for(int i = 0; i < todo_error_leaves.size(); i++) {
             TreeNode<IosStep> leaf = todo_error_leaves.get(i);
            // if ( leaf.getData().getOutput().getName().equals("error") )  {
                 TreeNode<IosStep> parent = leaf.getParent();
                 List<TreeNode<IosStep>> children = parent.getChildren();
                 int index=children.indexOf(leaf);
                 parent.removeChildAt(index);
            // }
         }
//         System.out.println("todo_leaves " + todo_leaves.size() + " error " + todo_error_leaves.size() );

         Set<LocationState> xmatchingStates = matchDistTree(mealy,localSdt);
         if (  xmatchingStates.size() !=1 ) {
             // undo cutting error leaves
             localSdt = sdt.getDeepCopy();
             todo_leaves = new LinkedList<>(TreeUtils.getLeaves(localSdt));

             todo_error_leaves = todo_leaves.stream().filter(l->l.getData().getOutput().getName().equals("error")).collect(Collectors.toList());
//             System.out.println("FAIL error strip " + xmatchingStates );

             for(int i = 0; i < todo_error_leaves.size(); i++) {
                 TreeNode<IosStep> leaf = todo_error_leaves.get(i);
                 TreeNode<IosStep> parent = leaf.getParent();
                 List<TreeNode<IosStep>> children = parent.getChildren();
                 int index=children.indexOf(leaf);
                 parent.removeChildAt(index);

                 Set<LocationState> matchingStates = matchDistTree(mealy,localSdt,xmatchingStates);
                 if (  matchingStates.size() !=1 ) {
                     // put leaf back, otherwise not a state identifier anymore
                     parent.addChildAt(index, leaf);
                 }
             }

//             System.out.println("further error strip");
             // get new  leaf nodes
             todo_leaves = new LinkedList<>(TreeUtils.getLeaves(localSdt));

         } else {
//             System.out.println("success error strip");
             // get new  leaf nodes
             todo_leaves = new LinkedList<>(TreeUtils.getLeaves(localSdt));
         }



         if ( localSdt.size() == 2 ) return localSdt;

         for(int i = 0; i < todo_leaves.size(); i++) {
             TreeNode<IosStep> leaf = todo_leaves.get(i);
             TreeNode<IosStep> parent = leaf.getParent();
             List<TreeNode<IosStep>> children = parent.getChildren();
             int index=children.indexOf(leaf);
             parent.removeChildAt(index);

             Set<LocationState> matchingStates = matchDistTree(mealy,localSdt);
             if (  matchingStates.size() ==1 ) {
                 // still state identifier, so we found a shorter sdt and keep it

                 // if size==2 then we found shortest sdt possible and we should stop!
                 if ( localSdt.size() == 2 ) return localSdt;

                 // if parent of leaf doesn't have any children anymore because you just remove its only child,
                 // then it becomes a leaf itself which must be processed
                 if ( parent.getChildren().size() == 0 ) {
                     todo_leaves.add(parent);
                 }
             } else {
                 // put leaf back, otherwise not a state identifier anymore
                 parent.addChildAt(index, leaf);
             }
         }
         return localSdt;
     }

     /** draw number in range [min,max]  ; note both min and max can be drawn (are inclusive)
      * usage:
      *    Random randomGenerator=Random(4);
      *    int first  = getRandomNumberInRange(2,5,randomGenerator);
      *    int second = getRandomNumberInRange(2,5,randomGenerator);
      */
     public static int getRandomNumberInRange(int min, int max, Random randomGenerator) {

         if (min >= max) {
             throw new IllegalArgumentException("max must be greater than min");
         }

         // nextInt(4) draws in range [0:3]
         // min = 4 , max =7  =>   max-min = 3  => nextInt(4) + 4 => range [0:3] +4 => range [4:7]
         return randomGenerator.nextInt((max - min) + 1) + min;
     }

    public static Map<LocationState,String> getState2DistinguishingFormulaPlukked( MealyDetEnabledModel mealy,Map<LocationState,Set<List<InputAction>>> separatingFamily) {


        Map<LocationState,String> state2formula = new HashMap<>();
        for ( LocationState state : separatingFamily.keySet()) {
            Tree<IosStep> sdt = Util.getStateDistinguishingTree(mealy, state, separatingFamily.get(state));

            Tree<IosStep> resultSdt=plukLeafFromSdtRepeatinglyUntilMatchesMultipleStates(mealy,sdt);

            String formula = Util.sdt2ctlFormula(resultSdt);
            state2formula.put(state, formula);
        }
        return state2formula;
    }

    /** run a state distinguisher on a specific state
     *
     *  if the observed behavior of the state in the mealy machine matches the observed behavior in the  state distinguisher
     *  then return true and else false.
     *
     *  Note: states in the state distinguishing tree are the states of the reference model but don't need to match
     *  the state labels in another model. States are equal if they have the same observable behavior. State indexing
     *  is specific per model instance of a mealy machine, and can be choosen different per instance even if both
     *  models represent the same mealy machine.
     *
     * @param stateDistTree
     * @return
    */
    public static boolean matchDistTreeOnState( MealyDetEnabledModel mealy, LocationState state, Tree<IosStep> stateDistTree) {
        List<List<IosStep>> traces = TreeUtils.getDataTraces(stateDistTree);
        for (List<IosStep> expectedTrace : traces) {
              List<InputAction> inputs = expectedTrace.stream().map(s -> s.getInput()).collect(Collectors.toList());
              inputs.remove(0);
              List<IosStep> resultTrace = mealy.getTraceFrom(state,inputs);

              List<IoStep> expectedBehavior = expectedTrace.stream().map(s -> s.getIoStep()).collect(Collectors.toList());
              List<IoStep> resultBehavior = resultTrace.stream().map(s -> s.getIoStep()).collect(Collectors.toList());
              if ( ! expectedBehavior.equals(resultBehavior)) return false;
        }
        return true;
    }



    /** verify state distinguisher ; a good state distinguisher should only match one state
     *
     * @param stateDistTree
     * @return
     */
    public static Set<LocationState> matchDistTree( MealyDetEnabledModel mealy, Tree<IosStep> stateDistTree) {
        HashSet<LocationState> matchingStates = new HashSet<>();
        for ( LocationState state: mealy.getLocations()) {
            if ( matchDistTreeOnState(mealy,state, stateDistTree) ) matchingStates.add(state);
        }
        return matchingStates;
    }

    /** verify state distinguisher for specific subset of states; a good state distinguisher should only match one state
    *
    * @param stateDistTree
    * @return
    */
   public static Set<LocationState> matchDistTree( MealyDetEnabledModel mealy, Tree<IosStep> stateDistTree,Set<LocationState> subsetOfStates) {
       HashSet<LocationState> matchingStates = new HashSet<>();
       for ( LocationState state: subsetOfStates) {
           if ( matchDistTreeOnState(mealy,state, stateDistTree) ) matchingStates.add(state);
       }
       return matchingStates;
   }

    private static Tree<IosStep> plukLeaf(Tree<IosStep> sdt,boolean first) {
        // pluk deepest leaf (if multiple at same highest depth, pluk first)

        Tree<IosStep> wrongsdt = sdt.getDeepCopy();

        // size == 2 is the smallest size, which is just a single transition as distinguishing sequence.
        // If we cut that then we don't have a tree anymore.  => in that case don't do anything.
        // note: if there are other leaves then it is ok to remove a branch of just only one transition
        if ( wrongsdt.size() == 2 ) return wrongsdt;

        // remove one of the deepest leaf nodes
        List<TreeNode<IosStep>> leaves = TreeUtils.getDeepestLeaves(wrongsdt);

        TreeNode<IosStep> removeLeaf;

        if ( first ) {
            // remove first leaf
            removeLeaf = leaves.get(0);
        } else {
            // remove last leaf
            removeLeaf = leaves.get(leaves.size()-1);;
        }
        removeLeaf.getParent().removeChild(removeLeaf);
        return wrongsdt;
    }

    private static Tree<IosStep> plukFirstLeaf(Tree<IosStep> sdt) {
        // TODO Auto-generated method stub
        //sdt.getShallowCopy(iterator)
        Tree<IosStep> wrongsdt = sdt.getDeepCopy();
        boolean success=false;
        //int size = wrongsdt.size()

        // size == 2 is the smallest size, which is just a single transition as distinguishing sequence.
        // If we cut that then we don't have a tree anymore.  => in that case don't do anything.
        // note: if there are other leaves then it is ok to remove a branch of just only one transition
        if ( wrongsdt.size() == 2 ) return wrongsdt;

        for (TreeNode<IosStep> node : wrongsdt) {
             if ( node.isLeaf() ) {
             //    if ( wrongsdt.size() ==2 && node.getParent().getParent() == null ) continue; // leaf must have at least two parents, if only one parent, then whole branch is removed.
                 node.getParent().removeChild(node);
                 success=true;
                 break; // only remove one
             }
        }

       // if (success) return wrongsdt;
      //  return null;
        return wrongsdt;
    }


    // TODO:   find out purpose of this test ??   => not called anywhere
    public static void testDistinguishingFormula( MealyDetEnabledModel mealy,Map<LocationState,Set<List<InputAction>>> separatingFamily) {
        Map<LocationState,String> state2formula = new HashMap<>();
        for ( LocationState state : separatingFamily.keySet()) {
            Tree<IosStep> sdt = Util.getStateDistinguishingTree(mealy, state, separatingFamily.get(state));
           // List<List<IosStep>> traces = TreeUtils.getDataTraces(sdt);
            for ( LocationState otherState : separatingFamily.keySet()) {
                Tree<IosStep> otherSdt = Util.getStateDistinguishingTree(mealy, otherState, separatingFamily.get(state));
                boolean result=TreeUtils.compareTree(sdt, otherSdt);
            }
        }
    }

    public static Tree<IosStep> getStateDistinguishingTree(MealyDetEnabledModel mealy,LocationState location,
            Set<List<InputAction>> stateDistinguishingSet ) {
           List<List<IosStep>> traces = new LinkedList<>();
           for ( List<InputAction> statedist : stateDistinguishingSet ) {
             //  List<IosStep> trace = mealy.getTrace(statedist);
               List<IosStep> trace = mealy.getTraceFrom(location,statedist);
               traces.add(trace);
           }
           return TreeUtils.traces2tree(traces);
    }

    public static HashMap<LocationState,Tree<IosStep>> stripped_sdt_cache = new HashMap<>();
    public static int cachehits=0;
    public static Tree<IosStep> getStrippedStateDistinguishingTree(
            MealyDetEnabledModel mealy, LocationState state, Set<List<InputAction>> stateDistinguishingSet) {

        if (stripped_sdt_cache.containsKey(state) ) {
            cachehits++;
            System.out.println("==============> cache hit " + cachehits);
            return stripped_sdt_cache.get(state);
        } else {
            // get harmonized state identifier (used for conformance testing)
            Tree<IosStep> sdt = Util.getStateDistinguishingTree(mealy, state, stateDistinguishingSet);
           // strip state identifier specific for model
            Tree<IosStep> stripped_sdt= stripSdt(mealy,sdt);

 // disable cache
 //           stripped_sdt_cache.put(state, stripped_sdt);

            return stripped_sdt;
        }
    }

    public static void exportSepFamilyTree2(String modelfile) {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel(modelfile);
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(detlts,3);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
        //  nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(state+"_StateDistinguishingTree.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(modelfile + ".stateDistinguishingTree.dot", combined);
    }

    public static void exportSepFamilyTree(String modelfile) {
        MealyNonDetModel nondetlts = statemachine.model.fsm.mealy.file.Import.dot2MealyModel(modelfile);
        MealyDetEnabledModel detlts  = MealyDetEnabledModel.fromMealyModel(nondetlts);

        Map<LocationState, Set<List<InputAction>>> separatingFamily = Util.importSeparatingFamily(detlts,3);

        TreeNode<IosStep> root_combined = new TreeNode<>(new IosStep(new InputAction(""), new OutputAction(""), new LocationState("")));
        Tree<IosStep> combined = new Tree<IosStep>(root_combined);
        for ( LocationState state : separatingFamily.keySet()) {
          Tree<IosStep> sdt = Util.getStateDistinguishingTree(detlts, state, separatingFamily.get(state));
          nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(modelfile + "_/" + state+"_StateDistinguishingTree.dot", sdt);
          root_combined.addChild(sdt.getRoot());
        }
        //nl.ru.cs.tree.file.Export.edgeNodeTree2dotfile(modelfile + ".stateDistinguishingTree.dot", combined);
    }


    public static  TreeNode<SplittingTreeData> findLowestNodeContainingStates(TreeNode<SplittingTreeData> node,Collection<LocationState> states) {
        TreeNode<SplittingTreeData> resultNode=null;
        if ( !node.getData().getStates().containsAll(states)) {
            throw new BugException("The rootnode splitting tree doesn't contain states required, which it should!");
        }
        searchNode: while ( resultNode == null ) {
            for ( TreeNode<SplittingTreeData> child: node.getChildren() ) {
                if ( child.getData().getStates().containsAll(states)) {
                     // child mathes, continue search with child node
                     node=child;
                     continue searchNode; //  to continue next loop of while which starts from child node
                }
            }
            // no match found in children, so result node found
            resultNode = node;
        }
        return resultNode;
    }

    public static List <TreeNode<ADSdata>>  processNode(MealyDetEnabledModel mealy,TreeNode<ADSdata> node, Tree<SplittingTreeData> splittingTree) {
           System.out.println("processNode:" + node);
          List <TreeNode<ADSdata>>  newTodoNodes = new LinkedList<>();

          LinkedList<LocationState> currentStates = node.getData().getCurrentStates();

          if (currentStates.size() <= 1) return newTodoNodes;  // no states to split !


          TreeNode<SplittingTreeData> lowestNodeContainingStates = findLowestNodeContainingStates(splittingTree.getRoot(),currentStates);
          ImmutableList<InputAction> separatingSequence = lowestNodeContainingStates.getData().getSeparatingSequence();

          if ( separatingSequence == null ) return newTodoNodes;  // no sequence to use to split !

          List <TreeNode<ADSdata>>  treeNodes = new LinkedList<>();
          treeNodes.add(node);

          for ( InputAction input: separatingSequence) {
              System.out.println("step:" + input);
              treeNodes=stepParallelADS(mealy,treeNodes,input);

          }
          return treeNodes;

    }
    private static
            List<TreeNode<ADSdata>> stepParallelADS(MealyDetEnabledModel mealy, List<TreeNode<ADSdata>> treeNodes, InputAction input) {
        System.out.println("stepParallelADS:" + input);

        List <TreeNode<ADSdata>>  newTreeNodes = new LinkedList<>();
        for (  TreeNode<ADSdata> node : treeNodes ) {
            System.out.println("node:" + node);
            newTreeNodes.addAll(stepADS(mealy,node,input));
        }
        return newTreeNodes;
    }

    private static List<TreeNode<ADSdata>> stepADS(MealyDetEnabledModel mealy,TreeNode<ADSdata> node, InputAction input) {
        System.out.println("stepADS:" + input + " node: " + node);

        List <TreeNode<ADSdata>>  newTreeNodes = new LinkedList<>();
        ADSdata nodeData = node.getData();
        LinkedList<LocationState> currentStates = nodeData.getCurrentStates();
        LinkedList<LocationState> initialStates = nodeData.getInitialStates();
        nodeData.setInput(input);

        HashMap<LocationState,Integer> currentState2index = new HashMap<>();
        for ( int index=0; index<currentStates.size();index++) {
            currentState2index.put(currentStates.get(index), index);
        }

        HashMultimap<OutputAction,LocationState> output2currentStates = HashMultimap.create();
        HashMap<LocationState,LocationState> source2dest = new HashMap<>();
        for (  LocationState location : currentStates ) {
              MealyTransition trans = mealy.getTriggeredTransition(location, input);
              OutputAction output = trans.getOutput();
              output2currentStates.put(output, location);
              source2dest.put(location, trans.getDestination());
        }
        for ( OutputAction output :output2currentStates.keySet()) {
            LinkedList<LocationState> newCurrentStates = new LinkedList<>();
            LinkedList<LocationState> newInitialStates = new LinkedList<>();
            Set<LocationState> currentSubStates = output2currentStates.get(output);
            for ( LocationState currentState : currentSubStates) {
                Integer index = currentState2index.get(currentState);
                newCurrentStates.add(source2dest.get(currentState));
                newInitialStates.add(initialStates.get(index));
            }
            ADSdata newNodeData = new ADSdata(output, newInitialStates, newCurrentStates, null);
            TreeNode<ADSdata> newTreeNode = new TreeNode<>(newNodeData);
            newTreeNodes.add(newTreeNode);
            node.addChild(newTreeNode);
        }
        return newTreeNodes;
    }

    public static  Tree<ADSdata> getAdaptiveDistinguishingSequence(MealyDetEnabledModel mealy,Tree<SplittingTreeData> splittingTree) {
        ImmutableSet<LocationState> states = mealy.getLocations();
        ImmutableSet<LocationState> initialStates = states;
        ImmutableSet<LocationState> currentStates = states;

        ADSdata data = new ADSdata(new OutputAction(""),  initialStates, currentStates, null);
        TreeNode<ADSdata> rootNode = new TreeNode<>(data);
        Tree<ADSdata> tree = new  Tree<ADSdata>(rootNode);

        List <TreeNode<ADSdata>>  todoNodes = new LinkedList<>();
        todoNodes.add(rootNode);

        while ( !todoNodes.isEmpty() ) {
            List <TreeNode<ADSdata>>  newTodoNodes = new LinkedList<>();
            for ( TreeNode<ADSdata> node : todoNodes) {
                newTodoNodes.addAll( processNode(mealy,node,splittingTree) );
            }
            todoNodes = newTodoNodes;
        }
        return tree;
    }


    public static  Map<LocationState,Set<List<InputAction>>> getSeparatingFamily(Tree<SplittingTreeData> splittingTree) {

         Map<LocationState,Set<List<InputAction>>> separatingFamily = new HashMap<>();
         List<TreeNode<SplittingTreeData>> leaves = TreeUtils.getLeaves(splittingTree);

         for ( TreeNode<SplittingTreeData> leaf: leaves ) {
             ImmutableSet<LocationState> states = leaf.getData().getStates();
             if ( states.size() != 1) {
                 throw new BugException("splitting tree has none-singleton leaf");
             }
             LocationState state = states.iterator().next();
             Set<List<InputAction>> stateDistinguishingSet = new HashSet<>();
             List<TreeNode<SplittingTreeData>> nodes = TreeUtils.getTraceFromNodeToRoot(leaf, splittingTree);
             // remove first node, which is leaf node, which doesn't has a separatingSequence
             nodes.remove(0);
             for ( TreeNode<SplittingTreeData> node : nodes ) {
                 ImmutableList<InputAction> separatingSequence = node.getData().getSeparatingSequence();
                 stateDistinguishingSet.add(separatingSequence);
             }
             stateDistinguishingSet = TreeUtils.removeSubTraces(stateDistinguishingSet);
             separatingFamily.put(state, stateDistinguishingSet);
         }
         return separatingFamily;
    }

    private static HashSet<List<InputAction>> getEnclosingSet(Set<List<InputAction>> stateDistinguishingSet) {
        TreeUtils.traces2treeWithEmptyRootNode(stateDistinguishingSet);
        return null;
    }


    public static String sdt2ctlFormula(Tree<IosStep> sdt) {
        List<List<IosStep>> traces = TreeUtils.getDataTraces(sdt);
        return traces.stream().map(t -> trace2formula(t)).collect(Collectors.joining(" & "));
    }

    private static String trace2formula(List<IosStep> trace) {
       // System.out.println(trace.get(0).getInput());
        trace.remove(0);
        if (trace.isEmpty()) {
            int x=3;
        }
        return trace.stream().map(s -> "<" + s.getInput().getName() + "><" + s.getOutput().getName() + ">" ).collect(Collectors.joining("")) + "true";

    }

    //   String input = "<a><x>true";
    //   String expected = "EX(input=a & EX(output=x)))";

    public static String parseAltTrace(String input,boolean nextIsInput) {
        // TODO Auto-generated method stub
        int startBracket=input.indexOf('<');
        int endBracket=input.indexOf('>');
        String action=input.substring(startBracket+1, endBracket).trim();
        String rest= input.substring(endBracket+1).trim();
        if ( rest.startsWith("true")) {
            if (nextIsInput) throw new BugException("last action should be an output");
            return "EX(output=" + action + ")";
        }
        String var = nextIsInput ? "input" : "output" ;
        return "EX("+var+"="+action+" & "+parseAltTrace(rest,!nextIsInput) + ")";
    }

    // String input = "AG( (<a><x>true) => <a><x>(<a><y>true) )";
    // String expected = "AG( (EX(input=a & EX(output=x))) -> EX(input=a & EX(output=x & (EX(input=a & EX(output=y)))) )";
    public static String parseFormula(String input) {
        int firstStartBracket=input.indexOf('<');
        int firstEndParenthesis=input.indexOf(')');
        String preConditions = input.substring(firstStartBracket,firstEndParenthesis).trim();
        LinkedList<String> nusmvConditions=new LinkedList<>();
        for ( String condition  : preConditions.split("&") ) {
            nusmvConditions.add( parseAltTrace(condition.trim(),true ) );
        }
        String nusmvPreCondition=  String.join(" & ", nusmvConditions);

        int firstEqualIndex=input.indexOf('=');
        String postCondition = input.substring(firstEqualIndex+2);

        firstStartBracket=postCondition.indexOf('<');
        int firstEndBracket=postCondition.indexOf('>');
        int secondStartBracket=postCondition.indexOf('<',firstEndBracket);
        int secondEndBracket=postCondition.indexOf('>',secondStartBracket);
        String nextInput = postCondition.substring( firstStartBracket+1,firstEndBracket ).trim();
        String nextOutput = postCondition.substring( secondStartBracket+1,secondEndBracket ).trim();

        int thirdStartBracket=postCondition.indexOf('<',secondEndBracket);
        int endParenthesis=postCondition.indexOf(')',thirdStartBracket);
        String postConditions = postCondition.substring(thirdStartBracket,endParenthesis).trim();
        nusmvConditions=new LinkedList<>();
        for ( String condition  : postConditions.split("&") ) {
            nusmvConditions.add( parseAltTrace(condition.trim(),true ) );
        }
        String nusmvPostCondition=  String.join(" & ", nusmvConditions);

        return "AG( (" + nusmvPreCondition + ") -> EX(input="+nextInput+" & EX(output="+nextOutput+" & ("+nusmvPostCondition+"))) )";
    }


    // String input = "AG( (<a><x>true) => EF (<a><y>true) )";
    // String expected = "AG( (EX(input=a & EX(output=x))) -> EF( EX(input=a & EX(output=y)) )";
    public static String parseFormula2(String input) {
        int firstStartBracket=input.indexOf('<');
        int firstEndParenthesis=input.indexOf(')');
        String preConditions = input.substring(firstStartBracket,firstEndParenthesis).trim();
        LinkedList<String> nusmvConditions=new LinkedList<>();
        for ( String condition  : preConditions.split("&") ) {
            nusmvConditions.add( parseAltTrace(condition.trim(),true ) );
        }
        String nusmvPreCondition=  String.join(" & ", nusmvConditions);

        int firstEqualIndex=input.indexOf('=');
        String postCondition = input.substring(firstEqualIndex+2);

        firstStartBracket=postCondition.indexOf('<');
//        int firstEndBracket=postCondition.indexOf('>');
//        int secondStartBracket=postCondition.indexOf('<',firstEndBracket);
//        int secondEndBracket=postCondition.indexOf('>',secondStartBracket);
//        String nextInput = postCondition.substring( firstStartBracket+1,firstEndBracket ).trim();
//        String nextOutput = postCondition.substring( secondStartBracket+1,secondEndBracket ).trim();

     //   int thirdStartBracket=postCondition.indexOf('<',secondEndBracket);
        int endParenthesis=postCondition.indexOf(')',firstStartBracket);
        String postConditions = postCondition.substring(firstStartBracket,endParenthesis).trim();
        nusmvConditions=new LinkedList<>();
        for ( String condition  : postConditions.split("&") ) {
            nusmvConditions.add( parseAltTrace(condition.trim(),true ) );
        }
        String nusmvPostCondition=  String.join(" & ", nusmvConditions);

        return "AG( (" + nusmvPreCondition + ") -> EF("+nusmvPostCondition+") )";
    }

    public static Map<LocationState, Set<List<InputAction>>> importSeparatingFamily(MealyDetEnabledModel model, Integer seed) {

        // result data object
        Map<LocationState,Set<List<InputAction>>> separatingFamily= new HashMap<>();

        // export mealy model to temporary file
        File tempFile = null;
        try {
            tempFile = File.createTempFile("model-", ".dot");
        } catch (IOException e1) {
            throw new FileIOException(e1);
        }
        @SuppressWarnings("null")
        String modelfile =  tempFile.getAbsolutePath();
        statemachine.model.fsm.mealy.file.Export.dot(model, modelfile);

        // run hybrid-ads command
        String output = null;
        try {
            // put hybrid-ads in your path (for eclipse  edit the PATH environment variable in your run configuration)
             output = new ProcessExecutor()
                     .command("hybrid-ads", "-x", seed.toString(), "-m","wset","-f",modelfile)
               //TODO:      .command("hybrid-ads", "-x", seed.toString(), "-i","-f",modelfile)
                     .exitValueNormal()
                     .readOutput(true)
                    .execute()
                    .outputUTF8();
        } catch ( IOException ioException) {
            String msg = ioException.getMessage();
             if ( msg.contains("Could not execute") && msg.contains("No such file") ) {
                 throw new DependencyException("Cannot find 'hybrid-ads' command in your system PATH. Please add it to your system PATH.");
             } else {
                 throw new BugException("problem running hybrid-ads algorithm: " + ioException.getMessage());
             }
        } catch (Exception e) {
              throw new BugException("problem running hybrid-ads algorithm: " + e.getMessage());
        }

        //System.out.println(output);

        // import json data into java
        JSONParser jsonParser = new JSONParser();
        Object obj;
        try {
            obj = jsonParser.parse(output);
        } catch (ParseException e) {
            throw new BugException("problem parsing output from hybrid-ads algorithm: " + e.getMessage());
        }
        JSONObject data = (JSONObject) obj;

        // retreive access sequences and separating family from json data
        @SuppressWarnings("unchecked")
        Map<String,List<String>> state2access =( Map<String,List<String>> ) data.get("access_sequences");
        @SuppressWarnings("unchecked")
        Map<String,List<List<String>>> separatingFamilyWrongStateLabels =( Map<String,List<List<String>>> ) data.get("separating_family");

        // replace state labels in json data by the model's state objects
        // - first get mapping of  state label to  model's state object
        Map<String,LocationState> oldstate2modelstate= new HashMap<>();
        for ( String oldstate : state2access.keySet()) {
            List<InputAction> inputs = state2access.get(oldstate).stream().map(name->new InputAction(name)).collect(Collectors.toList());
            List<IosStep> trace = model.getTrace(inputs);
            LocationState newstate =  trace.get(trace.size()-1).getLocation();
            oldstate2modelstate.put(oldstate,newstate );
        }
        // - then replace state labels with model's state objects in separating family
        for ( String oldstate :separatingFamilyWrongStateLabels.keySet()) {
            LocationState newstate = oldstate2modelstate.get(oldstate);
            List<List<String>> oldStateIdentifier = separatingFamilyWrongStateLabels.get(oldstate);
            Set<List<InputAction>> newStateIdentifier=new HashSet<>();
            for (List<String> inputWord:oldStateIdentifier) {
                newStateIdentifier.add(inputWord.stream().map(name->new InputAction(name)).collect(Collectors.toList()));
            }
            separatingFamily.put(newstate, newStateIdentifier);
        }
        return separatingFamily;
    }



}
