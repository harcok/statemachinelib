package statemachine.model.fsm.mealy.util;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;

public class TraceDiff {
    public List<InputAction> inputsSimular;
    public List<OutputAction> outputsSimular;
    public InputAction discriminatingInput;
    public OutputAction output1;
    public OutputAction output2;

    public boolean empty=true;

    public boolean isEqual() {
        return Objects.equals(output1,output2);
    }

    public TraceDiff(List<InputAction> inputsSimular, List<OutputAction> outputsSimular) {
        super();
        this.inputsSimular = inputsSimular;
        this.outputsSimular = outputsSimular;
    }

    public TraceDiff(List<InputAction> inputsSimular, List<OutputAction> outputsSimular, InputAction discriminatingInput,
            OutputAction output1, OutputAction output2) {
        super();
        this.inputsSimular = inputsSimular;
        this.outputsSimular = outputsSimular;
        this.discriminatingInput = discriminatingInput;
        this.output1 = output1;
        this.output2 = output2;
    }

    public String toString() {
        StringJoiner joiner = new StringJoiner(" ");
        for (int i = 0; i < inputsSimular.size(); i++) {
            InputAction input = inputsSimular.get(i);
            OutputAction output= outputsSimular.get(i);
            joiner.add(input + "/" + output);
        }
        if ( isEqual() ) {
            return "EQUAL:  " + joiner.toString();
        } else {
            return "DIFFERENT:  " + joiner.toString() + " " + discriminatingInput + "/" + outputToString(output1) + "," + outputToString(output2) ;
        }
    }

    private String outputToString(OutputAction output) {
        String out;
        if ( output == null ) {
            out="!QUIESCENCE";
        } else {
            out=output.toString();
        }
        return out;
    }


}
