package statemachine.model.fsm.mealy.util;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;

public class IoStep {
    private InputAction input;
    private OutputAction output;

    public IoStep(InputAction input, OutputAction output) {
        super();
        this.input = input;
        this.output = output;
    }
    public InputAction getInput() {
        return input;
    }
    public OutputAction getOutput() {
        return output;
    }

    @Override
    public String toString() {
        String in =  input == null ? "" : input.toString();
        String out =  output == null ? "" : output.toString();
        String res= in + "/" + out;
        if (res.equals("/")) return "";
        return res;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((input == null) ? 0 : input.hashCode());
        result = prime * result + ((output == null) ? 0 : output.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IoStep other = (IoStep) obj;
        if (input == null) {
            if (other.input != null)
                return false;
        } else if (!input.equals(other.input))
            return false;
        if (output == null) {
            if (other.output != null)
                return false;
        } else if (!output.equals(other.output))
            return false;
        return true;
    }

}
