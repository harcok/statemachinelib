package statemachine.model.fsm.mealy.util;

import java.util.LinkedList;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;

public class ADSnode {
    private InputAction input;
    private LinkedList<LocationState> initialStates;
    private LinkedList<LocationState> currentStates;

    public ADSnode(InputAction input, LinkedList<LocationState> initialStates, LinkedList<LocationState> currentStates) {
        super();
        this.input = input;
        this.initialStates = initialStates;
        this.currentStates = currentStates;
    }

    @Override
    public String toString() {
        String initial_and_current_states = "\nI=" + initialStates + "\n C=" + currentStates ;
        if ( input != null) {
            return input + " " + initial_and_current_states;
        }
        return initial_and_current_states;
    }

    public InputAction getInput() {
        return input;
    }

    public LinkedList<LocationState> getInitialStates() {
        return initialStates;
    }

    public LinkedList<LocationState> getCurrentStates() {
        return currentStates;
    }

}
