package statemachine.model.fsm.mealy.format;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.mealy.MealyModel;

public class NuSMV {

    /*
    *
    *

digraph g {
   __start0 [label="" shape="none"];
   __start0 -> s1;

   s1 [shape="circle" label="s1"];
   s2 [shape="circle" label="s2"];
   s1 -> s2 [label="a/x"];
   s1 -> s2 [label="b/x"];
   s2 -> s1 [label="a/y"];
   s2 -> s2 [label="b/x"];
}

    ||
    \/

MODULE main
VAR
   state : {s1, s2};
   input : {a, b, null};
   output : {x, y, null};
ASSIGN
   init(state)  := s1;
   init(input) := null;
   init(output) := x;  -- random choosen output
   next(state)  := case
       state = s1 & input = a : s2;
       state = s2 & input = a : s1;
       state = s1 & input = b : s2;
       state = s2 & input = b : s2;
       input = null : state;
   esac;
   next(output)  := case
       state = s1 & input = a : x;
       state = s2 & input = a : y;
       state = s1 & input = b : x;
       state = s2 & input = b : x;
       input = null : null;
   esac;
INVAR !(input = null & output = null)
   & !(input != null & output != null)


    */

   public static List<String> export(MealyModel model) {
       String indent="    ";
       LinkedList<String> lines= new LinkedList<>();
       lines.add("MODULE main");
       lines.add("VAR");
       String commasep_states=model.getLocations().stream().map(v->v.getName()).collect(Collectors.joining(", "));
       lines.add(indent + "state : { " + commasep_states +  " };");
       String commasep_inputs=model.getInputAlphabet().stream().map(v->v.getName()).collect(Collectors.joining(", "));
       String commasep_outputs=model.getOutputAlphabet().stream().map(v->v.getName()).collect(Collectors.joining(", "));
       lines.add(indent + "input : { "+ commasep_inputs +", null };");
       lines.add(indent + "output : { "+ commasep_outputs +", null };");
       lines.add("ASSIGN");
       lines.add(indent + "init(state) := " + model.getStartLocation() + ";");
       lines.add(indent + "init(input) := null;");
       lines.add(indent + "init(output) := " + model.getOutputAlphabet().iterator().next().getName() + ";");   // output must be set -> choose something doesn't matter

       String extraIndent=indent+indent+indent+indent+indent;

       lines.add(indent + "next(state) := case");
       for (MealyTransition transition: model.getModelTransitions() ) {
           lines.add(extraIndent + "state = " + transition.getSource() + " & input = " + transition.getInput().getName() + " : " +  transition.getDestination()  + ";" );
       }
       lines.add(extraIndent + "input = null : state;" );
       lines.add(indent + "               esac;");

       lines.add(indent + "next(output) := case");
       for (MealyTransition transition: model.getModelTransitions() ) {
           lines.add(extraIndent + "state = " + transition.getSource() + " & input = " + transition.getInput().getName() + " : " +  transition.getOutput().getName()  + ";" );
       }
       lines.add(extraIndent + "input = null : null;" );
       lines.add(indent + "               esac;");

       lines.add("INVAR !(input = null & output = null)");
       lines.add("    & !(input != null & output != null)");

       return lines;
   }


}
