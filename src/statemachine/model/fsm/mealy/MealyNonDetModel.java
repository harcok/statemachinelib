package statemachine.model.fsm.mealy;



public class MealyNonDetModel extends MealyBaseModel{


    //Builder class
    public static final class ImmutableBuilder extends MealyBaseModel.ImmutableBuilder<ImmutableBuilder> {

        public MealyNonDetModel build(){
    		    super.baseBuild();
            return new MealyNonDetModel(this);
        }
    }

	// Constructor
	private MealyNonDetModel(ImmutableBuilder builder) {
		super(builder);
	}

	// static factory from MealyModel
	static public MealyNonDetModel fromMealyModel(MealyModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromMealyModel(model);
		return builder.build();
	}

	static int counter=0;

	static public void printSimpleStatistics(String filename) {
	    MealyNonDetModel mealyModel=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(filename);
        int num_inputs=mealyModel.getInputAlphabet().size();
        int num_locations=mealyModel.getLocations().size();
        int num_outputs=mealyModel.getOutputAlphabet().size();
        int num_trans=mealyModel.getModelTransitions().size();
        String format= "states:%1$-10.10s  inputs:%2$-10.10s outputs:%3$-10.10s  transitions:%4$-10.10s";
        System.out.println(String.format(format,num_locations,num_inputs,num_outputs,num_trans));
	}

    static public void printStatistics(String filename, String name) {

      //  IaNonDetModel iaModel=statemachine.model.efsm.ia.file.Import.register2IaModel(filename);
        //System.out.println(iaModel.toString());

        MealyNonDetModel mealyModel=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(filename);
      //  System.out.println(mealyModel.toString());
       // Export.dot(mealyModel, "mealyModel.dot");


        int num_inputs=mealyModel.getInputAlphabet().size();
        int num_locations=mealyModel.getLocations().size();
        int num_outputs=mealyModel.getOutputAlphabet().size();
        int num_trans=mealyModel.getModelTransitions().size();

        int result= num_inputs * num_locations ;
        if ( result != num_trans) System.err.println(String.format("problem: model %s :  num_inputs(%d) * num_locations(%d) = %d != num_trans(%d)  ",name,num_inputs, num_locations,result, num_trans));

//        String header=  "       states   inputs/outputs   transitions   ";
//        String format = "          {1}          {2}/{3}             {4}            ";
//        String result= MessageFormat.format(format,"",num_locations,num_inputs,num_outputs,num_trans);
//        System.out.println(name);
//        System.out.println(header);
//        System.out.println(result);

        //pmwiki table
        String formatRow= "||%1$-60.60s || %2$10.10s || %3$10.10s/%4$-10.10s ||  %5$10.10s ||";
        String formatHeader= "||!%1$-60.60s ||! %2$10.10s ||! %3$10.10s/%4$-10.10s ||!  %5$10.10s ||";

        //text//String formatRow= "%1$-60.60s  %2$10.10s  %3$10.10s/%4$-10.10s  %5$10.10s";
        //text// String formatHeader=formatRow;

        //latex// String formatRow= "%1$-60.60s & %2$10.10s & %3$10.10s & / & %4$-10.10s & %5$10.10s \\\\";
        //latex// String formatHeader=formatRow;

        if ( (counter % 1000) == 0 )
            System.out.println(String.format(formatHeader,"model","states","inputs","outputs","transitions"));
        System.out.println(String.format(formatRow,name,num_locations,num_inputs,num_outputs,num_trans));


        counter++;

    }
}
