package statemachine.model.fsm.mealy;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.types.ModelMealy;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;

@NonNullByDefault
public interface MealyModel extends ModelMealy<InputAction, OutputAction, LocationState, MealyTransition>,
        InitialState<LocationState, LocationState> {

}
