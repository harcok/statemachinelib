package statemachine.model.fsm.mealy;

import java.util.StringJoiner;


import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;

@NonNullByDefault
public abstract class MealyBaseModel implements MealyModel {

    protected final ImmutableSet<InputAction> inputalphabet;
    protected final ImmutableSet<OutputAction> outputalphabet;
    private final LocationState startLocation;
    private final ImmutableSet<LocationState> locations;
    private final ImmutableSet<MealyTransition> transitions;
    protected final ImmutableSetMultimap<LocationState, MealyTransition> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder <S extends ImmutableBuilder<?>> {

        protected ImmutableSet<InputAction> inputalphabet;
        protected ImmutableSet<OutputAction> outputalphabet;
        protected LocationState startLocation;
        protected ImmutableSet<LocationState> locations;
        protected ImmutableSet<MealyTransition> transitions;
        protected ImmutableSetMultimap<LocationState, MealyTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<LocationState> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<MealyTransition> transitionsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<InputAction> inputalphabetBuilder = ImmutableSet.builder();
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<OutputAction> outputalphabetBuilder = ImmutableSet.builder();

        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<LocationState, MealyTransition> loc2transBuilder = ImmutableSetMultimap.builder();


        public S addTransition(LocationState src, LocationState dst, InputAction input, OutputAction output) {
            this.locationsBuilder.add(src, dst);
            this.inputalphabetBuilder.add(input);
            this.outputalphabetBuilder.add(output);
            MealyTransition transition = new MealyTransition(src, dst, input, output);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            @SuppressWarnings({ "unchecked" }) S subclass =(S)this; return subclass;
        }

        public S setStartLocation(LocationState startLocation) {
            this.startLocation = startLocation;
            @SuppressWarnings({ "unchecked" }) S subclass =(S)this; return subclass;
        }

        protected S addFromMealyModel(MealyModel model) {
            for (MealyTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination(), trans.getInput(), trans.getOutput());
            }
            this.setStartLocation(model.getStartLocation());
            @SuppressWarnings({ "unchecked" }) S subclass =(S)this; return subclass;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            this.inputalphabet = inputalphabetBuilder.build();
            this.outputalphabet = outputalphabetBuilder.build();
            //this.startLocation = ...; // no builder for startlocation
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }

    }

    // Constructor

    protected MealyBaseModel(ImmutableBuilder<?> builder) {
        this.inputalphabet = builder.inputalphabet;
        this.outputalphabet = builder.outputalphabet;
        this.startLocation = builder.startLocation;
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }

    // interface InitialState

    @Override
    public @NonNull LocationState getStartLocation() {
        return startLocation;
    }

    @Override
    public @NonNull LocationState getStartState() {
        return startLocation;
    }

    // interface ModelAutomaton

    @Override
    public ImmutableSet<LocationState> getLocations() {
        return locations;
    }

    @Override
    public ImmutableSet<MealyTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public ImmutableSet<MealyTransition> getModelTransitions(LocationState location) {
        return loc2trans.get(location);
    }

    // interface ModelMealy

    @Override
    public ImmutableSet<InputAction> getInputAlphabet() {
        return inputalphabet;
    }

    @Override
    public ImmutableSet<OutputAction> getOutputAlphabet() {
        return outputalphabet;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull MealyTransition> getModelTransitions(LocationState location, InputAction action) {
        ImmutableSet.Builder<MealyTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull MealyTransition> transitions = loc2trans.get(location);

        for (MealyTransition transition : transitions) {
            if (transition.getInput().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }



    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("start: " + startLocation);
        joiner.add("locations:");
        for (LocationState location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (MealyTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }

}
