package statemachine.model.fsm.lts;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.model.fsm.lts.LtsEnabledDetModel.ImmutableBuilder;

@NonNullByDefault
public final class LtsNonDetModel extends LtsBaseModel {

    // Constructor
    private LtsNonDetModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from LtsModel
    static public LtsNonDetModel fromLtsModel(LtsModel model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromLtsModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends LtsBaseModel.ImmutableBuilder {

        public LtsNonDetModel build() {
            super.baseBuild();
            return new LtsNonDetModel(this);
        }
    }
}
