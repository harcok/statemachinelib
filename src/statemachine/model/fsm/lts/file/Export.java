package statemachine.model.fsm.lts.file;

import java.util.List;

import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.format.Aldebaran;
import statemachine.model.fsm.lts.format.Dot;
import statemachine.model.fsm.lts.format.GML;
import statemachine.model.fsm.lts.format.NuSMV;
import statemachine.util.IO;

public class Export {

	static public void  dot(LtsModel model, String filename){
			List<String> lines=Dot.export(model);
			IO.writeSmallTextFile( lines, filename);
	   }

	static public void  gml(LtsModel model, String filename){

			List<String> lines=GML.export(model);
			IO.writeSmallTextFile( lines, filename);
	   }

	static public void  aldebaran(LtsModel model, String filename){

			List<String> lines=Aldebaran.export(model);
			IO.writeSmallTextFile( lines, filename);
	}

	static public void  nusmv(LtsModel model, String filename){

           List<String> lines=NuSMV.export(model);
           IO.writeSmallTextFile( lines, filename);
    }

}
