package statemachine.model.fsm.lts.file;

import java.util.List;

import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.model.fsm.lts.format.Aldebaran;
import statemachine.model.fsm.lts.format.Dot;
import statemachine.model.fsm.lts.format.GML;
import statemachine.util.IO;

public class Import {

	static public LtsNonDetModel aldebaran2LtsModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			LtsNonDetModel model= Aldebaran.importLtsNoneDetModel(lines); 
			return model;
	   }

	static public LtsNonDetModel  gml2LtsModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			LtsNonDetModel model= GML.importLtsNoneDetModel(lines); 
			return model;
	   }

	static public LtsNonDetModel  dot2LtsModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			LtsNonDetModel model= Dot.importLtsNoneDetModel(lines); 
			return model;
	   }

}
