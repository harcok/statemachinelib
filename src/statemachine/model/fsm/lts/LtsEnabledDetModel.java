package statemachine.model.fsm.lts;


import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.ActionEnabledDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;


@NonNullByDefault
public final class LtsEnabledDetModel extends LtsBaseModel implements ActionEnabledDeterministic<LocationState,Action,ActionTransition> {


    // Builder class
    public final static class ImmutableBuilder extends LtsBaseModel.ImmutableBuilder {
    	
        public LtsEnabledDetModel build(){
    		super.baseBuild();
            return new LtsEnabledDetModel(this);
        }
    }
	
    // Constructor
	private LtsEnabledDetModel(ImmutableBuilder builder) {
		super(builder);
		
		// check for none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2trans.keySet()) {
			for ( Action action : this.alphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}
	}
	
	// static factory from LTSModel
	static public LtsEnabledDetModel fromLtsModel(LtsModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromLtsModel(model);
		return builder.build();
	}
	
	// interface ActionEnabledDeterministic
	public ActionTransition getTriggeredTransition(LocationState location,Action action) {
		ImmutableSet<@NonNull ActionTransition> transitions = this.getModelTransitions(location, action);
		return Util.getTheTransition(transitions,location.getName(),action.getName());	
	}

}
