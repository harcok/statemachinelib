package statemachine.model.fsm.lts;

import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;

/**
 * label transition system  : automata with label on transition
 *
 * @author harcok
 *
 */

@NonNullByDefault
public abstract class LtsBaseModel implements LtsModel {

    protected final ImmutableSet<Action> alphabet;
    private final LocationState startLocation;
    private final ImmutableSet<LocationState> locations;
    private final ImmutableSet<ActionTransition> transitions;
    protected final ImmutableSetMultimap<LocationState, ActionTransition> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder {

        protected LocationState startLocation;
        protected ImmutableSet<Action> alphabet;
        protected ImmutableSet<LocationState> locations;
        protected ImmutableSet<ActionTransition> transitions;
        protected ImmutableSetMultimap<LocationState, ActionTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<LocationState> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<ActionTransition> transitionsBuilder = ImmutableSet.builder();
        //private StateLocationImpl startLocation;

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<Action> alphabetBuilder = ImmutableSet.builder();

        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<LocationState, ActionTransition> loc2transBuilder = ImmutableSetMultimap.builder();

        public ImmutableBuilder addTransition(LocationState src, LocationState dst, Action action) {
            this.locationsBuilder.add(src, dst);
            this.alphabetBuilder.add(action);
            ActionTransition transition = new ActionTransition(src, dst, action);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }

        public ImmutableBuilder setStartLocation(LocationState startLocation) {
            this.startLocation = startLocation;
            return this;
        }

        protected ImmutableBuilder addFromLtsModel(LtsModel model) {
            for (ActionTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination(), trans.getAction());
            }
            this.setStartLocation(model.getStartLocation());
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            this.alphabet = alphabetBuilder.build();
            //this.startLocation = ...; // no builder for startlocation
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }
    }

    // Constructor

    protected LtsBaseModel(ImmutableBuilder builder) {
        this.alphabet = builder.alphabet;
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.startLocation = builder.startLocation;
        this.loc2trans = builder.loc2trans;
    }

    // interface InitialState

    @Override
    public @NonNull LocationState getStartLocation() {
        return startLocation;
    }

    @Override
    public @NonNull LocationState getStartState() {
        return startLocation;
    }

    // interface ModelAutomaton

    @Override
    public @NonNull ImmutableSet<@NonNull LocationState> getLocations() {
        return locations;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull ActionTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull ActionTransition> getModelTransitions(@NonNull LocationState location) {
        return loc2trans.get(location);
    }

    // interface ModelLTS

    @Override
    public ImmutableSet<Action> getAlphabet() {
        return alphabet;
    }

    @Override
    public @NonNull
            ImmutableSet<@NonNull ActionTransition> getModelTransitions(@NonNull LocationState location, @NonNull Action action) {
        ImmutableSet.Builder<ActionTransition> transitionsBuilder = ImmutableSet.builder();
        @NonNull
        ImmutableSet<@NonNull ActionTransition> transitions = loc2trans.get(location);

        for (ActionTransition transition : transitions) {
            if (transition.getAction().equals(action)) {
                transitionsBuilder.add(transition);
            }
        }
        return transitionsBuilder.build();
    }

    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("start: " + startLocation);
        joiner.add("locations:");
        for (LocationState location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (ActionTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }


}
