package statemachine.model.fsm.lts.format;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.common.base.CharMatcher;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;


/*

Aldebaran format
=================

The Aldebaran file format is a simple format for storing labelled transition systems (LTS’s) explicitly.


Syntax

    The syntax of an Aldebaran file consists of a number of lines, where the first line is aut_header and the
    remaining lines are aut_edge.

    aut_header is defined as follows:

        aut_header        ::=  'des (' first_state ',' nr_of_transitions ',' nr_of_states ')'
        first_state       ::=  number
        nr_of_transitions ::=  number
        nr_of_states      ::=  number

    Here:

        first_state is a number representing the first state, which should always be 0
        nr_of_transitions is a number representing the number of transitions
        nr_of_states is a number representing the number of states

    An aut_edge is defined as follows:

        aut_edge    ::=  '(' start_state ',' label ',' end_state ')'
        start_state ::=  number
        label       ::=  '"' string '"'
        end_state   ::=  number

    Here:

        start_state is a number representing the start state of the edge;
        label is a string enclosed in double quotes representing the label of the edge;
        end_state is a number representing the end state of the edge.

Example

    The following example shows a simple labelled transition system of the dining philosophers problem for two philosophers, visualised using ltsgraph:


      see: http://www.mcrl2.org/web/user_manual/_images/Dining2_ns_seq.png    or redraw with ltsgraph

    This transition system is represented by the following Aldebaran file:  => in diningPhilosophers.aut

        des (0,12,10)
        (0,"lock(p2, f2)",1)
        (0,"lock(p1, f1)",2)
        (1,"lock(p1, f1)",3)
        (1,"lock(p2, f1)",4)
        (2,"lock(p2, f2)",3)
        (2,"lock(p1, f2)",5)
        (4,"eat(p2)",6)
        (5,"eat(p1)",7)
        (6,"free(p2, f2)",8)
        (7,"free(p1, f1)",9)
        (8,"free(p2, f1)",0)
        (9,"free(p1, f2)",0)




Acknowledgements

  The Aldebaran format is originally used in the CADP toolset. http://www.inrialpes.fr/vasy/cadp/
  To be fully compatible with the original syntax definition, the labels of the edges should consist of at most 5000 characters.


 */
public class Aldebaran {


	static public LtsEnabledDetModel importLtsEnabledDetModel( List<String> lines) {
         return LtsEnabledDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}

	static public LtsDetModel importLtsDetModel( List<String> lines) {
        return LtsDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}


	static public LtsNonDetModel importLtsNoneDetModel( List<String> lines) {

		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		String start;
		for(String line: lines){

			if ( line.contains("des") ) {
				int e1 = line.indexOf("(");
				int e2 = line.indexOf(",");
			    start = line.substring(e1+1, e2).trim();
				LocationState startLocation = new LocationState(start);
				builder.setStartLocation(startLocation);
			} else if (line.contains(",") ) {
				line = line.replace("(", "");
				line = line.replace(")", "");
				String[] pieces=line.split(",");

				String from = pieces[0].trim();
				String to = pieces[2].trim();


				String label = pieces[1].trim();

                CharMatcher matcher = CharMatcher.is('"');
                label=matcher.trimFrom(label);

				LocationState src = new LocationState(from);
				LocationState dst = new LocationState(to);
				Action action =  new Action(label);
				builder.addTransition(src, dst, action);
			}

		}
		return builder.build();

	}

	static public List<String> export(LtsModel model) {
		LinkedList<String> lines= new LinkedList<>();

		//'des (' first_state ',' nr_of_transitions ',' nr_of_states ')'

		Set<ActionTransition> transitions=model.getModelTransitions();
		Set<LocationState> locations=model.getLocations();
		LocationState startLocation = model.getStartLocation();

		HashMap<LocationState,Integer> loc2int = new HashMap<>();
		loc2int.put(startLocation, 0);
		int count=1;
		for (LocationState location : locations ) {
			if ( location.equals(startLocation)) continue;
			loc2int.put(location, count);
			count++;
		}

		lines.add("des (0,"+transitions.size()+","+ locations.size() + ")");
		for (ActionTransition transition: model.getModelTransitions() ) {
			/*
		     An aut_edge is defined as follows:

		        aut_edge    ::=  '(' start_state ',' label ',' end_state ')'
		        start_state ::=  number
		        label       ::=  '"' string '"'
		        end_state   ::=  number
			 */
			lines.add("(" + loc2int.get(transition.getSource())
			              + ",\""+ transition.getAction().getName() + "\","
			              +  loc2int.get(transition.getDestination()) + ")");
		}

		return lines;
	}


}
