package statemachine.model.fsm.lts.format;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

/*
 
          gml - a hierarchical ASCII-based file format for describing graphs

            http://en.wikipedia.org/wiki/Graph_Modelling_Language

 

                    graph [
                            comment "This is a sample graph"
                            directed 1
                            id n42
                            label "Hello, I am a graph"
                            node [
                                    id 1
                                    label "node 1"
                                    thisIsASampleAttribute 42
                            ]
                            node [
                                    id 2
                                    label "node 2"
                                    thisIsASampleAttribute 43
                            ]
                            node [
                                    id 3
                                    label "node 3"
                                    thisIsASampleAttribute 44
                            ]
                            edge [
                                    source 1
                                    target 2
                                    label "Edge from node 1 to node 2"
                            ]
                            edge [
                                    source 2
                                    target 3
                                    label "Edge from node 2 to node 3"
                            ]
                            edge [
                                    source 3
                                    target 1
                                    label "Edge from node 3 to node 1"
                            ]
                    ]

               => LTS

            http://networkx.lanl.gov/reference/readwrite.gml.html

                The idea for a common file format was born at the GD‘95; this proposal is the
                outcome of many discussions. GML is the standard file format in the Graphlet
                graph editor system. It has been overtaken and adapted by several other
                systems for drawing graphs.

            specification at :
              http://www.fim.uni-passau.de/fileadmin/files/lehrstuhl/brandenburg/projekte/gml/gml-technical-report.pdf 
 
 */
public class GML {


	
	static public LtsEnabledDetModel importLtsEnabledDetModel( List<String> lines) {
         return LtsEnabledDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}
	
	static public LtsDetModel importLtsDetModel( List<String> lines) {
        return LtsDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}	

	
	static public LtsNonDetModel importLtsNoneDetModel( List<String> lines) {
		
		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		for(String line: lines){
			//TODO  gml import 
		}
		return builder.build();
		
	}
	
	static public List<String> export(LtsModel model) {

		String indent1 = "    ";
		String indent2 = indent1+indent1;
		//String indent3 = indent2+indent1;
		
		
		
		LinkedList<String> lines= new LinkedList<>();
		LocationState startLocation = model.getStartLocation();
		//Set<ActionTransition> transitions=model.getTransitions();
		Set<LocationState> locations=model.getLocations();
		
		HashMap<LocationState,Integer> loc2int = new HashMap<>();
		loc2int.put(startLocation, 0);
		int count=1;
		for (LocationState location : locations ) {
			if ( location.equals(startLocation)) continue;
			loc2int.put(location, count);
			count++;
		}
		
		lines.add( "graph [");
		//lines.add(indent1 +"comment \"This is a sample graph\"");
		lines.add(indent1 +"directed 1");
		//lines.add(indent1 +"comment \"Hello, I am a graph\"");
		lines.add(indent1 +"id 0" ); // to designate start state => probably not official way to do it
		
		
		for (LocationState location : model.getLocations() ) {
			
			lines.add(indent1 +"node [");
			lines.add(indent2 +"id " + loc2int.get(location) );
			lines.add(indent2 +"label \"" + location + "\"");
			lines.add(indent1 +"]");
		}
		
		for (ActionTransition transition: model.getModelTransitions() ) {
			lines.add(indent1 +"edge [");
			lines.add(indent2 +"source " + loc2int.get(transition.getSource()) );
			lines.add(indent2 +"target " + loc2int.get(transition.getDestination()));
			lines.add(indent2 +"label \"" + transition.getAction().getName() + "\"");
			lines.add(indent1 +"]");
		}
		lines.add("]");


		return lines;
	}
	

}
