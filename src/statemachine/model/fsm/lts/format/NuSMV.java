package statemachine.model.fsm.lts.format;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.mealy.MealyModel;

public class NuSMV {

    public static List<String> export(LtsModel model) {
        String indent="    ";
        LinkedList<String> lines= new LinkedList<>();
        lines.add("MODULE main");
        lines.add("VAR");
        String commasep_states=model.getLocations().stream().map(v->v.getName()).collect(Collectors.joining(", "));
        lines.add(indent + "state : { " + commasep_states +  " }");
        String commasep_labels=model.getAlphabet().stream().map(v->v.getName()).collect(Collectors.joining(", "));
        lines.add(indent + "newact : { "+ commasep_labels +" }");
        lines.add(indent + "oldact : { "+ commasep_labels +" }");
        lines.add("ASSIGN");
        lines.add(indent + "init(state) := " + model.getStartLocation() + ";");
        lines.add(indent + "next(state) := case");
        String extraIndent=indent+indent+indent+indent+indent;
        for (ActionTransition transition: model.getModelTransitions() ) {
            lines.add(extraIndent + "state = " + transition.getSource() + " & newact = " + transition.getAction().getName() + " : " +  transition.getDestination()  + ";" );
        }
        lines.add(indent + "               esac");
        lines.add(indent + "oldact := newact;");
        return lines;
    }

}
