package statemachine.model.fsm.lts.format;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class GraphML {

	
	static public LtsEnabledDetModel importLtsEnabledDetModel( List<String> lines) {
         return LtsEnabledDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}
	
	static public LtsDetModel importLtsDetModel( List<String> lines) {
        return LtsDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}	

	
	static public LtsNonDetModel importLtsNoneDetModel( List<String> lines) {
		
		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		for(String line: lines){


			
			//if(!line.contains("label")) continue;

			//TODO importLtsNoneDetModel

		}
		return builder.build();
		
	}
	
	static public List<String> export(LtsModel model) {

		String indent1 = "    ";
		String indent2 = indent1+indent1;
		String indent3 = indent2+indent1;
		
		
		MessageFormat fmtNode = new MessageFormat(indent2 + "<node id=\"{0}\"/>");		
		MessageFormat fmtEdge = new MessageFormat(indent2 + "<edge  source=\"{0}\" target=\"{1}\">");
		MessageFormat fmtLabel = new MessageFormat(indent3 + "<data key=\"label\">{0}</data>");
		
		LinkedList<String> lines= new LinkedList<>();
		LocationState startLocation = model.getStartLocation();
		lines.add( "<graphml>");
		lines.add( indent1 + "<key id=\"label\"/>");
		lines.add( indent1 + "<graph id=\"G\" edgedefault=\"directed\">");

		
		for (LocationState location : model.getLocations() ) {
			//if ( location.equals(startLocation)) continue;
			lines.add( fmtNode.format(new Object[] {location.toString()}));
		}
		
		String closeEdge = indent2 + "</edge>";
		for (ActionTransition transition: model.getModelTransitions() ) {
			/*
		      <edge id="e1" directed="true" source="n0" target="n2"/>
			 */
			lines.add( fmtEdge.format(new Object[] {transition.getSource(),transition.getDestination()}) );
			lines.add( fmtLabel.format(new Object[] {transition.getAction().getName()}));
			lines.add( closeEdge);
		}
		
		lines.add( indent1 + "</graph>"); 
		lines.add( "</graphml>");

		return lines;
	}
	

}
