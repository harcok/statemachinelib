package statemachine.model.fsm.lts.format;


import java.util.LinkedList;
import java.util.List;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;
import statemachine.model.fsm.lts.LtsEnabledDetModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;


/*

digraph g {

    __start0 [label="" shape="none"]

    1 [shape="circle" label="1"];
    2 [shape="circle" label="2"];
    1 -> 2 [label="a"];
    2 -> 1 [label="a"];
    2 -> 1 [label="b"];
    1 -> 1 [label="a"];

    __start0 -> 1;
}


 */


public class Dot {


	static public LtsEnabledDetModel importLtsEnabledDetModel( List<String> lines) {
         return LtsEnabledDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}

	static public LtsDetModel importLtsDetModel( List<String> lines) {
        return LtsDetModel.fromLtsModel(importLtsNoneDetModel(lines));
	}


	static public LtsNonDetModel importLtsNoneDetModel( List<String> lines) {

		LtsNonDetModel.ImmutableBuilder builder =new LtsNonDetModel.ImmutableBuilder();
		for(String line: lines){

			// s0 [color="red"]       => s0 start state
			if(line.contains("color=\"red\"")) {
				int e1 = line.indexOf('[');
				String start = line.substring(0, e1).trim();
				LocationState startLocation = new LocationState(start);
				builder.setStartLocation(startLocation);
			}

			// __start0 -> 1;         => 1 start state
			if(line.contains("__start0") && !line.contains("label")) {
				int e1 = line.indexOf('-');
				int e2 = line.indexOf(';');
				String start = line.substring(e1+2, e2).trim();
				LocationState startLocation = new LocationState(start);
				builder.setStartLocation(startLocation);
			}

			if(!line.contains("label")) continue;

			if(line.contains("->")){

			    String label;

                if (line.contains("<")) {
                    int beginIndex=line.indexOf("<");
                    int lastIndex=line.lastIndexOf(">");
                    label=line.substring(beginIndex,lastIndex);
                    label=label.replaceAll("\\<[^>]*>","").trim();
                  } else {
                      int startLabel = line.indexOf('"');
                      int endLabel = line.lastIndexOf('"');
                      label = line.substring(startLabel+1, endLabel).trim();
                  }


                int e1 = line.indexOf('-');
                int e2 = line.indexOf('[');
				String from = line.substring(0, e1).trim();
				String to = line.substring(e1+2, e2).trim();


				LocationState src = new LocationState(from);
				LocationState dst = new LocationState(to);
				Action action =  new Action(label);
				builder.addTransition(src, dst, action);
			}
			/*
			else {
				int end = line.indexOf('[');
				if(end <= 0) continue;
				String node = line.substring(0, end).trim();

				nodes.add(node);
			}
			*/
		}
		return builder.build();

	}

	static public List<String> export(LtsModel model) {
		String indent="    ";
		LinkedList<String> lines= new LinkedList<>();
		lines.add("digraph g {");
		lines.add("");
		lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
		lines.add("");


		for ( LocationState location: model.getLocations()) {
	  	  lines.add(indent + "\"" + location + "\" [shape=\"circle\" label=\"" + location + "\"];");
		}
		for (ActionTransition transition: model.getModelTransitions() ) {
			lines.add(indent + "\"" + transition.getSource() + "\" -> \"" + transition.getDestination() + "\" [label=\"" + transition.getAction().getName() + "\"];");
		}
	  	lines.add("");
		lines.add(indent + "__start0 -> \"" + model.getStartLocation() + "\";");
		lines.add("}");
		return lines;
	}
}
