package statemachine.model.fsm.lts;

import statemachine.interfaces.model.types.ModelLabelTransitionSystem;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;

/**
 * LtsModel is a labeled transition system  with an initial state
 *
 */
public interface LtsModel
       extends ModelLabelTransitionSystem<Action,LocationState,ActionTransition>,
               InitialState<LocationState,LocationState>
{

}
