package statemachine.model.fsm.lts.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

import com.google.common.collect.ImmutableSet;

import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeNode;
import nl.ru.cs.tree.format.EdgeNode;
import nl.ru.cs.tree.format.EdgeNodeImpl;
import statemachine.interfaces.system.properties.ActionDeterministic;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;

public class Util {

    static public Tree<EdgeNode<Action, LocationState>> getSpanningTreeLts(LtsDetModel detlts) {
        LocationState startLocation = detlts.getStartLocation();
        ImmutableSet<Action> actions = detlts.getAlphabet();
        return getSpanningTreeLtsBase(detlts, startLocation, actions);

    }

    static public Tree<EdgeNode<Action,LocationState>> getSpanningTreeLtsBase(
            ActionDeterministic<LocationState, Action, ActionTransition> detlts, LocationState startLocation,
            ImmutableSet<Action> actions) {

        HashSet<LocationState> locationsSeen = new HashSet<LocationState>();
        Queue<LocationState> locationsTodo = new LinkedList<LocationState>();
        Map<LocationState, TreeNode<EdgeNode<Action,LocationState>>> location2node = new HashMap<>();

        TreeNode<EdgeNode<Action,LocationState>> root = new TreeNode<>(new EdgeNodeImpl<>(null,startLocation));

        location2node.put(startLocation, root);

        locationsSeen.add(startLocation);
        locationsTodo.add(startLocation);

        while (!locationsTodo.isEmpty()) {
            LocationState location = locationsTodo.remove();

            TreeNode<EdgeNode<Action,LocationState>> parentnode = location2node.get(location);
            for (Action action : actions) {
                Optional<ActionTransition> optTrans = detlts.getTriggeredTransition(location, action);
                if (optTrans.isPresent()) {
                    LocationState dest = optTrans.get().getDestination();
                    if (!locationsSeen.contains(dest)) {
                        // we haven't seen this location before
                        // but we know have so mark it seen
                        locationsSeen.add(dest);
                        // add tree node going to destination
                        TreeNode<EdgeNode<Action,LocationState>> childnode =  new TreeNode<>(new EdgeNodeImpl<>(action,dest));
                        parentnode.addChild(childnode);
                        location2node.put(dest, childnode);
                        // process location's outgoing transitions later
                        locationsTodo.add(dest);
                    }
                }
            }
        }

        Tree<EdgeNode<Action,LocationState>> tree = new Tree<>(root);
        return tree;

    }



}
