package statemachine.model.fsm.lts.walker;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.StepAction;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsEnabledDetModel;

@NonNullByDefault
public class LtsEnabledDetWalker extends LtsBaseWalker<LtsEnabledDetModel>
        implements
            StepAction<LocationState, Action> {

    public LtsEnabledDetWalker(LtsEnabledDetModel model) {
        super(model);
    }

    /**
     * if more then one path possible throws error
     *
     * @param action
     */
    @Override
    public void stepAction(Action action) {
        @NonNull
        ActionTransition transition = model.getTriggeredTransition(currentLocation, action);
        currentLocation = transition.getDestination();
    }

    @Override
    public boolean checkActionPossible(Action action) {
        return true; // because is actionEnabled!
    }
}
