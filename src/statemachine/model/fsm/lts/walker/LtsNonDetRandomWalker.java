package statemachine.model.fsm.lts.walker;

import java.util.List;
import java.util.Random;

import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.walker.StepAction;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsNonDetModel;

@NonNullByDefault
public class LtsNonDetRandomWalker extends LtsBaseWalker<LtsNonDetModel> implements StepAction<LocationState,Action> {

	private Random random;
	
	public LtsNonDetRandomWalker(LtsNonDetModel model, Random random ) {
		super(model);
		this.random = random;
	}
	
	// interface StepAction

	@Override
	public boolean checkActionPossible(Action action) {
   	     ImmutableSet<ActionTransition> possibleTransitions=model.getModelTransitions(currentLocation,action);
   	     return possibleTransitions.size() > 0;
	}

	
	 /**
	  *  if more then one path possible randomly chooses one
	  * @param action
	  */
	 @Override
    public void stepAction(Action action){
   	 ImmutableSet<ActionTransition> possibleTransitions=model.getModelTransitions(currentLocation,action);
   	 int numTrans=possibleTransitions.size();
   	 if ( numTrans > 1 ) {
   		 List<ActionTransition> list =possibleTransitions.asList();
   		int index=random.nextInt(numTrans);
   		currentLocation=list.get(index).getDestination();
   	 }
   	 if (numTrans == 1 ) {
   		 ActionTransition t=possibleTransitions.iterator().next();
   		 currentLocation = t.getDestination();
   	 } 
   	 // if ( numTrans == 0 )     DO NOTHING
    }

}
