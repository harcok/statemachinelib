package statemachine.model.fsm.lts.walker;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.system.walker.StepAction;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.fsm.lts.LtsDetModel;

@NonNullByDefault
public class LtsDetWalker extends LtsBaseWalker<LtsDetModel> implements StepAction<LocationState, Action> {

	public LtsDetWalker(LtsDetModel model) {
		super(model);
	}

	// interface StepAction

	@Override
	public boolean checkActionPossible(Action action) {
		Optional<ActionTransition> optTransition = model.getTriggeredTransition(currentLocation, action);

		return optTransition.isPresent();
	}

	/**
	 * if more then one path possible throws error
	 * 
	 * @param action
	 */
	@Override
	public void stepAction(Action action) {
		Optional<ActionTransition> optTransition = model.getTriggeredTransition(currentLocation, action);
		if (optTransition.isPresent()) {
			currentLocation = optTransition.get().getDestination();
		}
		// else do nothing
	}



}
