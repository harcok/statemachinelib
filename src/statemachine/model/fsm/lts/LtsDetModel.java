package statemachine.model.fsm.lts;

import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.NonNullByDefault;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.ActionDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;


@NonNullByDefault
public final class LtsDetModel extends LtsBaseModel implements ActionDeterministic<LocationState,Action,ActionTransition> {

    // Builder class
    public static final class ImmutableBuilder extends LtsBaseModel.ImmutableBuilder {
    	
        public LtsDetModel build(){
    		super.baseBuild();
            return new LtsDetModel(this);
        }
    }
    
    // Constructor 
	private LtsDetModel(ImmutableBuilder builder) {
		super(builder);
		
		// check for none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2trans.keySet()) {
			for ( Action action : this.alphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}
	}
	
	// static factory from LTSModel
	static public LtsDetModel fromLtsModel(LtsModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromLtsModel(model);
		return builder.build();
	}
	
	// interface ActionDeterministic
	@Override
	public Optional<ActionTransition> getTriggeredTransition(LocationState location,Action action) {
		ImmutableSet<@NonNull ActionTransition> transitions = this.getModelTransitions(location, action);
		return Util.getOptionalTransition(transitions);	
	}



}