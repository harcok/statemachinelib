package statemachine.model.fsm.mealymoore;

import statemachine.interfaces.model.types.ModelMealyMoore;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;

public interface MealyMooreModel
        extends ModelMealyMoore<InputAction, OutputAction, OutputAction, LocationState, MealyTransition>,
        InitialState<LocationState, LocationState> {
}