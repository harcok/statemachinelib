package statemachine.model.fsm.fa;


import java.util.Optional;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;

import statemachine.interfaces.system.properties.InputDeterministic;
import statemachine.model.Util;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;

/**
 *  DFA  Deterministic Finite Acceptor
 *       
 * @author harcok
 *
 */
public class DfaModel extends  FaBaseModel implements InputDeterministic<LocationState,InputAction,InputTransition> {

    //Builder class
    public static final class ImmutableBuilder extends FaBaseModel.ImmutableBuilder {
    	
        public DfaModel build(){
    		// do build into final fields
    		super.baseBuild();
    		return new DfaModel(this);
        }
    }
    
	// Constructor
	private DfaModel(ImmutableBuilder builder) {
		super(builder);
		
		// check for none-determinism ; if found it throws exception
		for (LocationState location:  this.loc2trans.keySet()) {
			for ( InputAction action : this.inputalphabet ) {
				getTriggeredTransition(location,action);  // throws exception when none-determinism is detected!
			}
		}
	}
	
	// static factory from DFAModel
	static public DfaModel fromFaModel(FaModel model) {
		ImmutableBuilder builder =  new ImmutableBuilder();
		builder.addFromFaModel(model);
		return builder.build();
	}
	
    
	// interface InputDeterministic
	
    @Override
	public Optional<InputTransition> getTriggeredTransition(LocationState location,InputAction action) {
		ImmutableSet<@NonNull InputTransition> transitions = this.getModelTransitions(location, action);
		return Util.getOptionalTransition(transitions);	
	}

}