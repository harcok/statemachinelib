package statemachine.model.fsm.fa;

import statemachine.interfaces.model.types.ModelFiniteAcceptor;
import statemachine.interfaces.system.properties.InitialState;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;

public interface FaModel extends ModelFiniteAcceptor<InputAction, LocationState, InputTransition>,
        InitialState<LocationState, LocationState> {

}
