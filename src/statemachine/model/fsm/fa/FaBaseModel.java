package statemachine.model.fsm.fa;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.moore.MooreBaseModel;
import statemachine.model.fsm.moore.MooreModel;

/**
 *   FA  is implemented as a Moore model with fixed output alphabet containing
 *       with only an ACCEPT and a REJECT output
 *       where by default a state is REJECT and
 *       we can make it ACCCEPT with the setAccepting builder method
 *       If a state is ACCEPT or REJECT can be inspected
 *       with the isAccepting(location) method.
 *
 *       Because FA is implemented on top of a Moore model
 *       we can still fetch ACCEPT and REJECT with getOutput from
 *       a location. However we don't allow to set the output
 *       within the builder with a setOutput method, because
 *       that only confuses the user. It must be clear that
 *       the user can only setAccepting or not.
 *
 * @author harcok
 *
 */
public abstract class FaBaseModel extends MooreBaseModel implements FaModel, MooreModel {
    // note: MooreBaseModel already implements MooreModel
    public static OutputAction ACCEPT = new OutputAction("ACCEPT");
    public static OutputAction REJECT = new OutputAction("REJECT");

    //Builder class
    public static class ImmutableBuilder extends MooreBaseModel.ImmutableBuilder {

        public void setAccepting(LocationState location) {
            loc2outputBuilder.put(location, ACCEPT);
        }

        public ImmutableBuilder addFromFaModel(FaModel model) {

            for (InputTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination(), trans.getInput());
            }
            for (LocationState location : model.getLocations()) {
                OutputAction output;// = model.getOutputAt(location);
                if (model.isAccepting(location)) {
                    output = ACCEPT;
                } else {
                    output = REJECT;
                }
                this.addOutput(location, output);
            }
            this.setStartLocation(model.getStartLocation());
            return this;
        }

        protected void baseBuild() {
            // set fixed output alphabet of a DFA
            outputalphabetBuilder.add(REJECT);
            outputalphabetBuilder.add(ACCEPT);

            // get so far set locations and corresponding outputs set
            ImmutableSet<LocationState> local_locations = locationsBuilder.build();
            ImmutableMap<LocationState, OutputAction> local_loc2output = loc2outputBuilder.build();

            // for locations where no accepting output is set, we set REJECT output
            for (LocationState location : local_locations) {
                if (!local_loc2output.containsKey(location)) {
                    this.addOutput(location, REJECT);
                }
            }
            // do build into final fields
            super.baseBuild();
        }
    }

    // Constructor
    protected FaBaseModel(ImmutableBuilder builder) {
        super(builder);
    }

    @Override
    public boolean isAccepting(LocationState location) {
        return loc2output.get(location) == ACCEPT;
    }

}
