package statemachine.model.fsm.fa.conversion;

import statemachine.model.elements.location.LocationState;


@FunctionalInterface
public interface LocationLabelFormatter {
	public String apply (LocationState location,Boolean active);
}

