package statemachine.model.fsm.fa.conversion;

import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.DfaModel;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.moore.MooreDetModel;
import statemachine.model.fsm.moore.MooreNonDetModel;

public class Conversion {

    static public MooreNonDetModel Fa2MooreNonDetModel(FaModel fa) {
        // returns MooreNonDetModel because that is implementation with least restrictions(lowest common  denominator)

        OutputAction accepting =  new OutputAction("1");
        OutputAction rejection =  new OutputAction("0");

        MooreNonDetModel.ImmutableBuilder builder =  new MooreNonDetModel.ImmutableBuilder();
        builder.setStartLocation(fa.getStartLocation());

        for ( LocationState location : fa.getLocations() ) {
            OutputAction output = fa.isAccepting(location)  ? accepting : rejection;
            builder.addOutput(location, output);
        }
        for ( InputTransition transition :fa.getModelTransitions()) {
            builder.addTransition(transition.getSource(), transition.getDestination(), transition.getInput());
        }
        return builder.build();
    }

    static public MooreDetModel Fa2MooreDetModel(DfaModel dfa) {
          MooreNonDetModel model=Fa2MooreNonDetModel(dfa);
          return MooreDetModel.fromMooreModel(model);

    }


}
