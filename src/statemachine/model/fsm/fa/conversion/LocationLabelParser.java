package statemachine.model.fsm.fa.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;

@FunctionalInterface
public interface LocationLabelParser {
	public Pair<LocationState,Boolean> apply (String label);
}
