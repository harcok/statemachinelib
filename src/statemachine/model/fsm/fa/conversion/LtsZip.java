package statemachine.model.fsm.fa.conversion;

import org.javatuples.Pair;

import statemachine.model.elements.action.Action;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.ActionTransition;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.model.fsm.lts.LtsNonDetModel;

public class LtsZip {

    static public LocationLabelFormatter defaultLocationFormatter = new LocationLabelFormatter() {
    	 public String apply (LocationState location ,Boolean active) {
    		 String stateOutput = "REJECT";
    		 if (active ) stateOutput = "ACCEPT";
    		 return location.getName() + "|" + stateOutput;
    	 }
    };

	static public LocationLabelParser defaultLabelParser = new LocationLabelParser() {
		public Pair<LocationState,Boolean> apply(String label) {
  			String[] parts;
  			parts = label.split("|");
  			LocationState location = new LocationState(parts[0].trim());
  			Boolean accepting = false;
  			if ( parts[1].trim().equals("ACCEPT")) {
  				accepting = true;
  			}
			return new Pair<LocationState,Boolean>(location, accepting);
		}
	};

	static public LtsNonDetModel zipFa2Lts(FaModel model, LocationLabelFormatter locationFormatter) {

		LtsNonDetModel.ImmutableBuilder builder = new LtsNonDetModel.ImmutableBuilder();

		for (InputTransition trans: model.getModelTransitions() ) {

			//for dfa no formatter for action : just take input name on transition
			// note: don't take transition string because for input transition that is input name prefixed with "?"
			Action action = new Action(trans.getInput().getName());

			LocationState src= trans.getSource();
			LocationState dest= trans.getDestination();

			String formattedSrcLabel = locationFormatter.apply(src, model.isAccepting(src));
			String formattedDestLabel = locationFormatter.apply(dest, model.isAccepting(dest));

			LocationState newsrc= new LocationState( formattedSrcLabel );
			LocationState newdest= new LocationState( formattedDestLabel );
			builder.addTransition(newsrc, newdest, action);
		}
		builder.setStartLocation(model.getStartLocation());
		return builder.build();
	}


	static public LtsNonDetModel zipFa2Lts(FaModel model) {
		return zipFa2Lts(model,defaultLocationFormatter);
	  }

	static  public NfaModel unzipLts2Fa(LtsModel model, LocationLabelParser labelParser) {
		    NfaModel.ImmutableBuilder builder = new NfaModel.ImmutableBuilder();

		    // convert transitions to input transitions in DFA, and
		    // fetch ACCEPT and REJECT from location names to set locations accepting or rejecting
	  		for (ActionTransition trans: model.getModelTransitions() ) {
	  			String label;
	  			Pair<LocationState,Boolean> location_accepting;
	  			Boolean accepting;

	  			label=trans.getSource().getName();
	  			location_accepting = labelParser.apply(label);
	  			LocationState newsrc = location_accepting.getValue0();
	  			accepting = location_accepting.getValue1();
	  			if ( accepting) {
	  				builder.setAccepting(newsrc);
	  			}

	  			label=trans.getDestination().getName();
	  			location_accepting = labelParser.apply(label);
	  			LocationState newdest = location_accepting.getValue0();
	  			accepting = location_accepting.getValue1();
	  			if ( accepting) {
	  				builder.setAccepting(newdest);
	  			}

				InputAction input = new InputAction(trans.getAction().getName());
	  			builder.addTransition(newsrc, newdest,input);
	 		}

	  		// set start location
	  		String[] parts=model.getStartLocation().getName().split("|");
	  		LocationState startLocation = new LocationState(parts[0].trim());
	  		builder.setStartLocation(startLocation);

	  		return builder.build();
	}

	static  public NfaModel unzipLts2Fa(LtsModel model) {
		return unzipLts2Fa(model,  defaultLabelParser);
	}

}
