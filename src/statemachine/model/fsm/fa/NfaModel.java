package statemachine.model.fsm.fa;

import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.moore.MooreModel;

/**
 *  NFA  Non-Deterministic Finite Acceptor
 *
 * @author harcok
 *
 */
public class NfaModel extends FaBaseModel implements FaModel, MooreModel {

    //Builder class
    public static final class ImmutableBuilder extends FaBaseModel.ImmutableBuilder {

        public NfaModel build() {
            // do build into final fields
            super.baseBuild();
            return new NfaModel(this);
        }
    }

    // Constructor
    private NfaModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DFAModel
    static public NfaModel fromFaModel(FaModel model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromFaModel(model);
        return builder.build();
    }

    static public void printSimpleStatistics(String filename) {
        NfaModel nfaModel=statemachine.model.fsm.fa.file.Import.dot2NfaModel(filename);
        int num_inputs=nfaModel.getInputAlphabet().size();
        int num_locations=nfaModel.getLocations().size();
        int num_outputs=nfaModel.getOutputAlphabet().size();
        int num_trans=nfaModel.getModelTransitions().size();
        String format= "states:%1$-10.10s  inputs:%2$-10.10s outputs:%3$-10.10s  transitions:%4$-10.10s";
        System.out.println(String.format(format,num_locations,num_inputs,num_outputs,num_trans));
    }

}
