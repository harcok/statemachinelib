package statemachine.model.fsm.fa.format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.BiFunction;

import com.google.common.collect.ComparisonChain;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.NfaModel;


/*

 digraph g {

    __start0 [label="" shape="none"];

    1 [shape="circle" label="1"];                   => rejection state
    2 [shape="doublecircle" label="2"];             => accepting state
    1 -> 2 [label="a"];
    1 -> 2 [label="b"];
    2 -> 1 [label="a"];
    1 -> 1 [label="a"];

    __start0 -> 1;
}


 */

public class Dot {



	static public NfaModel importNfaModel( List<String> lines) {


		NfaModel.ImmutableBuilder builder =new NfaModel.ImmutableBuilder();
		for(String line: lines){

		    // note: dot only allows lower case keywords so we can just match on lowercase "label"!

		    // fetch start location
		    // match transition from invisible __start0 node without label to the start node
		    // note: we assume that this transition has no  attribute list
			if(line.contains("__start0") && !line.contains("label")) {
				int arrow = line.indexOf('-');
				String start = line.substring(arrow+2).trim();
				LocationState startLocation = new LocationState(start);
				builder.setStartLocation(startLocation);
			}

			// we match only transitions and locations with labels;  locations we fetch from transitions, so they don't need to be declared
			if(!line.contains("label")) continue;

			// accepting location
	        if (line.contains("doublecircle")) {
	                int e1 = line.indexOf('[');
	                String locationStr = line.substring(0, e1).trim();
	                LocationState location = new LocationState(locationStr);
	                builder.setAccepting(location);
	        }

	        // transition
			if(line.contains("->")){
				int arrow = line.indexOf('-');
				int attrlist = line.indexOf('[');
				int labelattr= line.indexOf("label",attrlist);
				int labelbegin = line.indexOf('"',labelattr);
				int labelend = line.lastIndexOf('"');

				String from = line.substring(0, arrow).trim();
				String to = line.substring(arrow+2, attrlist).trim();
				String label = line.substring(labelbegin+1, labelend).trim();

				LocationState src = new LocationState(from);
				LocationState dst = new LocationState(to);
				InputAction action =  new InputAction(label);
				builder.addTransition(src, dst, action);
			}
		}
		return builder.build();

	}

    static private String formatID(String id) {
         return id.contains(" ") ?  "\"" + id +  "\""  : id;
    }

	static public List<String> export(FaModel model) {

        // get locations sorted
        ArrayList<LocationState> locations = new  ArrayList<>(model.getLocations());
        locations.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));


        // get transitions
        ArrayList<InputTransition> transitions= new  ArrayList<>(model.getModelTransitions());
        // sort transitions
        BiFunction<InputTransition,InputTransition,Integer> comparator =  (trans1,trans2) -> {
            return ComparisonChain.start()
            .compare(trans1.getSource().getName(),      trans2.getSource().getName())
            .compare(trans1.getDestination().getName(), trans2.getDestination().getName())
            .compare(trans1.getInput().getName(),       trans2.getInput().getName())
            .result();
        };
        transitions.sort(comparator::apply);


        // header
		String indent="    ";
		LinkedList<String> lines= new LinkedList<>();
		lines.add("digraph g {");
		lines.add("");
		lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
	    lines.add(indent + "__start0 -> " +   formatID(model.getStartLocation().getName()) );
        lines.add("");

        // content
		for ( LocationState location: locations) {
		    String label = location.getName();
		    String id =  formatID(label);
		    String shape = model.isAccepting(location) ? "doublecircle" : "circle";
		    String format = "%s%s [label=\"%s\" shape=\"%s\"]";
		    lines.add(String.format(format,indent,id,label,shape));
		}
        lines.add("");
		for (InputTransition transition: transitions ) {
		    String src= formatID(transition.getSource().getName());
		    String dst= formatID(transition.getDestination().getName());
		    String label = transition.getInput().getName();
		    String format = "%s%s -> %s  [label=\"%s\"]";
		    lines.add(String.format(format,indent,src,dst,label));
		}

		// footer
		lines.add("}");
		return lines;
	}

}
