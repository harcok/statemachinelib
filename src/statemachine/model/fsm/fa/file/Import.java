package statemachine.model.fsm.fa.file;

import java.util.List;

import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.fa.conversion.LtsZip;
import statemachine.model.fsm.lts.LtsModel;
import statemachine.util.IO;

public class Import {

	static public NfaModel  dot2NfaModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			NfaModel model= statemachine.model.fsm.fa.format.Dot.importNfaModel(lines); 
			return model;
	}
	
	static public NfaModel  gml2NfaModel( String filename){
		LtsModel lts= statemachine.model.fsm.lts.file.Import.gml2LtsModel(filename);
		NfaModel dfa = LtsZip.unzipLts2Fa(lts);
		return dfa;
   }
}
