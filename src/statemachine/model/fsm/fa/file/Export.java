package statemachine.model.fsm.fa.file;

import java.util.List;

import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.conversion.LtsZip;
import statemachine.model.fsm.lts.LtsNonDetModel;
import statemachine.util.IO;

public class Export {

	   static public void  dot(FaModel model, String filename){	
		   List<String> lines=statemachine.model.fsm.fa.format.Dot.export(model);
 		   IO.writeSmallTextFile( lines, filename);
	   }
	   
	   static public void  gml(FaModel model, String filename){
		   LtsNonDetModel lts=LtsZip.zipFa2Lts(model);
		   statemachine.model.fsm.lts.file.Export.gml(lts,filename);
	   }

	   /*  no aldebaran export because locations can only be represented by numbers in aldebaran format
	    *  => you can transform  dfa/moore to mealy machine and export that to aldebaran
		*/
	   // TODO: export to aldebaran using dfa2mealy 

}
