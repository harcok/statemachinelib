package statemachine.model.fsm.fa.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.InputTransition;
import statemachine.model.fsm.fa.DfaModel;
import statemachine.model.fsm.fa.FaModel;
import statemachine.model.fsm.fa.util.TraceDiff;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.util.LocationPair;


public class Util {


    //------------------------------------------------------------------------------------------------
    //  compare(DfaModel1,DfaModel2)
    //------------------------------------------------------------------------------------------------


    static class Link {
        public LocationPair previousLocationPair;
        public InputAction  input;
      public Link(LocationPair previousLocationPair, InputAction input) {
          super();
          this.previousLocationPair = previousLocationPair;
          this.input = input;
      }
  }


    static private List<InputAction> getInputs(
            Map<LocationPair, Link> statePair2actionOnIncomingTransition, LocationPair startPair, LocationPair endPair) {
        List<InputAction> inputs = new ArrayList<InputAction>();

        LocationPair currentPair = endPair;
        while (!currentPair.equals(startPair)) {
            Link prevPairAndInput = statePair2actionOnIncomingTransition.get(currentPair);
            inputs.add(prevPairAndInput.input);
            currentPair = prevPairAndInput.previousLocationPair;
        }
        Collections.reverse(inputs);
        return inputs;
    }


    static private List<Boolean> getAcceptingValues(DfaModel machine, List<InputAction> inputs) {
        List<Boolean> acceptingValues = new ArrayList<Boolean>();
        LocationState location = machine.getStartLocation();
        acceptingValues.add(machine.isAccepting(location));
        for (InputAction input : inputs) {
            // TODO: handle optional  transitions better
            InputTransition transition = machine.getTriggeredTransition(location, input).get();
            location = transition.getDestination();
            acceptingValues.add(machine.isAccepting(location));
        }
        return acceptingValues;
    }


    /**
     *  compare whether two dfa machines are observational equivalent
     *
     *  note: the dfa machines must not necessarily input enabled
     *
     * @param machine1
     * @param machine2
     * @param inputAlphabet
     */
    public static TraceDiff compare(DfaModel machine1, DfaModel machine2, ImmutableSet<InputAction> inputAlphabet) {

        LocationState startLoc1 = machine1.getStartLocation();
        LocationState startLoc2 = machine2.getStartLocation();
        LocationPair startPair = new LocationPair(startLoc1, startLoc2);


        boolean  startIsAccepting1 = machine1.isAccepting(startLoc1);
        boolean  startIsAccepting2 = machine2.isAccepting(startLoc2);

        if (startIsAccepting1!=startIsAccepting2) {
            List<InputAction> inputs = new ArrayList<>();
            List<Boolean> acceptingValues = new ArrayList<>();
            return new TraceDiff(inputs, acceptingValues ,  startIsAccepting1);
        }

        Map<LocationPair, Link> statePair2actionOnIncomingTransition = new HashMap<>();
        Set<LocationPair> seenStatePairs = new HashSet<>();
        Queue<LocationPair> locationPairsTodo = new LinkedList<LocationPair>();

        seenStatePairs.add(startPair);
        locationPairsTodo.add(startPair);

        while (!locationPairsTodo.isEmpty()) {
            LocationPair currentLocPair = locationPairsTodo.remove();
            for (InputAction input : inputAlphabet) {

                Optional<InputTransition> trans1 = machine1.getTriggeredTransition(currentLocPair.left, input);
                Optional<InputTransition> trans2 = machine2.getTriggeredTransition(currentLocPair.right, input);


                if (trans1.isPresent() != trans2.isPresent()) {
                    InputAction discriminatingInput;
                    boolean discriminatingInputForFirstAutomata;
                    if (trans1.isPresent()) {
                        discriminatingInput = trans1.get().getInput();
                        discriminatingInputForFirstAutomata=true;
                    } else {
                        discriminatingInput = trans2.get().getInput();
                        discriminatingInputForFirstAutomata=false;
                    }

                    List<InputAction> inputs = getInputs(statePair2actionOnIncomingTransition, startPair, currentLocPair);
                    List<Boolean> acceptingValues = getAcceptingValues(machine1,  inputs);
                    return new TraceDiff(inputs, acceptingValues, discriminatingInput,discriminatingInputForFirstAutomata);
                }
                if (!trans1.isPresent()) {
                    // both transitions not possible  => machines equal
                    continue;
                }

                boolean  isAccepting1 = machine1.isAccepting(trans1.get().getDestination());
                boolean  isAccepting2 = machine2.isAccepting(trans2.get().getDestination());

                if (isAccepting1==isAccepting2) {
                    LocationPair destinationPair = new LocationPair(trans1.get().getDestination(), trans2.get().getDestination());
                    if (!seenStatePairs.contains(destinationPair)) {
                        seenStatePairs.add(destinationPair);
                        locationPairsTodo.add(destinationPair);
                        statePair2actionOnIncomingTransition.put(destinationPair, new Link(currentLocPair,input) );
                    }
                    // else already seen pair which is already handled or on todo list to be handled
                } else {
                    List<InputAction> inputs = getInputs(statePair2actionOnIncomingTransition, startPair, currentLocPair);
                    List<Boolean> acceptingValues = getAcceptingValues(machine1,  inputs);
                    inputs.add(input); // belongs to similar input set
                    return new TraceDiff(inputs, acceptingValues ,  isAccepting1);
                }
            }
        }
        return null;

    }

    /**
     *  compare whether two dfa machines are observational equivalent
     *
     *  note: the dfa machines must not necessarily input enabled
     *
     * @param machine1
     * @param machine2
     */
    public static TraceDiff compare(DfaModel machine1, DfaModel machine2) {
        return compare(machine1, machine2, machine1.getInputAlphabet());
    }



    //------------------------------------------------------------------------------------------------
    //  determinize(FaModel)
    //------------------------------------------------------------------------------------------------




    static public InputAction epsilon = new InputAction("e");




    static public HashSet<LocationState> getReachableStates(FaModel model,LocationState location,InputAction input) {
        HashSet<LocationState> currentLocs =  new HashSet<>();

        ImmutableSet<InputTransition> transitions=model.getModelTransitions(location,input );

      //  currentLocs.add(location);
        for (InputTransition transition: transitions) {
            currentLocs.add(transition.getDestination());
        }
        return currentLocs;
    }

    static private void getEpsilonReachableStatesHelper(FaModel model,HashSet<LocationState> seen, HashSet<LocationState> todo) {

        HashSet<LocationState> reachable_locations = new HashSet<>();

        for ( LocationState location : todo ) {
            seen.add(location);
            reachable_locations.addAll(getReachableStates(model,location, epsilon));
        }
        todo.addAll(reachable_locations);
        todo.removeAll(seen);
    }

    static public HashSet<LocationState> getEpsilonReachableStates(FaModel model,LocationState location) {
        HashSet<LocationState> seen = new HashSet<>();
        HashSet<LocationState> todo;
        seen.add(location);
        todo= getReachableStates( model, location, epsilon);
        todo.removeAll(seen);
        while ( !todo.isEmpty()) {
            getEpsilonReachableStatesHelper( model, seen,  todo) ;
        }
        return seen;
    }


    private static HashSet<LocationState> getDestNfaLocationSet(FaModel model, HashSet<LocationState> locations, InputAction input) {

        HashSet<LocationState> result_locations = new HashSet<>();
        HashSet<LocationState> reachable_locations = new HashSet<>();

        for ( LocationState location : locations ) {
            reachable_locations.addAll(getReachableStates(model,location, input));
        }
        result_locations.addAll(reachable_locations);

        for ( LocationState location : reachable_locations ) {
            result_locations.addAll(getEpsilonReachableStates(model,location));
        }
        return result_locations;
    }

    private static String getName(HashSet<LocationState> locations) {
        ArrayList<LocationState> sortedLocations = new ArrayList<>(locations);
        sortedLocations.sort((p1, p2) -> {
            return p1.getName().compareTo(p2.getName());
        });

        String name= String.join(",",sortedLocations.stream().map(loc -> loc.getName()).collect(Collectors.toList()));
        return "{"+name +"}";
    }

    private static boolean isAccepting(FaModel model, HashSet<LocationState> destNfaLocationSet) {
        for ( LocationState location : destNfaLocationSet) {
            if ( model.isAccepting(location)) return true;
        }
        return false;
    }

    static public DfaModel determinize(FaModel model) {


        DfaModel.ImmutableBuilder builder =new DfaModel.ImmutableBuilder();

        ArrayList<InputAction> inputs=new ArrayList<InputAction>( model.getInputAlphabet());
        inputs.remove(epsilon);

        // The initial state of the DFA constructed from this NFA is the set of all NFA states
        // that are reachable from state 1 by ε-moves;

        LocationState start=model.getStartLocation();
        HashSet<LocationState>  startNfaLocationSet = getEpsilonReachableStates(model,start);
        LocationState startDfaLocation = new LocationState(getName(startNfaLocationSet));
        builder.setStartLocation(startDfaLocation);
        if ( isAccepting(model,startNfaLocationSet)) {
            builder.setAccepting(startDfaLocation);
        }

        // For each NFA location set  representing a DFA location find next DFA location by doing possible an input
        // on each location in source NFA location set giving you a NFA destination set which you complement with
        // epsilon reachable states from the destination set. Repeat this until you have all DFA locations and its
        // outgoing transitions found.
        Queue< HashSet<LocationState> > todo = new LinkedList<>();
        Set< LocationState > seenDfaLocations = new HashSet<>();
        Set< String > seenTransitions= new HashSet<>();

        // start with startDfaLocation/startDfaLocation and from there explore all dfalocations and their transitions
        seenDfaLocations.add(startDfaLocation);
        todo.add(startNfaLocationSet);
        // repeat  until todo queue is empty
        while ( ! todo.isEmpty() ) {
            HashSet<LocationState> sourceNfaLocationSet = todo.remove();
            LocationState sourceDfaLocation =  new LocationState(getName(sourceNfaLocationSet));
            for ( InputAction input : inputs ) {
                HashSet<LocationState>  destNfaLocationSet = getDestNfaLocationSet(model,sourceNfaLocationSet,input);
                LocationState destDfaLocation = new LocationState(getName(destNfaLocationSet));

                if ( destDfaLocation.getName().equals("{}")) continue;

                String transString=sourceDfaLocation + "_" + destDfaLocation + "_" + input;
                if ( !seenTransitions.contains(transString)) {
                    builder.addTransition(sourceDfaLocation, destDfaLocation, input);
                    seenTransitions.add(transString);
                }
                if ( ! seenDfaLocations.contains(destDfaLocation) ) {
                    seenDfaLocations.add(destDfaLocation);
                    todo.add(destNfaLocationSet);
                    if ( isAccepting(model,destNfaLocationSet)) {
                        builder.setAccepting(destDfaLocation);
                    }
                }
            }
        }

        return builder.build();
    }



}
