package statemachine.model.fsm.fa.util;


import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;

/**
 *  for fa we can find tracediff in two ways :
 *   - one fa can do input and other not                                           => discriminatingInput
 *   - one fa reaches accpting location where other reaches rejecting location     =>
 *
 * @author harcok
 *
 */
public class TraceDiff {

    public List<InputAction> inputsSimular;
    public List<Boolean> isAccepting;

    public InputAction discriminatingInput;
    public boolean discriminatingInputForFirstAutomata;

    public boolean discriminatingAcceptingValueForFirstAutomata;  // for first automata (other automata has opposite value


    public boolean isEqual;

    public boolean isEqual() {
        return isEqual;
    }

    public TraceDiff(List<InputAction> inputsSimular, List<Boolean> isAccepting) {
        super();
        this.inputsSimular = inputsSimular;
        this.isAccepting = isAccepting;
        if (  isAccepting.size()+1 != inputsSimular.size() ) {
            throw new RuntimeException("for equal trace  the number of similar accepting  values should be one more then similar inputs in the trace");
        }
        isEqual=true;
    }

    public TraceDiff(List<InputAction> inputsSimular, List<Boolean> isAccepting,
            boolean discriminatingAcceptingValueForFirstAutomata) {
        super();
        if (  isAccepting.size() != inputsSimular.size() ) {
            throw new RuntimeException("for discriminating trace on accepting value the number of similar inputs and similar accepting values in the trace should be the same");
        }
        this.inputsSimular = inputsSimular;
        this.isAccepting = isAccepting;
        this.discriminatingAcceptingValueForFirstAutomata=discriminatingAcceptingValueForFirstAutomata;
        isEqual=false;
    }

    public TraceDiff(List<InputAction> inputsSimular, List<Boolean> isAccepting,
            InputAction discriminatingInput, boolean discriminatingInputForFirstAutomata) {
        super();
        this.inputsSimular = inputsSimular;
        this.isAccepting = isAccepting;
        this.discriminatingInputForFirstAutomata=discriminatingInputForFirstAutomata;
        this.discriminatingInput = discriminatingInput;

        if (  isAccepting.size() != inputsSimular.size()+1 ) {
            throw new RuntimeException("for  discriminating trace on input  the number of similar accepting  values should be one more then similar inputs in the trace");
        }

        isEqual=false;
    }



    public String toString() {
        StringJoiner joiner = new StringJoiner(" ");

        for (int i = 0; i < inputsSimular.size(); i++) {
            InputAction input = inputsSimular.get(i);
            boolean sourceIsAccepting= isAccepting.get(i);
            joiner.add(sourceIsAccepting + " " + input + " ");
        }
        if ( isEqual() ) {
            return "EQUAL: " + joiner.toString() +  isAccepting.get(isAccepting.size()-1) ;
        } else {
            if (  isAccepting.size() == inputsSimular.size() ) {
                // inequal by accepting value in state reached
                boolean first = discriminatingAcceptingValueForFirstAutomata;
                boolean second = ! first;
                return "DIFFERENT: " + joiner.toString() +  first + "/" + second  ;
            } else {
                // inequal by input
                String diffinput;
                if ( discriminatingInputForFirstAutomata ) {
                    diffinput = discriminatingInput + "/MISSING";
                } else {
                    diffinput = "MISSING/" + discriminatingInput;
                }
                return "DIFFERENT: " + joiner.toString() +  isAccepting.get(isAccepting.size()-1) + " " + diffinput;
            }
        }
    }




}

