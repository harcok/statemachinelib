
package statemachine.model.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nl.ru.cs.tree.file.Export;
import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeNode;
import nl.ru.cs.tree.format.EdgeNode;
import nl.ru.cs.tree.format.EdgeNodeImpl;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.BreadthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.DepthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.TraverseStyle;
import statemachine.model.elements.transition.BaseTransition;

/** Create a traverse tree for given automaton with either directed or undirected transitions
 *  which describes how breadth first or depth first traversal walk through the given automaton.
 * <p>
 * Each node in the traverse tree contains the location corresponding to that node and
 * the automaton's transition used to reach that node from its parent node.
 * <p>
 * When generating to dot  for each tree node we create
 * a node with label of the location stored in the tree node
 * and an incoming edge for the node with the label of the transition stored in the tree node.
 */
public  class DetermineTraverseTree<L extends ModelLocation,T extends ModelTransition<L>>
       extends BaseGraphTraversalAlgorithm<L,T> {

    private ArrayList<Tree<EdgeNode<T,L>>> trees;
    private HashMap<L,TreeNode<EdgeNode<T,L>>> location2treenode;

    @Override
    public void startTreeNode(L node) {

        TreeNode<EdgeNode<T,L>> newTreeNode = new TreeNode<EdgeNode<T,L>>( new EdgeNodeImpl<>(null,node) );
        location2treenode.put(node, newTreeNode);
        trees.add(new Tree<EdgeNode<T,L>>(newTreeNode));
    }


    @Override
    public void traverseTreeEdge(L source, T edge, L destination) {
        EdgeNode<T,L> data = new EdgeNodeImpl<>(edge,destination);
        TreeNode<EdgeNode<T,L>> newTreeNode = new TreeNode<EdgeNode<T,L>>(data);
        location2treenode.put(destination, newTreeNode);
        TreeNode<EdgeNode<T, L>> parent = location2treenode.get(source);
        parent.addChild(newTreeNode);
    }


    public DetermineTraverseTree() {
        trees = new ArrayList<>();
        location2treenode = new HashMap<>();
    }

    /** Create a traverse tree for given automaton which must be connected to an initial location.
     * <p>
     * Each node in the traverse tree contains the location corresponding to that node and
     * the automaton's transition which is followed to reach that node from its parent node.
     * <p>
     * When generating to dot the transition's label is put on the node's incoming edge
     * and its source and destination on the parent and child node of the tree edge.
     */
    static public  <L extends ModelLocation, T extends ModelTransition<L>>
             Tree<EdgeNode<T,L>> execute(ModelAutomaton<L,T> model, L initialNode, TraverseStyle traverseStyle) {

        DetermineTraverseTree<L,T> algorithm = new DetermineTraverseTree<>();
        // only traverse from initial location
        switch (traverseStyle) {
        case DFS:
            DepthFirstGraphTraversal.traverse(model, initialNode, algorithm);
            break;
        case BFS:
            BreadthFirstGraphTraversal.traverse(model, initialNode, algorithm);
            break;
        }
        return algorithm.trees.get(0);
    }

    /** Create a traverse tree for given automaton which explores all locations of the automaton.
     * <p>
     * Each node in the traverse tree contains the location corresponding to that node and
     * the automaton's transition which is followed to reach that node from its parent node.
    */
    static public  <L extends ModelLocation, T extends ModelTransition<L>>
             List<Tree<EdgeNode<T,L>>> executeAll(ModelAutomaton<L,T> model, L initialNode, TraverseStyle traverseStyle) {

        DetermineTraverseTree<L,T> algorithm = new DetermineTraverseTree<>();
        // traverse from initial location, and continue with unseen locations
        switch (traverseStyle) {
        case DFS:
            DepthFirstGraphTraversal.traverseAll(model, initialNode, algorithm);
            break;
        case BFS:
            BreadthFirstGraphTraversal.traverseAll(model, initialNode, algorithm);
            break;
        }
        return algorithm.trees;
    }

    /** Create dot file for traverse tree.
     * <p>
     * When generating to dot the transition's label is put on the node's incoming edge
     * and its source and destination on the parent and child node of the tree edge.
     */
   static public  <L extends ModelLocation, T extends ModelDirectedTransition<L>>
   void TraverseTreeToDot(Tree<EdgeNode<T,L>> tree,String filepath) {
       Export.edgeNodeTree2dotfile(filepath, tree,e->e.getLabel(),n->n.toString());
   }



}
