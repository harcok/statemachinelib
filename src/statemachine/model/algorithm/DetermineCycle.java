package statemachine.model.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.DepthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.GraphTraverseInstruction;


/** Check whether a model with directed transitions has cycles
 * <p>
 * If doing a depth first search and we find a back edge then we have a cycle.
 */
public class DetermineCycle<L extends ModelLocation,T extends ModelTransition<L>>
             extends BaseGraphTraversalAlgorithm<L,T> {


    private boolean hasCycle,found,onlyCheck;
    private List<Pair<L,T>> cycle;
    private L cycleStartPoint;


    @Override
    public void processBackEdge(L source, T edge, L destination) {
        // found an edge which points back in traversed tree
        hasCycle=true;
        cycleStartPoint = destination;
        cycle.add(new Pair<>(source,edge));
        if (source.equals(cycleStartPoint)) {
            found=true;
        }

        if (onlyCheck) {
            found=true;
            setDefaultGraphTraverseInstruction(GraphTraverseInstruction.ABORT);
        } else {
            setDefaultGraphTraverseInstruction(GraphTraverseInstruction.SKIP);
        }
    }

    @Override
    public GraphTraverseInstruction backTrackTreeEdge(L source, T edge, L destination) {
        if (found) {
            return  GraphTraverseInstruction.ABORT;
        }
        if (hasCycle) {
            cycle.add(new Pair<>(source,edge));
            if (source.equals(cycleStartPoint)) {
                found=true;
                Collections.reverse(cycle);
                setDefaultGraphTraverseInstruction(GraphTraverseInstruction.ABORT);
            }
        }
        return GraphTraverseInstruction.TRAVERSE;
    }


    private DetermineCycle() {
        super();
        hasCycle=false;
        found=false;
        cycle = new ArrayList<Pair<L,T>>();
        onlyCheck=false;
    }


    /** determine if cycle exist anywhere in a model */
    static public <L extends ModelLocation,T extends ModelTransition<L>>
        List<Pair<L,T>> execute(ModelAutomaton<L,T> model) {

           // repeat traversal for not yet visited (indexed) locations
           DetermineCycle<L,T> algorithm = new DetermineCycle<>();
           DepthFirstGraphTraversal.traverseAll(model, algorithm);
           if (!algorithm.found) return null;
           return algorithm.cycle;
    }

    /** determine if cycle exist in a model's component connected to the given initial location */
    static public <L extends ModelLocation,T extends ModelTransition<L>>
        List<Pair<L,T>> execute(ModelAutomaton<L,T> model, L initialLocation) {

            DetermineCycle<L,T> algorithm = new DetermineCycle<>();
            DepthFirstGraphTraversal.traverse(model, initialLocation, algorithm);
            if (!algorithm.found) return null;
            return algorithm.cycle;
    }


    /** determine if cycle exist in anywhere in a model */
    static public <L extends ModelLocation,T extends ModelTransition<L>>
        boolean check(ModelAutomaton<L,T> model) {

           // repeat traversal for not yet visited (indexed) locations
           DetermineCycle<L,T> algorithm = new DetermineCycle<>();
           algorithm.onlyCheck=true;
           DepthFirstGraphTraversal.traverseAll(model, algorithm);
           return algorithm.hasCycle;
    }

    /** determine if cycle exist in a model's component connected to the given initial location */
    static public <L extends ModelLocation,T extends ModelTransition<L>>
        boolean check(ModelAutomaton<L,T> model, L initialLocation) {

            DetermineCycle<L,T> algorithm = new DetermineCycle<>();
            algorithm.onlyCheck=true;
            DepthFirstGraphTraversal.traverse(model, initialLocation, algorithm);
            return algorithm.hasCycle;
    }

}

