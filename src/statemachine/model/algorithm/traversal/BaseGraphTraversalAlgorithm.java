package statemachine.model.algorithm.traversal;

import java.util.HashMap;



public abstract class BaseGraphTraversalAlgorithm<L,T> implements GraphTraversalAlgorithm<L, T> {

    protected GraphTraverseInstruction defaultGraphTraverseInstruction = GraphTraverseInstruction.TRAVERSE;

    @Override
    public void startTreeNode(L node) {
    }


    @Override
    public GraphTraverseInstruction discoverNode(L node) {
        return defaultGraphTraverseInstruction;
    }

    @Override
    public void preProcessNode(L node) {
    }

    @Override
    public void postProcessNode(L node) {
    }

    @Override
    public GraphTraverseInstruction discoverEdge(L source, T edge, L destination) {
        return defaultGraphTraverseInstruction;
    }

    @Override
    public void traverseTreeEdge(L source, T edge, L destination) {
    }

    @Override
    public void processBackEdge(L source, T edge, L destination) {
    }

    @Override
    public void processForwardEdge(L source, T edge, L destination) {
    }

    @Override
    public void processCrossEdge(L source, T edge, L destination) {
    }

    @Override
    public GraphTraverseInstruction backTrackTreeEdge(L source, T edge, L destination) {
        return defaultGraphTraverseInstruction;
    }

    @Override
    public void postProcess(HashMap<L, Integer> node2discoveryTime, HashMap<L, Integer> node2finishTime) {
    }


    public GraphTraverseInstruction getDefaultGraphTraverseInstruction() {
        return defaultGraphTraverseInstruction;
    }


    public void setDefaultGraphTraverseInstruction(GraphTraverseInstruction defaultGraphTraverseInstruction) {
        this.defaultGraphTraverseInstruction = defaultGraphTraverseInstruction;
    }

}
