package statemachine.model.algorithm.traversal;

public enum GraphNodeColor {
     WHITE,
     GRAY,
     BLACK
}
