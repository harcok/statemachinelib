package statemachine.model.algorithm.traversal;


/**
 *  Traverse styles that can be used
 *  <li> DFS: Depth First Search </li>
 *  <li> BFS: Breadth First Search</li>
 */
public enum TraverseStyle {
   /**
    *  Depth First Search
    */
   DFS,
   /**
    *  Breadth First Search
    */
   BFS,
}
