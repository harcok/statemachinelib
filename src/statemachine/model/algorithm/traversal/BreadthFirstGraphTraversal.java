package statemachine.model.algorithm.traversal;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Queue;

import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;

public class BreadthFirstGraphTraversal <L extends ModelLocation,T extends ModelTransition<L>> {

    private Queue<Pair<L,T>> edgeQueue;
    private HashSet<T> isQueued;
    private HashMap<L,GraphNodeColor> node2color;
    private HashMap<L,Integer> node2discoveryTime;
    private HashMap<L,Integer> node2finishTime;
    private int time;

    private BreadthFirstGraphTraversal(ModelAutomaton<L, T> model) {
        edgeQueue = new ArrayDeque<Pair<L,T>>();
        isQueued = new HashSet<>();
        node2color = new HashMap<>();
        node2discoveryTime = new HashMap<>();
        node2finishTime = new HashMap<>();
        time=0;

        // color all nodes WHITE (unvisited)
        model.getLocations().stream().forEach(node -> node2color.put(node,GraphNodeColor.WHITE));
    }



    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverseAll(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

        BreadthFirstGraphTraversal<L,T> graphTraversal = new BreadthFirstGraphTraversal<>(model);
        // traverse first from given initial node
        graphTraversal.doBreadthFirstTraversalFromNode(model, initialNode, algorithm);
        // traverse from other not yet visited nodes
        for ( L initialLocation : model.getLocations() ) {
            if (graphTraversal.node2color.get(initialLocation)==GraphNodeColor.WHITE) {
                graphTraversal.doBreadthFirstTraversalFromNode(model, initialLocation, algorithm);
            }
        }
    }

    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverseAll(ModelAutomaton<L, T> model, GraphTraversalAlgorithm<L, T> algorithm) {

        BreadthFirstGraphTraversal<L,T> graphTraversal = new BreadthFirstGraphTraversal<>(model);
        // traverse from all possible not yet visited nodes
        for ( L initialLocation : model.getLocations() ) {
            if (graphTraversal.node2color.get(initialLocation)==GraphNodeColor.WHITE) {
                graphTraversal.doBreadthFirstTraversalFromNode(model, initialLocation, algorithm);
            }
        }
    }

    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverse(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

        BreadthFirstGraphTraversal<L,T> graphTraversal = new BreadthFirstGraphTraversal<>(model);
        // traverse only from given initial node
        graphTraversal.doBreadthFirstTraversalFromNode(model, initialNode, algorithm);

   }


    private boolean discoverAndTravelNode(ModelAutomaton<L, T> model, L node, GraphTraversalAlgorithm<L, T> algorithm) {

        GraphTraverseInstruction nodeInstruction = algorithm.discoverNode(node);
        switch(nodeInstruction) {
        case SKIP:
            // we skip node, so do nothing
            break;
        case ABORT:
            return true; // abort further traversing (e.g. if answer found)
        case TRAVERSE:
            // traverse the node (which is already discovered earlier) and enqueue its (outgoing edges)
            algorithm.preProcessNode(node);
            for ( T edge : model.getModelTransitions(node)) {
                Pair<L,T> pair = new Pair<>(node,edge);
                L one = edge.getLocations().getValue0();
                L two = edge.getLocations().getValue1();
                L destination = one.equals(node) ? two : one;

                GraphTraverseInstruction edgeInstruction = algorithm.discoverEdge(node,edge,destination);
                switch(edgeInstruction) {
                case SKIP:
                    // we skip edge, so not traversing it, so not calling any edge events for it anymore
                    return false;
                case ABORT:
                    return true; // abort further traversing (e.g. if answer found)
                case TRAVERSE: // ok to traverse edge
                    // traverse the edge
                    // depending on target node's color we process the edge different

                    // each transition's destination is considered discovered (gray) if not yet discovered (or traveled) before
                    if ( node2color.get(destination) == GraphNodeColor.WHITE ) {
                        node2color.put(destination, GraphNodeColor.GRAY );
                        node2discoveryTime.put(destination, time);
                        time=time+1;
                    }

                    // enqueue edge for later traveling (breadth first)
                    edgeQueue.add(pair);
                    isQueued.add(edge); // for undirected graph mark edge discovered
                    break;
                }
            }
            // finish node
            node2color.put(node, GraphNodeColor.BLACK );
            node2finishTime.put(node, time);
            time=time+1;
            algorithm.postProcessNode(node);
            break;
        default:
            throw new RuntimeException("BUG: should not happen");
        }
        return false; // do not abort
    }

    private void doBreadthFirstTraversalFromNode(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

      // dfs and bfs
      //     in dfs after discovery of edge it is immediately traveled
      //        -> we go depth first, and backtrack later to visit sibling edges
      //        -> other sibling edges are therefore put on stack for later discovery and traveling
      //        -> so an edge and edge's destination are discovered when its edge is pulled from stack
      //     in bfs sibling nodes are traveled first before traveling deeper edges
      //        -> first discovers all edges(and their destination nodes) one level deep  (from all their sibling nodes at that level)
      //        -> and enqueues them so that they then in next step can be processed together
      //        -> so an edge and edge's destination are discovered first
      //           and then enqueued and traveled later when dequeued
      //        note: bfs doesn't do backtracking because it is not needed

      // bfs :
      //   WHITE: node is not discovered
      //   GRAY: node is discovered but not yet traveled  ( it's enqueued and awaits traveling)
      //   BLACK: node has been visited and fully traveled (its outgoing edges are enqueued and await traveling)
      //   note: bfs does not do backtracking: traveled means forward traveling only.

      // dfs:
      //   WHITE: node is not discovered
      //   GRAY: node is discovered  but only forward traveled ( it's pre processed qnd awaits backtracking to node before it can be post processed: for post-process to be done all outgoing edges must be traveled depth first fully before we backtrack)
      //   BLACK: node has been visited and fully traveled ( is backtracked ; any outgoing edges and anything furthers are fully traveled after which the node is backtracked)
      //   note: dfs does do backtracking:  means each tree node and edge are first forward traveled and backward traveled during backtracking


      // dfs algorithm in a nuttshell:
      //   first discover edge, then discover edge's destination, if both TRAVEL then travel edge (enque in bfs, so to travel later)
      //   for skipped edge/node discovery : don't do anything (not enqueue nor mark anything) -> skipped by travelling
      //   for aborted edge/node discovery : immediately return -> aborting graph traversing

      // bfs(start node) {
      //    * initialize algorithm with start node
      //       - first discover start node
      //           if TRAVERSE then
      //             + mark it as discovered (GRAY)
      //             + travel(start node) => process node fully, and discovers outgoing edges, and only enqueues those who need traveling
      //           else return
      //    * loop over the edge queue, and travel each edge
      //         + note:
      //           - edge and destination already discovered and found ok to travel(otherwise not enqueued)
      //           - so destination cannot be white because edge and edge's destination already
      //             discovered when enqueued
      //         + dependent on destination color we travel
      //           a) if destination is GRAY : tree edge
      //              (travelEdge)
      //              travel(destination node)
      //           b) if destination is BLACK : cross edge
      //              note: destination is fully traversed ; can be a self loop, or edge to earlier traversed node reach via other edge
      //              (processCrossEdge)
      // }
      //
      // travel(node) {    => process node fully, and discovers outgoing edges, and only enqueues those who need traveling
      //      (preProces node)
      //      + for each (outgoing) edge from the node we
      //          = discover the edge
      //              - for undirected graphs skip edge if already discovered from other side)
      //              - (discoverEdge)
      //                  if not TRAVERSE then continue ( next edge)
      //          = discover the edge's destination
      //              - ((discoverNode edge's destination)
      //                   if TRAVERSE
      //                   then
      //                     if color(destination) == WHITE :
      //                        not already discovered
      //                        make it discovered => make it GRAY
      //                   else
      //                     continue ( next edge)
      //          = enqueue the edge for later really traveling
      //      + mark node as fully traveled (BLACK)
      //      (postProces node)
      // }


      // notify starting new tree traversal
      algorithm.startTreeNode(initialNode);


      // discover nodinitialNodee
      node2color.put(initialNode, GraphNodeColor.GRAY );
      node2discoveryTime.put(initialNode, time);
      time=time+1;

      // travel initial node
      boolean abort=discoverAndTravelNode(model, initialNode, algorithm);
      if (abort) return;


      EDGEQUEUE: while (! edgeQueue.isEmpty() )   {
          Pair<L,T> pair = edgeQueue.remove();
          T edge=pair.getValue1();
          L source=pair.getValue0();
          L one = edge.getLocations().getValue0();
          L two = edge.getLocations().getValue1();
          L destination = one.equals(source) ? two : one;

          // traverse the edge
          // depending on target node's color we process the edge different
          GraphNodeColor color=node2color.get(destination);
          switch(color) {
          case WHITE:  // edge to undiscovered node;

              //  should not happen because enqueued edges (and its destination node) are already discovered before enqueueing
              throw new RuntimeException("BUG: should not happen");

          case GRAY:   // edge to earlier discovered edge/node but not yet traveled

               // is a tree edge
               // note: edge and its destination are already discovered, only need to be traversed

               // traverse tree edge
               algorithm.traverseTreeEdge(source,edge,destination);

               // traverse edge's destination node ( discover its outgoing edges and nodes and enqueue them )
               // travel node
               abort=discoverAndTravelNode(model, destination, algorithm);
               if (abort) return;

               continue EDGEQUEUE;
          case BLACK:  // traverse edge to already fully processed node

              // When breadth first traveling a graph we have no forward edges
              // because we process all nodes of same distance from initial node first,
              // before continue with nodes with one edge distance large.
              // So it is impossible to travel and edge which forwards more then one edge distance!
              //
              // So we can only traverse a back edge in case of a self loop.
              //
              // Finally we have three types of cross edges when breadth first traveling
              //  a) we have two sibling nodes x and y, and y has directed edge to x.
              //     When x is visited/traveled before y is visited/traveled then x's edges are dequeued
              //     before y's edges, and y's directed edge to x is a cross edge. (edge between different 'branches' of bfs tree)
              //  b) two sibling nodes have both an edge to same destination node
              //     then the edge which is last dequeued is a cross edge  (edge between different 'branches' of bfs tree)
              //  c) we have two strongly connected components X and Y, and there is only a directed
              //     transition from Y to X. When we first do depth first on node x in  X,  and the
              //     restart bfs on node y in Y then the directed edge from Y to X is a cross edge.
              // Note: for undirected graphs we have only case b).

              // traverse edge
              // Note: we do not really traverse a back/cross edge because destination already earlier traversed earlier.
              //       Therefore the algorithm event methods are named "process" instead of "traverse"
             if ( destination.equals(source)) {
                  // traverse back edge : self loop
                  algorithm.processBackEdge(source,edge,destination);
             } else {
                  // is a cross edge : points to other branch in bfs tree, or other bfs tree
                  // process cross edge
                  algorithm.processCrossEdge(source,edge,destination);
             }

             continue EDGEQUEUE;
          }

      }

      algorithm.postProcess(node2discoveryTime,node2finishTime);
    }

}
