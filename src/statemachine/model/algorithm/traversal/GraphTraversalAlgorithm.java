package statemachine.model.algorithm.traversal;

import java.util.HashMap;

/**
 * Template interface for graph traversal algorithms.
 * <p>
 * Declares methods called during graph traversal.
 *
 * note: that when doing BFS we do breadth first traversal one distance step at a time so we cannot have
 *  - a back edge, except for a self loop,
 *  - and no forward edge
 * note: this holds for both directed and undirected graphs.
 *
 * so when traversing
 *  * a directed graph we have:
 *       for DFS
 *        - tree,forward, back, cross edges
 *        - backtracking
 *       for BFS
 *        - no forward edges   (see above)
 *        - and the only back edge is a self loop  (see above)
 *        - but we have tree and cross edges
 *        - no backtracking
 *  * an undirected graph we have:
 *       for DFS
 *        - no forward edge: seen as tree edge from other direction when we visited that other side's node earlier
 *        - no cross edge:  seen as tree edge from other direction  when we visited that other side's node earlier
 *        - but we have tree and back edges
 *        - backtracking
 *       for BFS
 *        - no forward edges   (see above)
 *        - and the only back edge is a self loop  (see above)
 *        - but we have tree and cross edges
 *        - no backtracking
 *
 */
public interface GraphTraversalAlgorithm<L,T> {

    // start Node for a new tree we are traveling forward
    public void startTreeNode(L node);

    // discover Node when traveling forward
    public GraphTraverseInstruction discoverNode(L node);
    // process Node when traveling forward (pre-process node)
    public void preProcessNode(L node);
    // process Node when traveling backward (post-process node) (followed by backTrackTreeEdge for edge which has node as target)
    public void postProcessNode(L node);

    // discover new Edge when traveling forward
    public GraphTraverseInstruction discoverEdge(L source, T edge, L destination); // new edge ; can be tree, back, forward, or cross edge
    // process Edge when traveling tree edge forward
    public void traverseTreeEdge(L source, T edge, L destination);
    // process none-tree edges which we do no traverse
    public void processBackEdge(L source, T edge, L destination);
    public void processForwardEdge(L source, T edge, L destination);
    public void processCrossEdge(L source, T edge, L destination);

    // process Edge when traveling  tree edge backward; travel backwards from destination to source where source is higher in the tree
    public GraphTraverseInstruction backTrackTreeEdge(L source, T edge, L destination);  // back track edge (only called for tree edge)


    public void postProcess(HashMap<L, Integer> node2discoveryTime, HashMap<L, Integer> node2finishTime);


}
