package statemachine.model.algorithm.traversal;



public enum GraphTraverseInstruction {

    /**
     * Skip traversal of respective node or edge
     */
    SKIP,
    /**
     * Traverse the respective node or edge.
     */
    TRAVERSE,
    /**
     * Abort Graph traversal
     */
    ABORT
}
