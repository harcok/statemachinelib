package statemachine.model.algorithm.traversal;

import java.util.HashMap;
import java.util.HashSet;
import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.util.DequeStack;
import statemachine.util.Stack;

public class DepthFirstGraphTraversal <L extends ModelLocation,T extends ModelTransition<L>> {

    private Stack<Pair<L,T>> edgeStack;
    private HashSet<T> traversed;
    private HashSet<T> finished;
    private HashMap<L,GraphNodeColor> node2color;
    private HashMap<L,Integer> node2discoveryTime;
    private HashMap<L,Integer> node2finishTime;
    private int time;

    private DepthFirstGraphTraversal(ModelAutomaton<L, T> model) {
        edgeStack = new DequeStack<>();
        traversed = new HashSet<>();
        finished = new HashSet<>();
        node2color = new HashMap<>();
        node2discoveryTime = new HashMap<>();
        node2finishTime = new HashMap<>();
        time=0;

        // color all nodes WHITE (unvisited)
        model.getLocations().stream().forEach(node -> node2color.put(node,GraphNodeColor.WHITE));

    }

    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverseAll(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

        DepthFirstGraphTraversal<L,T> graphTraversal = new DepthFirstGraphTraversal<>(model);
        // traverse first from given initial node
        graphTraversal.doDeptFirstTraversalFromNode(model, initialNode, algorithm);
        // traverse from other not yet visited nodes
        for ( L initialLocation : model.getLocations() ) {
            if (graphTraversal.node2color.get(initialLocation)==GraphNodeColor.WHITE) {
                graphTraversal.doDeptFirstTraversalFromNode(model, initialLocation, algorithm);
            }
        }
    }

    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverseAll(ModelAutomaton<L, T> model, GraphTraversalAlgorithm<L, T> algorithm) {

        DepthFirstGraphTraversal<L,T> graphTraversal = new DepthFirstGraphTraversal<>(model);
        // traverse from all possible not yet visited nodes
        for ( L initialLocation : model.getLocations() ) {
            if (graphTraversal.node2color.get(initialLocation)==GraphNodeColor.WHITE) {
                graphTraversal.doDeptFirstTraversalFromNode(model, initialLocation, algorithm);
            }
        }
    }

    static public <L extends ModelLocation,T extends ModelTransition<L>>
    void traverse(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

        DepthFirstGraphTraversal<L,T> graphTraversal = new DepthFirstGraphTraversal<>(model);
        // traverse only from given initial node
        graphTraversal.doDeptFirstTraversalFromNode(model, initialNode, algorithm);

   }

   private void doDeptFirstTraversalFromNode(ModelAutomaton<L, T> model, L initialNode, GraphTraversalAlgorithm<L, T> algorithm) {

       // dfs and bfs
       //     in dfs after discovery of edge it is immediately traveled
       //        -> we go depth first, and backtrack later to visit sibling edges
       //        -> other sibling edges are therefore put on stack for later discovery and traveling
       //        -> so an edge and edge's destination are discovered when its edge is pulled from stack
       //     in bfs sibling nodes are traveled first before traveling deeper edges
       //        -> first discovers all edges(and their destination nodes) one level deep  (from all their sibling nodes at that level)
       //        -> and enqueues them so that they then in next step can be processed together
       //        -> so an edge and edge's destination are discovered first
       //           and then enqueued and traveled later when dequeued
       //        note: bfs doesn't do backtracking because it is not needed

       // bfs :
       //   WHITE: node is not discovered
       //   GRAY: node is discovered but not yet traveled  ( it's enqueued and awaits traveling)
       //   BLACK: node has been visited and fully traveled (its outgoing edges are enqueued and await traveling)
       //   note: bfs does not do backtracking: traveled means forward traveling only.

       // dfs:
       //   WHITE: node is not discovered
       //   GRAY: node is discovered  but only forward traveled ( it's pre processed qnd awaits backtracking to node before it can be post processed: for post-process to be done all outgoing edges must be traveled depth first fully before we backtrack)
       //   BLACK: node has been visited and fully traveled ( is backtracked ; any outgoing edges and anything furthers are fully traveled after which the node is backtracked)
       //   note: dfs does do backtracking:  means each tree node and edge are first forward traveled and backward traveled during backtracking


      // notify starting new tree traversal
      algorithm.startTreeNode(initialNode);

      // discover initial node
      node2color.put(initialNode, GraphNodeColor.GRAY );
      node2discoveryTime.put(initialNode, time);
      time=time+1;
      GraphTraverseInstruction nodeInstruction = algorithm.discoverNode(initialNode);
      switch(nodeInstruction) {
      case SKIP:
          node2color.put(initialNode, GraphNodeColor.BLACK );
          node2finishTime.put(initialNode, time);
          time=time+1;
          // we skip node, so no processNode and finishNode
          return;
      case ABORT:
          return; // abort further traversing (e.g. if answer found)
      case TRAVERSE:
          // traverse the initialNode: process its children
          algorithm.preProcessNode(initialNode);
          for ( T transition : model.getModelTransitions(initialNode)) {
              Pair<L,T> pair = new Pair<>(initialNode,transition);
              edgeStack.push(pair);
          }
          break;
     default:
          throw new RuntimeException("BUG: should not happen");
      }

      EDGESTACK: while (! edgeStack.isEmpty() )   {
          Pair<L,T> pair = edgeStack.pop();
          T edge=pair.getValue1();
          L source=pair.getValue0();
          L one = edge.getLocations().getValue0();
          L two = edge.getLocations().getValue1();
          L destination = one.equals(source) ? two : one;

          if ( !traversed.contains(edge)) { // FORWARD TRAVERSE
              // new edge not traversed yet
              traversed.add(edge);

              // try to traverse edge forward
              // first discover edge
              GraphTraverseInstruction edgeInstruction =algorithm.discoverEdge(source,edge,destination);
              switch(edgeInstruction) {
              case SKIP:
                  // we skip edge, so not traversing it, so not calling any edge events for it anymore
                  continue EDGESTACK;
              case ABORT:
                  return; // abort further traversing (e.g. if answer found)
              case TRAVERSE: // ok to traverse edge
                  // traverse the edge
                  // depending on target node's color we process the edge different
                  GraphNodeColor color=node2color.get(destination);
                  switch(color) {
                  case WHITE:  // traverse edge to undiscovered node

                       // is a tree edge

                       // process tree edge when traversing it
                       algorithm.traverseTreeEdge(source,edge,destination);

                       // when traversing edge we discover new target node
                       node2color.put(destination, GraphNodeColor.GRAY );
                       node2discoveryTime.put(destination, time);
                       time=time+1;
                       GraphTraverseInstruction targetNodeInstruction = algorithm.discoverNode(destination);
                       DISCOVERNODE: switch(targetNodeInstruction) {
                       case SKIP:
                             // we skip node, so do nothing

//                           // we skip node, so no processNode and finishNode
//                           // and mark it handled : BLACK
//                           node2color.put(destination, GraphNodeColor.BLACK );
//                           // and set finish time
//                           node2finishTime.put(destination, time);
//                           time=time+1;
                           break DISCOVERNODE;
                       case ABORT:
                           return; // abort further traversing (e.g. if answer found)
                       case TRAVERSE:  // ok to traverse edge and its destination node
                           // we traverse a tree edge
                           // so push it back on the stack because we need to backtrack it
                           // later when we doing traversing it
                           edgeStack.push(pair);

                           // process the target node: process its children
                           algorithm.preProcessNode(destination);
                           // enqueue new edges on edge stack to be discovered from there and processed
                           for ( T transition : model.getModelTransitions(destination)) {
                               if (traversed.contains(transition) ) {
                                   // undirected edge can be traveled in possibly two directions,
                                   // but already traveled, then don't add the other direction
                                   continue;
                               }
                               Pair<L,T> node_edge_pair = new Pair<>(destination,transition);
                               edgeStack.push(node_edge_pair);
                           }
                           break DISCOVERNODE;
                       default:
                           throw new RuntimeException("BUG: should not happen");
                       }
                       continue EDGESTACK;
                  case GRAY: // traverse edge to earlier traversed but not backtracked node

                      // is a back edge
                      // we do not really traverse a back edge because destination already earlier traversed
                      algorithm.processBackEdge(source,edge,destination);

                      // for undirected graphs only:
                      // store as finished because in undirected graph they are scheduled twice on stack
                      // once for each possible direction; A back edge only needs to be processed once.
                      finished.add(edge);

                      continue EDGESTACK;
                  case BLACK:  // traverse edge to already fully processed node

                      // is either a forward or a cross edge
                      // we do not really traverse a forward/cross edge because destination already earlier traversed
                      // note: when depth first traveling an undirected graph we don't have forward/cross edges
                      //       because then we would already processed the edge earlier from the other side because
                      //       edges in undirected graph are undirectional which means bidirectional!!

                      // process forward/cross edge
                      if (  node2discoveryTime.get(source) <  node2discoveryTime.get(destination)) {
                           // process forward edge ( destination discovered after source -> point forward in dfs tree which is reachable from source)
                          algorithm.processForwardEdge(source,edge,destination);
                      } else {
                          // process cross edge ( destination discovered before source -> point to other branch in dfs tree or other dfs tree)
                          algorithm.processCrossEdge(source,edge,destination);
                      }

                      continue EDGESTACK;
                  }
              default:
                  throw new RuntimeException("BUG: should not happen");
              }
          } else {  // BACKTRACK
              // edge already processed traversing it forward
              // so now handle traversing it backwards

              // for undirected graphs only:
              // in undirected graph we have a choice in the direction in howto traverse an edge,
              // but once traversed in one direction we stick to that direction.
              // When traversing a node x we  push all edges on the stack, e.g. x, x--y, however it could be
              // that deeper in the traversal we reach y and we process there the same edge as
              // as a back edge y,y--x from y and it is popped from stack. So if we later
              //  pop x, x--y from the stack, then this edge is already dealed with in the traversal
              // and can be ignored.
              if (finished.contains(edge) ) {
                    //skip edge; already fully processed (as back edge)
                    continue EDGESTACK;
              }

              // finish target node
              node2color.put(destination, GraphNodeColor.BLACK );
              node2finishTime.put(destination, time);
              time=time+1;

              // post process node
              algorithm.postProcessNode(destination);
              // syntax sugar which is not really needed because could also do action in backTrackTreeEdge
              // however when only looking at nodes when traversing this extra method makes it more clearer which is convenient

              // backtrack edge from edge's target node to its source node
              algorithm.backTrackTreeEdge(source,edge,destination);
          }
      }
      // finish initial node
      node2color.put(initialNode, GraphNodeColor.BLACK );
      node2finishTime.put(initialNode, time);
      time=time+1;
      algorithm.postProcessNode(initialNode);

      algorithm.postProcess(node2discoveryTime,node2finishTime);

    }


}
