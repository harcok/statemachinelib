package statemachine.model.algorithm;

import java.util.HashSet;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.BreadthFirstGraphTraversal;
import statemachine.model.elements.transition.GenericGraphTransition;
import statemachine.model.graphs.digraph.conversion.Conversion;
import statemachine.model.graphs.graph.GenericMultiGraphModel;


public class DetermineWCC<L extends ModelLocation,UT extends ModelTransition<L>>
    extends BaseGraphTraversalAlgorithm<L,UT> {

    HashSet<HashSet<L>> weaklyConnectedComponents;
    HashSet<L> weaklyConnectedComponent;

    public DetermineWCC() {
        this.weaklyConnectedComponents = new HashSet<>();
    }



    @Override
    public void startTreeNode(L node) {

        weaklyConnectedComponent= new HashSet<>();
        weaklyConnectedComponents.add(weaklyConnectedComponent);
    }

    @Override
    public void preProcessNode(L node) {
        weaklyConnectedComponent.add(node);
    }


//    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
//    HashSet<HashSet<L>> execute1(ModelAutomaton<L,T> model) {
//
//             // convert directed model to undirected model
//             GenericMultiGraphModel<L, GenericGraphTransition<L>> newmodel = Conversion.DirectedAutomatonAsUndirectedGraph(model,GenericGraphTransition::new);
//             DetermineWCC<L,GenericGraphTransition<L>> algorithm = new DetermineWCC<>();
//             BreadthFirstGraphTraversal.traverseAll(newmodel, algorithm);
//             return algorithm.weaklyConnectedComponents;
//    }
//
//    static public <L extends ModelLocation,T extends ModelUndirectedTransition<L>>
//    HashSet<HashSet<L>> execute2(ModelAutomaton<L,T> model) {
//
//        DetermineWCC<L,T> algorithm = new DetermineWCC<>();
//        BreadthFirstGraphTraversal.traverseAll(model, algorithm);
//        return algorithm.weaklyConnectedComponents;
//    }

//     static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
//     HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model) {
//
//         T transition=model.getModelTransitions().iterator().next();
//         if ( transition instanceof ModelDirectedTransition<?>) {
//              // convert directed model to undirected model
//              GenericMultiGraphModel<L, GenericGraphTransition<L>> newmodel = Conversion.DirectedAutomatonAsUndirectedGraph(model,GenericGraphTransition::new);
//              DetermineWCC<L,GenericGraphTransition<L>> algorithm = new DetermineWCC<>();
//              BreadthFirstGraphTraversal.traverseAll(newmodel, algorithm);
//              return algorithm.weaklyConnectedComponents;
//         } else {
//             DetermineWCC<L,T> algorithm = new DetermineWCC<>();
//             BreadthFirstGraphTraversal.traverseAll(model, algorithm);
//             return algorithm.weaklyConnectedComponents;
//         }
//     }


    /** determine all weak connected components in a model **/
    static public <L extends ModelLocation,T extends ModelTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model) {

        T transition=model.getModelTransitions().iterator().next();
        if ( transition instanceof ModelDirectedTransition<?>) {
             // convert directed model to undirected model
             // first cast model to model with transitions of type ModelDirectedTransition
             @SuppressWarnings("unchecked")
             ModelAutomaton<L,ModelDirectedTransition<L>> castmodel = (ModelAutomaton<L, ModelDirectedTransition<L>>) model;
             // then convert directed model to an undirected model
             GenericMultiGraphModel<L, GenericGraphTransition<L>> newmodel = Conversion.convertDirectedAutomatonToUndirectedGraph(castmodel,GenericGraphTransition::new);
             // and finally determine WCC on undirected model (because to determine wcc we need to do bfs on undirected model)
             DetermineWCC<L,GenericGraphTransition<L>> algorithm = new DetermineWCC<>();
             BreadthFirstGraphTraversal.traverseAll(newmodel, algorithm);
             return algorithm.weaklyConnectedComponents;
        } else {
            DetermineWCC<L,T> algorithm = new DetermineWCC<>();
            BreadthFirstGraphTraversal.traverseAll(model, algorithm);
            return algorithm.weaklyConnectedComponents;
        }
    }

     /** determine the weak connected components in a model which must be connected to an initial location **/
     static public <L extends ModelLocation,T extends ModelTransition<L>>
         HashSet<L> execute(ModelAutomaton<L,T> model, L initialLocation) {

         T transition=model.getModelTransitions().iterator().next();
         if ( transition instanceof ModelDirectedTransition<?>) {
              // convert directed model to undirected model
              // first cast model to model with transitions of type ModelDirectedTransition
              @SuppressWarnings("unchecked")
              ModelAutomaton<L,ModelDirectedTransition<L>> castmodel = (ModelAutomaton<L, ModelDirectedTransition<L>>) model;
              // then convert directed model to an undirected model
              GenericMultiGraphModel<L, GenericGraphTransition<L>> newmodel = Conversion.convertDirectedAutomatonToUndirectedGraph(castmodel,GenericGraphTransition::new);
              // and finally determine WCC on undirected model (because to determine wcc we need to do bfs on undirected model)
              DetermineWCC<L,GenericGraphTransition<L>> algorithm = new DetermineWCC<>();
              BreadthFirstGraphTraversal.traverse(newmodel,initialLocation, algorithm);
              return algorithm.weaklyConnectedComponent;
         } else {
             DetermineWCC<L,T> algorithm = new DetermineWCC<>();
             BreadthFirstGraphTraversal.traverse(model, initialLocation,algorithm);
             return algorithm.weaklyConnectedComponent;
         }
     }
}
