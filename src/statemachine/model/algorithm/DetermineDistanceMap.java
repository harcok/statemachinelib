package statemachine.model.algorithm;

import java.util.HashMap;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.BreadthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.TraverseStyle;


/** Determine a distance map for nodes relative to an initial node.
 * <p>
 * If doing a breadth first search we can stepwise find the distances of the visited nodes from the initial
 * node where we start search.
 */
public  class DetermineDistanceMap<L extends ModelLocation,T extends ModelTransition<L>>
extends BaseGraphTraversalAlgorithm<L,T> {

    private HashMap<L,Integer> location2distance;

    @Override
    public void startTreeNode(L node) {
        location2distance.put(node, 0);
    }

    @Override
    public void traverseTreeEdge(L source, T edge, L destination) {
        int srcDistance=location2distance.get(source);
        location2distance.put(destination, srcDistance+1);
    }


    public DetermineDistanceMap() {
        location2distance = new HashMap<>();
    }

    /** Create a traverse tree for given automaton which must be connected to an initial location.
     * <p>
     * Each node in the traverse tree contains the location corresponding to that node and
     * the automaton's transition which is followed to reach that node from its parent node.
     * <p>
     * When generating to dot the transition's label is put on the node's incoming edge
     * and its source and destination on the parent and child node of the tree edge.
     */
    static public  <L extends ModelLocation, T extends ModelTransition<L>>
        HashMap<L,Integer> execute(ModelAutomaton<L,T> model, L initialNode) {

        DetermineDistanceMap<L,T> algorithm = new DetermineDistanceMap<>();
        model.getLocations().stream().forEach( node -> algorithm.location2distance.put(node, -1) );

        // do bfs traverse from initial location
        BreadthFirstGraphTraversal.traverse(model, initialNode, algorithm);

        return algorithm.location2distance;
    }

}
