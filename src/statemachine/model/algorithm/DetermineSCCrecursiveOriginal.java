package statemachine.model.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNull;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.elements.location.LocationState;

/** Implementation of Tarjan's recursive algorithm to determine the strongly connected components in an automaton.
 *  See https://en.wikipedia.org/wiki/Tarjan's_strongly_connected_components_algorithm .
 */
public class DetermineSCCrecursiveOriginal<L extends ModelLocation,T extends ModelDirectedTransition<L>> {

    // final result
    private HashSet<HashSet<L>> stronglyConnectedComponents;

    private HashMap<L,Integer> node2index;
    private HashMap<L,Integer>  node2lowlink;
    private int currentIndex;

    // Stack: contains(node) is O(n) , push and pop are O(1)) ;  preserves order
    private Stack<L> stack;

    // HashSet: contains(node), push and pop are O(1)) ; doesn't  preserves order
    //      This class offers constant time performance for
    //      the basic operations (add, remove, contains and size).
    //      Doesn't  preserves order.
    private HashSet<L> nodeOnStack;


    private DetermineSCCrecursiveOriginal(ModelAutomaton<L,T> model) {
        currentIndex = -1;
        node2index = new  HashMap<>();
        node2lowlink = new  HashMap<>();
        stack = new Stack<>();
        nodeOnStack = new HashSet<>();
        stronglyConnectedComponents=new HashSet<>();
        for ( L v : model.getLocations() ) {
            if (!node2index.containsKey(v)) strongConnect(model, v);
        }
    }

    private DetermineSCCrecursiveOriginal(ModelAutomaton<L,T> model, L initialLocation) {
        currentIndex = -1;
        node2index = new  HashMap<>();
        node2lowlink = new  HashMap<>();
        stack = new Stack<>();
        nodeOnStack = new HashSet<>();
        stronglyConnectedComponents=new HashSet<>();
        strongConnect(model, initialLocation);
    }

    /** determine all strong connected components in a model */
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model) {

        DetermineSCCrecursiveOriginal<L,T> algorithm = new DetermineSCCrecursiveOriginal<>(model);
        return algorithm.stronglyConnectedComponents;
    }

    /** determine all strong connected components in a model which must be connected to an initial location */
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model, L initialLocation) {
        DetermineSCCrecursiveOriginal<L,T> algorithm = new DetermineSCCrecursiveOriginal<>(model,initialLocation);
        return algorithm.stronglyConnectedComponents;
    }


    private void strongConnect(ModelAutomaton<L,T> model, L v)  {
        // Set the depth index for v to the smallest unused index
        currentIndex = currentIndex + 1;
        node2index.put(v, currentIndex);
        node2lowlink.put(v, currentIndex);

        stack.push(v);
        nodeOnStack.add(v);

        // Consider successors of v
        for (  @NonNull T transition :model.getModelTransitions(v) ) {

            L  w= transition.getDestination();
            int minLowLink;
            if ( node2index.containsKey(w) )  {
                // Successor w has already been visited

                // If successor w is not on stack, then v -> w is a cross-edge in the DFS tree and must be ignored
                //if (! stack.contains(w)) continue;  => O(n)
                if (! nodeOnStack.contains(w)) continue; // O(1)


                // Successor w is in stack S and hence in the current SCC  : v->w is a backEdge
                // Note: The next line may look odd - but is correct.
                // It says w.index not w.lowlink; that is deliberate and from the original paper
                minLowLink = java.lang.Math.min(node2lowlink.get(v),node2index.get(w));
            } else {
                // Successor w has not yet been visited; recurse on it
                strongConnect(model,w);
                // take minimal of current node or that found by depth first search
                minLowLink= java.lang.Math.min(node2lowlink.get(v),node2lowlink.get(w));
            }
            node2lowlink.put(v,minLowLink);
        }

        // If v is a root node, pop the stack and generate an SCC
        if (node2lowlink.get(v).equals(node2index.get(v)) ) {

            //start a new strongly connected component
            HashSet<L> connectedComponent = new HashSet<>();
            L node = stack.pop();
            nodeOnStack.remove(node);  // exclude as backlink
            while ( !node.equals(v) ) {
                connectedComponent.add(node);
                node = stack.pop();
                nodeOnStack.remove(node); // exclude as backlink
            }
            connectedComponent.add(v);
            stronglyConnectedComponents.add(connectedComponent);
        }

    }

}

