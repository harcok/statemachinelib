package statemachine.model.algorithm;

import java.util.HashMap;

import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.BreadthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.DepthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.GraphTraverseInstruction;
import statemachine.model.elements.action.Action;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.lts.LtsNonDetModel;

/** Traverse an automaton depth first from a given initial state.
 *
 *  Return label transition model which describes how depth first graph
 *  traversal is done through automaton model.
 */
public  class DetermineTraversalAnnotatedInLts<L, T>
        extends BaseGraphTraversalAlgorithm<L,T> {


    public HashMap<L, Integer> node2discoveryTime,node2finishTime;

    public HashMap<T,String> edge2label=new HashMap<>();
    private HashMap<T, Pair<L,L>> edge2direction=new HashMap<>();  // direction in which we traverse edge (only really needed to store for undirected edges)

    @Override
    public GraphTraverseInstruction discoverEdge(L source, T edge, L destination) {
        edge2direction.put(edge, new Pair<>(source,destination));   // direction in which we traverse edge (only really needed to store for undirected edges)
        return defaultGraphTraverseInstruction;
    }

    @Override
    public void traverseTreeEdge(L source, T edge, L destination) {
        edge2label.put(edge, "Tree");
    }

    @Override
    public void processBackEdge(L source, T edge, L destination) {
        edge2label.put(edge, "Back");
    }

    @Override
    public void processForwardEdge(L source, T edge, L destination) {
        edge2label.put(edge, "Forward");
    }

    @Override
    public void processCrossEdge(L source, T edge, L destination) {
        edge2label.put(edge, "Cross");
    }

    @Override
    public void postProcess(HashMap<L, Integer> node2discoveryTime, HashMap<L, Integer> node2finishTime) {
        this.node2discoveryTime= node2discoveryTime;
        this.node2finishTime=node2finishTime;
    }

    /** Traverse an automaton depth first from a given initial state.
    *
    *  Return label transition model which describes how depth first graph
    *  traversal is done through automaton model.
    */
    static public  <L extends ModelLocation, T extends ModelTransition<L> >
    LtsNonDetModel execute(ModelAutomaton<L,T> model , L initialLocation, boolean traverseAll) {
        DetermineTraversalAnnotatedInLts<L,T> algorithm = new DetermineTraversalAnnotatedInLts<>();
        if ( traverseAll) {
            // traverse from initial location, and continue with unseen locations
            DepthFirstGraphTraversal.traverseAll(model, initialLocation, algorithm);
        } else {
            // only traverse from initial location
            DepthFirstGraphTraversal.traverse(model, initialLocation, algorithm);
        }
        return buildLts( model,initialLocation,  algorithm );
    }

    /** Traverse an automaton depth first from a given initial state.
    *
    *  Return label transition model which describes how depth first graph
    *  traversal is done through automaton model.
    */
    static public  <L extends ModelLocation, T extends ModelTransition<L> >
    LtsNonDetModel executeBFS(ModelAutomaton<L,T> model , L initialLocation, boolean traverseAll) {
        DetermineTraversalAnnotatedInLts<L,T> algorithm = new DetermineTraversalAnnotatedInLts<>();
        if ( traverseAll) {
            // traverse from initial location, and continue with unseen locations
            BreadthFirstGraphTraversal.traverseAll(model, initialLocation, algorithm);
        } else {
            // only traverse from initial location
            BreadthFirstGraphTraversal.traverse(model, initialLocation, algorithm);
        }
        return buildLts( model,initialLocation,  algorithm );
    }

    static private   <L extends ModelLocation, T extends ModelTransition<L> >
       LtsNonDetModel buildLts(ModelAutomaton<L,T> model, L initialNode, DetermineTraversalAnnotatedInLts<L,T> algorithm ) {

        LtsNonDetModel.ImmutableBuilder builder=new LtsNonDetModel.ImmutableBuilder();
        HashMap<L,LocationState> oldnode2newnode = new HashMap<>();
        for (  L location: model.getLocations()) {
            String times_suffix="";
            if ( algorithm.node2discoveryTime.containsKey(location)) {
                times_suffix= " " +algorithm.node2discoveryTime.get(location).toString() + "/" +
                    algorithm.node2finishTime.get(location).toString();
            }
            LocationState newloc =new LocationState(location.toString() + times_suffix);
            oldnode2newnode.put(location, newloc);
        }
        for ( T transition: model.getModelTransitions()) {
            String label=algorithm.edge2label.get(transition);
            if (label==null) label="";//"NotTraveled";
            Pair<L, L> srcDest;
            if ( algorithm.edge2direction.containsKey(transition)) {
                srcDest = algorithm.edge2direction.get(transition);
            } else {
                //"NotTraveled"; just take any order
                srcDest = transition.getLocations();
            }
            LocationState src = oldnode2newnode.get(srcDest.getValue0());
            LocationState dst = oldnode2newnode.get(srcDest.getValue1());
            Action action = new Action(label);
            builder.addTransition(src, dst, action);
        }
        builder.setStartLocation(oldnode2newnode.get(initialNode));
        return builder.build();
    }

}
