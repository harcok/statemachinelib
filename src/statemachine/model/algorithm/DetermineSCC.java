package statemachine.model.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.DepthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.GraphTraverseInstruction;


/** Implementation of Tarjan's algorithm to determine the strongly connected components in an automaton.
 *  This implementation uses instead of Tarjan's recursive approach an iterative approach using graph traversal.
 *  The original recursive approach could give an stack overflow for too large models in java. This implementation however
 *  doesn't suffer from that problem.
 *  <br>
 *  See https://en.wikipedia.org/wiki/Tarjan's_strongly_connected_components_algorithm .
 */
public class DetermineSCC<L extends ModelLocation,T extends ModelDirectedTransition<L>>
             extends BaseGraphTraversalAlgorithm<L,T> {


    private HashMap<L,Integer> node2index;
    private HashMap<L,Integer>  node2lowlink;
    private int currentIndex;

    // Stack.contains(node)  : O(n) (push and pop are O(1)) ;  preserves order
    private Stack<L> stack;

    //HashSet: This class offers constant time performance for
    //         the basic operations (add, remove, contains and size),
    //        ; doesn't  preserves order
    private HashSet<L> nodeOnStack;

    HashSet<HashSet<L>> stronglyConnectedComponents;

    @Override
    public void preProcessNode(L node) {
        System.out.println("traverseNode: " + node);
        stack.push(node);
        nodeOnStack.add(node);
        node2index.put(node, currentIndex);
        node2lowlink.put(node, currentIndex);
        currentIndex=currentIndex+1;
    }

    @Override
    public void postProcessNode(L node) {

        if (node2index.get(node).equals(node2lowlink.get(node))) {
            HashSet<L> stronglyConnectedComponent=new HashSet<>();
            L poppedNode = stack.pop();
            nodeOnStack.remove(poppedNode);
            stronglyConnectedComponent.add(poppedNode);
            while ( poppedNode != node ) {
                poppedNode = stack.pop();
                nodeOnStack.remove(poppedNode);
                stronglyConnectedComponent.add(poppedNode);
            }
            stronglyConnectedComponents.add(stronglyConnectedComponent);
        }
    }



    @Override
    public void processBackEdge(L src, T edge, L dst) {
        // found an edge which points back in traversed tree
        // new lowlink is the minimal value of:
        //  - the current found minimal lowlink stored in src
        //  - or the index of dst
        int minLowLink = java.lang.Math.min(node2lowlink.get(src),node2index.get(dst));
        node2lowlink.put(src, minLowLink);
    }

    @Override
    public void processCrossEdge(L src, T edge, L dst) {
        if (nodeOnStack.contains(dst)) {
           int minLowLink = java.lang.Math.min(node2lowlink.get(src),node2lowlink.get(dst));
           node2lowlink.put(src, minLowLink);
        }
    }

    @Override
    public GraphTraverseInstruction backTrackTreeEdge(L src, T edge, L dst) {
        // backtrack edge: transfer a lower lowlink upwards in tree ; src is higher in the tree
        int minLowLink = java.lang.Math.min(node2lowlink.get(src),node2lowlink.get(dst));
        node2lowlink.put(src, minLowLink);
        return GraphTraverseInstruction.TRAVERSE;
    }

    private DetermineSCC() {
        super();
        currentIndex=0;
        node2index=new HashMap<>();
        node2lowlink=new HashMap<>();
        stronglyConnectedComponents=new HashSet<>();
        stack=new Stack<>();
        nodeOnStack=new HashSet<>();
    }



    /** determine all strong connected components in a model **/
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model) {

           // repeat depth first traversal for not yet visited (indexed) locations
           DetermineSCC<L,T> algorithm = new DetermineSCC<>();
           DepthFirstGraphTraversal.traverseAll(model, algorithm);
           return algorithm.stronglyConnectedComponents;
    }

    /** determine all strong connected components in a model which must be connected to an initial location **/
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model, L initialLocation) {

            DetermineSCC<L,T> algorithm = new DetermineSCC<>();
            DepthFirstGraphTraversal.traverse(model, initialLocation, algorithm);
            return algorithm.stronglyConnectedComponents;
    }

}
