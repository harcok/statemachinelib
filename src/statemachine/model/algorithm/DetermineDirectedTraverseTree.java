
package statemachine.model.algorithm;

import java.util.ArrayList;
import java.util.List;

import nl.ru.cs.tree.Tree;
import nl.ru.cs.tree.TreeNode;
import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.algorithm.traversal.BaseGraphTraversalAlgorithm;
import statemachine.model.algorithm.traversal.BreadthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.DepthFirstGraphTraversal;
import statemachine.model.algorithm.traversal.GraphTraverseInstruction;
import statemachine.model.algorithm.traversal.TraverseStyle;
import statemachine.model.elements.transition.BaseTransition;


/** Create a traverse tree for given automaton with directed transitions which describes how
 *  breadth first or depth first traversal walk through the given automaton.
 * <p>
 * Each node in the traverse tree contains the automaton's directed transition which is used
 * to reach that node from its parent node.
 * <p>
 *  Note that traverse tree only storing transitions in its nodes can only work with directed
 *  transitions because with undirected transitions the direction in the tree would be missing.
 * <p>
 * When generating dot the transition's label is put on the node's incoming edge
 * and its source and destination on the parent and child node of the tree edge.
 */
public  class DetermineDirectedTraverseTree<L extends ModelLocation,T extends ModelDirectedTransition<L>>
       extends BaseGraphTraversalAlgorithm<L,T> {

    private TreeNode<T> current ;
    private ArrayList<Tree<T>> trees;

    @Override
    public void startTreeNode(L node) {
        current= new TreeNode<T>( null );
        trees.add(new Tree<T>(current));
    }

    @Override
    public void traverseTreeEdge(L source, T edge, L destination) {
        TreeNode<T> newNode=new TreeNode<T>(edge);
        current.addChild(newNode);
        current=newNode;
    }


    @Override
    public GraphTraverseInstruction backTrackTreeEdge(L source, T edge, L destination) {
        current=current.getParent();
        return GraphTraverseInstruction.TRAVERSE;
    }


    public DetermineDirectedTraverseTree() {
        trees = new ArrayList<>();
    }



    /** Create a traverse tree for given automaton which must be connected to an initial location.
     * <p>
     * Each node in the traverse tree contains the automaton's transition which is followed
     * to reach that node from its parent node.
     */
     static public  <L extends ModelLocation, T extends ModelDirectedTransition<L>>
             Tree<T> execute(ModelAutomaton<L,T> model, L initialNode, TraverseStyle traverseStyle) {

        DetermineDirectedTraverseTree<L,T> algorithm = new DetermineDirectedTraverseTree<>();

        // only traverse from initial location
        switch (traverseStyle) {
        case DFS:
            DepthFirstGraphTraversal.traverse(model, initialNode, algorithm);
            break;
        case BFS:
            BreadthFirstGraphTraversal.traverse(model, initialNode, algorithm);
            break;
        }
        return algorithm.trees.get(0);
    }

     /** Create a traverse tree for given automaton which explores all locations of the automaton.
      * <p>
      * Each node in the traverse tree contains the automaton's transition which is followed
      * to reach that node from its parent node.
      */
     static public  <L extends ModelLocation, T extends ModelDirectedTransition<L>>
             List<Tree<T>> executeAll(ModelAutomaton<L,T> model, L initialNode, TraverseStyle traverseStyle) {

        DetermineDirectedTraverseTree<L,T> algorithm = new DetermineDirectedTraverseTree<>();
        // traverse from initial location, and continue with unseen locations
        switch (traverseStyle) {
        case DFS:
            DepthFirstGraphTraversal.traverseAll(model, initialNode, algorithm);
            break;
        case BFS:
            BreadthFirstGraphTraversal.traverseAll(model, initialNode, algorithm);
            break;
        }
        return algorithm.trees;
    }


     /** Create dot file for traverse tree.
       * <p>
       * When generating dot the transition's label is put on the node's incoming edge
       * and its source and destination on the parent and child node of the tree edge.
       */
     @SuppressWarnings("unchecked")
     static public  <L extends ModelLocation, T extends ModelDirectedTransition<L>>
     void TraverseTreeToDot(Tree<T> tree,String filepath) {

          TreeNode<T> treeroot = tree.getRoot();
          if ( treeroot.hasChildren() )  {
              // hack: create fake BaseTransition in root node which contains destination location for root node
              BaseTransition<L> newRootData = new BaseTransition<>(null,treeroot.getChildAt(0).getData().getSource());
              treeroot.setData((T) newRootData);
              nl.ru.cs.tree.file.Export.tree2dotfile(filepath, tree, e->e.getLabel(), e->e.getDestination().toString());
           }
     }



}
