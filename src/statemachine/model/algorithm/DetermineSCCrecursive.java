package statemachine.model.algorithm;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import org.eclipse.jdt.annotation.NonNull;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelTransition;
import statemachine.interfaces.model.types.ModelAutomaton;

/** Implementation of Tarjan's recursive algorithm to determine the strongly connected components in an automaton.
 *  See https://en.wikipedia.org/wiki/Tarjan's_strongly_connected_components_algorithm .
 *  <p>
 *  This is an improved version of DetermineSCCrecursiveOriginal.
 *  It excludes nodes from already found connected components  by
 *  set their lowlink to a maximal value 'excludeIndex' (=model.locations.size())
 *  instead of excluding it when its node is not on stack anymore.
 *  Which saves you from keeping a nodeOnStack with keeping the same operation cost.
 *  See https://algs4.cs.princeton.edu/42digraph/TarjanSCC.java
 */
public class DetermineSCCrecursive<L extends ModelLocation,T extends ModelDirectedTransition<L>> {

    // final result
    private HashSet<HashSet<L>> stronglyConnectedComponents;

    private HashMap<L,Integer> node2index;
    private HashMap<L,Integer>  node2lowlink;
    private int currentIndex;
    private int excludeIndex;

    // Stack :  contains(node) is O(n) , push and pop are O(1)) ;  preserves order
    private Stack<L> stack;


    private DetermineSCCrecursive(int numberLocations) {
        currentIndex = -1;
        node2index = new  HashMap<>();
        node2lowlink = new  HashMap<>();
        stack = new Stack<>();
        stronglyConnectedComponents=new HashSet<>();
        excludeIndex = numberLocations;
    }


    /** determine all strong connected components in a model */
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model) {

        DetermineSCCrecursive<L,T> algorithm = new DetermineSCCrecursive<>(model.getLocations().size());
        for ( L v : model.getLocations() ) {
            if (!algorithm.node2index.containsKey(v)) algorithm.strongConnect(model, v);
        }
        return algorithm.stronglyConnectedComponents;
    }

    /** determine all strong connected components in a model which must be connected to an initial location */
    static public <L extends ModelLocation,T extends ModelDirectedTransition<L>>
        HashSet<HashSet<L>> execute(ModelAutomaton<L,T> model, L initialLocation) {

        DetermineSCCrecursive<L,T> algorithm = new DetermineSCCrecursive<>(model.getLocations().size());
        algorithm.strongConnect(model, initialLocation);
        return algorithm.stronglyConnectedComponents;
    }

    private void strongConnect(ModelAutomaton<L,T> model, L v)  {
        // Set the depth index for v to the smallest unused index
        currentIndex = currentIndex + 1;
        node2index.put(v, currentIndex);
        node2lowlink.put(v, currentIndex);
        stack.push(v);

         // Consider successors of v
        int min=node2index.get(v);
        for (  @NonNull T transition :model.getModelTransitions(v) ) {
            L  w= transition.getDestination();
            if ( !node2index.containsKey(w) )  strongConnect(model,w);
            min= java.lang.Math.min(min,node2lowlink.get(w)); //take minimal of search
        }
        node2lowlink.put(v, java.lang.Math.min(min,node2index.get(v)));


        // If v is a root node, pop the stack and generate an SCC
        if (node2lowlink.get(v).equals(node2index.get(v)) ) {

            //start a new strongly connected component
            HashSet<L> connectedComponent = new HashSet<>();
            L node = stack.pop();
            node2lowlink.put(node,excludeIndex);  // exclude as backlink => same as checking not on stack
            while ( !node.equals(v) ) {
                connectedComponent.add(node);
                node = stack.pop();
                node2lowlink.put(node,excludeIndex); // exclude as backlink => same as checking not on stack
            }
            connectedComponent.add(v);
            stronglyConnectedComponents.add(connectedComponent);
        }
    }


}

