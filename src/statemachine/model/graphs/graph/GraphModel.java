package statemachine.model.graphs.graph;

import java.util.HashSet;

import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;


/** GraphModels are undirected graphs that have
 *  - loops (transitions that connect locations to themselves)
 *  - but no multiple directed transitions with same source and target nodes
 */
public class GraphModel extends GraphBaseModel {

    // Constructor
    private GraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public GraphModel fromGraphModel(GraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromGraphModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends GraphBaseModel.ImmutableBuilder {

        private HashSet<Pair<LocationState,LocationState>> srcDestPairs = new HashSet<>();

        public GraphModel build() {
            super.baseBuild();
            return new GraphModel(this);
        }

        @Override
        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                throw new RuntimeException(
                        "error in GraphModel model; no two transitions with same source and destination allowed; use a MultiGraphModel instead."  );

            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

    }

}
