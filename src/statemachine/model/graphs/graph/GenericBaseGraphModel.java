package statemachine.model.graphs.graph;


import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;

public abstract class GenericBaseGraphModel<L extends ModelLocation,T extends ModelUndirectedTransition<L>>
             implements ModelAutomaton<L,T> {

    private final ImmutableSet<L> locations;
    private final ImmutableSet<T> transitions;
    protected final ImmutableSetMultimap<L, T> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder<L extends ModelLocation,T extends ModelUndirectedTransition<L>,SUBCLASS extends ImmutableBuilder<?,?,?>> {

        protected ImmutableSet<L> locations;
        protected ImmutableSet<T> transitions;
        protected ImmutableSetMultimap<L, T> loc2trans;
        protected UnlabeledTransitionConstructor<L, T> transitionConstructor;

        // constructor getting transition constructor
        ImmutableBuilder (UnlabeledTransitionConstructor<L,T> transitionConstructor) {
            this.transitionConstructor = transitionConstructor;
        }

        // builders
        protected ImmutableSet.Builder<L> locationsBuilder = ImmutableSet.builder();

        protected ImmutableSet.Builder<T> transitionsBuilder = ImmutableSet.builder();


        // store transitions per location
        protected ImmutableSetMultimap.Builder<L, T> loc2transBuilder = ImmutableSetMultimap.builder();

        public SUBCLASS addTransition(L one, L two) {
            this.locationsBuilder.add(one);
            this.locationsBuilder.add(two);
            T transition = transitionConstructor.construct(one, two);
            this.transitionsBuilder.add(transition);
            // add transition to both sides, because transition is undirected
            this.loc2transBuilder.put(one, transition);
            this.loc2transBuilder.put(two, transition);
            @SuppressWarnings({ "unchecked" }) SUBCLASS subclass =(SUBCLASS)this; return subclass;
        }

        protected  SUBCLASS  addFromGraphModel(ModelAutomaton<L,T> model) {
            for (T transition : model.getModelTransitions()) {
                Pair<L,L> pair= transition.getLocations();
                this.addTransition(pair.getValue0(), pair.getValue1());
            }
            @SuppressWarnings({ "unchecked" }) SUBCLASS subclass =(SUBCLASS)this; return subclass;
        }

        protected void baseBuild() {
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }

    }

    // Constructor

    protected GenericBaseGraphModel(ImmutableBuilder<L,T,?> builder) {
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }

    // interface ModelAutomaton

    @Override
    public @NonNull ImmutableSet<@NonNull L> getLocations() {
        return locations;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull T> getModelTransitions() {
        return transitions;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull T> getModelTransitions(@NonNull L location) {
        return loc2trans.get(location);
    }


    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("locations:");
        for (L location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (T transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }

}
