package statemachine.model.graphs.graph;

import java.util.HashSet;

import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;


/** SimpleGraphModels are undirected graphs that have
 *  - no loops (transitions that connect locations to themselves)
 *  - but no multiple directed transitions with same source and target nodes
 */
public class SimpleGraphModel extends GraphBaseModel {

    // Constructor
    private SimpleGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public SimpleGraphModel fromGraphModel(GraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromGraphModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends GraphBaseModel.ImmutableBuilder {

        private HashSet<Pair<LocationState,LocationState>> srcDestPairs = new HashSet<>();

        public SimpleGraphModel build() {
            super.baseBuild();
            return new SimpleGraphModel(this);
        }

        @Override
        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {



            if ( src.equals(dst)) {
                throw new RuntimeException(
                        "error in SimpleGraphModel model; no loops allowed; found loop in state: " + src );
            }

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                throw new RuntimeException(
                        "error in SimpleGraphModel model; no two transitions with same source and destination allowed; use a MultiGraphModel instead."  );

            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

    }

}