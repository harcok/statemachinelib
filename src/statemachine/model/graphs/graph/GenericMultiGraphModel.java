package statemachine.model.graphs.graph;


import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;

/** MultiGraphModels are undirected graphs that have
 *  - loops (arrows that connect vertices to themselves)
 *  - and may have multiple undirected edges with same nodes in edge
 */
@NonNullByDefault
public class GenericMultiGraphModel<L extends ModelLocation,T extends ModelUndirectedTransition<L>>  extends GenericBaseGraphModel<L,T> {

    // Constructor
    protected GenericMultiGraphModel(ImmutableBuilder<L,T> builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public <L extends ModelLocation,T extends ModelUndirectedTransition<L>>
        GenericMultiGraphModel<L,T> fromGraphModel(ModelAutomaton<L,T> model) {
        ImmutableBuilder<L,T> builder = new ImmutableBuilder<L,T>(null);
        builder.addFromGraphModel(model);
        return builder.build();
    }

    //Builder class
    public static class ImmutableBuilder<L extends ModelLocation,T extends ModelUndirectedTransition<L>>
           extends GenericBaseGraphModel.ImmutableBuilder<L,T,ImmutableBuilder<L,T>> {

        public ImmutableBuilder(UnlabeledTransitionConstructor<L,T> transitionConstructor) {
            super(transitionConstructor);
        }

        public GenericMultiGraphModel<L,T> build() {
            super.baseBuild();
            return new GenericMultiGraphModel<>(this);
        }
    }
}


