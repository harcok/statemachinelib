package statemachine.model.graphs.graph;




import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;

/**
 * GraphModel models a undirected graph which is equivalent to an unlabeled transition system with undirected transitions and an initial state
 *
 */
public interface GraphModelInterface
       extends ModelAutomaton<LocationState,GraphTransition>
{

}
