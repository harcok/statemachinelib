package statemachine.model.graphs.graph.format;


import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;
import statemachine.model.graphs.graph.GraphModel;
import statemachine.model.graphs.graph.GraphModelInterface;
import statemachine.model.graphs.graph.MultiGraphModel;
import statemachine.model.graphs.graph.SimpleGraphModel;


/*

graph g {

    __start0 [label="" shape="none"]

    1 [shape="circle" label="1"];
    2 [shape="circle" label="2"];
    1 - 2;
    2 - 1;
    2 - 1;
    1 - 1;

    __start0 -> 1;
}


 */


public class Dot {


    static public GraphModel importGraphModel( List<String> lines) {
        return GraphModel.fromGraphModel(importMultiGraphModel(lines));
    }

    static public SimpleGraphModel importSimpleGraphModel( List<String> lines) {
        return SimpleGraphModel.fromGraphModel(importMultiGraphModel(lines));
    }

    static public MultiGraphModel importMultiGraphModel( List<String> lines) {

        MultiGraphModel.ImmutableBuilder builder =new MultiGraphModel.ImmutableBuilder();
        for(String line: lines){

            // no start/initial state(s)/location(s) in a graph

            if(!line.contains("label")) continue;

            if(line.contains("--")){

                String label;

                if (line.contains("<")) {
                    int beginIndex=line.indexOf("<");
                    int lastIndex=line.lastIndexOf(">");
                    label=line.substring(beginIndex,lastIndex);
                    label=label.replaceAll("\\<[^>]*>","").trim();
                  } else {
                      int startLabel = line.indexOf('"');
                      int endLabel = line.lastIndexOf('"');
                      label = line.substring(startLabel+1, endLabel).trim();
                  }


                int e1 = line.indexOf('-');
                int e2 = line.indexOf('[');
                String from = line.substring(0, e1).trim();
                String to = line.substring(e1+2, e2).trim();


                LocationState src = new LocationState(from);
                LocationState dst = new LocationState(to);
                builder.addTransition(src, dst);
            }

        }
        return builder.build();

    }

    static public List<String> export(GraphModelInterface model) {
        String indent="    ";
        LinkedList<String> lines= new LinkedList<>();
        lines.add("graph g {");
        lines.add("");
        lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
        lines.add("");


        for ( LocationState location: model.getLocations()) {
          lines.add(indent + location + " [shape=\"circle\" label=\"" + location + "\"];");
        }
        for (@NonNull GraphTransition transition: model.getModelTransitions() ) {
            Pair<LocationState, LocationState> locations = transition.getLocations();
            lines.add(indent + locations.getValue0() + " --  " + locations.getValue1() + ";");
        }
        lines.add("}");
        return lines;
    }
}
