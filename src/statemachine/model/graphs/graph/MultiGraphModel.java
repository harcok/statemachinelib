package statemachine.model.graphs.graph;

import org.eclipse.jdt.annotation.NonNullByDefault;

/** MultiGraphModels are undirected graphs that have
 *  - loops (arrows that connect vertices to themselves)
 *  - and may have multiple undirected edges with same nodes in edge
 */
@NonNullByDefault
public class MultiGraphModel  extends GraphBaseModel {

    // Constructor
    private MultiGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public MultiGraphModel fromGraphModel(GraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromGraphModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends GraphBaseModel.ImmutableBuilder {

        public MultiGraphModel build() {
            super.baseBuild();
            return new MultiGraphModel(this);
        }
    }
}


