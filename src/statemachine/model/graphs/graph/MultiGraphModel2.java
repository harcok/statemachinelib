package statemachine.model.graphs.graph;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;

public class MultiGraphModel2 extends GenericMultiGraphModel<LocationState,GraphTransition> {

    // Constructor
    private MultiGraphModel2(ImmutableBuilder immutableBuilder) {
        super(immutableBuilder);
    }

    //Builder class
    public final static class ImmutableBuilder
           extends GenericMultiGraphModel.ImmutableBuilder<LocationState,GraphTransition> {

        public ImmutableBuilder() {
            super(GraphTransition::new);
        }

        public MultiGraphModel2 build() {
            super.baseBuild();
            return new MultiGraphModel2(this);
        }
    }
}
