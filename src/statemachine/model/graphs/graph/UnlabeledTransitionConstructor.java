package statemachine.model.graphs.graph;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;

@FunctionalInterface
public interface UnlabeledTransitionConstructor<L extends ModelLocation,T extends ModelUndirectedTransition<L>> {
       T construct(L one,L two);
}
