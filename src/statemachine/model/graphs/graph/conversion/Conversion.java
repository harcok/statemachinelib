package statemachine.model.graphs.graph.conversion;

import java.util.ArrayList;
import java.util.HashSet;

import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;
import statemachine.model.graphs.digraph.SymmetricMultiDiGraphModel;
import statemachine.model.graphs.graph.GraphModel;
import statemachine.model.graphs.graph.GraphModelInterface;

public class Conversion {


    /**  merge multiple transition with same location to single transition
     *
     * @param graph
     * @return GraphModel
     */
    public static GraphModel merge(GraphModelInterface graph) {
        GraphModel.ImmutableBuilder builder = new  GraphModel.ImmutableBuilder();
        HashSet<Pair<LocationState,LocationState>> pairs = new HashSet<>();
        for ( GraphTransition transition :graph.getModelTransitions() ) {
            Pair<LocationState,LocationState> pair= transition.getLocations();
            if (!pairs.contains(pair)) {
                pairs.add(pair);
                builder.addTransition(pair.getValue0(), pair.getValue1());
            }
        }
        return builder.build();
    }

    /** convert a undirected graph in a directed graph by
     *  converting each undirected transition from the graph into two opposite directed transitions in the digraph.
     *
     * @param graph
     * @return symmetric multi digraph
     */
    static public SymmetricMultiDiGraphModel graph2SymmetricDiGraph(GraphModelInterface graph) {
        SymmetricMultiDiGraphModel.ImmutableBuilder builder = new  SymmetricMultiDiGraphModel.ImmutableBuilder();
        for ( GraphTransition transition :graph.getModelTransitions() ) {
            Pair<LocationState,LocationState> pair= transition.getLocations();
            builder.addTransition(pair.getValue0(), pair.getValue1());
            builder.addTransition(pair.getValue1(), pair.getValue0());;
        }
        return builder.build(false); // don't need to verify because way constructed is symmetric
    }
}
