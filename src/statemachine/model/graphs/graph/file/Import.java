package statemachine.model.graphs.graph.file;

import java.util.List;

import statemachine.model.graphs.graph.MultiGraphModel;
import statemachine.model.graphs.graph.format.Dot;
import statemachine.util.IO;

public class Import {

	static public MultiGraphModel  dot2GraphModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			MultiGraphModel model= Dot.importMultiGraphModel(lines);
			return model;
	}

}
