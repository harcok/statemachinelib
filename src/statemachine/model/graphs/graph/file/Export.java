package statemachine.model.graphs.graph.file;

import java.util.List;

import statemachine.model.graphs.graph.GraphModelInterface;
import statemachine.model.graphs.graph.format.Dot;
import statemachine.util.IO;

public class Export {

    static public void  dot(GraphModelInterface model, String filename){
        List<String> lines=Dot.export(model);
        IO.writeSmallTextFile( lines, filename);
    }

}
