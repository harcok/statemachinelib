package statemachine.model.graphs.graph;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;


/**
 * GraphModel models a undirected graph which is equivalent to an unlabeled transition system with undirected transitions and an initial state
 *
 */
public interface GenericGraphModelInterface<L extends ModelLocation,T extends ModelUndirectedTransition<L>>
       extends ModelAutomaton<L,T>
{

}