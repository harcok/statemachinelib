package statemachine.model.graphs.graph;

import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;
import org.javatuples.Pair;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.GraphTransition;

public abstract class GraphBaseModel implements GraphModelInterface {

    private final ImmutableSet<LocationState> locations;
    private final ImmutableSet<GraphTransition> transitions;
    protected final ImmutableSetMultimap<LocationState, GraphTransition> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder {

        protected ImmutableSet<LocationState> locations;
        protected ImmutableSet<GraphTransition> transitions;
        protected ImmutableSetMultimap<LocationState, GraphTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<LocationState> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<GraphTransition> transitionsBuilder = ImmutableSet.builder();


        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<LocationState, GraphTransition> loc2transBuilder = ImmutableSetMultimap.builder();

        public ImmutableBuilder addTransition(LocationState one, LocationState two) {
            this.locationsBuilder.add(one, two);
            GraphTransition transition = new GraphTransition(one, two);
            this.transitionsBuilder.add(transition);
            // add transition to both sides, because transition is undirected
            this.loc2transBuilder.put(one, transition);
            this.loc2transBuilder.put(two, transition);
            return this;
        }

        protected ImmutableBuilder addFromGraphModel(GraphModelInterface model) {
            for (GraphTransition transition : model.getModelTransitions()) {
                Pair<LocationState,LocationState> pair= transition.getLocations();
                this.addTransition(pair.getValue0(), pair.getValue1());
            }
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            //this.startLocation = ...; // no builder for startlocation
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }
    }

    // Constructor

    protected GraphBaseModel(ImmutableBuilder builder) {
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }



    // interface ModelAutomaton

    @Override
    public @NonNull ImmutableSet<@NonNull LocationState> getLocations() {
        return locations;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull GraphTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull GraphTransition> getModelTransitions(@NonNull LocationState location) {
        return loc2trans.get(location);
    }


    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("locations:");
        for (LocationState location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (GraphTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }

}
