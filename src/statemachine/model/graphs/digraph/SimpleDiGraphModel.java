package statemachine.model.graphs.digraph;

import java.util.HashSet;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;

/** Simple directed graphs are directed graphs that have
 *  - no loops (transitions that connect locations to themselves)
 *  - no multiple directed transitions with same source and target nodes
 */
@NonNullByDefault
public final class SimpleDiGraphModel extends DiGraphBaseModel {

    // Constructor
    private SimpleDiGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public SimpleDiGraphModel fromDigraphModel(DiGraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromDiGraphModel(model);
        return builder.build();
    }


    //Builder class
    public final static class ImmutableBuilder extends DiGraphBaseModel.ImmutableBuilder {

        private HashSet<Pair<LocationState,LocationState>> srcDestPairs = new HashSet<>();

        @Override
        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {
            if ( src.equals(dst)) {
                throw new RuntimeException(
                        "error in SimpleDiGraphModel model; no loops allowed; found loop in state: " + src );
            }

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                    throw new RuntimeException(
                            "error in SimpleDiGraphModel model; no two transitions with same source and destination allowed; use a MultiDigraphModel instead."  );
            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

        public SimpleDiGraphModel build() {
            super.baseBuild();
            return new SimpleDiGraphModel(this);
        }
    }
}
