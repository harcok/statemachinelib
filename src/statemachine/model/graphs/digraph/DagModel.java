package statemachine.model.graphs.digraph;

import java.util.HashSet;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.model.algorithm.DetermineCycle;
import statemachine.model.elements.location.LocationState;
import statemachine.model.graphs.digraph.SimpleDiGraphModel.ImmutableBuilder;
import statemachine.model.graphs.digraph.util.Util;


/** DagModels are directed graphs that have
 *  - no cycle loops ( also no loops )
 *  - no multiple directed transitions with same source and target nodes  (with multiple is a MultiDagModel)
 */
@NonNullByDefault
public final class DagModel extends DiGraphBaseModel {

    // Constructor
    private DagModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public DagModel fromDigraphModel(DiGraphModelInterface model, boolean verify) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromDiGraphModel(model);
        return builder.build(verify);
    }

    //Builder class
    public final static class ImmutableBuilder extends DiGraphBaseModel.ImmutableBuilder {

        private HashSet<Pair<LocationState,LocationState>> srcDestPairs = new HashSet<>();

        /** Add transition, but throw exception if it is already added.
        *
        * @param src
        * @param dst
        * @return
        */
        @Override
        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {

            if ( src.equals(dst)) {
                throw new RuntimeException(
                        "error in DagModel model; no loops allowed; found loop in state: " + src );
            }

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                    throw new RuntimeException(
                            "error in DagModel model; no two transitions with same source and destination allowed; use a MultiDagModel instead."  );
            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

        /** Add transition, but don't throw exception if it is already added.
        *
        * @param src
        * @param dst
        * @return
        */
        public ImmutableBuilder addTransitionOrIgnoreIfAlreadyAdded(LocationState src, LocationState dst) {

            if ( src.equals(dst)) {
                throw new RuntimeException(
                        "error in DagModel model; no loops allowed; found loop in state: " + src );
            }

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                    throw new RuntimeException(
                            "error in DagModel model; no two transitions with same source and destination allowed; use a MultiDagModel instead."  );
            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

        public DagModel build(boolean verify) {
            super.baseBuild();

            DagModel model= new DagModel(this);
            if ( verify ) {
                if ( DetermineCycle.check(model) )
                      throw new RuntimeException("error in DagModel model: no cycles allowed."  );
            }
            return model;
        }

    }
}

