package statemachine.model.graphs.digraph;

import java.util.StringJoiner;

import org.eclipse.jdt.annotation.NonNull;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.DiGraphTransition;

public abstract class DiGraphBaseModel implements DiGraphModelInterface {

    private final ImmutableSet<LocationState> locations;
    private final ImmutableSet<DiGraphTransition> transitions;
    protected final ImmutableSetMultimap<LocationState, DiGraphTransition> loc2trans;

    // Builder class
    public static abstract class ImmutableBuilder {

        protected ImmutableSet<LocationState> locations;
        protected ImmutableSet<DiGraphTransition> transitions;
        protected ImmutableSetMultimap<LocationState, DiGraphTransition> loc2trans;

        // builders
        @SuppressWarnings("null")
        protected ImmutableSet.Builder<LocationState> locationsBuilder = ImmutableSet.builder();

        @SuppressWarnings("null")
        protected ImmutableSet.Builder<DiGraphTransition> transitionsBuilder = ImmutableSet.builder();


        // store transitions per location
        @SuppressWarnings("null")
        protected ImmutableSetMultimap.Builder<LocationState, DiGraphTransition> loc2transBuilder = ImmutableSetMultimap.builder();

        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {
            this.locationsBuilder.add(src, dst);
            DiGraphTransition transition = new DiGraphTransition(src, dst);
            this.transitionsBuilder.add(transition);
            this.loc2transBuilder.put(src, transition);
            return this;
        }


        protected ImmutableBuilder addFromDiGraphModel(DiGraphModelInterface model) {
            for (DiGraphTransition trans : model.getModelTransitions()) {
                this.addTransition(trans.getSource(), trans.getDestination());
            }
            return this;
        }

        @SuppressWarnings("null")
        protected void baseBuild() {
            //this.startLocation = ...; // no builder for startlocation
            this.locations = locationsBuilder.build();
            this.transitions = transitionsBuilder.build();
            this.loc2trans = loc2transBuilder.build();
        }
    }

    // Constructor

    protected DiGraphBaseModel(ImmutableBuilder builder) {
        this.locations = builder.locations;
        this.transitions = builder.transitions;
        this.loc2trans = builder.loc2trans;
    }

    // interface ModelAutomaton

    @Override
    public @NonNull ImmutableSet<@NonNull LocationState> getLocations() {
        return locations;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull DiGraphTransition> getModelTransitions() {
        return transitions;
    }

    @Override
    public @NonNull ImmutableSet<@NonNull DiGraphTransition> getModelTransitions(@NonNull LocationState location) {
        return loc2trans.get(location);
    }


    // for debugging

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("locations:");
        for (LocationState location : getLocations()) {
            joiner.add(location.toString());
        }
        joiner.add("transitions:");
        for (DiGraphTransition transition : getModelTransitions()) {
            joiner.add(transition.toString());
        }
        joiner.add("");
        return joiner.toString();
    }

}
