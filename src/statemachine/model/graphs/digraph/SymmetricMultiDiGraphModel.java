package statemachine.model.graphs.digraph;



import org.eclipse.jdt.annotation.NonNullByDefault;

import statemachine.model.Util;

/** SymmetricMultiDiGraphModel are directed graphs that have
 *  - loops (transitions that connect locations to themselves)
 *  - and may have multiple directed transitions with same source and target nodes
 *  - and for every directed transition have an opposite directed transition (symmetric)
 */
@NonNullByDefault
public final class SymmetricMultiDiGraphModel extends DiGraphBaseModel {

    // Constructor
    private SymmetricMultiDiGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public SymmetricMultiDiGraphModel fromDigraphModel(DiGraphModelInterface model, boolean verifySymmetry) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromDiGraphModel(model);
        return builder.build(verifySymmetry);
    }

    //Builder class
    public final static class ImmutableBuilder extends DiGraphBaseModel.ImmutableBuilder {

        public SymmetricMultiDiGraphModel build(boolean verify) {
            super.baseBuild();

            SymmetricMultiDiGraphModel model= new SymmetricMultiDiGraphModel(this);
            if ( verify ) {
                if ( ! Util.isDirectedTransitionSymmetric(model) )
                      throw new RuntimeException( "error in SymmetricMultiDiGraphModel: model is not symmetric" );
            }
            return model;
        }


    }
}