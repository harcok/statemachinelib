package statemachine.model.graphs.digraph.conversion;

import java.util.HashMap;
import java.util.HashSet;

import org.javatuples.Pair;

import statemachine.interfaces.model.locationmarkers.ModelLocation;
import statemachine.interfaces.model.transitions.ModelDirectedTransition;
import statemachine.interfaces.model.transitions.ModelUndirectedTransition;
import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.DiGraphTransition;
import statemachine.model.graphs.digraph.DiGraphModel;
import statemachine.model.graphs.digraph.DiGraphModelInterface;
import statemachine.model.graphs.digraph.DagModel;
import statemachine.model.graphs.graph.GenericMultiGraphModel;
import statemachine.model.graphs.graph.MultiGraphModel;
import statemachine.model.graphs.graph.UnlabeledTransitionConstructor;

import statemachine.model.algorithm.DetermineSCC;

public class Conversion {



    /**  merge multiple transitions with same source and destination locations to a single transition
     *
     * @param graph
     * @return DiGraphModel
     */
    public static DiGraphModel merge(DiGraphModelInterface graph) {
        DiGraphModel.ImmutableBuilder builder= new DiGraphModel.ImmutableBuilder();
        for ( DiGraphTransition transition:graph.getModelTransitions() ) {
            builder.addTransitionOrIgnoreIfAlreadyAdded(transition.getSource(), transition.getDestination());
        }
        return builder.build();
    }

    /** Convert a directed graph to an undirected graph by
     *  converting each directed transition to an undirected transition in the graph by just
     *  dropping the direction information.
     *
     * @param graph
     * @return MultiGraphModel
     */
   public static MultiGraphModel digraphAsGraph(DiGraphModelInterface digraph) {
       MultiGraphModel.ImmutableBuilder builder= new MultiGraphModel.ImmutableBuilder();
       for ( DiGraphTransition transition:digraph.getModelTransitions() ) {
           Pair<LocationState, LocationState> locations = transition.getLocations();
           builder.addTransition(locations.getValue0(), locations.getValue1());
       }
       return builder.build();
   }

   /** Convert a directed automaton using LocationState locations to an undirected graph by
    *  converting each directed transition to an undirected transition in the graph by just
    *  dropping the direction information.
    *
    * @param ModelAutomaton<LocationState,T extends ModelDirectedTransition<LocationState>>
    * @return MultiGraphModel
    */
  public static <T extends ModelDirectedTransition<LocationState>> MultiGraphModel convertDirectedAutomatonToUndirectedGraph(ModelAutomaton<LocationState,T> directedModel) {
      MultiGraphModel.ImmutableBuilder builder= new MultiGraphModel.ImmutableBuilder();
      for ( T transition:directedModel.getModelTransitions() ) {
          Pair<LocationState, LocationState> locations = transition.getLocations();
          builder.addTransition(locations.getValue0(), locations.getValue1());
      }
      return builder.build();
  }


  /** Convert a directed automaton to an undirected graph by converting each directed transition
   *  to an undirected transition in the graph by just dropping the direction information.
   *
   *  The generated undirected graph gets the same location type(L) as the directed graph, but
   *  gets an undirected transition type(UT) from the UnlabeledTransitionConstructor function.
   *
   * @param graph
   * @param UnlabeledTransitionConstructor<L,UT>
   * @return GenericMultiGraphModel<L,UT>
   */
  public static <L extends ModelLocation,DT extends ModelDirectedTransition<L>,UT extends ModelUndirectedTransition<L>>
     GenericMultiGraphModel<L,UT> convertDirectedAutomatonToUndirectedGraph(ModelAutomaton<L,DT> digraph, UnlabeledTransitionConstructor<L,UT> transitionConstructor) {


      GenericMultiGraphModel.ImmutableBuilder<L, UT> builder =
              new GenericMultiGraphModel.ImmutableBuilder<L, UT>(transitionConstructor);

      for ( DT transition:digraph.getModelTransitions() ) {
           Pair<L, L> locations = transition.getLocations();
           builder.addTransition(locations.getValue0(), locations.getValue1());
      }

      return builder.build();
  }

  static public <DT extends ModelDirectedTransition<LocationState>>
      DagModel digraph2sccDag( ModelAutomaton<LocationState,DT> digraph) {
      // 1) find scc componentis in digraph
      LocationState startLocation=digraph.getLocations().iterator().next();
      HashSet<HashSet<LocationState>> sccs = DetermineSCC.execute(digraph,startLocation);
      // 2) create DAG by merging states in a strongly connected component

      // first map old to new locations
      HashMap<LocationState,LocationState> location2DagLocation = new HashMap<>();
      for( HashSet<LocationState> scc : sccs) {
         LocationState dagLocation = new LocationState( scc.toString() );
         for ( LocationState location: scc) {
             location2DagLocation.put(location, dagLocation);
         }
      }
      // create dag
      DagModel.ImmutableBuilder builder = new DagModel.ImmutableBuilder();
      for ( DT transition:digraph.getModelTransitions() ) {
          LocationState src= location2DagLocation.get(transition.getSource());
          LocationState dst= location2DagLocation.get(transition.getDestination());
          builder.addTransition(src,dst);
     }
      return null;
  }


}
