package statemachine.model.graphs.digraph;


import statemachine.interfaces.model.types.ModelAutomaton;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.DiGraphTransition;

/**
 * DiGraphModel models a directed graph which equivalent to an unlabeled transition system with directed transitions and an initial state
 *
 */
public interface DiGraphModelInterface
       extends ModelAutomaton<LocationState,DiGraphTransition>
{

}
