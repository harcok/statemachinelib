package statemachine.model.graphs.digraph;



import org.eclipse.jdt.annotation.NonNullByDefault;

/** MultiDiGraphModels are directed graphs that have
 *  - loops (transitions that connect locations to themselves)
 *  - and may have multiple directed transitions with same source and target nodes
 */
@NonNullByDefault
public final class MultiDiGraphModel extends DiGraphBaseModel {

    // Constructor
    private MultiDiGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public MultiDiGraphModel fromDigraphModel(DiGraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromDiGraphModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends DiGraphBaseModel.ImmutableBuilder {

        public MultiDiGraphModel build() {
            super.baseBuild();
            return new MultiDiGraphModel(this);
        }
    }
}

