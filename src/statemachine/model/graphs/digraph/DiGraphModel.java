package statemachine.model.graphs.digraph;

import java.util.HashSet;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.javatuples.Pair;

import statemachine.model.elements.location.LocationState;


/** DiGraphModels are directed graphs that have
 *  - loops (transitions that connect locations to themselves)
 *  - but no multiple directed transitions with same source and target nodes
 */
@NonNullByDefault
public final class DiGraphModel extends DiGraphBaseModel {

    // Constructor
    private DiGraphModel(ImmutableBuilder builder) {
        super(builder);
    }

    // static factory from DiGraphModelInterface
    static public DiGraphModel fromDigraphModel(DiGraphModelInterface model) {
        ImmutableBuilder builder = new ImmutableBuilder();
        builder.addFromDiGraphModel(model);
        return builder.build();
    }

    //Builder class
    public final static class ImmutableBuilder extends DiGraphBaseModel.ImmutableBuilder {

        private HashSet<Pair<LocationState,LocationState>> srcDestPairs = new HashSet<>();

        public DiGraphModel build() {
            super.baseBuild();
            return new DiGraphModel(this);
        }

        /** Add transition, but throw exception if it is already added.
        *
        * @param src
        * @param dst
        * @return
        */
        @Override
        public ImmutableBuilder addTransition(LocationState src, LocationState dst) {

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                throw new RuntimeException(
                        "error in DiGraphModel model; no two transitions with same source and destination allowed; use a MultiDigraphModel instead."  );

            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

        /** Add transition, but don't throw exception if it is already added.
         *
         * @param src
         * @param dst
         * @return
         */
        public ImmutableBuilder addTransitionOrIgnoreIfAlreadyAdded(LocationState src, LocationState dst) {

            Pair<LocationState,LocationState> pair=new Pair<>(src,dst);
            if ( srcDestPairs.contains(pair) ) {
                return this;
            }
            srcDestPairs.add(pair);

            super.addTransition(src, dst);
            return this;
        }

    }
}
