package statemachine.model.graphs.digraph.file;

import java.util.List;

import statemachine.model.graphs.digraph.DiGraphModelInterface;
import statemachine.model.graphs.digraph.format.Dot;
import statemachine.util.IO;

public class Export {

	static public void  dot(DiGraphModelInterface model, String filename){
			List<String> lines=Dot.export(model);
			IO.writeSmallTextFile( lines, filename);
	   }

//	static public void  gml(DiGraphModelInterface model, String filename){
//
//			List<String> lines=GML.export(model);
//			IO.writeSmallTextFile( lines, filename);
//	   }
//
//	static public void  aldebaran(DiGraphModelInterface model, String filename){
//
//			List<String> lines=Aldebaran.export(model);
//			IO.writeSmallTextFile( lines, filename);
//	}



}
