package statemachine.model.graphs.digraph.file;

import java.util.List;

import statemachine.model.graphs.digraph.MultiDiGraphModel;
import statemachine.model.graphs.digraph.format.Dot;

//import statemachine.model.fsm.digraph.format.GML;
//import statemachine.model.fsm.digraph.format.Aldebaran;

import statemachine.util.IO;

public class Import {

//	static public MultiDiGraphModel aldebaran2DiGraphModel( String filename){
//			List<String> lines=IO.readSmallTextFile(filename);
//			MultiDiGraphModel model= Aldebaran.importDiGraphModel(lines);
//			return model;
//	   }
//
//	static public MultiDiGraphModel  gml2DiGraphModel( String filename){
//			List<String> lines=IO.readSmallTextFile(filename);
//			MultiDiGraphModel model= GML.importDiGraphModel(lines);
//			return model;
//	   }

	static public MultiDiGraphModel  dot2DiGraphModel( String filename){
			List<String> lines=IO.readSmallTextFile(filename);
			MultiDiGraphModel model= Dot.importMultiDiGraphModel(lines);
			return model;
	}

}
