package statemachine.model.graphs.digraph.format;


import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;

import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.DiGraphTransition;
import statemachine.model.graphs.digraph.DagModel;
import statemachine.model.graphs.digraph.DiGraphModel;
import statemachine.model.graphs.digraph.DiGraphModelInterface;
import statemachine.model.graphs.digraph.MultiDiGraphModel;
import statemachine.model.graphs.digraph.SimpleDiGraphModel;


/*

digraph g {

    __start0 [label="" shape="none"]

    1 [shape="circle" label="1"];
    2 [shape="circle" label="2"];
    1 -> 2;
    2 -> 1;
    2 -> 1;
    1 -> 1;

    __start0 -> 1;
}


 */


public class Dot {


    static public DiGraphModel importDiGraphModel( List<String> lines) {
        return DiGraphModel.fromDigraphModel(importMultiDiGraphModel(lines));
    }

    static public SimpleDiGraphModel importSimpleDiGraphModel( List<String> lines) {
        return SimpleDiGraphModel.fromDigraphModel(importMultiDiGraphModel(lines));
    }

    static public DagModel importDagModel( List<String> lines, boolean verify ) {
        return DagModel.fromDigraphModel(importMultiDiGraphModel(lines),verify);
    }

    static public MultiDiGraphModel importMultiDiGraphModel( List<String> lines) {

        MultiDiGraphModel.ImmutableBuilder builder =new MultiDiGraphModel.ImmutableBuilder();
        for(String line: lines){

            // digraph has no initial location/start state/location

            if(!line.contains("label")) continue;

            if(line.contains("->")){

                String label;

                if (line.contains("<")) {
                    int beginIndex=line.indexOf("<");
                    int lastIndex=line.lastIndexOf(">");
                    label=line.substring(beginIndex,lastIndex);
                    label=label.replaceAll("\\<[^>]*>","").trim();
                  } else {
                      int startLabel = line.indexOf('"');
                      int endLabel = line.lastIndexOf('"');
                      label = line.substring(startLabel+1, endLabel).trim();
                  }


                int e1 = line.indexOf('-');
                int e2 = line.indexOf('[');
                String from = line.substring(0, e1).trim();
                String to = line.substring(e1+2, e2).trim();


                LocationState src = new LocationState(from);
                LocationState dst = new LocationState(to);
                builder.addTransition(src, dst);
            }

        }
        return builder.build();

    }

    static public List<String> export(DiGraphModelInterface model) {
        String indent="    ";
        LinkedList<String> lines= new LinkedList<>();
        lines.add("digraph g {");
        lines.add("");
        lines.add(indent + "__start0 [label=\"\" shape=\"none\"]");
        lines.add("");


        for ( LocationState location: model.getLocations()) {
          lines.add(indent + location + " [shape=\"circle\" label=\"" + location + "\"];");
        }
        for (@NonNull DiGraphTransition transition: model.getModelTransitions() ) {
            lines.add(indent + transition.getSource() + " -> " + transition.getDestination() + ";");
        }
        lines.add("}");
        return lines;
    }
}
