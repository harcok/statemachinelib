package statemachine.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import exceptionslib.FileIOException;


public class IO {

	public static List<String> readSmallTextFile(String aFileName)  {
	    Path path = Paths.get(aFileName);
	    try {
			return Files.readAllLines(path, ENCODING);
		} catch (Exception e) {
			throw new FileIOException(e).addDecoration("filename",aFileName );
		}
	}

	public static void writeSmallTextFile(List<String> aLines, String aFileName) {
	    Path path = Paths.get(aFileName);
	    try {
			Files.write(path, aLines, ENCODING);
		} catch (Exception e) {
			throw new FileIOException(e).addDecoration("filename",aFileName );
		}
	}

	// note: use java7 api to read/write text lines from/to file 
	//  src: http://www.javapractices.com/topic/TopicAction.do?Id=42
	final static Charset ENCODING = StandardCharsets.UTF_8;

}
