package statemachine.util;

import java.util.Collection;
import java.util.HashMap;

import com.google.common.collect.ImmutableSet;

/** UniqueInstanceCache lets you save memory by giving you the ability to lookup a unique instances from a cache.
 *
 *  More precisely it lets you for a given instance  of V lookup an unique instance which is equal to it.
 *  By using this unique instance instead of the given instance it lets you reduce the number of equal instances used,
 *  and therefore saving a lot of heap memory.
 *
 *  You can configure the cache whether it may be extended with new values after creation or not.
 *
 * @param <V>
 */
public class UniqueInstanceCache<V> {
    private HashMap<V,V> store = new HashMap<V,V>() ;
    private boolean allowExtending;

    /**
     *  Create an empty UniqueInstanceCache which by default allows to be extended.
     */
    public UniqueInstanceCache() {
        this.allowExtending = true;
    }

    /**
     *  Create an UniqueInstanceCache which is initialized with the values from the given Collection.
     *
     *  With the boolean allowExtending you can choose whether to allow extending of the cache or not.
     */
    public UniqueInstanceCache(Collection<V> collection, boolean allowExtending ) {
        this.allowExtending = true;
        for ( V value : collection ) {
            this.uniqueInstance(value); // puts new unique instance of value if not yet exist in cache
        }
        this.allowExtending = allowExtending;
    }



    /** unique instance of value from the cache.
     *
     *  For a given value gets the unique instance for that value from the cache.
     *  The given value and the one fetched from the cache are equal. However by reusing the unique instance as much as
     *  possible instead of creating many equal copies we can save a lot of heap memory.
     *
     *  If the cache doesn't contain an unique instance for the given value then the behavior depends on whether extending
     *  is allowed or not. If extending is not allowed  then null is returned. However if extending is allowed the given
     *  instance is stored as a new unique instance in the cache and its value is returned. So this method is both
     *  a getter as a setter!
     *
     * @param value
     * @return unique instance of value
     */
    public V uniqueInstance(V value) {
        if (  store.containsKey(value) ) {
            return store.get(value);  // get stored unique instance
        } else {
            if ( allowExtending ) {
                store.put(value, value);  // store value as the new unique instance
                return value;
            } else {
                return null;
            }
        }
    }



    /** Get immutable set of unique value instances in cache
     *
     * @return
     */
    public ImmutableSet<V> getValues() {
         return  ImmutableSet.copyOf(store.values());
    }


}
