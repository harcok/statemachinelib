package statemachine.util;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeStack<T> implements Stack<T>  {

    private final Deque<T> deque = new ArrayDeque<T>();

    @Override
    public void push(T object) {
        deque.addFirst(object);  // stack is FIFO: first in first out
    }

    @Override
    public T pop() {
        return deque.removeFirst();  // stack is FIFO: first in first out
    }

    @Override
    public boolean isEmpty() {
        return deque.isEmpty();
    }

}
