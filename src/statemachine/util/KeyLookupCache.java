package statemachine.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.function.Function;

import com.google.common.collect.ImmutableSet;

import statemachine.model.elements.location.LocationState;

/**
 *  KeyLookupCache lets you lookup values in a collection using keys.
 *<p>
 *  A convenient use case is to make from a given set a KeyLookupCache so that you easily can lookup the unique value
 *  instance in the set for a given key.
 *<p>
 *  It internally does lookup a value in a HashMap using a key which uniquely identifies the value in the collection.
 *  To calculate a key for a value you must supply the function which calculates the key from the value.
 *<p>
 *  By using the key you can fetch the same exact object from the collection instead of recreating the object.
 *  So by using the key we can reuse the object saving a lot of heap space we would need
 *  if we would need to create  a new exact copy.
 *<p>
 *  eg. KeyLookupCache<String,Location> lookup= new KeyLookupCache<String,Location>( locations, Location::toString, false );
 *      Location one=lookup.get("1");  // always gets same unique instance for that location
 *
 * @param <K>
 * @param <V>
 */
public class  KeyLookupCache<K,V> {

      private HashMap<K,V> key2value = new HashMap<K,V>() ;

      private Function<V,K> getKeyFromValueFunction;

      private boolean allowExtending;


      /**
       *  Create an empty KeyLookupCache which by default allows to be extended.
       *<p>
       *  To calculate a key for a value you must supply the function which calculates the key from the value.
       */
      public KeyLookupCache(Function<V,K> getKeyFromValueFunction) {
          this.getKeyFromValueFunction = getKeyFromValueFunction;
          this.allowExtending = true;
      }

      /**
       *  Create an KeyLookupCache which is initialized with the values from the given Collection.
       *<p>
       *  To calculate a key for a value you must supply the function which calculates the key from the value.
       *<p>
       *  With the boolean allowExtending you can choose whether to allow extending of the cache or not.
       */
      public KeyLookupCache(Collection<V> collection, Function<V,K> getKeyFromValueFunction, boolean allowExtending  ) {
          this.getKeyFromValueFunction = getKeyFromValueFunction;
          this.allowExtending = true;
          for ( V value : collection ) {
              this.uniqueInstance(value); // puts new unique instance of value if not yet exist in cache
          }
          this.allowExtending = allowExtending;
      }


      /** Get for given key the corresponding unique instance of a value from the cache.
      *
      */
      public V get(K key) {
          return key2value.get(key);
      }

      /**  unique instance of value from the cache.
      *<p>
      *  For a given value gets the unique instance for that value from the cache.
      *  The given value and the one fetched from the cache are equal. However by reusing the unique instance as much as
      *  possible instead of creating many equal copies we can save a lot of heap memory.
      *<p>
      *  If the cache doesn't contain an unique instance for the given value then the behavior depends on whether extending
      *  is allowed or not. If extending is not allowed  then null is returned. However if extending is allowed the given
      *  instance is stored as a new unique instance in the cache and its value is returned. So this method is both
      *  a getter as a setter!
      *
      * @param value
      * @return unique instance of value
      */
      public V uniqueInstance(V value) {
          K key = this.getKeyFromValueFunction.apply(value);
          if (  key2value.containsKey(key) ) {
              return key2value.get(key);  // get stored unique instance
          } else {
              if ( allowExtending ) {
                  key2value.put(key, value);  // store value as the new unique instance
                  return value;
              } else {
                  return null;
              }
          }
      }


      /** Get immutable set of unique value instances in cache
       *
       * @return
       */
      public ImmutableSet<V> getValues() {
           return  ImmutableSet.copyOf(key2value.values());
      }

}
