package statemachine;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import net.sourceforge.argparse4j.inf.Subparsers;
import statemachine.model.fsm.fa.NfaModel;
import statemachine.model.fsm.ia.IaNonDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.moore.MooreModel;
import statemachine.model.fsm.moore.MooreNonDetModel;
import statemachine.model.fsm.moore.file.Export;

public class Main {

    public static void main(String[] args) {

       ArgumentParser parser = ArgumentParsers.newFor("stm").build()
               .defaultHelp(true)
               .description("statemachine tool");


       Subparsers subparsers = parser.addSubparsers().help("sub-command help");

       // Subparsers also has Subparsers.title() method and Subparsers.description() method. When either is present,
       // the subparser's commands will appear in their own group in the help output.
       subparsers.title("subcommands").help(" ").metavar(" ");

       Subparser parserConvert = subparsers.addParser("convert").help("convert lossless the statemachine representation");
       parserConvert.addArgument("conversion_method").choices("fa2moore", "moore2mealy", "mealy2moore","mealy2ia").help("type of conversion");
       parserConvert.addArgument("inputfile").type(String.class).help("input file");
       parserConvert.addArgument("outputfile").type(String.class).help("output file");
       parserConvert.description("convert lossless the statemachine representation");  // only displayed for "stm convert -- help"

       Subparser parserFormat = subparsers.addParser("format").help("convert the file format of the statemachine model");
       parserFormat.addArgument("conversion_method").choices("au2dot").help("type of reformat");
       parserFormat.addArgument("inputfile").type(String.class).help("input file");
       parserFormat.addArgument("outputfile").type(String.class).help("output file");
       parserFormat.description("convert file format");  // only displayed for "stm format -- help"


       Subparser parserInfo = subparsers.addParser("info").help("displays information about the statemachine:\nnumber of states, number of transitions,  ...");
       parserInfo.addArgument("type").choices("fa","moore","mealy","register").help("type of statemachine");
       parserInfo.addArgument("file").type(String.class).help("model file");
       parserInfo.description("displays information about the statemachine:\nnumber of states, number of transitions,  ...");

       subparsers.dest("subcommand");  // convert or info



       Namespace parseResult = null;
       try {
           //String[] myargs = {"--help"};
           //String[] myargs = {"convert","--help"};
           //String[] myargs = {"info","--help"};
           //String[] myargs = {"info","mealy","RiverCrossingPuzzle/RiverCrossingPuzzle.dot"};
           //String[] myargs = {"info","register","RiverCrossingPuzzle/FWGC2.register.xml"};
           //String[] myargs = {"info","fa","simpleDfa.dot"};
           //String[] myargs = {"info","moore","testMooreExport.dot"};   // TODO: import moore from dot file

         //  String[] myargs = {"convert","fa2moore","simpleDfa.dot","mooreFromFa.dot"};
         // String[] myargs = {"convert","moore2mealy","testMooreExport.dot","mealyFromMoore.dot"};
          // String[] myargs = {"convert","mealy2moore","testDetMealyExport.dot","mooreFromMealy.dot"};
          //String[] myargs = {"convert","mealy2ia","testDetMealyExport.dot","iaFromMealy.dot"};
          // parseResult = parser.parseArgs(myargs);
           parseResult = parser.parseArgs(args);
       } catch (ArgumentParserException e) {
           //  parse error or  help option selected
           parser.handleError(e);
           System.exit(1);
       }


       if ( parseResult.getString("subcommand").equals("info") ) {
           String type=parseResult.getString("type");
           String filename=parseResult.getString("file");
           switch (type) {
           case "fa":
               statemachine.model.fsm.fa.NfaModel.printSimpleStatistics(filename);
               break;
           case "moore" :
               statemachine.model.fsm.moore.MooreNonDetModel.printSimpleStatistics(filename);
               break;
           case "mealy" :
               statemachine.model.fsm.mealy.MealyNonDetModel.printSimpleStatistics(filename);
               break;
           case "fia" :
               System.out.println("not yet supported");
               //TODO:  statemachine.model.fsm.mealy.MealyNonDetModel.printSimpleStatistics(filename);
               break;
           case "register" :
               statemachine.model.efsm.mealy.MealyNonDetModel.printSimpleStatistics(filename);
               break;
           default: break;
           }
       }


       if ( parseResult.getString("subcommand").equals("convert") ) {
           String conversion_method=parseResult.getString("conversion_method");
           String inputfile=parseResult.getString("inputfile");
           String outputfile=parseResult.getString("outputfile");
           switch (conversion_method) {
               case "fa2moore":
                   //System.out.println("not yet supported");
                   NfaModel nfaModel=statemachine.model.fsm.fa.file.Import.dot2NfaModel(inputfile);
                   MooreNonDetModel mooreModel=statemachine.model.fsm.fa.conversion.Conversion.Fa2MooreNonDetModel(nfaModel);
                   statemachine.model.fsm.moore.file.Export.dot(mooreModel, outputfile);
                   break;
               case "moore2mealy" :
                   MooreNonDetModel mooreModel2=statemachine.model.fsm.moore.file.Import.dot2MooreModel(inputfile);
                   MealyNonDetModel mealyModel=statemachine.model.fsm.moore.conversion.Conversion.Moore2MealyNonDetModel(mooreModel2);
                   statemachine.model.fsm.mealy.file.Export.dot(mealyModel,outputfile);
                   break;
               case "mealy2moore" :
                   MealyNonDetModel mealyModel2=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(inputfile);
                   MooreNonDetModel mooreModel3=statemachine.model.fsm.mealy.conversion.Conversion.Mealy2MooreNonDetModel(mealyModel2);
                   statemachine.model.fsm.moore.file.Export.dot(mooreModel3,outputfile);
                   break;
               case "mealy2ia" :
                   MealyNonDetModel mealyModel3=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(inputfile);
                   IaNonDetModel iaModel=statemachine.model.fsm.mealy.conversion.Conversion.Mealy2IaNonDetModel(mealyModel3);
                   statemachine.model.fsm.ia.file.Export.dot(iaModel,outputfile);
                   break;
               default: break;
           }
       }

       if ( parseResult.getString("subcommand").equals("format") ) {
           String conversion_method=parseResult.getString("conversion_method");
           String inputfile=parseResult.getString("inputfile");
           String outputfile=parseResult.getString("outputfile");
           switch (conversion_method) {
               case "au2dot":
                   MealyNonDetModel mealyModel=statemachine.model.fsm.mealy.file.Import.aldebaran2MealyModel(inputfile);
                   statemachine.model.fsm.mealy.file.Export.dot(mealyModel, outputfile);
                   break;
               case "dot2au":
                   MealyNonDetModel mealyModel2=statemachine.model.fsm.mealy.file.Import.dot2MealyModel(inputfile);
                   statemachine.model.fsm.mealy.file.Export.aldebaran(mealyModel2, outputfile);
                   break;
               default: break;
           }
       }


    }

}
