package statemachine.model.fsm.mealy.conversion;

import static org.junit.Assert.assertNull;

import org.junit.Test;

import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;
import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.fsm.mealy.MealyDetModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.mealy.util.Util;

public class AutomataLibTest {

    static public MealyDetModel buildCoffeeMachineWithStatemachinelib() {

        InputAction WATER = new InputAction("WATER");
        InputAction POD = new InputAction("POD");
        InputAction BUTTON = new InputAction("BUTTON");
        InputAction CLEAN = new InputAction("CLEAN");

        OutputAction out_ok = new OutputAction("ok");
        OutputAction out_error = new OutputAction("error");
        OutputAction out_coffee = new OutputAction("coffee");

        LocationState a = new LocationState("a");
        LocationState b = new LocationState("b");
        LocationState c = new LocationState("c");
        LocationState d = new LocationState("d");
        LocationState e = new LocationState("e");
        LocationState f = new LocationState("f");

        MealyDetModel.ImmutableBuilder builder = new MealyDetModel.ImmutableBuilder();
        builder.setStartLocation(a);

        builder.addTransition(a, c, WATER, out_ok);
        builder.addTransition(a, b, POD, out_ok);
        builder.addTransition(a, f, BUTTON, out_error);
        builder.addTransition(a, a, CLEAN, out_ok);

        builder.addTransition(b, d, WATER, out_ok);
        builder.addTransition(b, b, POD, out_ok);
        builder.addTransition(b, f, BUTTON, out_error);
        builder.addTransition(b, a, CLEAN, out_ok);

        builder.addTransition(c, c, WATER, out_ok);
        builder.addTransition(c, d, POD, out_ok);
        builder.addTransition(c, f, BUTTON, out_error);
        builder.addTransition(c, a, CLEAN, out_ok);

        builder.addTransition(d, d, WATER, out_ok);
        builder.addTransition(d, d, POD, out_ok);
        builder.addTransition(d, e, BUTTON, out_coffee);
        builder.addTransition(d, a, CLEAN, out_ok);

        builder.addTransition(e, f, WATER, out_error);
        builder.addTransition(e, f, POD, out_error);
        builder.addTransition(e, f, BUTTON, out_error);
        builder.addTransition(e, a, CLEAN, out_ok);

        builder.addTransition(f, f, WATER, out_error);
        builder.addTransition(f, f, POD, out_error);
        builder.addTransition(f, f, BUTTON, out_error);
        builder.addTransition(f, f, CLEAN, out_error);

        MealyDetModel mealyDet = builder.build();

        return mealyDet;
    }

    static public CompactMealy<String, String> buildCoffeeMachineWithAutomatalib() {

        String WATER = "WATER";
        String POD = "POD";
        String BUTTON = "BUTTON";
        String CLEAN = "CLEAN";

        String out_ok = "ok";
        String out_error = "error";
        String out_coffee = "coffee";

        Alphabet<String> inputAlphabet = Alphabets.fromArray(WATER, POD, BUTTON, CLEAN);

        CompactMealy<String, String> machine = new CompactMealy<String, String>(inputAlphabet);

        Integer a = machine.addInitialState(), b = machine.addState(), c = machine.addState(), d = machine.addState(),
                e = machine.addState(), f = machine.addState();

        machine.addTransition(a, WATER, c, out_ok);
        machine.addTransition(a, POD, b, out_ok);
        machine.addTransition(a, BUTTON, f, out_error);
        machine.addTransition(a, CLEAN, a, out_ok);

        machine.addTransition(b, WATER, d, out_ok);
        machine.addTransition(b, POD, b, out_ok);
        machine.addTransition(b, BUTTON, f, out_error);
        machine.addTransition(b, CLEAN, a, out_ok);

        machine.addTransition(c, WATER, c, out_ok);
        machine.addTransition(c, POD, d, out_ok);
        machine.addTransition(c, BUTTON, f, out_error);
        machine.addTransition(c, CLEAN, a, out_ok);

        machine.addTransition(d, WATER, d, out_ok);
        machine.addTransition(d, POD, d, out_ok);
        machine.addTransition(d, BUTTON, e, out_coffee);
        machine.addTransition(d, CLEAN, a, out_ok);

        machine.addTransition(e, WATER, f, out_error);
        machine.addTransition(e, POD, f, out_error);
        machine.addTransition(e, BUTTON, f, out_error);
        machine.addTransition(e, CLEAN, a, out_ok);

        machine.addTransition(f, WATER, f, out_error);
        machine.addTransition(f, POD, f, out_error);
        machine.addTransition(f, BUTTON, f, out_error);
        machine.addTransition(f, CLEAN, f, out_error);

        return machine;
    }

    /*
    	static public CompactMealy<String,String> buildFluentCoffeeMachineWithAutomatalib(){

    		String WATER = "WATER";
    		String POD = "POD";
    		String BUTTON = "BUTTON";
    		String CLEAN = "CLEAN";

    		String out_ok = "ok";
    		String out_error = "error";
    		String out_coffee = "coffee";

    		Alphabet<String> inputAlphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);

    		CompactMealy<String, String> fluentCoffeeMachine = AutomatonBuilders.<String, String> newMealy(inputAlphabet)
        .withInitial("a")
    		.from("a")
    			.on(WATER).withOutput(out_ok).to("c")
    			.on(POD).withOutput(out_ok).to("b")
    			.on(BUTTON).withOutput(out_error).to("f")
    			.on(CLEAN).withOutput(out_ok).loop()
    		.from("b")
    			.on(WATER).withOutput(out_ok).to("d")
    			.on(POD).withOutput(out_ok).loop()
    			.on(BUTTON).withOutput(out_error).to("f")
    			.on(CLEAN).withOutput(out_ok).to("a")
    		.from("c")
    			.on(WATER).withOutput(out_ok).loop()
    			.on(POD).withOutput(out_ok).to("d")
    			.on(BUTTON).withOutput(out_error).to("f")
    			.on(CLEAN).withOutput(out_ok).to("a")
    		.from("d")
    			.on(WATER, POD).withOutput(out_ok).loop()
    			.on(BUTTON).withOutput(out_coffee).to("e")
    			.on(CLEAN).withOutput(out_ok).to("a")
    		.from("e")
    			.on(WATER, POD, BUTTON).withOutput(out_error).to("f")
    			.on(CLEAN).withOutput(out_ok).to("a")
    		.from("f")
    			.on(WATER, POD, BUTTON, CLEAN).withOutput(out_error).loop()
    	    .create();


    		return fluentCoffeeMachine;
    	 }
    */

    @Test
    public void testConvert2statemachinelibMealy() {
        // expected
        MealyDetModel statemachinelibMealy = buildCoffeeMachineWithStatemachinelib();

        CompactMealy<String, String> automatalibMealy = buildCoffeeMachineWithAutomatalib();
        MealyNonDetModel nondetmealy = AutomataLib.convert2statemachinelibMealy(automatalibMealy);
        MealyDetModel result = MealyDetModel.fromMealyModel(nondetmealy);

        //assertLenientEquals(statemachinelibMealy.toString().trim(),result.toString().trim());
        //assertLenientEquals(statemachinelibMealy,result);
        assertNull(Util.compare(statemachinelibMealy, result));

    }

    /*	   @Test
    	    public void testConvert2automatalibMealy() {
    	        // expected
    	       CompactMealy<String,String> expected = buildCoffeeMachineWithAutomatalib();

    	        // create expected
    	        MealyNonDetModel statemachinelibMealy = buildCoffeeMachineWithStatemachinelib();
    	        CompactMealy<String,String> result = AutomataLib.convert2automatalibMealy(statemachinelibMealy);

    //          //compare
    //	        assertLenientEquals(expected,result);
    //	        // `-> fails because CompactMealy doesn't like reflection equals

    	         //TODO: use algorithm from automatalib  to test equality of automata
    	    }

    */

    @Test
    public void testConvert2automatalibAndBack() {
        // expected
        MealyDetModel expected = buildCoffeeMachineWithStatemachinelib();

        // convert to automatalib mealy and back to statemachinelib mealy model
        CompactMealy<String, String> automatalibMealy = AutomataLib.convert2automatalibMealy(expected);
        MealyNonDetModel nondetmealy = AutomataLib.convert2statemachinelibMealy(automatalibMealy);
        MealyDetModel result = MealyDetModel.fromMealyModel(nondetmealy);

        //// assertLenientEquals(expected,result);
        //   `=> doesn't work because automalib doesn't support location labels,
        //       so when converting to automatalib model we loose the location labels
        //       and when converting back to statemachinelib model
        //       then on the fly location labels are generated
        //    => we must compare independent of location labeling!!
        assertNull(Util.compare(expected, result));
    }

}
