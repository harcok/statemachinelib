package statemachine.model.fsm.mealy.conversion;

import statemachine.model.elements.action.InputAction;
import statemachine.model.elements.action.OutputAction;
import statemachine.model.elements.location.LocationState;
import statemachine.model.elements.transition.MealyTransition;
import statemachine.model.fsm.mealy.MealyModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.automata.transout.impl.compact.CompactMealyTransition;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

public class AutomataLib {


/*
   automatalib
     mealy interface :  MealyMachine<S,I,T,O>
     mealy implementation : CompactMealy<I, O>

   statemachinelib
     mealy interface :   MealyModel
     mealy implementation: MealyNonDetModel
 */
//

//
//	static public MealyNonDetModel  convert2statemachinelibMealy(MealyMachine<?,String,?,String> model) {
//
//		model.getInitialState();
//	   return null;
//	}

	static public MealyNonDetModel  convert2statemachinelibMealy(CompactMealy<String,String> model) {

		MealyNonDetModel.ImmutableBuilder builder =new MealyNonDetModel.ImmutableBuilder();
	    Integer startState=model.getInitialState();
	    builder.setStartLocation( new LocationState(startState.toString()));

	    Collection<Integer> states=model.getStates();
	    Alphabet<String> inputAlphabet = model.getInputAlphabet();
	    for (Integer state: states) {
		   for ( String input:  inputAlphabet) {
			   CompactMealyTransition<String> t=model.getTransition(state, input);
			   if ( t != null ) {
				   String output = t.getOutput();
				   Integer destination=model.getSuccessor(t);

				   InputAction in=new InputAction(input);
				   OutputAction out=new OutputAction(output);
				   LocationState src=new LocationState(state.toString());
				   LocationState dst=new LocationState(destination.toString());
				   builder.addTransition(src, dst, in, out);

			   }
		   }
	    }

	    return builder.build();
	}

	static public CompactMealy<String, String> convert2automatalibMealy(MealyModel model) {


		ArrayList<String> inputs=new ArrayList<>();
		for ( InputAction input: model.getInputAlphabet() ) {
			inputs.add(input.getName());
		}

		Alphabet<String> inputAlphabet = Alphabets.fromList(inputs);

		CompactMealy<String, String> machine = new CompactMealy<String,String>(inputAlphabet);

		// map locations
		HashMap<LocationState,Integer> statelib2autolibLocation = new HashMap<>();
		LocationState startLocation=model.getStartLocation();
		Integer startState = machine.addInitialState();
		statelib2autolibLocation.put(startLocation,startState);

		for ( LocationState l: model.getLocations() ) {
			if ( l != startLocation ) {
				Integer state=machine.addState();   // => cannot add label LocationState , no label supported by automatalib (internal uses integer though)
				statelib2autolibLocation.put(l,state);
			}
		}

		for ( MealyTransition t: model.getModelTransitions()) {
			Integer source = statelib2autolibLocation.get(t.getSource());
			Integer destination = statelib2autolibLocation.get(t.getDestination());
			machine.addTransition(source, t.getInput().getName(), destination, t.getOutput().getName());
		}

		return machine;
	}
}
